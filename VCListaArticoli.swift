    //
    //  VCListaOpere.swift
    //  Main Screen
    //
    //  Created by Lorenzo Antista on 30/11/2017.
    //  Copyright © 2017 Lorenzo Antista. All rights reserved.
    //
    
    import UIKit
    
    class VCListaArticoli: UIViewController, UITableViewDelegate, UITableViewDataSource {
        
        
        @IBOutlet weak var buttonAggiungiOpera: UIBarButtonItem!
        
        var listaOpera: [Opera] = []
        
        @IBOutlet weak var tableView: UITableView!
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            tableView.delegate = self
            tableView.dataSource = self
            
            setupContenuti()
            setupGrafica()
            
            // Do any additional setup after loading the view.
        }
        
        func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return listaOpera.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellArticolo") as! CellArticolo
            
            //Metto i dati sulla cella
            let operaCorrente = listaOpera[indexPath.row]
            cell.setupConArticolo(operaDaRappresentare: operaCorrente)
            //Restituisco la cella
            return cell
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            // Prendo l'opera selezionata
            let operaSelezionata = listaOpera[indexPath.row]
            
            // La salvo nella variabile di appoggio
            operaProssimaSchermata = operaSelezionata
            
            // Avvio la segue dalla lista al dettaglio
            performSegue(withIdentifier: "SegueDettaglioOpera", sender: self)
            
        }
        
        // Variabile di appoggio per passare l'opera selezionata al view controller di dettaglio
        var operaProssimaSchermata: Opera?
        
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            if let next = segue.destination as? VCDettaglioOpera{
                // Prendo dalla variabile di appoggio l'opera selezionata
                next.operaDaRappresentare = operaProssimaSchermata
            }
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        
        func setupGrafica() {
            
            navigationItem.title = "Lista Articoli"
            
        }
        
        func setupContenuti() {
            /*
             let operaUno = Opera()
             operaUno.titolo = "Prima Opera"
             
             let operaDue = Opera()
             operaDue.titolo = "Seconda Opera"
             
             listaOpera = [operaUno, operaDue]
             */
            
            
            aggiornaContenuti()
            
        }
        
        func aggiornaContenuti() {
            //Questa funzione si occupa di prendere i contenuti aggiornati da mostrare sul view controller
            //Network.getMeteoRoma()
            
            Network.getListaOpereAggiornata{
                //Parte del codice eseguita quando l'app riceve la risposta del server
                (listaOpereAggiornata) in
                
                self.listaOpera = listaOpereAggiornata
                
                //Ricarico i dati visualizzati sulla TableView
                self.tableView.reloadData()
            }
            
        }
        
        /*
         // MARK: - Navigation
         
         // In a storyboard-based application, you will often want to do a little preparation before navigation
         override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
         // Get the new view controller using segue.destinationViewController.
         // Pass the selected object to the new view controller.
         }
         */
        
        
        
        
        
        
        
        
        
        
    }
    
