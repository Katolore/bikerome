//
//  DBPopupController.h
//  File version: 1.0.1
//  Last modified: 12/07/2015
//
//  Created by Davide Balistreri on 08/13/2015
//  Copyright 2013-2016 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

// La dichiarazione del DBPopup si trova in fondo a questo file.
@class DBPopup;


/// Stile dello sfondo del Popup
typedef NS_ENUM(NSUInteger, DBPopupBackgroundStyle) {
    /// Sfondo opaco
    DBPopupBackgroundStyleOpaco,
    /// Sfondo con una leggera trasparenza
    DBPopupBackgroundStyleSemiTrasparente,
    /// Nessuno sfondo
    DBPopupBackgroundStyleTrasparente,
    /// Effetto sfocato
    DBPopupBackgroundStyleEffettoSfocato
};


/**
 * Classe per gestire la creazione e la chiusura dei popup.
 */
@interface DBPopupController : NSObject

/**
 * Block generico per eseguire un blocco di codice.
 */
typedef void (^DBPopupControllerBlock)(void);


#pragma mark - Popup personalizzati

/**
 * Controlla se ci sono popup attualmente aperti.
 */
+ (BOOL)popupAperti;

/**
 * Crea un popup con il contenuto del ViewController.
 * @param viewController Il ViewController che occuperà la vista del popup.
 */
+ (DBPopup *)popupViewController:(UIViewController *)viewController;

/**
 * Crea un popup con il contenuto del ViewController, permettendo di eseguire una parte di codice
 * quando il popup viene mostrato sullo schermo, e/o quando viene chiuso.
 * @param viewController Il ViewController che occuperà la vista del popup.
 * @param aperturaBlock  Il codice da eseguire quando il popup viene aperto.
 * @param chiusuraBlock  Il codice da eseguire quando il popup viene chiuso.
 */
+ (DBPopup *)popupViewController:(UIViewController *)viewController conAperturaBlock:(DBPopupControllerBlock)aperturaBlock conChiusuraBlock:(DBPopupControllerBlock)chiusuraBlock;

/**
 * Chiude il popup specificato ed esegue il chiusuraBlock (se presente).
 */
+ (void)chiudiPopup:(DBPopup *)popup;

/**
 * Chiude il popup del ViewController specificato ed esegue il chiusuraBlock (se presente).
 */
+ (void)chiudiPopupViewController:(UIViewController *)viewController;

/**
 * Chiude il popup del ViewController specificato ed esegue il chiusuraBlock specificato.
 */
+ (void)chiudiPopupViewController:(UIViewController *)viewController conChiusuraBlock:(DBPopupControllerBlock)chiusuraBlock;

/**
 * Chiude tutti i popup aperti, eseguendo i loro chiusuraBlock (se presenti).
 */
+ (void)chiudiTuttiPopups;

@end


/**
 * Classe per gestire l'istanza del singolo popup.
 */
@interface DBPopup : NSObject

@property (nonatomic) DBPopupBackgroundStyle background;
@property (nonatomic) CGFloat padding;

@property (strong, nonatomic) UIColor *tintColor;

/**
 * Chiude il popup ed esegue il chiusuraBlock (se presente).
 */
- (void)chiudiPopup;

@end
