//
//  DBPageViewController.h
//  Versione file: 1.0.0
//  Ultimo aggiornamento: 01/12/2015
//
//  Creato da Davide Balistreri il 10/10/2014
//  Copyright 2013-2016 © Davide Balistreri. Tutti i diritti riservati.
//

#import <UIKit/UIKit.h>

@protocol DBPageViewControllerDelegate <NSObject>

- (void)pageViewDidShowItem:(id)item atIndex:(NSUInteger)index;
- (void)pageViewDidSelectItem:(id)item atIndex:(NSUInteger)index;

@end


@interface DBPageViewController : UIViewController <UIPageViewControllerDataSource>

@property (weak, nonatomic) id<DBPageViewControllerDelegate> delegate;

@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) UIPageControl *pageControl;
@property (strong, nonatomic) UIColor *tintColor;

@property (nonatomic, getter = isSenzaAnimazione) BOOL senzaAnimazione;
@property (nonatomic, getter = isScorrimentoInvertito) BOOL scorrimentoInvertito;

@property (strong, nonatomic) NSArray *dataSource;
- (void)aggiungiViewController:(UIViewController *)viewController;
- (void)rimuoviViewControllers;

- (void)mostraViewControllerConIndex:(NSUInteger)index animazione:(BOOL)animazione;
- (UIViewController *)viewControllerAtIndex:(NSUInteger)index;

@end
