//
//  DBCentralManager.m
//  File version: 1.0.0
//  Last modified: 12/01/2015
//
//  Created by Davide Balistreri on 09/17/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBCentralManager.h"
#import "DBDictionaryExtensions.h"
#import "DBStringExtensions.h"

@interface DBCentralManager ()

/// Dictionary contenente le risorse condivise
@property (strong, nonatomic) NSMutableDictionary *risorse;

@end


@implementation DBCentralManager

/// L'istanza condivisa del DBCentralManager.
DBCentralManager *dbSharedCentralManager = nil;

/**
 * Restituisce l'istanza condivisa del DBCentralManager.
 */
+ (instancetype)manager
{
    @synchronized(self) {
        if (!dbSharedCentralManager) {
            dbSharedCentralManager = [DBCentralManager new];
            dbSharedCentralManager.risorse = [NSMutableDictionary dictionary];
        }
        
        return dbSharedCentralManager;
    }
}

/**
 * Restituisce la risorsa condivisa salvata con l'identifier specificato.
 * @param identifier L'identifier univoco della risorsa.
 */
+ (id)getRisorsa:(NSString *)identifier
{
    @synchronized(self) {
        DBCentralManager *manager = [DBCentralManager manager];
        return [manager.risorse safeValueForKey:[NSString safeString:identifier]];
    }
}

/**
 * Salva la risorsa condivisa con l'identifier specificato.
 * @param risorsa    La risorsa da salvare.
 * @param identifier L'identifier univoco della risorsa.
 */
+ (void)setRisorsa:(id)risorsa conIdentifier:(NSString *)identifier
{
    @synchronized(self) {
        DBCentralManager *manager = [DBCentralManager manager];
        [manager.risorse safeSetValue:risorsa forKey:[NSString safeString:identifier]];
    }
}

@end
