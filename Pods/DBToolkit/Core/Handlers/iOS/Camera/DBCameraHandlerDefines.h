//
//  DBCameraHandlerDefines.h
//  File version: 1.0.0
//  Last modified: 02/05/2018
//
//  Created by Davide Balistreri on 05/26/2017
//  Copyright 2018 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

@class DBCameraHandler;


@class AVMetadataObject;


/**
 * I dispositivi di output.
 */
typedef NS_ENUM(NSUInteger, DBCameraDevice) {
    /// La fotocamera anteriore.
    DBCameraDeviceFrontCamera,
    /// La fotocamera posteriore.
    DBCameraDeviceRearCamera
};


/**
 * Qualità dei file di output.
 */
typedef NS_ENUM(NSUInteger, DBCameraOutputQuality) {
    /// Per la condivisione di file via internet con rete 3G.
    DBCameraOutputQualityWeb,
    /// Per la condivisione di file via internet con rete WiFi.
    DBCameraOutputQualityMedium,
    /// Per la creazione di file con ottima qualità.
    DBCameraOutputQualityHigh,
    /// Per scattare foto con la qualità massima.
    /// Non è possibile registrare video con questa impostazione.
    DBCameraOutputQualityBestForPhotos
};


typedef NS_ENUM(NSUInteger, DBCameraFlashMode) {
    DBCameraFlashModeOff,
    DBCameraFlashModeAuto,
    DBCameraFlashModeOn
};


typedef void (^DBCameraHandlerImageBlock)(UIImage *outputImage);
typedef void (^DBCameraHandlerVideoBlock)(BOOL success);


// MARK: - Protocol

@protocol DBCameraHandlerDelegate <NSObject>

- (void)handler:(DBCameraHandler *)handler didCaptureMetadataObjects:(NSArray<AVMetadataObject *> *)metadataObjects;

@end
