//
//  DBCameraHandler.m
//  File version: 1.0.1
//  Last modified: 09/14/2016
//
//  Created by Davide Balistreri on 05/04/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBCameraHandler.h"
#import "DBImageExtensions.h"
#import "DBObjectExtensions.h"
#import "DBArrayExtensions.h"
#import "DBAnimation.h"
#import "DBThread.h"
#import "DBUtility.h"
#import "DBDevice.h"

#import <AVFoundation/AVFoundation.h>

const CGFloat kDurataDissolvenzaIniziale = 0.3;
const CGFloat kDurataDissolvenza = 0.2;

// MARK: - CameraView subclass

@protocol CameraViewDelegate <NSObject>

- (void)viewDidLayoutSubviews:(UIView *)view;

@end

@interface CameraView : UIView

@property (nonatomic) id<CameraViewDelegate> delegate;

@end

@implementation CameraView

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if ([self.delegate respondsToSelector:@selector(viewDidLayoutSubviews:)]) {
        [self.delegate viewDidLayoutSubviews:self];
    }
}

@end


// MARK: - DBCameraHandler interface

@interface DBCameraHandler () <AVCaptureFileOutputRecordingDelegate, CameraViewDelegate, AVCaptureVideoDataOutputSampleBufferDelegate, AVCaptureMetadataOutputObjectsDelegate>

@property (nonatomic) BOOL animazioneIniziale;

/// L'eventuale animazione di apertura/chiusura in corso.
@property (strong, nonatomic) DBAnimation *animation;

@property (strong, nonatomic) AVCaptureSession *captureSession;
@property (strong, nonatomic) AVCaptureDeviceInput *deviceInput;

/// Per scattare foto con la qualità massima
@property (strong, nonatomic) AVCaptureStillImageOutput *imageOutput;

/// Per scattare foto con la qualità visualizzata
@property (strong, nonatomic) AVCaptureVideoDataOutput *videoOutput;

/// Per registrare video con la qualità massima
@property (strong, nonatomic) AVCaptureMovieFileOutput *movieOutput;

@property (strong, nonatomic) AVCaptureVideoPreviewLayer *previewLayer;
@property (strong, nonatomic) CameraView *containerPreview;

@property (strong, nonatomic) AVCaptureMetadataOutput *metadataOutput;

@property (nonatomic) BOOL cattura;
@property (copy) DBCameraHandlerVideoBlock videoBlock;

@property (nonatomic) BOOL catturaPreview;
@property (copy) DBCameraHandlerImageBlock previewBlock;

@property (nonatomic, getter=isEnabled) BOOL enabled;

@end


// MARK: - DBCameraHandler implementation

@implementation DBCameraHandler

+ (NSMutableArray *)sharedInstances
{
    @synchronized(self) {
        NSMutableArray *sharedInstances = [DBCentralManager getRisorsa:@"DBSharedCameraHandlers"];
        
        if (!sharedInstances) {
            sharedInstances = [NSMutableArray array];
            [DBCentralManager setRisorsa:sharedInstances conIdentifier:@"DBSharedCameraHandlers"];
        }
        
        return sharedInstances;
    }
}


// MARK: - Static methods

+ (DBCameraHandler *)handlerWithView:(UIView *)view cameraDevice:(DBCameraDevice)cameraDevice outputQuality:(DBCameraOutputQuality)outputQuality
{
    // Istanzio il nuovo Handler
    DBCameraHandler *handler = [DBCameraHandler new];
    handler.animazioneIniziale = YES;
    handler.view = view;
    [handler forceCameraDevice:cameraDevice];
    [handler forceOutputQuality:outputQuality];
    [handler enableHandler:YES];
    
    // Lo aggiungo allo stack
    NSMutableArray *sharedInstances = [DBCameraHandler sharedInstances];
    [sharedInstances addObject:handler];
    
    return handler;
}

+ (void)destroyHandlerWithView:(UIView *)view
{
    // Cerco tra gli Handler attualmente attivi
    NSMutableArray *sharedInstances = [DBCameraHandler sharedInstances];
    
    for (NSInteger counter = 0; counter < sharedInstances.count; counter++) {
        DBCameraHandler *handler = sharedInstances[counter];
        
        if ([handler.view isEqual:view]) {
            // Handler trovato, lo disabilito e rimuovo dallo stack di Handlers
            [handler destroyHandler];
            counter--;
        }
    }
}


// MARK: - Methods

// MARK: Gestione dell'Handler

@synthesize view = _view;
- (void)setView:(UIView *)view
{
    if (_view) {
        // Rimuovo la vecchia Preview
        [self.containerPreview removeFromSuperview];
    }
    
    _view = view;
    [view layoutIfNeeded];
    
    self.containerPreview = [CameraView new];
    self.containerPreview.frame = view.bounds;
    self.containerPreview.autoresizingMask = DBViewAdattaDimensioni;
    self.containerPreview.delegate = self;
    self.view.backgroundColor = [UIColor blackColor];
    [self.view insertSubview:self.containerPreview atIndex:0];
}

@synthesize cameraDevice = _cameraDevice;
- (void)setCameraDevice:(DBCameraDevice)cameraDevice
{
    if (self.animation) {
        // Invalido l'animazione precedente
        [self.animation cancel];
    }
    
    _cameraDevice = cameraDevice;
    
    [self stopCameraWithCompletionBlock:^{
        // Riavvio l'handler
        [self enableHandler:NO];
        [self enableHandler:YES];
    }];
}

- (void)forceCameraDevice:(DBCameraDevice)cameraDevice
{
    _cameraDevice = cameraDevice;
}

@synthesize outputQuality = _outputQuality;
- (void)setOutputQuality:(DBCameraOutputQuality)outputQuality
{
    if (self.animation) {
        // Invalido l'animazione precedente
        [self.animation cancel];
    }
    
    [self stopCameraWithCompletionBlock:^{
        [self enableHandler:NO];
        
        _outputQuality = outputQuality;
        
        [self enableHandler:YES];
    }];
}

@synthesize flashMode = _flashMode;
- (void)setFlashMode:(DBCameraFlashMode)flashMode
{
    _flashMode = flashMode;
    [self updateFlashMode];
}

- (void)updateFlashMode
{
    AVCaptureDevice *captureDevice = self.deviceInput.device;
    
    if (captureDevice && [captureDevice isFlashAvailable]) {
        NSError *error = nil;
        [captureDevice lockForConfiguration:&error];
        
        if (!error) {
            AVCaptureFlashMode flashMode = AVCaptureFlashModeOff;
            
            if (self.flashMode == DBCameraFlashModeAuto) {
                flashMode = AVCaptureFlashModeAuto;
            } else if (self.flashMode == DBCameraFlashModeOn) {
                flashMode = AVCaptureFlashModeOn;
            }
            
            if ([captureDevice isFlashModeSupported:flashMode]) {
                [captureDevice setFlashMode:flashMode];
            }
        }
    }
    else {
        _flashMode = DBCameraFlashModeOff;
    }
}

- (void)forceOutputQuality:(DBCameraOutputQuality)outputQuality
{
    _outputQuality = outputQuality;
}

- (NSString *)captureSessionPreset
{
    switch (self.outputQuality) {
        case DBCameraOutputQualityWeb:
            return AVCaptureSessionPresetLow;
        case DBCameraOutputQualityMedium:
            return AVCaptureSessionPresetMedium;
        case DBCameraOutputQualityHigh:
            return AVCaptureSessionPresetHigh;
        case DBCameraOutputQualityBestForPhotos:
            return AVCaptureSessionPresetPhoto;
    }
}

- (AVCaptureDevice *)captureDevice
{
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    
    for (AVCaptureDevice *device in devices) {
        if (self.cameraDevice == DBCameraDeviceFrontCamera && device.position == AVCaptureDevicePositionFront) {
            return device;
        }
        else if (self.cameraDevice == DBCameraDeviceRearCamera && device.position == AVCaptureDevicePositionBack) {
            return device;
        }
    }
    
    return nil;
}

- (void)enableHandler:(BOOL)enable
{
    if ([DBDevice isSimulator]) {
        // Fotocamera non disponibile
        NSLog(@"Fotocamera non disponibile sul simulatore.");
        return;
    }
    
    if (enable && self.isEnabled == NO) {
        self.captureSession = [AVCaptureSession new];
        
        NSString *sessionPreset = [self captureSessionPreset];
        
        if ([self.captureSession canSetSessionPreset:sessionPreset]) {
            [self.captureSession setSessionPreset:sessionPreset];
        }
        else {
            // Selected quality not available
            return;
        }
        
        // Input
        AVCaptureDevice *captureDevice = [self captureDevice];
        self.deviceInput = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:nil];
        [self updateFlashMode];
        
        if (!self.deviceInput) {
            // Input not available
            return;
        }
        
        if ([self.captureSession canAddInput:self.deviceInput]) {
            [self.captureSession addInput:self.deviceInput];
        }
        else {
            // Input not available
            return;
        }
        
        // Image Output
        self.imageOutput = [AVCaptureStillImageOutput new];
        self.imageOutput.outputSettings = @{AVVideoCodecJPEG: AVVideoCodecKey};
        
        if ([self.captureSession canAddOutput:self.imageOutput]) {
            [self.captureSession addOutput:self.imageOutput];
        }
        else {
            // Output not available
        }
        
        // Video Output
        self.videoOutput = [AVCaptureVideoDataOutput new];
        self.videoOutput.videoSettings = @{(id)kCVPixelBufferPixelFormatTypeKey: [NSNumber numberWithInt: kCVPixelFormatType_32BGRA]};
        
        // Delegate
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0);
        [self.videoOutput setSampleBufferDelegate:self queue:queue];
        
        if ([self.captureSession canAddOutput:self.videoOutput]) {
            [self.captureSession addOutput:self.videoOutput];
        }
        else {
            // Output not available
        }
        
        // Movie Output
        self.movieOutput = [AVCaptureMovieFileOutput new];
        
        if ([self.captureSession canAddOutput:self.movieOutput]) {
            [self.captureSession addOutput:self.movieOutput];
        }
        else {
            // Output not available
            NSLog(@"MovieOutput not available");
        }
        
        // Preview Output
        [self.view layoutIfNeeded];
        
        self.previewLayer = [AVCaptureVideoPreviewLayer layerWithSession:self.captureSession];
        [self.previewLayer setFrame:self.view.bounds];
        [self.previewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
        [self.containerPreview.layer addSublayer:self.previewLayer];
        self.containerPreview.alpha = 0.0;
        
        // Metadata Output
        self.metadataOutput = [AVCaptureMetadataOutput new];
        [self.metadataOutput setMetadataObjectsDelegate:self queue:queue];
        
        if ([self.captureSession canAddOutput:self.metadataOutput]) {
            [self.captureSession addOutput:self.metadataOutput];
        }
        else {
            // Output not available
            NSLog(@"MetadataOutput not available");
        }
        
        [self.metadataOutput setMetadataObjectTypes:self.metadataObjectTypes];
        
        self.enabled = YES;
        [self startCameraWithCompletionBlock:nil];
    }
    else if (enable == NO && self.isEnabled) {
        // Interrompo la sessione
        [self.captureSession stopRunning];
        [self.previewLayer removeFromSuperlayer];
        [self.captureSession removeOutput:self.metadataOutput];
        [self.captureSession removeOutput:self.movieOutput];
        [self.captureSession removeOutput:self.videoOutput];
        [self.captureSession removeOutput:self.imageOutput];
        [self.captureSession removeInput:self.deviceInput];
        self.metadataOutput = nil;
        self.movieOutput = nil;
        self.videoOutput = nil;
        self.imageOutput = nil;
        self.deviceInput = nil;
        self.captureSession = nil;
        self.enabled = NO;
        
        [self stopCameraWithCompletionBlock:nil];
    }
}

- (void)startCameraWithCompletionBlock:(void (^)(void))completionBlock
{
    self.enabled = YES;
    
    [DBThread eseguiBlock:^{
        // Avvio la sessione in modo asincrono
        [self.captureSession startRunning];
        
        // Dissolvenza
        CGFloat durata = (self.animazioneIniziale) ? kDurataDissolvenzaIniziale : kDurataDissolvenza;
        
        // Memorizzo l'animazione così posso annullarla in ogni momento (ed evitare di eseguire il completionBlock)
        self.animation = [DBAnimation performWithDuration:durata finalState:^{
            self.containerPreview.alpha = 1.0;
            
        } completionBlock:^{
            self.animation = nil;
            
            // Camera pronta
            if (completionBlock) completionBlock();
        }];
    }];
}

- (void)stopCameraWithCompletionBlock:(void (^)(void))completionBlock
{
    // Camera non pronta
    self.enabled = NO;
    
    // Dissolvenza
    CGFloat durata = (self.animazioneIniziale) ? kDurataDissolvenzaIniziale : kDurataDissolvenza;
    
    // Memorizzo l'animazione così posso annullarla in ogni momento (ed evitare di eseguire il completionBlock)
    self.animation = [DBAnimation performWithDuration:durata finalState:^{
        self.containerPreview.alpha = 0.0;
        
    } completionBlock:^{
        self.animation = nil;
        
        // Interrompo la sessione
        [self.captureSession stopRunning];
        if (completionBlock) completionBlock();
    }];
}

- (void)destroyHandler
{
    [self enableHandler:NO];
    
    NSMutableArray *sharedInstances = [DBCameraHandler sharedInstances];
    [sharedInstances removeObject:self];
}


// MARK: Gestione della camera

- (void)capturePreviewImageWithCompletionBlock:(DBCameraHandlerImageBlock)completionBlock
{
    // Non funziona perché il delegate di "videoOutput" non viene richiamato :(
    return;
    
    // Leggero ritardo per far stabilizzare il telefono dopo la pressione del tasto per scattare la foto
    [DBThread eseguiBlockConRitardo:0.15 block:^{
        // Il delegate del buffer catturerà automaticamente la foto e la passerà al metodo -[self fotoScattata:]
        self.previewBlock = completionBlock;
        self.catturaPreview = YES;
    }];
}

- (void)captureOutputImageWithCompletionBlock:(void (^)(UIImage *))completionBlock
{
    AVCaptureConnection *connection = [self.imageOutput connectionWithMediaType:AVMediaTypeVideo];
    
    [self.imageOutput captureStillImageAsynchronouslyFromConnection:connection completionHandler:^(CMSampleBufferRef imageDataSampleBuffer, NSError *error) {
        UIImage *image = nil;
        
        if (imageDataSampleBuffer) {
            @autoreleasepool {
                NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageDataSampleBuffer];
                image = [[UIImage alloc] initWithData:imageData];
                
                // Ridisegno l'immagine, così perde i dati EXIF e ripristina l'orientamento originale
                UIGraphicsBeginImageContext(image.size);
                [image drawInRect:CGRectMake(0.0, 0.0, image.size.width, image.size.height)];
                image = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
            }
        }
        
        if (completionBlock) {
            completionBlock(image);
        }
    }];
}

- (void)startRecordingOutputVideoToFileURL:(NSURL *)fileURL
{
    if (self.outputQuality == DBCameraOutputQualityBestForPhotos) {
        DBLog(@"Non è possibile registrare video con l'impostazione outputQuality DBCameraOutputQualityBestForPhotos.");
        DBLog(@"Cambio la modalità in DBCameraOutputQualityHigh e inizio a registrare tra 1 secondo.");
        self.outputQuality = DBCameraOutputQualityHigh;
        
        // Attendo che la fotocamera sia pronta di nuovo
        [DBThread eseguiBlockConRitardo:1.0 block:^{
            [self startRecordingOutputVideoToFileURL:fileURL];
        }];
        
        return;
    }
    
    @try {
        [self.movieOutput startRecordingToOutputFileURL:fileURL recordingDelegate:self];
    } @catch (NSException *exception) {
        // Configurazione non valida
    }
}

- (void)stopRecordingOutputVideoWithCompletionBlock:(void (^)(BOOL))completionBlock
{
    if (self.movieOutput.isRecording) {
        self.videoBlock = completionBlock;
        [self.movieOutput stopRecording];
    }
}


// MARK: Media recognition

@synthesize metadataObjectTypes = _metadataObjectTypes;

- (void)setMetadataObjectTypes:(NSArray<__kindof NSString *> *)metadataObjectTypes
{
    _metadataObjectTypes = metadataObjectTypes;
}

- (void)startCapturingMetadataObjectTypes:(NSArray<__kindof NSString *> *)metadataObjectTypes
{
    NSArray *availableMetadataObjectTypes = self.metadataOutput.availableMetadataObjectTypes;
    
    NSMutableArray *types = [NSMutableArray array];
    [types addObjectsFromArray:self.metadataObjectTypes];
    [types addObjectsFromArray:metadataObjectTypes];
    
    // We can add only types available in the current capture session
    // TODO: Add an informative log for removed elements!
    [types removeObjectsNotInArray:availableMetadataObjectTypes];
    
    self.metadataObjectTypes = types;
    
    [self.metadataOutput setMetadataObjectTypes:self.metadataObjectTypes];
}

- (void)stopCapturingMetadataObjectTypes:(NSArray<__kindof NSString *> *)metadataObjectTypes
{
    NSMutableArray *types = [NSMutableArray array];
    [types addObjectsFromArray:self.metadataObjectTypes];
    [types removeObjectsInArray:metadataObjectTypes];
    self.metadataObjectTypes = types;
    
    [self.metadataOutput setMetadataObjectTypes:self.metadataObjectTypes];
}

- (void)stopCapturingAllMetadataObjectTypes
{
    self.metadataObjectTypes = [NSMutableArray array];
    [self.metadataOutput setMetadataObjectTypes:self.metadataObjectTypes];
}


// MARK: - MovieFileOutput delegate

- (void)captureOutput:(AVCaptureFileOutput *)captureOutput didFinishRecordingToOutputFileAtURL:(NSURL *)outputFileURL fromConnections:(NSArray *)connections error:(NSError *)error
{
    BOOL success = YES;
    
    if ([error code] != noErr) {
        // Si è verificato un errore, ma il salvataggio del video potrebbe comunque essere andato a buon fine
        id check = [[error userInfo] objectForKey:AVErrorRecordingSuccessfullyFinishedKey];
        if (check) {
            success = [check boolValue];
        }
    }
    
    // Returning to the main thread now
    [DBThread eseguiBlock:^{
        if (self.videoBlock) {
            self.videoBlock(success);
            self.videoBlock = nil;
        }
    }];
}


// MARK: - MetadataOutputObjects delegate

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection
{
    if (self.isEnabled == NO) {
        // Handler not enabled
        return;
    }
    
    if ([self.delegate respondsToSelector:@selector(handler:didCaptureMetadataObjects:)]) {
        // This delegate method is called in the background thread
        // Returning to the main thread now
        [DBThread eseguiBlockSync:^{
            [self.delegate handler:self didCaptureMetadataObjects:metadataObjects];
        }];
    }
}

- (void)captureOutput:(AVCaptureOutput *)output didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection
{
    if (self.catturaPreview) {
        self.catturaPreview = NO;
        
        // Create a UIImage from the sample buffer data
        UIImage *image = [self imageFromSampleBuffer:sampleBuffer];
        // image = [image immagineRuotataConGradi:90.0];
        
        [DBThread eseguiBlock:^{
            if (self.previewBlock) {
                self.previewBlock(image);
                self.previewBlock = nil;
            }
        }];
    }
}


// MARK: - CapturePreview

// Create a UIImage from sample buffer data
- (UIImage *)imageFromSampleBuffer:(CMSampleBufferRef)sampleBuffer
{
    // Get a CMSampleBuffer's Core Video image buffer for the media data
    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    
    // Lock the base address of the pixel buffer
    CVPixelBufferLockBaseAddress(imageBuffer, 0);
    
    // Get the number of bytes per row for the pixel buffer
    void *baseAddress = CVPixelBufferGetBaseAddress(imageBuffer);
    
    // Get the number of bytes per row for the pixel buffer
    size_t bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer);
    
    // Get the pixel buffer width and height
    size_t width = CVPixelBufferGetWidth(imageBuffer);
    size_t height = CVPixelBufferGetHeight(imageBuffer);
    
    // Create a device-dependent RGB color space
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    // Create a bitmap graphics context with the sample buffer data
    CGContextRef context = CGBitmapContextCreate(baseAddress, width, height, 8,
                                                 bytesPerRow, colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst);
    
    // Create a Quartz image from the pixel data in the bitmap graphics context
    CGImageRef quartzImage = CGBitmapContextCreateImage(context);
    
    // Unlock the pixel buffer
    CVPixelBufferUnlockBaseAddress(imageBuffer, 0);
    
    // Free up the context and color space
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    
    // Create an image object from the Quartz image
    UIImage *image = [UIImage imageWithCGImage:quartzImage];
    
    // Release the Quartz image
    CGImageRelease(quartzImage);
    
    return (image);
}


// MARK: - CameraView delegate

- (void)viewDidLayoutSubviews:(UIView *)view
{
    // Aggiorno la dimensione del preview layer
    [self.previewLayer setFrame:self.view.bounds];
}

@end
