//
//  DBTableViewHandlerDefines.h
//  File version: 1.0.0
//  Last modified: 05/26/2017
//
//  Created by Davide Balistreri on 05/26/2017
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

@class DBTableViewHandler;


// MARK: - DBTableViewHandler Delegate

@protocol DBTableViewHandlerDelegate <NSObject, UITableViewDelegate, UITableViewDataSource>

@optional

/**
 * Implementare questo metodo per gestire il tipo di cella che verrà istanziata in base all'oggetto o all'IndexPath.
 *
 * <b>Come funziona:</b> <br>
 * Saranno configurate in maniera automatica le proprietà di DBTableViewCell nel caso in cui la cella discendesse da quella classe. <br>
 * <i>Aprire la dichiarazione di DBTableViewCell per conoscere quali sono e cosa offrono.</i>
 */
- (UITableViewCell *)handler:(DBTableViewHandler *)handler cellForObject:(id)object atIndexPath:(NSIndexPath *)indexPath;

/**
 * Implementare questo metodo per informare il delegate quando viene selezionata una cella.
 * <p>Si avrà accesso semplificato all'oggetto che la cella rappresentava e il suo IndexPath.</p>
 */
- (void)handler:(DBTableViewHandler *)handler didSelectCell:(UITableViewCell *)cell withObject:(id)object atIndexPath:(NSIndexPath *)indexPath;

/**
 * Implementare questo metodo per informare il delegate quando viene aggiornata la TableView.
 */
- (void)handlerDidReloadTableView:(DBTableViewHandler *)handler;


//// MARK: UITableView Delegate methods autocomplete
//
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
//
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
//- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath NS_AVAILABLE_IOS(7_0);
//
//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section;
//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section;
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section;
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
//- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath;
//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;

@end
