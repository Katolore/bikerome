//
//  DBCoreData.h
//  File version: 1.1.1
//  Last modified: 12/15/2015
//
//  Created by Davide Balistreri on 04/17/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

#if DBFRAMEWORK_VERSION_EARLIER_THAN(__DBFRAMEWORK_0_3_2)
    #warning Bisogna adattare il codice DBCoreData alla nuova versione del Framework.
#endif


@interface DBCoreData : NSObject

// MARK: - Metodi di classe

/**
 * Istanza Database CoreData condivisa, utile se l'applicazione utilizza un solo Database.
 * @return L'istanza condivisa.
 */
+ (nonnull instancetype)sharedInstance;

// DISABILITATO PER ORA
///**
// * Crea un'istanza Database CoreData e apre un Database impostando il Modello specificato.
// * @param URLDatabase L'indirizzo del Database.
// * @param URLModelloDatabase L'indirizzo del Modello del Database.
// * @return Nuova istanza configurata con i parametri specificati.
// */
//+ (nonnull instancetype)apriDatabase:(nullable NSURL *)URLDatabase conModello:(nullable NSURL *)URLModelloDatabase;
// DISABILITATO PER ORA

// MARK: - Metodi di istanza

/**
 * Default: attivo.
 * Determina se salvare il Database ogni volta che viene inserito/rimosso un oggetto.
 */
@property (nonatomic) BOOL salvataggioAutomatico;

/**
 * Default: attivo.
 * Abilita il log completo delle operazioni effettuate sul Database,
 * non limitando il debug solo agli errori.
 */
@property (nonatomic) BOOL debugCompleto;

/**
 * Il Modello del Database CoreData di questa istanza.
 */
@property (strong, nonatomic, nullable, readonly) NSURL *URLModelloDatabase;

/**
 * Gli indirizzi dei Database attualmente aperti in questa istanza.
 */
@property (strong, nonatomic, nonnull, readonly) NSArray <NSURL *> *URLDatabases;

/**
 * Imposta il Modello del Database CoreData.
 * @param URLModelloDatabase L'indirizzo del Modello del Database.
 */
- (void)impostaModelloDatabase:(nullable NSURL *)URLModelloDatabase;

/**
 * Imposta e apre un Database CoreData, configurato con le opzioni specificate.
 * È obbligatorio impostare il Modello del Database prima di poter effettuare operazioni su di esso.
 * @param URLDatabase L'indirizzo del Database.
 * @param opzioni Le opzioni di configurazione del Database.
 * @return Boolean che indica se l'operazione è andata a buon fine.
 */
- (BOOL)apriDatabase:(nullable NSURL *)URLDatabase conOpzioni:(nullable NSDictionary *)opzioni;

/**
 * Imposta e apre un Database CoreData configurato come Locale.
 * È obbligatorio impostare il Modello del Database prima di poter effettuare operazioni su di esso.
 * @param URLDatabase L'indirizzo del Database.
 * @return Boolean che indica se l'operazione è andata a buon fine.
 */
- (BOOL)apriDatabaseLocale:(nullable NSURL *)URLDatabase;

/**
 * Imposta e apre un Database CoreData configurato come iCloud (Remoto).
 * È obbligatorio impostare il Modello del Database prima di poter effettuare operazioni su di esso.
 * @param URLDatabase L'indirizzo del Database.
 * @param nomeContenitore Il nome del contenitore di iCloud.
 * @return Boolean che indica se l'operazione è andata a buon fine.
 */
- (BOOL)apriDatabaseiCloud:(nullable NSURL *)URLDatabase conNomeContenitore:(nullable NSString *)nomeContenitore;

/**
 * Chiude un Database CoreData e lo rimuove dalla memoria.
 * @param URLDatabase L'indirizzo del Database.
 * @return Boolean che indica se l'operazione è andata a buon fine.
 */
- (BOOL)chiudiDatabase:(nullable NSURL *)URLDatabase;

/**
 * Chiude tutti i Database CoreData aperti e li rimuove dalla memoria.
 * @return Boolean che indica se l'operazione è andata a buon fine.
 */
- (BOOL)chiudiDatabaseAperti;

/**
 * Scrive su disco le modifiche effettuate al Database.
 * @return Boolean che indica se l'operazione è andata a buon fine.
 */
- (BOOL)salvaDatabase;

/**
 * Effettua la migrazione di un Database da Locale a iCloud.
 * @param URLDatabase L'indirizzo del Database.
 * @param nomeContenitore Il nome del contenitore iCloud.
 */
- (void)migrazioneDatabaseDaLocaleAiCloud:(nullable NSURL *)URLDatabase conNomeContenitore:(nullable NSString *)nomeContenitore;

/**
 * Effettua la migrazione di un Database da iCloud a Locale.
 * @param URLDatabase L'indirizzo del Database.
 */
- (void)migrazioneDatabaseDaiCloudALocale:(nullable NSURL *)URLDatabase;

/**
 * Estrae dal Database tutti gli oggetti conformi all'entità specificata.
 * @param nomeEntita L'entità degli oggetti da estrarre. L'entità deve esistere sul Modello impostato.
 * @return Array contenente tutti gli oggetti trovati sul Database, o <i>nil</i> se l'entità specificata non esiste sul Modello impostato.
 */
- (nullable NSArray *)oggettiConNomeEntita:(nullable NSString *)nomeEntita;

/**
 * Conta sul Database la quantità di oggetti conformi all'entità specificata.
 * @param nomeEntita L'entità degli oggetti da enumerare. L'entità deve esistere sul Modello impostato.
 * @return Numero che indica la quantità di oggetti trovati sul Database, o <i>NSNotFound</i> se l'entità specificata non esiste sul Modello impostato.
 */
- (NSUInteger)numeroOggettiConNomeEntita:(nullable NSString *)nomeEntita;

/**
 * Crea un oggetto conforme all'entità specificata, ma non lo inserisce nel Database.
 * @param nomeEntita L'entità dell'oggetto da creare. Deve esistere sul Modello impostato.
 * @return L'oggetto creato, o <i>nil</i> se l'entità specificata non esiste sul Modello impostato.
 */
- (nullable id)creaOggettoConNomeEntita:(nullable NSString *)nomeEntita;

/**
 * Crea un oggetto conforme all'entità specificata e lo inserisce nel Database.
 * @param nomeEntita L'entità dell'oggetto da creare. Deve esistere sul Modello impostato.
 * @return L'oggetto creato, o <i>nil</i> se l'entità specificata non esiste sul Modello impostato o se si verifica un errore.
 */
- (nullable id)inserisciNuovoOggettoConNomeEntita:(nullable NSString *)nomeEntita;

/**
 * Inserisce un oggetto nel Database.
 * @param oggetto L'oggetto da inserire.
 * @return Boolean che indica se l'operazione è andata a buon fine.
 */
- (BOOL)inserisciOggetto:(nullable id)oggetto;

/**
 * Rimuove un oggetto dal Database.
 * @param oggetto L'oggetto da rimuovere.
 * @return Boolean che indica se l'operazione è andata a buon fine.
 */
- (BOOL)rimuoviOggetto:(nullable id)oggetto;


// MARK: - DBCoreData 1.0

NS_ASSUME_NONNULL_BEGIN

+ (void)impostaManagedObjectContext:(NSManagedObjectContext *)managedObjectContext
DBFRAMEWORK_DEPRECATED(0.3.2, "utilizzare l'istanza DBCoreData condivisa o crearne una");

+ (NSManagedObjectContext *)managedObjectContext
DBFRAMEWORK_DEPRECATED(0.3.2, "utilizzare l'istanza DBCoreData condivisa o crearne una");

+ (NSError *)salvaDatabase
DBFRAMEWORK_DEPRECATED(0.3.2, "utilizzare l'istanza DBCoreData condivisa o crearne una");

+ (NSArray *)entitaConNome:(NSString *)nome
DBFRAMEWORK_DEPRECATED(0.3.2, "utilizzare l'istanza DBCoreData condivisa o crearne una");

/// Crea un oggetto senza inserirlo nel managedObjectContext
+ (id)creaOggettoConNomeEntita:(NSString *)nomeEntita
DBFRAMEWORK_DEPRECATED(0.3.2, "utilizzare l'istanza DBCoreData condivisa o crearne una");

+ (BOOL)inserisciOggetto:(id)oggetto
DBFRAMEWORK_DEPRECATED(0.3.2, "utilizzare l'istanza DBCoreData condivisa o crearne una");

+ (BOOL)rimuoviOggetto:(id)oggetto
DBFRAMEWORK_DEPRECATED(0.3.2, "utilizzare l'istanza DBCoreData condivisa o crearne una");

NS_ASSUME_NONNULL_END


// MARK: - DBCoreData 2.0

/**
 * Imposta il nome del contenitore da utilizzare per il PersistentStore del database iCloud.
 * @param nomeContenitoreiCloud Il nome del contenitore.
 */
+ (void)impostaNomeContenitoreiCloud:(nullable NSString *)nomeContenitoreiCloud
DBFRAMEWORK_DEPRECATED(0.3.2, "utilizzare l'istanza DBCoreData condivisa o crearne una");

/**
 * Opzioni per il PersistentStore di CoreData per il database locale. <br>
 * Queste opzioni gestiscono automaticamente le lightweight migrations di CoreData,
 * e il passaggio da un database di tipo iCloud a uno di tipo locale.
 */
+ (nonnull NSDictionary *)opzioniPersistentStoreLocale
DBFRAMEWORK_DEPRECATED(0.3.2, "utilizzare l'istanza DBCoreData condivisa o crearne una");

/**
 * Opzioni per il PersistentStore di CoreData per il database iCloud. <br>
 * Queste opzioni gestiscono automaticamente le lightweight migrations di CoreData,
 * e il passaggio da un database di tipo locale a uno di tipo iCloud. <br>
 * Verrà utilizzato il nomeContenitore impostato con il metodo <b>+impostaNomeContenitoreiCloud:</b>
 */
+ (nonnull NSDictionary *)opzioniPersistentStoreiCloud
DBFRAMEWORK_DEPRECATED(0.3.2, "utilizzare l'istanza DBCoreData condivisa o crearne una");

/**
 * Opzioni per il PersistentStore di CoreData per il database iCloud. <br>
 * Queste opzioni gestiscono automaticamente le lightweight migrations di CoreData,
 * e il passaggio da un database di tipo locale a uno di tipo iCloud.
 * @param nomeContenitore Il nome del contenitore iCloud. Deve essere una stringa valida altrimenti non
 *                        verrà restituito il Dictionary con le opzioni.
 */
+ (nullable NSDictionary *)opzioniPersistentStoreiCloudConNomeContenitore:(nullable NSString *)nomeContenitore
DBFRAMEWORK_DEPRECATED(0.3.2, "utilizzare l'istanza DBCoreData condivisa o crearne una");

@end
