//
//  DBManagedObject.h
//  File version: 1.0.1
//  Last modified: 12/15/2015
//
//  Created by Davide Balistreri on 04/21/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

#import <CoreData/CoreData.h>

@interface DBManagedObject : NSManagedObject

@property (strong, nonatomic, readonly, nonnull) NSString *nomeEntita;
+ (nonnull NSString *)nomeEntita;

@end
