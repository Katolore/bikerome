//
//  DBCoreData.m
//  File version: 1.1.1
//  Last modified: 12/15/2015
//
//  Created by Davide Balistreri on 04/17/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBCoreData.h"
#import "DBFramework.h"

#import <CoreData/CoreData.h>

@interface DBCoreData ()

@property (strong, nonatomic, nullable) NSManagedObjectModel *managedObjectModel;
@property (strong, nonatomic, nullable) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (strong, nonatomic, nullable) NSManagedObjectContext *managedObjectContext;

@property (strong, nonatomic, nullable) NSString *nomeContenitoreiCloud;

@end


@implementation DBCoreData

// MARK: - Metodi di classe

+ (instancetype)sharedInstance
{
    @synchronized(self) {
        DBCoreData *sharedInstance = [DBCentralManager getRisorsa:@"DBSharedCoreData"];
        
        if (!sharedInstance) {
            sharedInstance = [DBCoreData new];
            [DBCentralManager setRisorsa:sharedInstance conIdentifier:@"DBSharedCoreData"];
        }
        
        return sharedInstance;
    }
}

// DISABILITATO PER ORA
//+ (instancetype)apriDatabase:(NSURL *)URLDatabase conModello:(NSURL *)URLModelloDatabase
//{
//    DBCoreData *database = [DBCoreData new];
//    [database impostaModelloDatabase:URLModelloDatabase];
//    [database apriDatabase:URLDatabase];
//    return database;
//}
// DISABILITATO PER ORA

// MARK: - Metodi di istanza

@synthesize URLModelloDatabase = _URLModelloDatabase;
@synthesize URLDatabases = _URLDatabases;

- (void)impostaModelloDatabase:(NSURL *)URLModelloDatabase
{
    // Check
    if (!URLModelloDatabase) {
        return;
    }
    
    // Gestire il cambio di modello
    // ...
    
    _URLModelloDatabase = URLModelloDatabase;
}

- (BOOL)apriDatabase:(NSURL *)URLDatabase conOpzioni:(NSDictionary *)opzioni
{
    if ([self configura] == NO) {
        if (self.debugCompleto)
            DBExtendedLog(@"Non è ancora possibile eseguire operazioni sui Database di questa istanza");
        
        return NO;
    }
    
    @try {
        NSError *errore = nil;
        [self.persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:URLDatabase options:opzioni error:&errore];
        
        if (errore) {
            DBExtendedLog(@"Si è verificato un errore durante l'apertura del database:\n", [errore description], @"\n", [errore userInfo]);
            return NO;
        }
        else {
            if (self.debugCompleto)
                DBLog(@"Database aperto con successo!");
            
            // Aggiungo l'URL alla lista dei Database aperti
            NSMutableArray *URLDatabases = [_URLDatabases mutableCopy];
            [URLDatabases addObject:URLDatabase];
            _URLDatabases = URLDatabases;
            
            return YES;
        }
    }
    @catch (NSException *exception) {
        DBExtendedLog(@"Si è verificato un errore durante l'apertura del database:\n", [exception description], @"\n", [exception userInfo]);
        return NO;
    }
}

- (BOOL)apriDatabaseLocale:(NSURL *)URLDatabase
{
    NSDictionary *opzioni = [DBCoreData privateOpzioniPersistentStoreLocale];
    return [self apriDatabase:URLDatabase conOpzioni:opzioni];
}

- (BOOL)apriDatabaseiCloud:(NSURL *)URLDatabase conNomeContenitore:(NSString *)nomeContenitore
{
    NSDictionary *opzioni = [DBCoreData privateOpzioniPersistentStoreiCloudConNomeContenitore:nomeContenitore];
    return [self apriDatabase:URLDatabase conOpzioni:opzioni];
}

- (BOOL)chiudiDatabase:(NSURL *)URLDatabase
{
    NSUInteger indiceDatabase = [self.URLDatabases indexOfObject:URLDatabase];
    
    if (indiceDatabase != NSNotFound) {
        // Chiudo il database
        NSArray *persistentStores = self.persistentStoreCoordinator.persistentStores;
        NSPersistentStore *persistentStore = [persistentStores safeObjectAtIndex:indiceDatabase];
        
        if (persistentStore) {
            @try {
                NSError *errore = nil;
                [self.persistentStoreCoordinator removePersistentStore:persistentStore error:&errore];
                
                if (errore) {
                    DBExtendedLog(@"Si è verificato un errore durante la chiusura del database:\n", [errore description], @"\n", [errore userInfo]);
                    return NO;
                }
                else {
                    if (self.debugCompleto)
                        DBLog(@"Database chiuso con successo!");
                    
                    // Rimuovo l'URL alla lista dei Database aperti
                    NSMutableArray *URLDatabases = [_URLDatabases mutableCopy];
                    [URLDatabases removeObject:URLDatabase];
                    _URLDatabases = URLDatabases;
                    
                    return YES;
                }
            }
            @catch (NSException *exception) {
                DBExtendedLog(@"Si è verificato un errore durante la chiusura del database:\n", [exception description], @"\n", [exception userInfo]);
                return NO;
            }
        }
    }
}

- (BOOL)chiudiDatabaseAperti
{
    for (NSURL *URLDatabase in self.URLDatabases) {
        BOOL success = [self chiudiDatabase:URLDatabase];
        
        if (success == NO) {
            // Interrompo l'esecuzione
            return NO;
        }
    }
    
    return YES;
}

- (BOOL)salvaDatabase
{
    if ([self configura] == NO) {
        if (self.debugCompleto)
            DBExtendedLog(@"Non è ancora possibile eseguire operazioni sui Database di questa istanza");
        
        return NO;
    }
    
    [self.managedObjectContext performBlockAndWait:^{
        @try {
            NSError *errore = nil;
            [self.managedObjectContext save:&errore];
            
            if (errore) {
                DBExtendedLog(@"Errore durante il salvataggio del database:\n", [errore description], @"\n", [errore userInfo]);
            }
            else {
                if (self.debugCompleto)
                    DBLog(@"Database salvato con successo!");
            }
        }
        @catch (NSException *exception) {
            DBExtendedLog(@"Errore durante il salvataggio del database:\n", [exception description], @"\n", [exception userInfo]);
        }
    }];
    
    return YES;
}

- (void)migrazioneDatabaseDaLocaleAiCloud:(NSURL *)URLDatabase conNomeContenitore:(NSString *)nomeContenitore
{
    NSDictionary *opzioni = [DBCoreData privateOpzioniPersistentStoreiCloudConNomeContenitore:nomeContenitore];
    
    NSPersistentStoreCoordinator *persistentStoreCoordinator = self.persistentStoreCoordinator;
    NSPersistentStore *persistentStoreDaMigrare = [persistentStoreCoordinator.persistentStores firstObject];
    
    if (persistentStoreDaMigrare) {
        // NSFileManager *fileManager = [NSFileManager defaultManager];
        NSError *error = nil;
        
        // Rimuovo i vecchi file del database
        // [fileManager removeItemAtURL:databaseLocale error:&error];
        // [fileManager removeItemAtURL:databaseiCloud error:&error];
        
        @try {
            // Effettuo la migrazione
            [persistentStoreCoordinator migratePersistentStore:persistentStoreDaMigrare toURL:URLDatabase options:opzioni withType:NSSQLiteStoreType error:&error];
        }
        @catch (NSException *exception) {
            DBExtendedLog(@"Errore durante la migrazione del Database da Locale a iCloud:\n", [exception description], @"\n", [exception userInfo]);
        }
    }
}

- (void)migrazioneDatabaseDaiCloudALocale:(NSURL *)URLDatabase
{
    NSDictionary *opzioni = [DBCoreData privateOpzioniPersistentStoreLocale];
    
    NSPersistentStoreCoordinator *persistentStoreCoordinator = self.persistentStoreCoordinator;
    NSPersistentStore *persistentStoreDaMigrare = [persistentStoreCoordinator.persistentStores firstObject];
    
    if (persistentStoreDaMigrare) {
        @try {
            // Effettuo la migrazione
            NSError *error = nil;
            [persistentStoreCoordinator migratePersistentStore:persistentStoreDaMigrare toURL:URLDatabase options:opzioni withType:NSSQLiteStoreType error:&error];
        }
        @catch (NSException *exception) {
            DBExtendedLog(@"Errore durante la migrazione del Database da iCloud a Locale:\n", [exception description], @"\n", [exception userInfo]);
        }
    }
}

- (NSArray *)oggettiConNomeEntita:(NSString *)nomeEntita
{
    if ([self configura] == NO) {
        if (self.debugCompleto)
            DBExtendedLog(@"Non è ancora possibile eseguire operazioni sui Database di questa istanza");
        
        return nil;
    }
    
    if ([NSString isEmpty:nomeEntita]) {
        return nil;
    }
    
    @try {
        // Prendo tutti gli oggetti sul database
        NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:nomeEntita];
        // [fetchRequest setReturnsObjectsAsFaults:NO]; // Ottimizzo la memoria
        
        NSError *errore = nil;
        NSArray *oggetti = [self.managedObjectContext executeFetchRequest:fetchRequest error:&errore];
        
        if (errore) {
            DBExtendedLog(@"Errore durante la ricerca di entità sul database:\n", [errore description], @"\n", [errore userInfo]);
            return nil;
        }
        
        if (self.debugCompleto)
            DBLog(@"Entita '", nomeEntita, @"' estratte: ", $(parseInt(oggetti.count)));
        
        return oggetti;
    }
    @catch (NSException *exception) {
        DBExtendedLog(@"Errore durante la ricerca di entità sul database:\n", [exception description], @"\n", [exception userInfo]);
        return nil;
    }
}

- (NSUInteger)numeroOggettiConNomeEntita:(nullable NSString *)nomeEntita
{
    if ([self configura] == NO) {
        if (self.debugCompleto)
            DBExtendedLog(@"Non è ancora possibile eseguire operazioni sui Database di questa istanza");
        
        return NSNotFound;
    }
    
    if ([NSString isEmpty:nomeEntita]) {
        return NSNotFound;
    }
    
    @try {
        // Prendo tutti gli oggetti sul database
        NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:nomeEntita];
        [fetchRequest setIncludesSubentities:NO];
        
        NSError *errore = nil;
        NSUInteger numeroOggetti = [self.managedObjectContext countForFetchRequest:fetchRequest error:&errore];
        
        if (errore) {
            DBExtendedLog(@"Errore durante la ricerca di entità sul database:\n", [errore description], @"\n", [errore userInfo]);
            return NSNotFound;
        }
        
        if (self.debugCompleto)
            DBLog(@"Entita '", nomeEntita, @"' trovate: ", $(parseInt(numeroOggetti)));
        
        return numeroOggetti;
    }
    @catch (NSException *exception) {
        DBExtendedLog(@"Errore durante la ricerca di entità sul database:\n", [exception description], @"\n", [exception userInfo]);
        return NSNotFound;
    }
}

- (id)creaOggettoConNomeEntita:(NSString *)nomeEntita
{
    if ([self configura] == NO) {
        if (self.debugCompleto)
            DBExtendedLog(@"Non è ancora possibile eseguire operazioni sui Database di questa istanza");
        
        return nil;
    }
    
    @try {
        NSEntityDescription *entita = [NSEntityDescription entityForName:nomeEntita inManagedObjectContext:self.managedObjectContext];
        
        Class ClasseEntita = NSClassFromString(nomeEntita);
        id istanza = [ClasseEntita alloc];
        id oggetto = [istanza initWithEntity:entita insertIntoManagedObjectContext:nil];
        
        return oggetto;
    }
    @catch (NSException *exception) {
        // Se il nome entita non è valido o non è possibile trovare/allocare un'istanza con quel nome
        DBExtendedLog(@"Errore durante la creazione di un oggetto con entità '", nomeEntita, @"' sul database:\n", [exception description], @"\n", [exception userInfo]);
        return nil;
    }
}

- (id)inserisciNuovoOggettoConNomeEntita:(NSString *)nomeEntita
{
    id oggetto = [self creaOggettoConNomeEntita:nomeEntita];
    if (oggetto == nil) {
        return nil;
    }
    
    BOOL success = [self inserisciOggetto:oggetto];
    if (success == NO) {
        return nil;
    }
    
    return oggetto;
}

- (BOOL)inserisciOggetto:(id)oggetto
{
    if ([self configura] == NO) {
        if (self.debugCompleto)
            DBExtendedLog(@"Non è ancora possibile eseguire operazioni sui Database di questa istanza");
        
        return NO;
    }
    
    @try {
        // // Controllo se l'oggetto è valido
        // Class ClasseOggetto = [oggetto class];
        //
        // if (![ClasseOggetto respondsToSelector:@selector(entityName)] &&
        //     ![ClasseOggetto respondsToSelector:@selector(nomeEntita)]) {
        //     return NO;
        // }
        
        // Inserisco l'oggetto nel database
        [self.managedObjectContext insertObject:oggetto];
        
        if (self.salvataggioAutomatico) {
            // Salvo la modifica appena effettuata
            BOOL success = [self salvaDatabase];
            return success;
        }
        else {
            // Continuo senza salvare
            return YES;
        }
    }
    @catch (NSException *exception) {
        // Si è verificato un errore
        if (oggetto && [oggetto respondsToSelector:@selector(description)]) {
            DBExtendedLog(@"Errore durante l'inserimento dell'oggetto '", [oggetto description] , @"' sul database:\n", [exception description], @"\n", [exception userInfo]);
        }
        else {
            DBExtendedLog(@"Errore durante l'inserimento di un oggetto sul database:\n", [exception description], @"\n", [exception userInfo]);
        }
        
        return NO;
    }
}

- (BOOL)rimuoviOggetto:(id)oggetto
{
    if ([self configura] == NO) {
        if (self.debugCompleto)
            DBExtendedLog(@"Non è ancora possibile eseguire operazioni sui Database di questa istanza");
        
        return NO;
    }
    
    @try {
        // // Controllo se l'oggetto è valido
        // if (![[oggetto class] respondsToSelector:@selector(entityName)]) return NO;
        
        // Rimuovo l'oggetto dal database
        [self.managedObjectContext deleteObject:oggetto];
        
        if (self.salvataggioAutomatico) {
            // Salvo la modifica appena effettuata
            BOOL success = [self salvaDatabase];
            return success;
        }
        else {
            // Continuo senza salvare
            return YES;
        }
    }
    @catch (NSException *exception) {
        // Si è verificato un errore
        if (oggetto && [oggetto respondsToSelector:@selector(description)]) {
            DBExtendedLog(@"Errore durante la rimozione dell'oggetto '", [oggetto description] , @"' sul database:\n", [exception description], @"\n", [exception userInfo]);
        }
        else {
            DBExtendedLog(@"Errore durante la rimozione di un oggetto sul database:\n", [exception description], @"\n", [exception userInfo]);
        }
        
        return NO;
    }
}

// MARK: - Metodi interni

- (instancetype)init
{
    self = [super init];
    if (self) {
        _URLDatabases = [NSArray array];
        self.salvataggioAutomatico = YES;
        self.debugCompleto = YES;
    }
    return self;
}

/**
 * Configura l'istanza, se possibile.
 * @return Boolean che indica se l'istanza è configurata e pronta per essere utilizzata.
 */
- (BOOL)configura
{
    if (!self.managedObjectModel) {
        if (!self.URLModelloDatabase) {
            // Non è possibile configurare l'istanza senza un Modello
            return NO;
        }
        else {
            self.managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:self.URLModelloDatabase];
        }
    }
    
    if (!self.persistentStoreCoordinator) {
        self.persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.managedObjectModel];
    }
    
    if (!self.managedObjectContext) {
        self.managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        [self.managedObjectContext setPersistentStoreCoordinator:self.persistentStoreCoordinator];
        
        // This policy merges conflicts between the persistent store's version of the
        // object and the current in-memory version, giving priority to in-memory changes.
        [self.managedObjectContext setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
    }
    
    return YES;
}

/**
 * Opzioni per il PersistentStore di CoreData per il database locale. <br>
 * Queste opzioni gestiscono automaticamente le lightweight migrations di CoreData,
 * e il passaggio da un database di tipo iCloud a uno di tipo locale.
 */
+ (nonnull NSDictionary *)privateOpzioniPersistentStoreLocale
{
    NSDictionary *opzioni = @{NSPersistentStoreRemoveUbiquitousMetadataOption:[NSNumber numberWithBool:YES],
                              NSMigratePersistentStoresAutomaticallyOption:[NSNumber numberWithBool:YES],
                              NSInferMappingModelAutomaticallyOption:[NSNumber numberWithBool:YES],
                              NSSQLitePragmasOption:@{@"journal_mode":@"DELETE"}};
    return opzioni;
}

/**
 * Opzioni per il PersistentStore di CoreData per il database iCloud. <br>
 * Queste opzioni gestiscono automaticamente le lightweight migrations di CoreData,
 * e il passaggio da un database di tipo locale a uno di tipo iCloud.
 * @param nomeContenitore Il nome del contenitore iCloud. Deve essere una stringa valida altrimenti non
 *                        verrà restituito il Dictionary con le opzioni.
 */
+ (NSDictionary *)privateOpzioniPersistentStoreiCloudConNomeContenitore:(nullable NSString *)nomeContenitore
{
    if ([NSString isEmpty:nomeContenitore]) {
        // Nome contenitore non valido
        return nil;
    }
    
    NSDictionary *opzioni = @{NSPersistentStoreUbiquitousContentNameKey:nomeContenitore,
                              NSMigratePersistentStoresAutomaticallyOption:[NSNumber numberWithBool:YES],
                              NSInferMappingModelAutomaticallyOption:[NSNumber numberWithBool:YES]};
    return opzioni;
}

// MARK: - DBCoreData 1.0

+ (void)impostaManagedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
    DBFRAMEWORK_DEPRECATED_LOG(0.3.2, "utilizzare l'istanza DBCoreData condivisa o crearne una");
    [[DBCoreData sharedInstance] setManagedObjectContext:managedObjectContext];
}

+ (NSManagedObjectContext *)managedObjectContext
{
    DBFRAMEWORK_DEPRECATED_LOG(0.3.2, "utilizzare l'istanza DBCoreData condivisa o crearne una");
    return [[DBCoreData sharedInstance] managedObjectContext];
}

+ (NSError *)salvaDatabase
{
    DBFRAMEWORK_DEPRECATED_LOG(0.3.2, "utilizzare l'istanza DBCoreData condivisa o crearne una");
    
    @try {
        NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
        
        // This policy merges conflicts between the persistent store's version of the
        // object and the current in-memory version, giving priority to in-memory changes.
        [managedObjectContext setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
        
        NSError *error = nil;
        [managedObjectContext save:&error];
        
        if (error) {
            NSLog(@"\nErrore durante il salvataggio del database:\n%@, %@\n\n", error, [error userInfo]);
            return error;
        }
        else {
            DBLog(@"Database salvato con successo!");
            return nil;
        }
    }
    @catch (NSException *exception) {
        NSLog(@"\nErrore durante il salvataggio del database:\n%@, %@\n\n", [exception description], [exception userInfo]);
        return [NSError new];
    }
}

+ (NSArray *)entitaConNome:(NSString *)nome
{
    DBFRAMEWORK_DEPRECATED_LOG(0.3.2, "utilizzare l'istanza DBCoreData condivisa o crearne una");
    
    if ([NSString isEmpty:nome]) {
        return nil;
    }
    
    @try {
        // Prendo tutti gli oggetti sul database
        NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:nome];
        [fetchRequest setReturnsObjectsAsFaults:NO];
        
        NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
        
        NSError *error = nil;
        NSArray *objects = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
        NSLog(@"Entita %@ trovate: %d", nome, (int)objects.count);
        return objects;
    }
    @catch (NSException *exception) {
        NSLog(@"\nErrore durante la ricerca di entità sul database:\n%@, %@\n\n", [exception description], [exception userInfo]);
        return nil;
    }
}

+ (id)creaOggettoConNomeEntita:(NSString *)nomeEntita
{
    DBFRAMEWORK_DEPRECATED_LOG(0.3.2, "utilizzare l'istanza DBCoreData condivisa o crearne una");
    
    @try {
        NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
        
        NSEntityDescription *entita = [NSEntityDescription entityForName:nomeEntita inManagedObjectContext:managedObjectContext];
        Class ClasseEntita = NSClassFromString(nomeEntita);
        id istanza = [ClasseEntita alloc];
        
        id oggetto = [istanza initWithEntity:entita insertIntoManagedObjectContext:nil];
        return oggetto;
    }
    @catch (NSException *exception) {
        // Se il nome entita non è valido o non è possibile trovare/allocare un'istanza con quel nome
        return nil;
    }
}

+ (BOOL)inserisciOggetto:(id)oggetto
{
    DBFRAMEWORK_DEPRECATED_LOG(0.3.2, "utilizzare l'istanza DBCoreData condivisa o crearne una");
    
    @try {
        // // Controllo se l'oggetto è valido
        // Class ClasseOggetto = [oggetto class];
        //
        // if (![ClasseOggetto respondsToSelector:@selector(entityName)] &&
        //     ![ClasseOggetto respondsToSelector:@selector(nomeEntita)]) {
        //     return NO;
        // }
        
        // Salvo l'oggetto nel database
        NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
        [managedObjectContext insertObject:oggetto];
        
        NSError *errore = [self salvaDatabase];
        return (!errore) ? YES : NO;
    }
    @catch (NSException *exception) {
        return NO;
    }
}

+ (BOOL)rimuoviOggetto:(id)oggetto
{
    DBFRAMEWORK_DEPRECATED_LOG(0.3.2, "utilizzare l'istanza DBCoreData condivisa o crearne una");
    
    @try {
        // // Controllo se l'oggetto è valido
        // if (![[oggetto class] respondsToSelector:@selector(entityName)]) return NO;
        
        // Rimuovo l'oggetto dal database
        NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
        [managedObjectContext deleteObject:oggetto];
        
        NSError *errore = [self salvaDatabase];
        return (!errore) ? YES : NO;
    }
    @catch (NSException *exception) {
        return NO;
    }
}

// Metodo non utilizzato per ora
//+ (BOOL)isManagedObjectContextValid:(NSManagedObjectContext *)managedObjectContext
//{
//    @try {
//        if (!managedObjectContext) return NO;
//        if (!managedObjectContext.persistentStoreCoordinator) return NO;
//        if ([NSArray isEmpty:managedObjectContext.persistentStoreCoordinator.persistentStores]) return NO;
//        return YES;
//    }
//    @catch (NSException *exception) {
//        return NO;
//    }
//}

// MARK: - DBCoreData 2.0

/**
 * Imposta il nome del contenitore da utilizzare per il PersistentStore del database iCloud.
 * @param nomeContenitoreiCloud Il nome del contenitore.
 */
+ (void)impostaNomeContenitoreiCloud:(nullable NSString *)nomeContenitoreiCloud
{
    DBFRAMEWORK_DEPRECATED_LOG(0.3.2, "utilizzare l'istanza DBCoreData condivisa o crearne una");
    
    DBCoreData *sharedInstance = [DBCoreData sharedInstance];
    sharedInstance.nomeContenitoreiCloud = nomeContenitoreiCloud;
}

/**
 * Opzioni per il PersistentStore di CoreData per il database locale. <br>
 * Queste opzioni gestiscono automaticamente le lightweight migrations di CoreData,
 * e il passaggio da un database di tipo iCloud a uno di tipo locale.
 */
+ (nonnull NSDictionary *)opzioniPersistentStoreLocale
{
    DBFRAMEWORK_DEPRECATED_LOG(0.3.2, "utilizzare l'istanza DBCoreData condivisa o crearne una");
    
    NSDictionary *opzioni = @{// NSPersistentStoreRemoveUbiquitousMetadataOption:[NSNumber numberWithBool:YES],
                              NSMigratePersistentStoresAutomaticallyOption:[NSNumber numberWithBool:YES],
                              NSInferMappingModelAutomaticallyOption:[NSNumber numberWithBool:YES],
                              NSSQLitePragmasOption:@{@"journal_mode":@"DELETE"}};
    return opzioni;
}

/**
 * Opzioni per il PersistentStore di CoreData per il database iCloud. <br>
 * Queste opzioni gestiscono automaticamente le lightweight migrations di CoreData,
 * e il passaggio da un database di tipo locale a uno di tipo iCloud. <br>
 * Verrà utilizzato il nomeContenitore impostato con il metodo <b>+impostaNomeContenitoreiCloud:</b>
 */
+ (nonnull NSDictionary *)opzioniPersistentStoreiCloud
{
    DBFRAMEWORK_DEPRECATED_LOG(0.3.2, "utilizzare l'istanza DBCoreData condivisa o crearne una");
    
    DBCoreData *sharedInstance = [DBCoreData sharedInstance];
    return [self opzioniPersistentStoreiCloudConNomeContenitore:sharedInstance.nomeContenitoreiCloud];
}

/**
 * Opzioni per il PersistentStore di CoreData per il database iCloud. <br>
 * Queste opzioni gestiscono automaticamente le lightweight migrations di CoreData,
 * e il passaggio da un database di tipo locale a uno di tipo iCloud.
 * @param nomeContenitore Il nome del contenitore iCloud. Deve essere una stringa valida altrimenti non
 *                        verrà restituito il Dictionary con le opzioni.
 */
+ (nullable NSDictionary *)opzioniPersistentStoreiCloudConNomeContenitore:(nullable NSString *)nomeContenitore
{
    DBFRAMEWORK_DEPRECATED_LOG(0.3.2, "utilizzare l'istanza DBCoreData condivisa o crearne una");
    
    if ([NSString isEmpty:nomeContenitore]) {
        // Nome contenitore non valido
        return nil;
    }
    
    NSDictionary *opzioni = @{NSPersistentStoreUbiquitousContentNameKey:nomeContenitore,
                              NSMigratePersistentStoresAutomaticallyOption:[NSNumber numberWithBool:YES],
                              NSInferMappingModelAutomaticallyOption:[NSNumber numberWithBool:YES]};
    return opzioni;
}

@end
