//
//  DBThread.m
//  File version: 1.0.2
//  Last modified: 03/13/2016
//
//  Created by Davide Balistreri on 07/23/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBThread.h"
#import "DBFramework.h"

@interface DBThread ()

// Per i thread ripetuti ogni N secondi
@property (strong, atomic) NSTimer *dbThreadingTimer;

#if DBFRAMEWORK_TARGET_IOS
// Per i thread ripetuti N volte al secondo
@property (strong, atomic) CADisplayLink *dbThreadingDisplayLink;
#endif

@end


@implementation DBThread

- (void)mainLoop
{
    // Developers may override this method and provide custom behavior
}

// MARK: - Metodi di classe

+ (instancetype)avviaThreadConRipetizioniOgniSecondi:(NSTimeInterval)secondi
{
    id thread = [self new];
    [thread impostaEsecuzioneConRipetizioniOgniSecondi:secondi];
    [thread avvia];
    return thread;
}

#if DBFRAMEWORK_TARGET_IOS
+ (instancetype)avviaThreadConRipetizioniAlSecondo:(NSUInteger)ripetizioni
{
    id thread = [self new];
    [thread impostaEsecuzioneConRipetizioniAlSecondo:ripetizioni];
    [thread avvia];
    return thread;
}
#endif

+ (instancetype)avviaThreadConRipetizioni:(NSUInteger)ripetizioni
{
    id thread = [self new];
    [thread impostaEsecuzioneConRipetizioni:ripetizioni];
    [thread avvia];
    return thread;
}

+ (void)avviaThreadUnaVolta
{
    id thread = [self new];
    [thread impostaEsecuzioneUnaVolta];
    [thread avvia];
}

+ (void)interrompiThread:(DBThread *)thread
{
    [thread interrompi];
}

// MARK: - Metodi d'istanza

- (instancetype)init
{
    self = [super init];
    if (self) {
        // Inizializzazione
        [self inizializzaThread];
    }
    return self;
}

- (void)inizializzaThread
{
    // Inizializzo i parametri di configurazione del thread
    _isThreadInEsecuzione = NO;
    _isThreadInterrotto = NO;
    _dbThreadMode = DBThreadModeEseguiUnaVolta;
    _dbThreadIntervalloRipetizioni = 0.0;
    _dbThreadNumeroRipetizioni = 0;
}

- (void)impostaEsecuzioneConRipetizioniOgniSecondi:(NSTimeInterval)secondi
{
    [self inizializzaThread];
    _dbThreadMode = DBThreadModeEseguiOgniSecondi;
    _dbThreadIntervalloRipetizioni = secondi;
}

#if DBFRAMEWORK_TARGET_IOS
- (void)impostaEsecuzioneConRipetizioniAlSecondo:(NSUInteger)ripetizioni
{
    [self inizializzaThread];
    _dbThreadMode = DBThreadModeEseguiAlSecondo;
    _dbThreadNumeroRipetizioni = ripetizioni;
    _dbThreadIntervalloRipetizioni = 1.0 / ripetizioni;
}
#endif

- (void)impostaEsecuzioneConRipetizioni:(NSUInteger)ripetizioni
{
    [self inizializzaThread];
    _dbThreadMode = DBThreadModeEseguiConRipetizioni;
    _dbThreadNumeroRipetizioni = ripetizioni;
}

- (void)impostaEsecuzioneUnaVolta
{
    [self inizializzaThread];
    _dbThreadMode = DBThreadModeEseguiUnaVolta;
}

- (void)avvia
{
    _isThreadInterrotto = NO;
    
    switch (self.dbThreadMode) {
        case DBThreadModeEseguiOgniSecondi:
            [self avviaConRipetizioniOgniSecondi:self.dbThreadIntervalloRipetizioni];
            break;
#if DBFRAMEWORK_TARGET_IOS
        case DBThreadModeEseguiAlSecondo:
            [self avviaConRipetizioniAlSecondo:self.dbThreadNumeroRipetizioni];
            break;
#endif
        case DBThreadModeEseguiConRipetizioni:
            [self avviaConRipetizioni:self.dbThreadNumeroRipetizioni];
            break;
        case DBThreadModeEseguiUnaVolta:
            [self avviaUnaVolta];
            break;
    }
}

- (void)avviaConRipetizioniOgniSecondi:(NSTimeInterval)secondi
{
    // Check
    if (secondi <= 0.0) secondi = 1.0;
    
    _isThreadInEsecuzione = YES;
    self.dbThreadingTimer = [NSTimer scheduledTimerWithTimeInterval:secondi target:self selector:@selector(autoreleasedMainLoop) userInfo:nil repeats:YES];
}

#if DBFRAMEWORK_TARGET_IOS
- (void)avviaConRipetizioniAlSecondo:(NSUInteger)ripetizioni
{
    // Check
    if (ripetizioni <= 0) ripetizioni = 1;
    if (ripetizioni > 60) ripetizioni = 60;
    
    _isThreadInEsecuzione = YES;
    
    self.dbThreadingDisplayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(autoreleasedMainLoop)];
    
    self.dbThreadingDisplayLink.frameInterval = (60 / ripetizioni);
    [self.dbThreadingDisplayLink addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSRunLoopCommonModes];
}
#endif

- (void)avviaConRipetizioni:(NSUInteger)ripetizioni
{
    // Check
    if (ripetizioni <= 0) ripetizioni = 1;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        _isThreadInEsecuzione = YES;
        for (NSUInteger counter = 0; counter < ripetizioni; counter++) {
            [self autoreleasedMainLoop];
        }
        _isThreadInEsecuzione = NO;
    });
}

- (void)avviaUnaVolta
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        _isThreadInEsecuzione = YES;
        [self autoreleasedMainLoop];
        _isThreadInEsecuzione = NO;
    });
}

- (void)interrompi
{
    _isThreadInterrotto = YES;
    _isThreadInEsecuzione = NO;
    
    // Per i thread ripetuti ogni N secondi
    if (self.dbThreadingTimer) {
        [self.dbThreadingTimer invalidate];
        self.dbThreadingTimer = nil;
    }
    
#if DBFRAMEWORK_TARGET_IOS
    // Per i thread ripetuti N volte al secondo
    if (self.dbThreadingDisplayLink) {
        [self.dbThreadingDisplayLink removeFromRunLoop:[NSRunLoop mainRunLoop] forMode:NSRunLoopCommonModes];
        self.dbThreadingDisplayLink = nil;
    }
#endif
}

/**
 * Metodo privato per ottimizzare le risorse utilizzate ad ogni esecuzione.
 */
- (void)autoreleasedMainLoop
{
    @autoreleasepool {
        // Controllo se il thread è stato interrotto
        if (!_isThreadInterrotto) {
            [self mainLoop];
        }
    }
}

// MARK: - Metodi per funzioni esterne

+ (void)eseguiSelectorConTarget:(id)target selector:(SEL)selector conObject:(id)object
{
    [target performSelectorOnMainThread:selector withObject:object waitUntilDone:NO];
}

+ (void)eseguiSelectorConRitardo:(NSTimeInterval)ritardo target:(id)target selector:(SEL)selector conObject:(id)object
{
    [self eseguiBlockConRitardo:ritardo block:^{
        [self eseguiSelectorConTarget:target selector:selector conObject:object];
    }];
}

+ (void)eseguiBlock:(DBThreadBlock)block
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (block) block();
    });
}

+ (void)eseguiBlockSync:(DBThreadBlock)block
{
    if ([NSThread isMainThread]) {
        if (block) block();
    }
    else {
        [self eseguiBlock:block];
    }
}

+ (void)eseguiBlockConRitardo:(NSTimeInterval)ritardo block:(DBThreadBlock)block
{
    if (ritardo <= 0.0) {
        [self eseguiBlockSync:block];
        return;
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(ritardo * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        // Se il block esiste ancora in memoria, lo eseguo
        if (block) block();
    });
}

+ (void)eseguiSelectorSuThreadInBackgroundConTarget:(id)target selector:(SEL)selector conObject:(id)object
{
    [NSThread detachNewThreadSelector:selector toTarget:target withObject:object];
}

+ (void)eseguiBlockSuThreadInBackground:(DBThreadBlock)block
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        if (block) block();
    });
}

+ (void)eseguiBlockSuThreadInBackgroundConRitardo:(NSTimeInterval)ritardo block:(DBThreadBlock)block
{
    // Check
    if (!block) return;
    
    if (ritardo <= 0.0) {
        [self eseguiBlockSuThreadInBackground:block];
        return;
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(ritardo * NSEC_PER_SEC)), dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // Se il block esiste ancora in memoria, lo eseguo
        if (block) block();
    });
}

/**
 * Esegue un blocco di codice sul thread principale.
 */
+ (void)eseguiBlockSulMainThread:(DBThreadBlock)block
{
    DBFRAMEWORK_DEPRECATED_LOG(0.5.1, "utilizza +[DBThread eseguiBlock:]");
    [self eseguiBlock:block];
}

/**
 * Esegue un blocco di codice sul thread principale, dopo aver atteso N secondi.
 */
+ (void)eseguiBlockSulMainThreadConRitardo:(NSTimeInterval)ritardo block:(DBThreadBlock)block
{
    DBFRAMEWORK_DEPRECATED_LOG(0.5.1, "utilizza +[DBThread eseguiBlockConRitardo:block:]");
    [self eseguiBlockConRitardo:ritardo block:block];
}

@end


// MARK: - NSObject

@implementation NSObject (DBThread)

+ (void)performBlockAfterDelay:(NSTimeInterval)delay block:(DBThreadBlock)block
{
    [DBThread eseguiBlockConRitardo:delay block:block];
}

- (void)performBlockAfterDelay:(NSTimeInterval)delay block:(DBThreadBlock)block
{
    [DBThread eseguiBlockConRitardo:delay block:block];
}

@end
