//
//  DBMath.h
//  File version: 1.0.0
//  Last modified: 03/27/2016
//
//  Created by Davide Balistreri on 03/27/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

// MARK: - Funzioni matematiche

int    parseInt(double valore);
float  parseFloat(double valore);
double parseDouble(double valore);

BOOL doubleIsValid(double number);

double unsignedDouble(double number);
double safeDouble(double numero);
double mantieniAllInterno(double numero, double min, double max);
double percentuale(double numero, double altroNumero);
double percentualeChange(double numero, double altroNumero);
double percentualeTraNumeri(double numero, double min, double max, BOOL mantieniAllInterno);

double floorConDecimali(double valore, int decimali);


// MARK: - Strutture

typedef struct {
    CGFloat x;
    CGFloat y;
    CGFloat z;
} DBMath3DVector;

extern DBMath3DVector DBMath3DVectorMake(CGFloat x, CGFloat y, CGFloat z);

extern const DBMath3DVector DBMath3DVectorZero;


// MARK: - NSNumber

@interface NSNumber (DBToolkit)

+ (NSNumber *)safeNumber:(NSNumber *)numero;

+ (NSNumber *)mantieniNumero:(NSNumber *)numero traMin:(NSNumber *)min max:(NSNumber *)max;

+ (NSNumber *)mediaNumeri:(NSArray *)numeri;

+ (NSNumber *)percentualeNumero:(NSNumber *)numero altroNumero:(NSNumber *)altroNumero;
+ (NSNumber *)percentualeChangeNumero:(NSNumber *)numero altroNumero:(NSNumber *)altroNumero;
+ (NSNumber *)percentualeNumero:(NSNumber *)numero traMin:(NSNumber *)min max:(NSNumber *)max;
+ (NSNumber *)percentualeNumero:(NSNumber *)numero traMin:(NSNumber *)min max:(NSNumber *)max mantieniAllInterno:(BOOL)mantieniAllInterno;

+ (double)safeDouble:(double)numero;

@end
