//
//  DBBluetoothEddystone.h
//  File version: 1.0.0
//  Last modified: 05/10/2016
//
//  Created by Davide Balistreri on 05/10/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBBluetoothPeripheral.h"

/**
 * Google announced Eddystone™ (protocol/firmware) in July 2015.
 * Side note: it was called UriBeacon for a while, then it was announced that the tech had evolved beyond the original specs.
 * To differentiate, they changed the name to Eddystone.
 */
@interface DBBluetoothEddystone : DBBluetoothPeripheral

/**
 * Eddystone-URL is the beacon URL, which is simply a web page.
 * It's the format for the "Physical Web", where you put content if you want everybody to be able to access it.
 */
@property (strong, nonatomic, readonly) NSURL *URL;

/**
 * Eddystone-UID is a 16 digit string of characters, divided into a 10 character "Namespace" and a 6 character "Instance" ID.
 * Typically the "Namespace" is used to identify an entity and the "Instance" an individual beacon.
 * No two beacons will ever have the same UID.
 */
@property (strong, nonatomic, readonly) NSString *UID;

/**
 * Eddystone-Namespace is a 10 digit string of characters.
 * Typically the "Namespace" is used to identify an entity and the "Instance" an individual beacon.
 */
@property (strong, nonatomic, readonly) NSString *UIDNamespace;

/**
 * Eddystone-Instance is a 6 digit string of characters.
 * Typically the "Namespace" is used to identify an entity and the "Instance" an individual beacon.
 */
@property (strong, nonatomic, readonly) NSString *UIDInstance;

// MARK: - Inizializzazione

/// Crea un'istanza Eddystone basata sulla periferica specificata.
+ (instancetype)createWithPeripheral:(DBBluetoothPeripheral *)peripheral;

/// Boolean che indica se la periferica rispetta il protocollo Eddystone.
+ (BOOL)isPeripheralValid:(DBBluetoothPeripheral *)peripheral;

@end
