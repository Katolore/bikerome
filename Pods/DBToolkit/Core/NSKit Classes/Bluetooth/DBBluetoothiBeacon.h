//
//  DBBluetoothiBeacon.h
//  File version: 1.0.0
//  Last modified: 05/10/2016
//
//  Created by Davide Balistreri on 05/10/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBBluetoothPeripheral.h"

/**
 * Apple announced iBeacon (protocol/firmware) in December 2013.
 * iBeacon works with iOS and Android.
 */
@interface DBBluetoothiBeacon : DBBluetoothPeripheral

/**
 * iBeacon-UUID is a 16 digit string of numbers.
 * The UUID is typically the same for all iBeacons working with a specific app.
 * The \c -major and \c -minor IDs are used to identify each beacon uniquely.
 */
@property (strong, nonatomic, readonly) NSString *UUID;

/**
 * iBeacon-Major is a 4 digit string of numbers.
 */
@property (strong, nonatomic, readonly) NSString *major;

/**
 * iBeacon-Minor is a 4 digit string of numbers.
 */
@property (strong, nonatomic, readonly) NSString *minor;

// MARK: - Inizializzazione

/// Crea un'istanza iBeacon basata sulla periferica specificata.
+ (instancetype)createWithPeripheral:(DBBluetoothPeripheral *)peripheral;

/// Boolean che indica se la periferica rispetta il protocollo iBeacon.
+ (BOOL)isPeripheralValid:(DBBluetoothPeripheral *)peripheral;

@end
