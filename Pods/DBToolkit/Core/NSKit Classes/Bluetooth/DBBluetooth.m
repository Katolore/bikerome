//
//  DBBluetooth.m
//  File version: 1.0.0
//  Last modified: 09/12/2016
//
//  Created by Davide Balistreri on 04/23/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBBluetooth.h"

#import "DBThread.h"
#import "DBObjectExtensions.h"
#import "DBArrayExtensions.h"
#import "DBNotificationCenter.h"


NSString *DBBluetoothDidChangeState = @"kDBBluetoothDidChangeState";

NSString *DBBluetoothDidFindPeripheralNotification = @"kDBBluetoothDidFindPeripheralNotification";

NSString *DBBluetoothDidFindEddystoneNotification = @"kDBBluetoothDidFindEddystoneNotification";
NSString *DBBluetoothDidFindiBeaconNotification   = @"kDBBluetoothDidFindiBeaconNotification";

NSString *DBBluetoothDidConnectPeripheralNotification = @"kDBBluetoothDidConnectPeripheralNotification";


// Category used to expose private properties or methods
@interface DBBluetoothPeripheral (PrivateMethods)

@property (nonatomic) NSInteger numberOfDiscoveredCharacteristics;

@end


@interface DBBluetooth () <CBCentralManagerDelegate, CBPeripheralDelegate>

// Default: disabled.
@property (nonatomic) BOOL debugMode;

@property (strong, nonatomic) CBCentralManager *manager;
@property (nonatomic, getter = isManagerReady) BOOL managerReady;
@property (nonatomic, getter = isBluetoothReady) BOOL bluetoothReady;

@property (strong, nonatomic) NSArray<NSString *> *scanSimilarNames;
@property (strong, nonatomic) NSArray<CBUUID *> *scanServices;

@property (strong, nonatomic) NSMutableArray<DBBluetoothPeripheral *> *privateScannedPeripherals;

@end


@implementation DBBluetooth

// MARK: - Setup

+ (instancetype)sharedInstance
{
    @synchronized(self) {
        __kindof DBBluetooth *sharedInstance = [DBCentralManager getRisorsa:@"DBSharedBluetooth"];
        
        if (!sharedInstance) {
            sharedInstance = [self new];
            [DBCentralManager setRisorsa:sharedInstance conIdentifier:@"DBSharedBluetooth"];
        }
        
        return sharedInstance;
    }
}

- (void)setupObject
{
    NSDictionary *options = @{CBCentralManagerOptionShowPowerAlertKey:@NO};
    self.manager = [[CBCentralManager alloc] initWithDelegate:self queue:nil options:options];
}


// MARK: - Private methods

- (DBBluetoothPeripheral *)bluetoothPeripheralWithPeripheral:(CBPeripheral *)peripheral
{
    for (DBBluetoothPeripheral *bluetoothPeripheral in self.privateScannedPeripherals) {
        if ([bluetoothPeripheral.peripheral isEqual:peripheral]) {
            return bluetoothPeripheral;
        }
    }
    
    return nil;
}

- (void)notifyIfPeripheralConnectionIsCompleted:(DBBluetoothPeripheral *)bluetoothPeripheral
{
    // Checking if all characteristics have been discovered
    if (bluetoothPeripheral.numberOfDiscoveredCharacteristics == bluetoothPeripheral.peripheral.services.count) {
        // Connection process completed
        [DBNotificationCenter postNotificationWithName:DBBluetoothDidConnectPeripheralNotification];
    }
}


// MARK: - Public methods

+ (void)setDebugMode:(BOOL)debugMode
{
    DBBluetooth *sharedInstance = [DBBluetooth sharedInstance];
    sharedInstance.debugMode = debugMode;
}

+ (BOOL)isBluetoothPoweredOn
{
    DBBluetooth *sharedInstance = [DBBluetooth sharedInstance];
    return sharedInstance.isBluetoothReady;
}


// MARK: - Search

+ (void)scanForPeripheralsNearby
{
    [self scanForPeripheralsWithSimilarName:nil withServices:nil];
}

+ (void)scanForPeripheralsWithSimilarName:(NSString *)similarName withServices:(NSArray<CBUUID *> *)services
{
    DBBluetooth *sharedInstance = [DBBluetooth sharedInstance];
    sharedInstance.scanSimilarNames = [NSArray safeArrayWithObject:similarName];
    sharedInstance.scanServices = services;
    
    [sharedInstance startScanningForPeripherals];
}

+ (void)scanForPeripheralsWithSimilarNames:(NSArray<NSString *> *)similarNames withServices:(NSArray<CBUUID *> *)services
{
    DBBluetooth *sharedInstance = [DBBluetooth sharedInstance];
    sharedInstance.scanSimilarNames = [NSArray safeArrayWithArray:similarNames];
    sharedInstance.scanServices = services;
    
    [sharedInstance startScanningForPeripherals];
}

+ (void)stopScanningForPeripherals
{
    DBBluetooth *sharedInstance = [DBBluetooth sharedInstance];
    [sharedInstance stopScanningForPeripherals];
}

+ (NSArray<DBBluetoothPeripheral *> *)scannedPeripherals
{
    DBBluetooth *sharedInstance = [DBBluetooth sharedInstance];
    return [NSArray arrayWithArray:sharedInstance.privateScannedPeripherals];
}

- (void)startScanningForPeripherals
{
    self.privateScannedPeripherals = [NSMutableArray array];
    
    if (self.isManagerReady) {
        NSDictionary *options = @{CBCentralManagerScanOptionAllowDuplicatesKey:@YES};
        [self.manager scanForPeripheralsWithServices:self.scanServices options:options];
    }
    else {
        // Il CentralManager del Bluetooth ancora non è pronto, riprovo tra N secondi
        [DBThread eseguiSelectorConRitardo:0.5 target:self selector:@selector(startScanningForPeripherals) conObject:nil];
    }
}

- (void)stopScanningForPeripherals
{
    [self.manager stopScan];
}

- (void)connectToPeripheral:(DBBluetoothPeripheral *)peripheral
{
    @try {
        if (peripheral.peripheral) {
            // Logging
            if (self.debugMode) {
                NSLog(@"^ Connecting to peripheral %@", peripheral);
            }
            
            [self.manager connectPeripheral:peripheral.peripheral options:nil];
        }
    }
    @catch (NSException *exception) {
        // Logging
        if (self.debugMode) {
            NSLog(@"^ Exception raised during device connection");
        }
    }
}

- (void)disconnectFromPeripheral:(DBBluetoothPeripheral *)peripheral
{
    @try {
        if (peripheral.peripheral) {
            // Logging
            if (self.debugMode) {
                NSLog(@"^ Disconnecting from peripheral %@", peripheral);
            }
            
            [self.manager cancelPeripheralConnection:peripheral.peripheral];
        }
    }
    @catch (NSException *exception) {
        // Logging
        if (self.debugMode) {
            NSLog(@"^ Exception raised during device disconnection");
        }
    }
}


// MARK: - Connection

+ (void)connectToPeripheral:(DBBluetoothPeripheral *)peripheral
{
    [[DBBluetooth sharedInstance] connectToPeripheral:peripheral];
}

+ (void)disconnectFromPeripheral:(DBBluetoothPeripheral *)peripheral
{
    [[DBBluetooth sharedInstance] disconnectFromPeripheral:peripheral];
}


// MARK: - Writing

+ (BOOL)writeData:(NSData *)value toPeripheral:(DBBluetoothPeripheral *)peripheral
{
    return [[DBBluetooth sharedInstance] writeData:value toPeripheral:peripheral];
}

- (BOOL)writeData:(NSData *)value toPeripheral:(DBBluetoothPeripheral *)bluetoothPeripheral
{
    @try {
        // Logging
        if (self.debugMode) {
            NSLog(@"^ Writing value to peripheral %@", bluetoothPeripheral);
        }
        
        CBPeripheral *peripheral = bluetoothPeripheral.peripheral;
        CBCharacteristic *characteristic = bluetoothPeripheral.TXCharacteristic;
        CBCharacteristicProperties properties = characteristic.properties;
        CBCharacteristicWriteType type = (properties == CBCharacteristicPropertyWriteWithoutResponse) ? CBCharacteristicWriteWithoutResponse : CBCharacteristicWriteWithResponse;
        
        [peripheral writeValue:value forCharacteristic:characteristic type:type];
        
        return YES;
        
        // DA TESTARE: Send data in lengths of <= 20 bytes
        //        NSUInteger dataLength = value.length;
        //        NSUInteger limit = 2; // Da testare!
        //
        //        // Below limit, send as-is
        //        if (dataLength <= limit) {
        //            [peripheral writeValue:value forCharacteristic:characteristic type:type];
        //        }
        //        // Above limit, send in lengths <= 20 bytes
        //        else {
        //            NSUInteger len = limit;
        //            NSUInteger loc = 0;
        //            NSUInteger idx = 0; //for debug
        //
        //            while (loc < dataLength) {
        //                NSUInteger rmdr = dataLength - loc;
        //                if (rmdr <= len) {
        //                    len = rmdr;
        //                }
        //
        //                NSRange range = NSMakeRange(loc, len);
        //
        //                uint8_t *newBytes; // [UInt8](count: len, repeatedValue: 0)
        //                [value getBytes:&newBytes range:range];
        //
        //                NSData *packedValue = [NSData dataWithBytes:newBytes length:len];
        //                [peripheral writeValue:packedValue forCharacteristic:characteristic type:type];
        //
        //                loc += len;
        //                idx += 1;
        //            }
        //        }
    }
    @catch (NSException *exception) {
        // Logging
        if (self.debugMode) {
            NSLog(@"^ Exception raised during writeValue toPeripheral");
        }
    }
    
    return NO;
}


// MARK: - CBCentralManager delegate

- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    self.managerReady = YES;
    
    switch (central.state) {
        case CBCentralManagerStatePoweredOn:
            self.bluetoothReady = YES;
            break;
        default:
            self.bluetoothReady = NO;
            break;
    }
    
    // Invio la notifica del cambio di stato
    [DBNotificationCenter postNotificationWithName:DBBluetoothDidChangeState];
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary<NSString *,id> *)advertisementData RSSI:(NSNumber *)RSSI
{
    //    for (DBBluetoothPeripheral *search in self.privateScannedPeripherals) {
    //        if ([search.peripheral isEqual:peripheral]) {
    //            // Periferica già trovata
    //            return;
    //        }
    //    }
    
    if ([NSArray isNotEmpty:self.scanSimilarNames]) {
        NSString *peripheralName = peripheral.name;
        BOOL isNameSimilar = NO;
        
        if ([NSString isNotEmpty:peripheralName]) {
            for (NSString *similarName in self.scanSimilarNames) {
                NSRange range = [peripheralName rangeOfString:similarName options:NSCaseInsensitiveSearch];
                
                if (range.location != NSNotFound) {
                    isNameSimilar = YES;
                    break;
                }
            }
        }
        else {
            NSString *advertisementName = [advertisementData objectForKey:CBAdvertisementDataLocalNameKey];
            NSLog(@"^ Advertisement name: %@", advertisementName);
        }
        
        if (isNameSimilar == NO) {
            // La periferica non ha passato il controllo del nome
            return;
        }
    }
    
    // La periferica rientra nei parametri di ricerca
    DBBluetoothPeripheral *bluetoothPeripheral = [DBBluetoothPeripheral createWithPeripheral:peripheral advertisementData:advertisementData RSSI:RSSI];
    
    if ([DBBluetoothEddystone isPeripheralValid:bluetoothPeripheral]) {
        // La periferica rispetta il protocollo Eddystone
        bluetoothPeripheral = [DBBluetoothEddystone createWithPeripheral:bluetoothPeripheral];
    }
    
    if ([DBBluetoothiBeacon isPeripheralValid:bluetoothPeripheral]) {
        // La periferica rispetta il protocollo iBeacon
        bluetoothPeripheral = [DBBluetoothiBeacon createWithPeripheral:bluetoothPeripheral];
    }
    
    // Salvo la periferica nella lista di quelle trovate durante questa scansione
    [self.privateScannedPeripherals addObject:bluetoothPeripheral];
    
    // Invio la notifica della periferica trovata
    [DBNotificationCenter postNotificationWithName:DBBluetoothDidFindPeripheralNotification object:bluetoothPeripheral];
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
    // Logging
    if (self.debugMode) {
        NSLog(@"^ Peripheral connected: %@", peripheral);
    }
    
    peripheral.delegate = self;
    
    [peripheral discoverServices:nil];
}

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    // Logging
    if (self.debugMode) {
        NSLog(@"^ Peripheral disconnected: %@", peripheral);
    }
    
    peripheral.delegate = nil;
}


// MARK: - CBPeripheral delegate

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
    // Logging
    if (self.debugMode) {
        NSLog(@"^ Discovered services for peripheral: %@", peripheral);
    }
    
    DBBluetoothPeripheral *bluetoothPeripheral = [self bluetoothPeripheralWithPeripheral:peripheral];
    [self notifyIfPeripheralConnectionIsCompleted:bluetoothPeripheral];
    
    for (CBService *service in peripheral.services) {
        [peripheral discoverCharacteristics:nil forService:service];
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(nonnull CBService *)service error:(nullable NSError *)error
{
    // Logging
    if (self.debugMode) {
        NSLog(@"^ Discovered characteristics for service: %@ of peripheral: %@", service, peripheral);
    }
    
    DBBluetoothPeripheral *bluetoothPeripheral = [self bluetoothPeripheralWithPeripheral:peripheral];
    bluetoothPeripheral.numberOfDiscoveredCharacteristics++;
    
    [self notifyIfPeripheralConnectionIsCompleted:bluetoothPeripheral];
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    id value = characteristic.value;
    
    // Logging
    if (self.debugMode) {
        NSString *string = [[NSString alloc] initWithData:value encoding:NSUTF8StringEncoding];
        NSLog(@"^^ %@", string);
    }
    
    DBBluetoothPeripheral *bluetoothPeripheral = [self bluetoothPeripheralWithPeripheral:peripheral];
    
    if ([bluetoothPeripheral.delegate respondsToSelector:@selector(bluetoothPeripheral:characteristic:didReceiveValue:)]) {
        [bluetoothPeripheral.delegate bluetoothPeripheral:bluetoothPeripheral characteristic:characteristic didReceiveValue:value];
    }
    
    if ([bluetoothPeripheral.delegate respondsToSelector:@selector(bluetoothPeripheral:didReceiveValue:)]) {
        [bluetoothPeripheral.delegate bluetoothPeripheral:bluetoothPeripheral didReceiveValue:value];
    }
}

- (void)peripheralDidUpdateName:(CBPeripheral *)peripheral
{
    // Logging
    if (self.debugMode) {
        NSLog(@"^ Peripheral %@ did update name: %@", peripheral, peripheral.name);
    }
}

@end


@implementation DBBluetooth (BeaconExtension)

+ (void)scanForBeaconsNearby
{
    CBUUID *UUID = [CBUUID UUIDWithString:@"FEAA"];
    [self scanForPeripheralsWithSimilarName:nil withServices:@[UUID]];
}

+ (void)scanForEddystonesNearby
{
    // CBUUID *UUID = [CBUUID UUIDWithString:@"FEAA"];
    // [self scanForPeripheralsWithSimilarName:nil withServices:@[UUID]];
    [self scanForPeripheralsWithSimilarName:@"estimote" withServices:nil];
}

+ (void)scanForiBeaconsNearby
{
    CBUUID *UUID = [CBUUID UUIDWithString:@"FEAA"];
    [self scanForPeripheralsWithSimilarName:nil withServices:@[UUID]];
}

@end

