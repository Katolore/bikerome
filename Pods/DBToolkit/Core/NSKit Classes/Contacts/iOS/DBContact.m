//
//  DBContact.m
//  File version: 1.0.0 beta
//  Last modified: 05/31/2017
//
//  Created by Davide Balistreri on 05/31/2017
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

/**
 * IMPORTANT DISCLAIMER:
 * This is a preliminary version for a technology in development.
 * It is not final, and it is subject to change.
 * Newer versions may break compatibility of software implemented using this API.
 */


#import "DBContact.h"

#import "DBStringExtensions.h"

#import <AddressBook/AddressBook.h>


@interface DBContact ()

@property (readonly) ABRecordRef record;

@end


@implementation DBContact

// MARK: - Setup

- (void)setupObject
{
    CFTypeRef rawDataRef = ABPersonCreate();
    self.rawData = CFBridgingRelease(rawDataRef);
}

// MARK: - Private methods

- (ABRecordRef)record
{
    return (__bridge ABRecordRef)self.rawData;
}

- (id)valueForProperty:(ABPropertyID)property
{
    CFTypeRef valueRef = ABRecordCopyValue(self.record, property);
    return CFBridgingRelease(valueRef);
}

- (BOOL)setValue:(id)value forProperty:(ABPropertyID)property
{
    bool success = ABRecordSetValue(self.record, property, (__bridge CFStringRef)value, nil);
    return (success) ? YES : NO;
}

- (NSData *)imageData
{
    CFDataRef imageDataRef = ABPersonCopyImageData(self.record);
    return CFBridgingRelease(imageDataRef);
}

- (BOOL)setImageData:(NSData *)imageData
{
    bool success = ABPersonSetImageData(self.record, (__bridge CFDataRef)imageData, nil);
    return (success) ? YES : NO;
}

// MARK: - Public methods

- (void)setFirstName:(NSString *)firstName
{
    NSString *value = [NSString cleanString:firstName];
    [self setValue:value forProperty:kABPersonFirstNameProperty];
}

- (NSString *)firstName
{
    NSString *value = [self valueForProperty:kABPersonFirstNameProperty];
    return value;
}

- (void)setLastName:(NSString *)lastName
{
    NSString *value = [NSString cleanString:lastName];
    [self setValue:value forProperty:kABPersonLastNameProperty];
}

- (NSString *)lastName
{
    NSString *value = [self valueForProperty:kABPersonLastNameProperty];
    return value;
}

- (void)setOrganizationName:(NSString *)organizationName
{
    NSString *value = [NSString cleanString:organizationName];
    [self setValue:value forProperty:kABPersonOrganizationProperty];
}

- (NSString *)organizationName
{
    NSString *value = [self valueForProperty:kABPersonOrganizationProperty];
    return value;
}

- (void)setPhoneNumber:(NSString *)phoneNumber
{
    CFStringRef phoneNumberRef = (__bridge CFStringRef)phoneNumber;
    ABMutableMultiValueRef phoneNumbersRef = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    ABMultiValueAddValueAndLabel(phoneNumbersRef, phoneNumberRef, kABPersonPhoneMainLabel, nil);
    
    [self setValue:CFBridgingRelease(phoneNumbersRef) forProperty:kABPersonPhoneProperty];
}

- (NSString *)phoneNumber
{
    id phoneNumbers = [self valueForProperty:kABPersonPhoneProperty];
    ABMutableMultiValueRef phoneNumbersRef = (__bridge ABMutableMultiValueRef)phoneNumbers;
    
    CFIndex index = ABMultiValueGetFirstIndexOfValue(phoneNumbersRef, kABPersonPhoneMainLabel);
    CFTypeRef valueRef = ABMultiValueCopyValueAtIndex(phoneNumbersRef, index);
    
    return CFBridgingRelease(valueRef);
}

- (void)setImage:(UIImage *)image
{
    if (image) {
        NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
        [self setImageData:imageData];
    }
}

- (UIImage *)image
{
    NSData *imageData = [self imageData];
    
    if (imageData) {
        UIImage *image = [UIImage imageWithData:imageData];
        return image;
    }
    
    return nil;
}

@end
