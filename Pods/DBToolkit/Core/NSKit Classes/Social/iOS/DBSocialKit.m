//
//  DBSocialKit.m
//  File version: 1.0.0
//  Last modified: 04/21/2016
//
//  Created by Davide Balistreri on 18/04/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBSocialKit.h"
#import "DBRuntimeHandler.h"

@interface DBSocialKit ()

@end


@implementation DBSocialKit

// MARK: - Metodi di classe

+ (instancetype)sharedInstance
{
    @synchronized(self) {
        DBSocialKit *sharedInstance = [DBCentralManager getRisorsa:@"DBSharedSocialKit"];
        
        if (!sharedInstance) {
            sharedInstance = [DBSocialKit new];
            [DBCentralManager setRisorsa:sharedInstance conIdentifier:@"DBSharedSocialKit"];
        }
        
        return sharedInstance;
    }
}

// MARK: - Configurazione da AppDelegate

+ (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
#if __has_include("DBFacebookKit.h")
    if ([DBFacebookKit overridesClassSelector:@selector(application:didFinishLaunchingWithOptions:)]) {
        [DBFacebookKit application:application didFinishLaunchingWithOptions:launchOptions];
    }
#endif
    
#if __has_include("DBTwitterKit.h")
    if ([DBTwitterKit overridesClassSelector:@selector(application:didFinishLaunchingWithOptions:)]) {
        [DBTwitterKit application:application didFinishLaunchingWithOptions:launchOptions];
    }
#endif
    
#if __has_include("DBGoogleKit.h")
    if ([DBGoogleKit overridesClassSelector:@selector(application:didFinishLaunchingWithOptions:)]) {
        [DBGoogleKit application:application didFinishLaunchingWithOptions:launchOptions];
    }
#endif
    
    return YES;
}

+ (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    BOOL result = NO;
    
#if __has_include("DBFacebookKit.h")
    if ([DBFacebookKit overridesClassSelector:@selector(application:openURL:sourceApplication:annotation:)]) {
        result = (result) ? result : [DBFacebookKit application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
    }
#endif
    
#if __has_include("DBTwitterKit.h")
    if ([DBTwitterKit overridesClassSelector:@selector(application:openURL:sourceApplication:annotation:)]) {
        result = (result) ? result : [DBTwitterKit application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
    }
#endif
    
#if __has_include("DBGoogleKit.h")
    if ([DBGoogleKit overridesClassSelector:@selector(application:openURL:sourceApplication:annotation:)]) {
        result = (result) ? result : [DBGoogleKit application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
    }
#endif
    
    return result;
}

+ (void)applicationDidBecomeActive:(UIApplication *)application
{
#if __has_include("DBFacebookKit.h")
    if ([DBFacebookKit overridesClassSelector:@selector(applicationDidBecomeActive:)]) {
        [DBFacebookKit applicationDidBecomeActive:application];
    }
#endif
    
#if __has_include("DBTwitterKit.h")
    if ([DBTwitterKit overridesClassSelector:@selector(applicationDidBecomeActive:)]) {
        [DBTwitterKit applicationDidBecomeActive:application];
    }
#endif
    
#if __has_include("DBGoogleKit.h")
    if ([DBGoogleKit overridesClassSelector:@selector(applicationDidBecomeActive:)]) {
        [DBGoogleKit applicationDidBecomeActive:application];
    }
#endif
}

@end
