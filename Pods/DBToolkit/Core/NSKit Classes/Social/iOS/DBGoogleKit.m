//
//  DBGoogleKit.m
//  File version: 1.1.0
//  Last modified: 04/07/2017
//
//  Created by Davide Balistreri on 18/04/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBGoogleKit.h"
#import "DBApplication.h"


#ifdef DBFRAMEWORK_SUBSPEC_GOOGLEKIT

#import <Google/SignIn.h>

@interface DBGoogleKit () <GIDSignInDelegate>

@property (copy) DBSocialCompletionBlock completionBlock;
@property (nonatomic) UIStatusBarStyle statusBarStyle;

@end

#endif


@implementation DBGoogleKit

#ifdef DBFRAMEWORK_SUBSPEC_GOOGLEKIT

// MARK: - Metodi di classe

+ (instancetype)sharedInstance
{
    @synchronized(self) {
        DBGoogleKit *sharedInstance = [DBCentralManager getRisorsa:@"DBSharedGoogleKit"];
        
        if (!sharedInstance) {
            sharedInstance = [DBGoogleKit new];
            [DBCentralManager setRisorsa:sharedInstance conIdentifier:@"DBSharedGoogleKit"];
        }
        
        return sharedInstance;
    }
}

// MARK: - Configurazione da AppDelegate

+ (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSError *configureError = nil;
    [[GGLContext sharedInstance] configureWithError:&configureError];
    
    DBGoogleKit *sharedInstance = [DBGoogleKit sharedInstance];
    [[GIDSignIn sharedInstance] setDelegate:sharedInstance];
   
    return YES;
}

+ (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    return [[GIDSignIn sharedInstance] handleURL:url sourceApplication:sourceApplication annotation:annotation];
}

#endif

// MARK: - Setup

+ (void)configureWithClientID:(NSString *)clientID
{
#ifdef DBFRAMEWORK_SUBSPEC_GOOGLEKIT
    [[GIDSignIn sharedInstance] setClientID:clientID];
#endif
}

// MARK: - Methods

+ (void)loginWithCompletionBlock:(DBSocialCompletionBlock)completionBlock
{
#ifdef DBFRAMEWORK_SUBSPEC_GOOGLEKIT
    DBGoogleKit *sharedInstance = [DBGoogleKit sharedInstance];
    sharedInstance.completionBlock = completionBlock;
    
    UIViewController *presentedViewController = [DBApplication presentedViewController];
    
    [[GIDSignIn sharedInstance] setUiDelegate:(id)presentedViewController];
    [[GIDSignIn sharedInstance] signIn];
    
    // Aggiorno la grafica automaticamente
    UIApplication *application = [UIApplication sharedApplication];
    sharedInstance.statusBarStyle = application.statusBarStyle;
    [application setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
#endif
}

+ (void)logout
{
#ifdef DBFRAMEWORK_SUBSPEC_GOOGLEKIT
    [[GIDSignIn sharedInstance] signOut];
#endif
}

#ifdef DBFRAMEWORK_SUBSPEC_GOOGLEKIT

// MARK: - Delegate

- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error
{
    // Aggiorno la grafica automaticamente
    UIApplication *application = [UIApplication sharedApplication];
    [application setStatusBarStyle:self.statusBarStyle animated:YES];
    
    // Compongo l'oggetto da restituire in risposta
    // Reference: https://developers.google.com/identity/sign-in/ios/backend-auth
    DBSocialKitResponse *response = [DBSocialKitResponse new];
    response.error = error;
    response.success = YES; // TODO Controllare perché è sempre YES?
    response.token = user.authentication.accessToken;
    
    if (self.completionBlock) {
        self.completionBlock(response);
    }
}

#endif

@end


@implementation DBGoogleKit (Deprecated)

+ (void)loginConCompletionBlock:(DBSocialCompletionBlock)completionBlock
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.2, "please use +[DBGoogleKit loginWithCompletionBlock:]");
    
    [self loginWithCompletionBlock:completionBlock];
}

@end
