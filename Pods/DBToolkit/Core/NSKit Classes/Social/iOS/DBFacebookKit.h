//
//  DBFacebookKit.h
//  File version: 1.1.0
//  Last modified: 04/07/2017
//
//  Created by Davide Balistreri on 18/04/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBSocialKit.h"

/**
 * Modulo creato per semplificare l'interazione con l'SDK di Facebook.
 *
 * <b>Installazione:</b>
 * <p>L'SDK di Facebook necessita dell'inserimento di alcune righe nell'Info.plist, che variano in base alla FacebookApp a cui ci si vuole collegare.</p>
 * <p>Bisogna inoltre richiamare i metodi di configurazione dall'AppDelegate.</p>
 *
 * Please reference and follow the official guide to properly integrate this Facebook module:
 * https://developers.facebook.com/docs/ios/getting-started
 */

@interface DBFacebookKit : DBSocialKit

// MARK: - Methods

+ (void)loginWithCompletionBlock:(nullable DBSocialCompletionBlock)completionBlock;

+ (void)loginWithReadPermissions:(nullable NSArray<NSString *> *)readPermissions completionBlock:(nullable DBSocialCompletionBlock)completionBlock;

+ (void)loginWithPublishPermissions:(nullable NSArray<NSString *> *)publishPermissions completionBlock:(nullable DBSocialCompletionBlock)completionBlock;

/// Use this to logout any possible logged user (i.e. when you want to log in with a brand new user)
+ (void)logout;


+ (nullable NSString *)loggedUserToken;

+ (BOOL)loggedUserHasPermissions:(nullable NSArray<NSString *> *)permissions;

@end


@interface DBFacebookKit (Deprecated)

+ (void)loginConCompletionBlock:(nullable DBSocialCompletionBlock)completionBlock
    DBFRAMEWORK_DEPRECATED(0.7.2, "please use +[DBFacebookKit loginWithCompletionBlock:]");

+ (void)loginConPermessi:(nullable NSArray<NSString *> *)permessi completionBlock:(nullable DBSocialCompletionBlock)completionBlock
    DBFRAMEWORK_DEPRECATED(0.7.2, "please use +[DBFacebookKit loginWithReadPermissions:completionBlock:]");

@end
