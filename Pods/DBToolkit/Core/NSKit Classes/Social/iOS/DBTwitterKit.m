//
//  DBTwitterKit.m
//  File version: 1.1.0
//  Last modified: 04/07/2017
//
//  Created by Davide Balistreri on 18/04/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBTwitterKit.h"
#import "DBApplication.h"

#ifdef DBFRAMEWORK_SUBSPEC_TWITTERKIT

#import <Fabric/Fabric.h>
#import <TwitterCore/TwitterCore.h>
#import <TwitterKit/TwitterKit.h>

#endif


@implementation DBTwitterKit

+ (void)configureWithConsumerKey:(NSString *)consumerKey consumerSecret:(NSString *)consumerSecret
{
#ifdef DBFRAMEWORK_SUBSPEC_TWITTERKIT
    [[Twitter sharedInstance] startWithConsumerKey:consumerKey consumerSecret:consumerSecret];
    [Fabric with:@[TwitterKit]];
#endif
}

+ (void)loginWithCompletionBlock:(DBSocialCompletionBlock)completionBlock
{
#ifdef DBFRAMEWORK_SUBSPEC_TWITTERKIT
    UIViewController *presentedViewController = [DBApplication presentedViewController];
    
    Twitter *sharedInstance = [Twitter sharedInstance];
    
    [sharedInstance logInWithViewController:presentedViewController completion:^(TWTRSession * _Nullable session, NSError * _Nullable error) {
        // Compongo l'oggetto da restituire in risposta
        DBSocialKitResponse *response = [DBSocialKitResponse new];
        response.error = error;
        response.success = (error) ? NO : YES;
        response.token = session.authToken;
        response.secretToken = session.authTokenSecret;
        
        if (completionBlock) {
            completionBlock(response);
        }
    }];
#endif
}

+ (void)logout
{
#ifdef DBFRAMEWORK_SUBSPEC_TWITTERKIT
    Twitter *sharedInstance = [Twitter sharedInstance];
    [sharedInstance logOut];
    // [sharedInstance.sessionStore logOutUserID:sharedInstance.sessionStore.session.userID];
#endif
}

@end


@implementation DBTwitterKit (Deprecated)

+ (void)configuraConConsumerKey:(NSString *)consumerKey consumerSecret:(NSString *)consumerSecret
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.2, "please use +[DBTwitterKit configureWithConsumerKey:consumerSecret:]");
    
    [self configureWithConsumerKey:consumerKey consumerSecret:consumerSecret];
}

+ (void)loginConCompletionBlock:(DBSocialCompletionBlock)completionBlock
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.2, "please use +[DBTwitterKit loginWithCompletionBlock:]");
    
    [self loginWithCompletionBlock:completionBlock];
}

@end
