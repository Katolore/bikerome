//
//  DBTwitterKit.h
//  File version: 1.1.0
//  Last modified: 04/07/2017
//
//  Created by Davide Balistreri on 18/04/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBSocialKit.h"

/**
 * Modulo creato per semplificare l'interazione con l'SDK di Twitter.
 *
 * Please reference and follow the official guide to properly integrate this Twitter module:
 * https://docs.fabric.io/ios/twitter/authentication.html
 */
@interface DBTwitterKit : DBSocialKit

// MARK: - Setup

+ (void)configureWithConsumerKey:(nonnull NSString *)consumerKey consumerSecret:(nonnull NSString *)consumerSecret;


// MARK: - Methods

+ (void)loginWithCompletionBlock:(nullable DBSocialCompletionBlock)completionBlock;

/// Use this to logout any possible logged user (i.e. when you want to log in with a brand new user)
+ (void)logout;

@end


@interface DBTwitterKit (Deprecated)

+ (void)configuraConConsumerKey:(nonnull NSString *)consumerKey consumerSecret:(nonnull NSString *)consumerSecret
    DBFRAMEWORK_DEPRECATED(0.7.2, "please use +[DBTwitterKit configureWithConsumerKey:consumerSecret:]");

+ (void)loginConCompletionBlock:(nullable DBSocialCompletionBlock)completionBlock
    DBFRAMEWORK_DEPRECATED(0.7.2, "please use +[DBTwitterKit loginWithCompletionBlock:]");

@end
