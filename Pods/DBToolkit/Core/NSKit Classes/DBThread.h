//
//  DBThread.h
//  File version: 1.0.2
//  Last modified: 03/13/2016
//
//  Created by Davide Balistreri on 07/23/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

#if DBFRAMEWORK_VERSION_EARLIER_THAN(__DBFRAMEWORK_0_5_1)
    #warning Bisogna adattare il codice DBThread alla nuova versione del Framework.
#endif


@interface DBThread : NSObject

/**
 * Block per eseguire un blocco di codice.
 */
typedef void (^DBThreadBlock)(void);

/**
 * Le varie modalità di esecuzione di un thread.
 */
typedef NS_ENUM(NSUInteger, DBThreadMode) {
    /// Il thread verrà eseguito ogni N secondi.
    DBThreadModeEseguiOgniSecondi,
#if DBFRAMEWORK_TARGET_IOS
    /// Il thread verrà eseguito N volte al secondo.
    DBThreadModeEseguiAlSecondo,
#endif
    /// Il thread verrà eseguito N volte.
    DBThreadModeEseguiConRipetizioni,
    /// Il thread verrà eseguito una sola volta.
    DBThreadModeEseguiUnaVolta
};


/**
 * Il codice di esecuzione del thread.
 * Da implementare nelle classi che estendono da questa.
 */
- (void)mainLoop;


// MARK: - Metodi di classe

/**
 * Istanzia e avvia un thread che verrà eseguito ogni N secondi.
 * @param secondi L'intervallo di tempo tra un'esecuzione e l'altra.
 */
+ (instancetype)avviaThreadConRipetizioniOgniSecondi:(NSTimeInterval)secondi;

#if DBFRAMEWORK_TARGET_IOS
/**
 * Istanzia e avvia un thread che verrà eseguito N volte al secondo.
 * @param ripetizioni Il numero di ripetizioni da eseguire ogni secondo.
 * L'esecuzione può essere ripetuta un massimo di 60 volte al secondo.
 */
+ (instancetype)avviaThreadConRipetizioniAlSecondo:(NSUInteger)ripetizioni;
#endif

/**
 * Istanzia e avvia un thread che verrà eseguito N volte.
 * @param ripetizioni Il numero di ripetizioni da eseguire.
 */
+ (instancetype)avviaThreadConRipetizioni:(NSUInteger)ripetizioni;

/**
 * Istanzia e avvia un thread che verrà eseguito una sola volta.
 */
+ (void)avviaThreadUnaVolta;

/**
 * Interrompe l'esecuzione di un thread.
 * @param thread Il thread da interrompere.
 */
+ (void)interrompiThread:(DBThread *)thread;


// MARK: - Metodi d'istanza

/// Boolean che indica se il thread è attualmente in esecuzione.
@property (nonatomic, readonly) BOOL isThreadInEsecuzione;

/// Boolean che indica se il thread è stato interrotto prima della fine.
@property (nonatomic, readonly) BOOL isThreadInterrotto;

/// La modalità di esecuzione impostata.
@property (nonatomic, readonly) DBThreadMode dbThreadMode;

/// L'intervallo di tempo tra un'esecuzione e l'altra.
@property (nonatomic, readonly) NSTimeInterval dbThreadIntervalloRipetizioni;

/// Il numero di ripetizioni da eseguire.
@property (nonatomic, readonly) NSUInteger dbThreadNumeroRipetizioni;

/**
 * Imposta il thread per essere eseguito ogni N secondi.
 * @param secondi L'intervallo di tempo tra un'esecuzione e l'altra.
 */
- (void)impostaEsecuzioneConRipetizioniOgniSecondi:(NSTimeInterval)secondi;

#if DBFRAMEWORK_TARGET_IOS
/**
 * Imposta il thread per essere eseguito N volte al secondo.
 * @param ripetizioni Il numero di ripetizioni da eseguire ogni secondo.
 * L'esecuzione può essere ripetuta un massimo di 60 volte al secondo.
 */
- (void)impostaEsecuzioneConRipetizioniAlSecondo:(NSUInteger)ripetizioni;
#endif

/**
 * Imposta il thread per essere eseguito N volte.
 * @param ripetizioni Il numero di ripetizioni da eseguire.
 */
- (void)impostaEsecuzioneConRipetizioni:(NSUInteger)ripetizioni;

/**
 * Imposta il thread per essere eseguito una sola volta.
 */
- (void)impostaEsecuzioneUnaVolta;

/**
 * Avvia il thread con la modalità di esecuzione impostata.
 */
- (void)avvia;

/**
 * Interrompe l'esecuzione del thread.
 */
- (void)interrompi;


// MARK: - Metodi per funzioni esterne

/**
 * Esegue un metodo sul thread principale.
 * @param target   L'istanza su cui eseguire il metodo.
 * @param selector Il metodo da eseguire.
 * @param object   L'oggetto da passare come argomento.
 */
+ (void)eseguiSelectorConTarget:(id)target selector:(SEL)selector conObject:(id)object;

/**
 * Esegue un metodo sul thread principale, dopo aver atteso N secondi.
 * @param target   L'istanza su cui eseguire il metodo.
 * @param ritardo  Il tempo di ritardo prima dell'esecuzione.
 *                 Se il tempo di ritardo è zero o minore di zero,
 *                 il blocco di codice sarà eseguito immediatamente.
 * @param selector Il metodo da eseguire.
 * @param object   L'oggetto da passare come argomento.
 */
+ (void)eseguiSelectorConRitardo:(NSTimeInterval)ritardo target:(id)target selector:(SEL)selector conObject:(id)object;

/**
 * Esegue un blocco di codice sul thread principale in modalità asincrona.
 * @param block Il blocco di codice da eseguire.
 */
+ (void)eseguiBlock:(DBThreadBlock)block;

/**
 * Esegue un blocco di codice sul thread principale in modalità sincrona.
 * @param block Il blocco di codice da eseguire.
 */
+ (void)eseguiBlockSync:(DBThreadBlock)block;

/**
 * Esegue un blocco di codice sul thread principale, dopo aver atteso N secondi.
 * @param ritardo Il tempo di ritardo prima dell'esecuzione.
 *                Se il tempo di ritardo è zero o minore di zero,
 *                il blocco di codice sarà eseguito immediatamente.
 * @param block   Il blocco di codice da eseguire.
 */
+ (void)eseguiBlockConRitardo:(NSTimeInterval)ritardo block:(DBThreadBlock)block;

/**
 * Esegue un metodo su un altro thread.
 * @param target   L'istanza su cui eseguire il metodo.
 * @param selector Il metodo da eseguire.
 * @param object   L'oggetto da passare come argomento.
 */
+ (void)eseguiSelectorSuThreadInBackgroundConTarget:(id)target selector:(SEL)selector conObject:(id)object;

/**
 * Esegue un blocco di codice su un altro thread.
 * @param block Il blocco di codice da eseguire.
 */
+ (void)eseguiBlockSuThreadInBackground:(DBThreadBlock)block;

/**
 * Esegue un blocco di codice su un altro thread, dopo aver atteso N secondi.
 * @param ritardo Il tempo di ritardo prima dell'esecuzione.
 *                Se il tempo di ritardo è zero o minore di zero,
 *                il blocco di codice sarà eseguito immediatamente.
 * @param block   Il blocco di codice da eseguire.
 */
+ (void)eseguiBlockSuThreadInBackgroundConRitardo:(NSTimeInterval)ritardo block:(DBThreadBlock)block;

/**
 * Esegue un metodo sul thread principale.
 */
+ (void)eseguiSelectorSulMainThread:(SEL)selector conObject:(id)object
    DBFRAMEWORK_FORBIDDEN(0.7.0, "utilizza +[DBThread eseguiSelectorConTarget:selector:conObject:]");

/**
 * Esegue un blocco di codice sul thread principale.
 */
+ (void)eseguiBlockSulMainThread:(DBThreadBlock)block
    DBFRAMEWORK_DEPRECATED(0.5.1, "utilizza +[DBThread eseguiBlock:]");

/**
 * Esegue un blocco di codice sul thread principale, dopo aver atteso N secondi.
 */
+ (void)eseguiBlockSulMainThreadConRitardo:(NSTimeInterval)ritardo block:(DBThreadBlock)block
    DBFRAMEWORK_DEPRECATED(0.5.1, "utilizza +[DBThread eseguiBlockConRitardo:block:]");

@end


// MARK: - NSObject

@interface NSObject (DBThread)

/**
 * Esegue un blocco di codice sul thread principale, dopo aver atteso N secondi.
 * @param delay Il tempo di ritardo prima dell'esecuzione.
 *              Se il tempo di ritardo è zero o minore di zero,
 *              il blocco di codice sarà eseguito immediatamente.
 * @param block Il blocco di codice da eseguire.
 */
+ (void)performBlockAfterDelay:(NSTimeInterval)delay block:(DBThreadBlock)block;

/**
 * Esegue un blocco di codice sul thread principale, dopo aver atteso N secondi.
 * @param delay Il tempo di ritardo prima dell'esecuzione.
 *              Se il tempo di ritardo è zero o minore di zero,
 *              il blocco di codice sarà eseguito immediatamente.
 * @param block Il blocco di codice da eseguire.
 */
- (void)performBlockAfterDelay:(NSTimeInterval)delay block:(DBThreadBlock)block;

@end
