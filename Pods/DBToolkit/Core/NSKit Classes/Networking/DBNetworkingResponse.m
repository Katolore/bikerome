//
//  DBNetworkingResponse.m
//  File version: 1.0.0
//  Last modified: 04/23/2016
//
//  Created by Davide Balistreri on 04/01/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBNetworkingResponse.h"
#import "DBObjectExtensions.h"
#import "DBStringExtensions.h"

@implementation DBNetworkingResponse

/**
 * Risposta generica quando la richiesta web è andata a buon fine.
 */
+ (DBNetworkingResponse *)successResponse
{
    DBNetworkingResponse *response = [DBNetworkingResponse new];
    response.statusCode = 0;
    response.success = YES;
    return response;
}

/**
 * Risposta generica quando la richiesta web è fallita.
 */
+ (DBNetworkingResponse *)failureResponse
{
    return [DBNetworkingResponse failureResponseConStatusCode:-1 conMessaggioErrore:nil];
}

/**
 * Risposta generica quando la richiesta web è fallita.
 */
+ (DBNetworkingResponse *)failureResponseConStatusCode:(NSInteger)statusCode conMessaggioErrore:(NSString *)messaggioErrore
{
    DBNetworkingResponse *response = [DBNetworkingResponse new];
    response.statusCode = -1;
    
    NSDictionary *userInfo = nil;
    
    if ([NSString isNotEmpty:messaggioErrore]) {
        NSString *safeErrorDescription = [NSString safeString:messaggioErrore];
        userInfo = @{NSLocalizedDescriptionKey:safeErrorDescription};
    }
    
    response.error = [NSError errorWithDomain:@"DBNetworkingResponseError" code:statusCode userInfo:userInfo];
    
    return response;
}

@end


@implementation DBNetworkingResponse (Caching)

- (NSString *)toSerializedString
{
    if ([NSObject isNotEmpty:self.responseObject]) {
        NSData *data = [NSJSONSerialization dataWithJSONObject:self.responseObject options:0 error:nil];
        return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    }
    else {
        return nil;
    }
}

+ (DBNetworkingResponse *)fromSerializedString:(NSString *)string
{
    if (string) {
        NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
        id responseObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        
        DBNetworkingResponse *response = [DBNetworkingResponse new];
        response.success = YES;
        response.responseObject = responseObject;
        return response;
    }
    else {
        return nil;
    }
}

@end
