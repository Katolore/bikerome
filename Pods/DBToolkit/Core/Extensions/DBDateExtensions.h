//
//  DBDateExtensions.h
//  File version: 1.0.1
//  Last modified: 04/14/2016
//
//  Created by Davide Balistreri on 03/01/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

#if DBFRAMEWORK_VERSION_EARLIER_THAN(__DBFRAMEWORK_0_6_4)
    #warning Bisogna adattare il codice DBDateExtensions alla nuova versione del Framework. \
    Il metodo -[NSDate giorno] ora si chiama -[NSDate dataGiorno], \
    il metodo con il vecchio nome ora restituisce il numero del giorno del mese.
#endif


// MARK: - NSDateFormatter

@interface NSDateFormatter (DBDateExtensions)

NS_ASSUME_NONNULL_BEGIN

+ (NSDateFormatter *)dateFormatterConFormato:(nullable NSString *)formato;
+ (NSString *)stringaConData:(nullable NSDate *)data conFormato:(nullable NSString *)formato;
+ (nullable NSDate *)dataConStringa:(nullable NSString *)stringa conFormato:(nullable NSString *)formato;

+ (NSString *)stringaFormatoStandardData:(NSDate *)data;
+ (NSString *)stringaFormatoStandardOra:(NSDate *)data;
+ (NSString *)stringaFormatoStandardDataOra:(NSDate *)data;

NS_ASSUME_NONNULL_END

@end


// MARK: - NSDate

@interface NSDate (DBDateExtensions)

/// 1 = Sunday, 2 = Monday
@property (readonly) NSInteger weekday;

/// 7 = Sunday, 1 = Monday
@property (readonly) NSInteger italianWeekday;

@property (readonly) NSInteger secondo;
@property (readonly) NSInteger minuto;
@property (readonly) NSInteger ora;
@property (readonly) NSInteger giorno;
@property (readonly) NSInteger mese;
@property (readonly) NSInteger anno;

+ (NSDate *)dataConAnno:(NSInteger)anno mese:(NSInteger)mese giorno:(NSInteger)giorno ore:(NSInteger)ore minuti:(NSInteger)minuti secondi:(NSInteger)secondi;
+ (NSDate *)dataConAnno:(NSInteger)anno mese:(NSInteger)mese giorno:(NSInteger)giorno;

+ (NSTimeInterval)timeIntervalConGiorni:(CGFloat)giorni;

- (NSDate *)aggiungiAnni:(NSInteger)anni;
- (NSDate *)aggiungiMese:(NSInteger)mesi;
- (NSDate *)aggiungiGiorni:(NSInteger)giorni;
- (NSDate *)aggiungiOre:(NSInteger)ore;
- (NSDate *)aggiungiMinuti:(NSInteger)minuti;
- (NSDate *)aggiungiSecondi:(NSInteger)secondi;

- (NSDate *)impostaAnno:(NSInteger)anno;
- (NSDate *)impostaMese:(NSInteger)mese;
- (NSDate *)impostaGiorno:(NSInteger)giorno;
- (NSDate *)impostaOre:(NSInteger)ore;
- (NSDate *)impostaMinuti:(NSInteger)minuti;
- (NSDate *)impostaSecondi:(NSInteger)secondi;

+ (NSDate *)adesso;
+ (NSDate *)oggi;
+ (NSDate *)ieri;
+ (NSDate *)domani;

- (NSDate *)dataGiorno;
- (NSDate *)dataFineGiorno;
- (NSDate *)firstDayOfMonth;

- (BOOL)isGiorno:(NSDate *)giorno;
- (BOOL)isPrimaDiGiorno:(NSDate *)giorno;
- (BOOL)isDopoDiGiorno:(NSDate *)giorno;

- (BOOL)isPrimaDi:(NSDate *)data;
- (BOOL)isDopoDi:(NSDate *)data;

- (BOOL)isOggi;
- (BOOL)isPrimaDiOggi;
- (BOOL)isDopoDiOggi;
- (BOOL)isPrimaDiAdesso;
- (BOOL)isDopoDiAdesso;

- (NSInteger)anniPassatiDallaData:(NSDate *)data;
- (NSInteger)anniMancantiAllaData:(NSDate *)data;
- (NSInteger)anniPassati;
- (NSInteger)anniMancanti;

- (CGFloat)giorniPassatiDallaData:(NSDate *)data;
- (CGFloat)giorniMancantiAllaData:(NSDate *)data;
- (CGFloat)giorniPassati;
- (CGFloat)giorniMancanti;

+ (NSTimeInterval)secondiPassatiDallaData:(NSDate *)data;
+ (NSTimeInterval)secondiMancantiAllaData:(NSDate *)data;
- (NSTimeInterval)secondiPassatiDallaData:(NSDate *)data;
- (NSTimeInterval)secondiMancantiAllaData:(NSDate *)data;
- (NSTimeInterval)secondiPassati;
- (NSTimeInterval)secondiMancanti;

@end
