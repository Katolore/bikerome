//
//  DBImageExtensions.m
//  File version: 1.0.0
//  Last modified: 03/27/2016
//
//  Created by Davide Balistreri on 03/27/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBImageExtensions.h"
#import <AVFoundation/AVFoundation.h>
#import "DBObjectExtensions.h"
#import "DBStringExtensions.h"
#import "DBApplication.h"
#import "DBGeometry.h"

@implementation UIImage (DBImageExtensions)

+ (UIImage *)unisciImmagineSopra:(UIImage *)immagineSopra conImmagineSotto:(UIImage *)immagineSotto conTrasparenza:(BOOL)trasparenza
{
    if (!immagineSotto && !immagineSopra) return nil;
    if (!immagineSotto) return immagineSopra;
    if (!immagineSopra) return immagineSotto;
    
    CGFloat width  = immagineSotto.size.width;
    CGFloat height = immagineSotto.size.height;
    
    CGRect frame = CGRectZero;
    frame.size = CGSizeMake(width, height);
    
    UIImage *immagine = nil;
    
    @autoreleasepool {
#if DBFRAMEWORK_TARGET_IOS
        CGFloat scale = MAX([[UIScreen mainScreen] scale], 2.0);
        UIGraphicsBeginImageContextWithOptions(frame.size, !trasparenza, scale);
#else
        immagine = [[NSImage alloc] initWithSize:frame.size];
        [immagine lockFocus];
#endif
        
        // Se non c'è trasparenza, coloro di bianco il background
        if (!trasparenza) {
            [[UIColor whiteColor] setFill];
            [[UIBezierPath bezierPathWithRect:CGRectMake(0, 0, width, height)] fill];
        }
        
        [immagineSotto drawInRect:frame];
        [immagineSopra drawInRect:frame blendMode:kCGBlendModeNormal alpha:1.0];
        
#if DBFRAMEWORK_TARGET_IOS
        immagine = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
#else
        [immagine unlockFocus];
#endif
    }
    
    return immagine;
}

+ (UIImage *)safeImageNamed:(NSString *)name
{
    NSString *safeName = [NSString safeString:name];
    UIImage *image = [UIImage imageNamed:safeName];
    return (image) ? image : [UIImage image];
}

#if DBFRAMEWORK_TARGET_IOS

+ (UIImage *)image
{
    return [UIImage immagineConColore:[UIColor clearColor] dimensioni:CGSizeMake(1.0, 1.0)];
}

- (UIImage *)immagineRidimensionataConRisoluzione:(CGFloat)risoluzione
{
    @autoreleasepool {
        CGFloat width = self.size.width;
        CGFloat height = self.size.height;
        CGRect  bounds = CGRectMake(0, 0, width, height);
        
        // Se è già compresa nella risoluzione minima passata
        if (width <= risoluzione && height <= risoluzione) {
            return self;
        }
        
        CGFloat ratio = width / height;
        
        if (ratio > 1.0) {
            bounds.size.width = risoluzione;
            bounds.size.height = bounds.size.width / ratio;
        } else {
            bounds.size.height = risoluzione;
            bounds.size.width = bounds.size.height * ratio;
        }
        
        UIGraphicsBeginImageContextWithOptions(bounds.size, NO, 0.0);
        [self drawInRect:CGRectMake(0.0, 0.0, bounds.size.width, bounds.size.height)];
        
        UIImage *nuovaImmagine = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return nuovaImmagine;
    }
}

- (UIImage *)immagineRidimensionataConRisoluzioneMinima:(CGFloat)risoluzione
{
    @autoreleasepool {
        CGFloat width  = self.size.width;
        CGFloat height = self.size.height;
        CGRect  bounds = CGRectMake(0, 0, width, height);
        
        // Se è già compresa nella risoluzione minima passata
        if (width > risoluzione && height > risoluzione) {
            return self;
        }
        
        CGFloat ratio = width / height;
        
        if (ratio < 1.0) {
            bounds.size.width = risoluzione;
            bounds.size.height = bounds.size.width / ratio;
        } else {
            bounds.size.height = risoluzione;
            bounds.size.width = bounds.size.height * ratio;
        }
        
        UIGraphicsBeginImageContextWithOptions(bounds.size, NO, 0.0);
        [self drawInRect:CGRectMake(0.0, 0.0, bounds.size.width, bounds.size.height)];
        
        UIImage *nuovaImmagine = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return nuovaImmagine;
    }
}

- (UIImage *)immagineRitagliataConRect:(CGRect)rect
{
    @autoreleasepool {
        UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0.0);
        [self drawInRect:CGRectMake(-rect.origin.x, -rect.origin.y, self.size.width, self.size.height)];
        
        UIImage *nuovaImmagine = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return nuovaImmagine;
    }
}

- (UIImage *)immagineRuotataDiGradi:(CGFloat)gradi
{
    @autoreleasepool {
        // Calculate the size of the rotated view's containing box for our drawing space
        UIView *rotatedViewBox = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.size.width, self.size.height)];
        CGAffineTransform transform = CGAffineTransformMakeRotation(CGFloatGradiInRadianti(gradi));
        rotatedViewBox.transform = transform;
        CGSize rotatedSize = rotatedViewBox.frame.size;
        
        // Create the bitmap context
        UIGraphicsBeginImageContext(rotatedSize);
        CGContextRef bitmap = UIGraphicsGetCurrentContext();
        
        // Move the origin to the middle of the image so we will rotate and scale around the center.
        CGContextTranslateCTM(bitmap, rotatedSize.width / 2.0, rotatedSize.height / 2.0);
        
        // Rotate the image context
        CGContextRotateCTM(bitmap, CGFloatGradiInRadianti(gradi));
        
        // Now, draw the rotated/scaled image into the context
        CGContextScaleCTM(bitmap, 1.0, -1.0);
        CGContextDrawImage(bitmap, CGRectMake(-self.size.width / 2.0, -self.size.height / 2.0, self.size.width, self.size.height), [self CGImage]);
        
        UIImage *nuovaImmagine = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return nuovaImmagine;
    }
}

- (UIImage *)immagineRicolorataComeMaschera:(UIColor *)colore
{
    @autoreleasepool {
        UIGraphicsBeginImageContextWithOptions(self.size, NO, self.scale);
        CGContextRef context = UIGraphicsGetCurrentContext();
        [colore setFill];
        
        CGContextTranslateCTM(context, 0, self.size.height);
        CGContextScaleCTM(context, 1.0, -1.0);
        CGContextClipToMask(context, CGRectMake(0, 0, self.size.width, self.size.height), [self CGImage]);
        CGContextFillRect(context, CGRectMake(0, 0, self.size.width, self.size.height));
        
        UIImage *nuovaImmagine = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return nuovaImmagine;
    }
}

+ (UIImage *)iconaTabBarConNome:(NSString *)nome
{
    return [self iconaTabBarConNome:nome conFullscreen:NO conPadding:0.0];
}

+ (UIImage *)iconaTabBarConNome:(NSString *)nome conFullscreen:(BOOL)fullscreen
{
    return [self iconaTabBarConNome:nome conFullscreen:fullscreen conPadding:0.0];
}

+ (UIImage *)iconaTabBarConNome:(NSString *)nome conFullscreen:(BOOL)fullscreen conPadding:(CGFloat)padding
{
    UIImage *image = [UIImage imageNamed:nome];
    return [self iconaTabBar:image conFullscreen:fullscreen conPadding:padding];
}

+ (UIImage *)iconaTabBar:(UIImage *)image conFullscreen:(BOOL)fullscreen conPadding:(CGFloat)padding
{
    // Prendo l'altezza della TabBar in base al dispositivo e alla versione del sistema operativo
    CGFloat altezza = [UITabBarController new].tabBar.bounds.size.height;
    CGFloat separatore = 2.0;
    CGFloat margine = separatore + (altezza * 0.2);
    
    // Calcolo il frame della vista
    CGRect frameView = CGRectMake(0.0, 0.0, altezza, altezza + margine);
    CGRect frameImageView = CGRectMake(0.0, margine, altezza, altezza);
    
    if (!fullscreen) {
        // Se non è fullscreen tolgo dall'altezza totale la parte del testo sotto l'icona
        CGFloat altezzaTesto = altezza * 0.27;
        CGFloat margineTesto = altezza * 0.06;
        CGFloat margineTestox2 = margineTesto * 2.0;
        frameImageView = CGRectMake(0.0, margine + margineTesto, altezza, altezza - altezzaTesto - margineTestox2);
    }
    
    // Aggiungo il padding
    frameImageView.origin.x += padding;
    frameImageView.origin.y += padding;
    frameImageView.size.width  -= (padding * 2.0);
    frameImageView.size.height -= (padding * 2.0);
    
    // Preparo la View con l'immagine delle corrette dimensioni
    UIView *view = [[UIView alloc] initWithFrame:frameView];
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:frameImageView];
    imageView.image = image;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [view addSubview:imageView];
    
    // Debug:
    // imageView.backgroundColor = [UIColor blueColor];
    // view.backgroundColor = [UIColor redColor];
    
    // Catturo l'immagine della View
    UIImage *icona;
    
    @autoreleasepool {
        CGFloat scale = MAX([[UIScreen mainScreen] scale], 2.0);
        
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, NO, scale);
        CGContextRef context = UIGraphicsGetCurrentContext();
        [view.layer renderInContext:context];
        icona = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    
    return [icona imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
}

+ (UIImage *)creaImmagineConColore:(UIColor *)colore conDimensioni:(CGSize)dimensioni
{
    DBFRAMEWORK_DEPRECATED_LOG(0.5.2, "utilizza il metodo +[UIImage immagineConColore:dimensioni:]");
    return [self immagineConColore:colore dimensioni:dimensioni];
}

+ (UIImage *)creaImmagineConColore:(UIColor *)colore conImmagineMaschera:(UIImage *)immagineMaschera
{
    DBFRAMEWORK_DEPRECATED_LOG(0.5.2, "utilizza il metodo +[UIImage immagineConColore:immagineMaschera:]");
    return [self immagineConColore:colore immagineMaschera:immagineMaschera];
}

+ (UIImage *)immagineConColore:(UIColor *)colore dimensioni:(CGSize)dimensioni
{
    @autoreleasepool {
        CGRect frame = CGRectMake(0.0, 0.0, dimensioni.width, dimensioni.height);
        UIGraphicsBeginImageContext(frame.size);
        CGContextRef context = UIGraphicsGetCurrentContext();
        
        CGContextSetFillColorWithColor(context, [colore CGColor]);
        CGContextFillRect(context, frame);
        
        UIImage *immagine = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return immagine;
    }
}

+ (UIImage *)immagineConColore:(UIColor *)colore immagineMaschera:(UIImage *)immagineMaschera
{
    return [immagineMaschera immagineRicolorataComeMaschera:colore];
}

+ (UIImage *)immagineConColori:(NSArray<UIColor *> *)colori direzione:(DBLayoutDirection)direzione dimensioni:(CGSize)dimensioni
{
    CGPoint puntoPartenza = CGPointZero;
    CGPoint puntoArrivo = CGPointZero;
    
    // Direzione
    switch (direzione) {
        case DBViewLayoutDirectionTop:
        case DBViewLayoutDirectionTopLeft:
        case DBViewLayoutDirectionTopRight:
            puntoPartenza = CGPointMake(0.0, 1.0);
            puntoArrivo   = CGPointMake(0.0, 0.0);
            break;
        case DBViewLayoutDirectionBottom:
        case DBViewLayoutDirectionBottomLeft:
        case DBViewLayoutDirectionBottomRight:
            puntoPartenza = CGPointMake(0.0, 0.0);
            puntoArrivo   = CGPointMake(0.0, 1.0);
            break;
        case DBViewLayoutDirectionLeft:
            puntoPartenza = CGPointMake(1.0, 0.0);
            puntoArrivo   = CGPointMake(0.0, 0.0);
            break;
        case DBViewLayoutDirectionRight:
            puntoPartenza = CGPointMake(0.0, 0.0);
            puntoArrivo   = CGPointMake(1.0, 0.0);
            break;
    }
    
    puntoPartenza.x *= dimensioni.width;
    puntoPartenza.y *= dimensioni.height;
    
    puntoArrivo.x *= dimensioni.width;
    puntoArrivo.y *= dimensioni.height;
    
    return [self immagineConColori:colori puntoPartenza:puntoPartenza puntoArrivo:puntoArrivo dimensioni:dimensioni];
}

+ (UIImage *)immagineConColori:(NSArray<UIColor *> *)colori puntoPartenza:(CGPoint)puntoPartenza puntoArrivo:(CGPoint)puntoArrivo dimensioni:(CGSize)dimensioni
{
    @autoreleasepool {
        // Dimensione
        CGRect frame = CGRectMake(0.0, 0.0, dimensioni.width, dimensioni.height);
        
        // Colori
        NSMutableArray *coloriCG = [NSMutableArray array];
        for (UIColor *colore in colori) {
            [coloriCG addObject:(__bridge id)colore.CGColor];
        }
        
        // Disegno
        // UIGraphicsBeginImageContextWithOptions(frame.size, NO, 0.0);
        UIGraphicsBeginImageContext(frame.size);
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGColorSpaceRef space = CGColorSpaceCreateDeviceRGB();
        
        CGGradientRef gradient = CGGradientCreateWithColors(space, (__bridge CFArrayRef)coloriCG, NULL);
        CGContextDrawLinearGradient(context, gradient, puntoPartenza, puntoArrivo, kCGGradientDrawsBeforeStartLocation|kCGGradientDrawsAfterEndLocation);
        
        UIImage *immagine = UIGraphicsGetImageFromCurrentImageContext();
        
        CGGradientRelease(gradient);
        CGColorSpaceRelease(space);
        UIGraphicsEndImageContext();
        
        return immagine;
    }
}

+ (UIImage *)catturaFrameVideoConURL:(NSURL *)URL
{
    return [self catturaFrameVideoConURL:URL secondi:5];
}

+ (UIImage *)catturaFrameVideoConURL:(NSURL *)URL secondi:(NSTimeInterval)secondi
{
    // Check
    if (!URL) return nil;
    
    @autoreleasepool {
        AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:URL options:nil];
        AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
        generator.appliesPreferredTrackTransform = YES;
        
        UIWindow *mainWindow = [DBApplication mainWindow];
        generator.maximumSize = mainWindow.bounds.size;
        
        CMTime thumbTime = CMTimeMakeWithSeconds(secondi, 1);
        
        CGImageRef imageRef = [generator copyCGImageAtTime:thumbTime actualTime:nil error:nil];
        UIImage *frame = [UIImage imageWithCGImage:imageRef];
        
        CGImageRelease(imageRef);
        generator = nil;
        asset = nil;
        
        return frame;
    }
}

+ (UIImage *)catturaSchermata:(UIView *)schermata
{
    return [schermata catturaSchermata];
}

+ (UIImage *)immagineSchermata:(UIView *)schermata
{
    return [schermata catturaSchermata];
}

#else

+ (UIImage *)immagineSpecchiataOrizzontalmente:(UIImage *)immagine
{
    return [immagine immagineSpecchiataOrizzontalmente];
}

- (UIImage *)immagineSpecchiataOrizzontalmente
{
    UIImage *existingImage = self;
    NSSize existingSize = [existingImage size];
    NSSize newSize = NSMakeSize(existingSize.width, existingSize.height);
    UIImage *flippedImage = [[UIImage alloc] initWithSize:newSize];
    
    [flippedImage lockFocus];
    [[NSGraphicsContext currentContext] setImageInterpolation:NSImageInterpolationHigh];
    
    NSAffineTransform *transform = [NSAffineTransform transform];
    [transform translateXBy:existingSize.width yBy:0];
    [transform scaleXBy:-1 yBy:1];
    [transform concat];
    
    [existingImage drawAtPoint:NSZeroPoint fromRect:NSMakeRect(0, 0, newSize.width, newSize.height) operation:NSCompositeSourceOver fraction:1.0];
    
    [flippedImage unlockFocus];
    
    return flippedImage;
}

#endif

@end
