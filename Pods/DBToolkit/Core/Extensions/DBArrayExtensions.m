//
//  DBArrayExtensions.m
//  File version: 1.0.0
//  Last modified: 03/01/2016
//
//  Created by Davide Balistreri on 03/01/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBArrayExtensions.h"

// MARK: - NSArray

@implementation NSArray (DBArrayExtensions)

+ (instancetype)safeArrayWithObject:(id)object
{
    if (object) {
        return [[super class] arrayWithObject:object];
    }
    
    return [[self class] new];
}

+ (instancetype)safeArrayWithArray:(NSArray *)array
{
    if (array) {
        return [[super class] arrayWithArray:array];
    }
    
    return [[self class] new];
}

- (BOOL)objectAtIndexExists:(NSInteger)index
{
    if (index < 0 || self.count == 0) return NO;
    if (index >= self.count) return NO;
    return YES;
}

- (id)safeObjectAtIndex:(NSUInteger)index
{
    if ([self objectAtIndexExists:index] == NO) {
        return nil;
    }
    
    @try {
        id value = [self objectAtIndex:index];
        if (value && value != [NSNull null]) return value;
        return nil;
    }
    @catch (NSException *exception) {
        return nil;
    }
}

+ (instancetype)arrayWithArrays:(NSArray *)primoArray, ... NS_REQUIRES_NIL_TERMINATION
{
    NSMutableArray *array = [NSMutableArray array];
    
    va_list args;
    va_start(args, primoArray);
    
    NSArray *arrayCorrente = primoArray;
    while (arrayCorrente) {
        [array addObjectsFromArray:arrayCorrente];
        arrayCorrente = va_arg(args, id);
    }
    
    va_end(args);
    
    return array;
}

- (NSArray *)arrayByRemovingObject:(id)anObject
{
    NSMutableArray *array = [self mutableCopy];
    [array removeObject:anObject];
    return [NSArray arrayWithArray:array];
}

@end


// MARK: - NSMutableArray

@implementation NSMutableArray (DBArrayExtensions)

/**
 * Istanzia un mutable array contenente N oggetti NSNull.
 */
+ (instancetype)arrayConDimensione:(NSUInteger)dimensione
{
    NSMutableArray *array = [NSMutableArray array];
    for (NSUInteger counter = 0; counter < dimensione; counter++) {
        [array addObject:[NSNull null]];
    }
    return array;
}

- (void)safeAddObject:(id)object
{
    if (!object) return;
    
    @try {
        [self addObject:object];
    } @catch (NSException *exception) {}
}

- (void)addBool:(BOOL)value
{
    @try {
        [self addObject:[NSNumber numberWithBool:value]];
    } @catch (NSException *exception) {}
}

- (void)addInt:(int)value
{
    @try {
        [self addObject:[NSNumber numberWithInt:value]];
    } @catch (NSException *exception) {}
}

- (void)addInteger:(NSInteger)value
{
    @try {
        [self addObject:[NSNumber numberWithInteger:value]];
    } @catch (NSException *exception) {}
}

- (void)addFloat:(float)value
{
    @try {
        [self addObject:[NSNumber numberWithFloat:value]];
    } @catch (NSException *exception) {}
}

- (void)addDouble:(double)value
{
    @try {
        [self addObject:[NSNumber numberWithDouble:value]];
    } @catch (NSException *exception) {}
}

- (void)removeFirstObject
{
    @try {
        [self removeObjectAtIndex:0];
    } @catch (NSException *exception) {}
}

- (void)moveObjectFromIndex:(NSUInteger)from toIndex:(NSUInteger)to
{
    // Controllo se gli elementi sono presenti nell'array
    if (self.count <= from) return;
    if (self.count <= to) return;
    
    id oggetto = [self safeObjectAtIndex:from];
    if (oggetto) {
        [self removeObjectAtIndex:from];
        [self insertObject:oggetto atIndex:to];
    }
}

- (void)toggleObject:(id)object
{
    if ([self containsObject:object]) {
        [self removeObject:object];
    }
    else {
        [self safeAddObject:object];
    }
}

- (void)removeObjectsNotInArray:(NSArray *)otherArray
{
    // Reverse enumeration to simplify for-cycle indexes
    NSInteger lastIndex = self.count - 1;
    
    for (NSInteger index = lastIndex; index >= 0; index--) {
        id object = [self objectAtIndex:index];
        
        if ([otherArray containsObject:object] == NO) {
            // This object is not present in otherArray
            [self removeObject:object];
        }
    }
}

@end
