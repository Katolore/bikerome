//
//  DBDictionaryExtensions.h
//  File version: 1.0.1
//  Last modified: 03/26/2016
//
//  Created by Davide Balistreri on 03/01/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

@interface NSDictionary (DBDictionaryExtensions)

NS_ASSUME_NONNULL_BEGIN

/// Metodo che restituisce un dizionario vuoto se quello passato non è valido.
+ (NSDictionary *)safeDictionary:(nullable NSDictionary *)dictionary;

- (nullable id)safeObjectForKey:(nullable NSString *)key;
- (nullable id)safeValueForKey:(nullable NSString *)key;

- (NSDictionary *)safeDictionaryForKey:(nullable NSString *)key;
- (NSArray *)safeArrayForKey:(nullable NSString *)key;

- (NSString *)safeStringForKey:(nullable NSString *)key;
- (NSNumber *)safeNumberForKey:(nullable NSString *)key;

- (BOOL)safeBoolForKey:(nullable NSString *)key;
- (int)safeIntForKey:(nullable NSString *)key;
- (NSInteger)safeIntegerForKey:(nullable NSString *)key;
- (float)safeFloatForKey:(nullable NSString *)key;
- (double)safeDoubleForKey:(nullable NSString *)key;

- (void)safeSetObject:(nullable id)object forKey:(nullable NSString *)key;
- (void)safeSetValue:(nullable id)value forKey:(nullable NSString *)key;
- (void)setBool:(BOOL)value forKey:(nullable NSString *)key;
- (void)setInt:(int)value forKey:(nullable NSString *)key;
- (void)setInteger:(NSInteger)value forKey:(nullable NSString *)key;
- (void)setFloat:(float)value forKey:(nullable NSString *)key;
- (void)setDouble:(double)value forKey:(nullable NSString *)key;

NS_ASSUME_NONNULL_END

@end
