//
//  DBViewExtensions.h
//  File version: 1.1.4
//  Last modified: 09/20/2016
//
//  Created by Davide Balistreri on 05/30/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBViewExtensions.h"
#import "DBFramework.h"
#import "DBRuntimeHandler.h"

// MARK: - View

@implementation UIView (DBViewExtensions)

- (void)removeAllSubviews
{
    for (UIView *subview in self.subviews) {
        [subview removeFromSuperview];
    }
}

- (void)removeAllConstraints
{
    [self removeConstraints:self.constraints];
}

- (BOOL)isDescendantOfViewWithClass:(Class)klass
{
    UIView *view = self;
    
    while (view) {
        if ([view isKindOfClass:klass]) {
            return YES;
        }
        
        view = view.superview;
    }
    
    return NO;
}

- (__kindof UIView *)superviewWithClass:(Class)klass
{
    UIView *superview = self.superview;
    
    while (superview) {
        if ([superview isKindOfClass:klass]) {
            return superview;
        }
        
        superview = superview.superview;
    }
    
    return nil;
}

- (__kindof UIView *)topmostSuperview
{
    UIView *superview = self.superview;
    
    while (superview.superview) {
        superview = superview.superview;
    }
    
    return superview;
}

- (void)bringToFront
{
    [self.superview bringSubviewToFront:self];
}

- (void)sendToBack
{
    [self.superview sendSubviewToBack:self];
}

- (__kindof UIViewController *)parentViewController
{
    UIResponder *responder = self;
    
    while (responder && [responder isNotKindOfClass:[UIViewController class]]) {
        responder = [responder nextResponder];
    }
    
    if ([responder isNotKindOfClass:[UIViewController class]]) {
        return nil;
    }
    
    return (id)responder;
}

#if DBFRAMEWORK_TARGET_IOS

- (void)setVisualEffect:(DBViewVisualEffect)visualEffect
{
    if (!NSClassFromString(@"UIBlurEffect")) {
        // Workaround pre-UIBlurEffect
        [self setVisualEffectPreiOS8:visualEffect];
        return;
    }
    
    NSInteger tag = -696969;
    UIView *blurView = [self viewWithTag:tag];
    
    if (blurView) {
        // Rimuovo il vecchio effetto
        [blurView removeFromSuperview];
    }
    
    if (visualEffect == DBViewVisualEffectNone) {
        return;
    }
    
    UIBlurEffect *blurEffect;
    
    switch (visualEffect) {
        case DBViewVisualEffectDarkBlur:
            blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
            break;
        case DBViewVisualEffectLightBlur:
            blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
            break;
        case DBViewVisualEffectExtraLightBlur:
            blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
            break;
        default:
            break;
    }
    
    blurView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurView.frame = self.bounds;
    blurView.autoresizingMask = DBViewAdattaDimensioni;
    blurView.tag = tag;
    
    [self insertSubview:blurView atIndex:0];
    [self setBackgroundColor:nil];
}

- (void)setVisualEffectPreiOS8:(DBViewVisualEffect)visualEffect
{
    switch (visualEffect) {
        case DBViewVisualEffectDarkBlur:
            self.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.8];
            break;
        case DBViewVisualEffectLightBlur:
            self.backgroundColor = [UIColor colorWithWhite:0.4 alpha:0.8];
            break;
        case DBViewVisualEffectExtraLightBlur:
            self.backgroundColor = [UIColor colorWithWhite:0.8 alpha:0.8];
            break;
        default:
            break;
    }
}

- (void)aggiungiOmbraConColore:(UIColor *)colore conSpessore:(CGFloat)spessore
{
    self.layer.masksToBounds = NO;
    self.layer.shadowColor = [colore CGColor];
    self.layer.shadowOffset = CGSizeZero;
    self.layer.shadowOpacity = 1.0;
    self.layer.shadowRadius = spessore;
    // self.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.bounds].CGPath;
    // self.layer.shouldRasterize = YES;
    // self.layer.rasterizationScale = 3.0;
}

- (void)rimuoviOmbra
{
    self.layer.shadowColor = nil;
    self.layer.shadowRadius = 0.0;
    self.layer.shadowOpacity = 0.0;
}

- (void)aggiungiBordoConColore:(UIColor *)colore conSpessore:(CGFloat)spessore
{
    self.layer.masksToBounds = YES;
    self.layer.borderColor = [colore CGColor];
    self.layer.borderWidth = spessore;
    // self.layer.shouldRasterize = YES;
    // self.layer.rasterizationScale = 3.0;
}

- (void)rimuoviBordo
{
    self.layer.borderColor = nil;
    self.layer.borderWidth = 0.0;
}

- (void)aggiungiAngoliArrotondatiConRaggio:(CGFloat)raggio
{
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = raggio;
    // self.layer.shouldRasterize = YES;
    // self.layer.rasterizationScale = 3.0;
}

- (void)aggiungiAngoliArrotondatiCerchio
{
    [self layoutIfNeeded];
    
    CGFloat raggio = fmin(self.altezza, self.larghezza);
    [self aggiungiAngoliArrotondatiConRaggio:(raggio / 2.0)];
}

- (void)rimuoviAngoliArrotondati
{
    self.layer.cornerRadius = 0.0;
}

- (void)setShouldRasterize:(BOOL)shouldRasterize
{
    self.layer.shouldRasterize = shouldRasterize;
    self.layer.rasterizationScale = [UIScreen mainScreen].scale;
}

- (UIImage *)catturaSchermata
{
    @autoreleasepool {
        CGSize imageSize = self.bounds.size;
        
        UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0);
        
        CGContextRef context = UIGraphicsGetCurrentContext();
        [self.layer renderInContext:context];
        
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return image;
    }
}

- (UIImage *)immagineSchermata
{
    return [self catturaSchermata];
}

/// Sposta la vista su un'altra, mantenendo la posizione assoluta rispetto allo schermo
- (void)spostaSullaVista:(UIView *)vista
{
    // Check
    if (!vista) return;
    
    CGRect nuovoFrame = self.frame;
    UIView *superview = nil;
    
    // Passaggio ad una vista indietro sullo stack
    superview = self.superview;
    while (superview && ![superview isEqual:vista]) {
        nuovoFrame.origin.x += superview.frame.origin.x;
        nuovoFrame.origin.y += superview.frame.origin.y;
        superview = superview.superview;
    }
    
    // Passaggio ad una vista avanti sullo stack
    superview = vista;
    while (superview && ![superview isEqual:self.superview]) {
        nuovoFrame.origin.x -= superview.frame.origin.x;
        nuovoFrame.origin.y -= superview.frame.origin.y;
        superview = superview.superview;
    }
    
    [self removeFromSuperview];
    self.frame = nuovoFrame;
    [vista addSubview:self];
}

- (void)spostaSubviewsSullaVista:(UIView *)vista
{
    // Check
    if (!vista) return;
    
    for (UIView *subview in self.subviews) {
        [subview removeFromSuperview];
        
        CGRect nuovoFrame = subview.frame;
        nuovoFrame.origin.x += self.frame.origin.x;
        nuovoFrame.origin.y += self.frame.origin.y;
        subview.frame = nuovoFrame;
        
        [vista addSubview:subview];
    }
}

- (UIScrollView *)creaScrollView
{
    return [UIScrollView creaScrollViewConVista:self];
}

- (CGSize)dimensioniReali
{
    // [self.superview setNeedsLayout];
    // [self.superview layoutIfNeeded];
    
    CGSize dimensioni = self.frame.size;
    
    // Prendo l'altezza massima delle subviews
    for (UIView *subview in self.subviews) {
        CGSize dimensioniSubview = [subview dimensioniReali];
        dimensioniSubview.width  += subview.frame.origin.x;
        dimensioniSubview.height += subview.frame.origin.y;
        dimensioni.width  = fmax(dimensioni.width,  dimensioniSubview.width);
        dimensioni.height = fmax(dimensioni.height, dimensioniSubview.height);
    }
    
    return dimensioni;
}

- (UIView *)maskViewWithSubviews:(NSArray<UIView *> *)subviews
{
    UIImage *maskImage = [self maskImageWithSubviews:subviews];
    UIImageView *maskView = [[UIImageView alloc] initWithImage:maskImage];
    return maskView;
}

- (UIImage *)maskImageWithSubviews:(NSArray<UIView *> *)subviews
{
    @autoreleasepool {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, NO, 0.0);
        CGContextRef context = UIGraphicsGetCurrentContext();
        
        CGContextSetFillColorWithColor(context, [UIColor blackColor].CGColor);
        CGContextFillRect(context, self.bounds);
        
        CGContextSetBlendMode(context, kCGBlendModeDestinationOut);
        for (UIView *view in subviews) {
            CGPoint origin = [view convertPoint:CGPointZero toView:self];
            CGContextTranslateCTM(context, origin.x, origin.y);
            [view.layer renderInContext:context];
            CGContextTranslateCTM(context, -origin.x, -origin.y);
        }
        
        UIImage *mask = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return mask;
    }
}

#else

- (NSColor *)backgroundColor
{
    if (self.layer.backgroundColor) {
        return [NSColor colorWithCGColor:self.layer.backgroundColor];
    }
    
    return nil;
}

- (void)setBackgroundColor:(NSColor *)backgroundColor
{
    if (!self.layer) {
        self.wantsLayer = YES;
    }
    
    self.layer.backgroundColor = [backgroundColor CGColor];
}

- (BOOL)clipsToBounds
{
    return self.layer.masksToBounds;
}

- (void)setClipsToBounds:(BOOL)clipsToBounds
{
    if (!self.layer) {
        self.wantsLayer = YES;
    }
    
    self.layer.masksToBounds = clipsToBounds;
}

- (CGFloat)alpha
{
    return self.layer.opacity;
}

- (void)setAlpha:(CGFloat)alpha
{
    if (!self.layer) {
        self.wantsLayer = YES;
    }
    
    self.layer.opacity = alpha;
    
    if (alpha == 0.0) {
        self.hidden = YES;
    }
    else {
        self.hidden = NO;
    }
}

- (BOOL)opaque
{
    return self.layer.opaque;
}

- (void)setOpaque:(BOOL)opaque
{
    if (!self.layer) {
        self.wantsLayer = YES;
    }
    
    self.layer.opaque = opaque;
}

- (void)disableSubviews:(BOOL)disable
{
    if ([self respondsToSelector:@selector(setEnabled:)]) {
        ((NSControl *) self).enabled = !disable;
    }
    
    for (NSView *subview in self.subviews) {
        [subview disableSubviews:disable];
    }
}

#endif

@end


#if DBFRAMEWORK_TARGET_IOS

// MARK: - TableView

@implementation UITableView (DBViewExtensions)

/**
 * Override del metodo di sistema: permette di calcolare il contentSize esatto della TableView.
 */
- (CGSize)contentSizeReale
{
    UITableView *tableView = self;
    id <UITableViewDataSource> dataSource = self.dataSource;
    id <UITableViewDelegate> delegate = self.delegate;
    
    [tableView layoutIfNeeded];
    
    CGSize contentSize = CGSizeMake(tableView.frame.size.width, 0.0);
    
    if ([tableView respondsToSelector:@selector(numberOfSections)]) {
        for (NSUInteger section = 0; section < [tableView numberOfSections]; section++) {
            // Header
            if ([dataSource respondsToSelector:@selector(tableView:heightForHeaderInSection:)]) {
                CGFloat height = [delegate tableView:tableView heightForHeaderInSection:section];
                contentSize.height += unsignedDouble(floor(height));
            }
            
            // Body
            if ([dataSource respondsToSelector:@selector(tableView:numberOfRowsInSection:)]) {
                for (NSUInteger row = 0; row < [dataSource tableView:tableView numberOfRowsInSection:section]; row++) {
                    CGFloat height = tableView.rowHeight;
                    
                    if ([dataSource respondsToSelector:@selector(tableView:heightForRowAtIndexPath:)]) {
                        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
                        height = [delegate tableView:tableView heightForRowAtIndexPath:indexPath];
                        
                        if (height == UITableViewAutomaticDimension) {
                            // La TableView utilizza il calcolo dinamico dell'altezza,
                            // devo per forza calcolare l'altezza della cella in base al contenuto
                            if ([dataSource respondsToSelector:@selector(tableView:cellForRowAtIndexPath:)]) {
                                UITableViewCell *cell = [dataSource tableView:tableView cellForRowAtIndexPath:indexPath];
                                [cell setNeedsLayout];
                                [cell layoutIfNeeded];
                                
                                CGSize size = [cell systemLayoutSizeFittingSize:CGSizeMake(contentSize.width, 0.0) withHorizontalFittingPriority:UILayoutPriorityRequired verticalFittingPriority:UILayoutPriorityFittingSizeLevel];
                                // NSLog(@"^ %@", NSStringFromCGSize(size));
                                
                                height = size.height;
                            }
                            else if ([delegate respondsToSelector:@selector(tableView:estimatedHeightForRowAtIndexPath:)]) {
                                height = [delegate tableView:tableView estimatedHeightForRowAtIndexPath:indexPath];
                            }
                            else {
                                height = tableView.estimatedRowHeight;
                            }
                        }
                    }
                    
                    contentSize.height += unsignedDouble(floor(height));
                }
            }
            
            // Footer
            if ([dataSource respondsToSelector:@selector(tableView:heightForFooterInSection:)]) {
                CGFloat height = [delegate tableView:tableView heightForFooterInSection:section];
                contentSize.height += unsignedDouble(floor(height));
            }
        }
    }
    
    // Content Inset
    CGFloat heightInset = self.contentInset.top + self.contentInset.bottom;
    contentSize.height += heightInset;
    
    return contentSize;
}

/**
 * Aggiorna tutte le sezioni della TableView con o senza animazione.
 */
- (void)reloadData:(BOOL)animated
{
    if (animated) {
        [self reloadDataWithDuration:0.3 completionBlock:nil];
    } else {
        [self reloadData];
    }
}

- (void)reloadDataWithDuration:(CGFloat)duration completionBlock:(void (^)(void))completionBlock
{
    UIViewAnimationOptions options = UIViewAnimationOptionTransitionCrossDissolve|UIViewAnimationOptionAllowUserInteraction;
    
    [UIView transitionWithView:self duration:duration options:options animations:^{
        [self reloadData];
        
    } completion:^(BOOL finished) {
        if (completionBlock) {
            completionBlock();
        }
    }];
}

- (void)scrollToTop:(BOOL)animated
{
    @try {
        NSIndexPath *firstRow = [NSIndexPath indexPathForRow:0 inSection:0];
        [self scrollToRowAtIndexPath:firstRow atScrollPosition:UITableViewScrollPositionTop animated:animated];
        [self layoutIfNeeded];
        
    } @catch (NSException *exception) {
        // ...
    }
}

- (void)scrollToBottom:(BOOL)animated
{
    if ([self.dataSource respondsToSelector:@selector(numberOfSectionsInTableView:)]) {
        NSInteger sections = [self.dataSource numberOfSectionsInTableView:self];
        
        if (sections > 0 && [self.dataSource respondsToSelector:@selector(tableView:numberOfRowsInSection:)]) {
            NSInteger rows = [self.dataSource tableView:self numberOfRowsInSection:(sections - 1)];
            
            if (rows > 0) {
                @try {
                    NSIndexPath *lastRow = [NSIndexPath indexPathForRow:(rows - 1) inSection:(sections - 1)];
                    [self scrollToRowAtIndexPath:lastRow atScrollPosition:UITableViewScrollPositionBottom animated:animated];
                    [self layoutIfNeeded];
                    
                } @catch (NSException *exception) {
                    // ...
                }
            }
        }
    }
}

/**
 * Permette di animare l'aggiornamento di layout della TableView, e di eseguire un blocco di codice al termine dell'animazione.
 *
 * NB: Questo metodo non deve essere richiamato se il dataSource della TableView è cambiato e non sono state aggiornate le sezioni della tabella, altrimenti verrà generata un'eccezione.
 */
- (void)aggiornaLayoutTableViewConCompletionBlock:(void(^)(void))completionBlock
{
    [CATransaction begin];
    
    [CATransaction setCompletionBlock:^{
        if (completionBlock) {
            completionBlock();
        }
    }];
    
    [self beginUpdates];
    
    /**
     * Nel caso in cui il dataSource della TableView fosse cambiato, bisognerebbe inserire qui il codice per riordinare le sezioni e le celle eventualmente aggiunte/spostate/eliminate
     */
    
    [self endUpdates];
    
    [CATransaction commit];
}

/**
 * Crea una UIView replicando il contenuto di questa UITableView.
 */
- (UIView *)creaView
{
    UIView *view = [UIView new];
    view.larghezza = self.larghezza;
    
    UITableViewController *tableViewController = (UITableViewController *) self.dataSource;
    UITableView *tableView = self;
    
    // Controllo se qualcosa impedisce la creazione della vista
    if (![tableView respondsToSelector:@selector(numberOfSections)]) return nil;
    if (![tableViewController respondsToSelector:@selector(tableView:numberOfRowsInSection:)]) return nil;
    if (![tableViewController respondsToSelector:@selector(tableView:cellForRowAtIndexPath:)]) return nil;
    
    for (NSInteger counterSections = 0; counterSections < [tableView numberOfSections]; counterSections++) {
        // Header
        if ([tableViewController respondsToSelector:@selector(tableView:viewForHeaderInSection:)]) {
            UIView *header = [tableViewController tableView:tableView viewForHeaderInSection:counterSections];
            CGRect frameHeader = header.frame;
            frameHeader.origin.y = view.altezza;
            
            if ([tableViewController respondsToSelector:@selector(tableView:heightForHeaderInSection:)]) {
                // Altezza personalizzata dal delegate
                frameHeader.size.height = [tableViewController tableView:tableView heightForHeaderInSection:counterSections];
            }
            
            header.frame = frameHeader;
            [view addSubview:header];
            
            view.altezza += frameHeader.size.height;
        }
        
        // Celle
        for (NSInteger counterRows = 0; counterRows < [tableView numberOfRowsInSection:counterSections]; counterRows++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:counterRows inSection:counterSections];
            
            // Debug
            // NSLog(@"Sezione %d Cella %d", (int) counterSections, (int) counterRows);
            
            UIView *cella = [tableViewController tableView:tableView cellForRowAtIndexPath:indexPath];
            CGRect frameCella = cella.frame;
            frameCella.origin.y = view.altezza;
            
            if ([tableViewController respondsToSelector:@selector(tableView:heightForRowAtIndexPath:)]) {
                // Altezza personalizzata dal delegate
                frameCella.size.height = [tableViewController tableView:tableView heightForRowAtIndexPath:indexPath];
            }
            
            cella.frame = frameCella;
            [view addSubview:cella];
            
            view.altezza += frameCella.size.height;
        }
    }
    
    return view;
}

@end


// MARK: - CollectionView

@implementation UICollectionView (DBViewExtensions)

/**
 * Override del metodo di sistema: permette di calcolare il contentSize esatto della CollectionView.
 */
- (CGSize)contentSizeReale
{
    UICollectionView *collectionView = self;
    UICollectionViewFlowLayout *layout = (id)collectionView.collectionViewLayout;
    id <UICollectionViewDataSource> dataSource = self.dataSource;
    id <UICollectionViewDelegateFlowLayout> layoutDelegate = (id)self.delegate;
    
    // [collectionView layoutIfNeeded];
    
    CGSize contentSize = CGSizeMake(collectionView.bounds.size.width, 0.0);
    
    if ([collectionView respondsToSelector:@selector(numberOfSections)]) {
        for (NSUInteger section = 0; section < [collectionView numberOfSections]; section++) {
            // Header
            if ([layoutDelegate respondsToSelector:@selector(collectionView:layout:referenceSizeForHeaderInSection:)]) {
                contentSize.height += [layoutDelegate collectionView:collectionView layout:layout referenceSizeForHeaderInSection:section].height;
            }
            
            // Body
            if ([dataSource respondsToSelector:@selector(collectionView:numberOfItemsInSection:)]) {
                NSInteger numberOfItems = [dataSource collectionView:collectionView numberOfItemsInSection:section];
                
                // Interitem spacing
                CGFloat interitemSpacing = layout.minimumInteritemSpacing;
                
                if ([layoutDelegate respondsToSelector:@selector(collectionView:layout:minimumInteritemSpacingForSectionAtIndex:)]) {
                    interitemSpacing = [layoutDelegate collectionView:collectionView layout:layout minimumInteritemSpacingForSectionAtIndex:section];
                }
                
                // Line spacing
                CGFloat lineSpacing = layout.minimumLineSpacing;
                
                if ([layoutDelegate respondsToSelector:@selector(collectionView:layout:minimumLineSpacingForSectionAtIndex:)]) {
                    lineSpacing = [layoutDelegate collectionView:collectionView layout:layout minimumLineSpacingForSectionAtIndex:section];
                }
                
                // Section inset
                UIEdgeInsets sectionInset = layout.sectionInset;
                
                if ([layoutDelegate respondsToSelector:@selector(collectionView:layout:insetForSectionAtIndex:)]) {
                    sectionInset = [layoutDelegate collectionView:collectionView layout:layout insetForSectionAtIndex:section];
                }
                
                CGFloat sectionInsetWidth  = sectionInset.left + sectionInset.right;
                CGFloat sectionInsetHeight = sectionInset.top + sectionInset.bottom;
                
                // Ogni cella può avere dimensioni diverse, devo calcolare l'altezza di ogni riga in base alla sua cella più alta
                CGSize itemSize = CGSizeZero;
                CGFloat width = 0.0;
                
                for (NSUInteger item = 0; item < numberOfItems; item++) {
                    // Item size
                    CGSize thisItemSize = layout.itemSize;
                    
                    if ([layoutDelegate respondsToSelector:@selector(collectionView:layout:sizeForItemAtIndexPath:)]) {
                        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:item inSection:section];
                        thisItemSize = [layoutDelegate collectionView:collectionView layout:layout sizeForItemAtIndexPath:indexPath];
                    }
                    
                    if (width == 0.0) {
                        // Prima riga
                        width = thisItemSize.width;
                    }
                    else {
                        width += interitemSpacing + thisItemSize.width;
                    }
                    
                    // Controllo se è ancora la stessa riga
                    if (width > contentSize.width - sectionInsetWidth) {
                        // Nuova riga
                        contentSize.height += lineSpacing + itemSize.height;
                        width = thisItemSize.width;
                        itemSize = thisItemSize;
                    }
                    // Controllo se l'altezza della cella è maggiore di quella precedente
                    else if (thisItemSize.height > itemSize.height) {
                        itemSize = thisItemSize;
                    }
                }
                
                // Salvo l'ultima riga
                contentSize.height += itemSize.height;
                
                // Section inset
                contentSize.height += sectionInsetHeight;
            }
            
            // Footer
            if ([layoutDelegate respondsToSelector:@selector(collectionView:layout:referenceSizeForFooterInSection:)]) {
                contentSize.height += [layoutDelegate collectionView:collectionView layout:layout referenceSizeForFooterInSection:section].height;
            }
        }
    }
    
    // Content Inset
    __unused CGFloat contentInsetWidth  = self.contentInset.left + self.contentInset.right;
    CGFloat contentInsetHeight = self.contentInset.top + self.contentInset.bottom;
    contentSize.height += contentInsetHeight;
    
    return contentSize;
}

/**
 * Aggiorna tutte le sezioni della CollectionView con o senza animazione.
 */
- (void)reloadData:(BOOL)animazione
{
    if (!animazione) {
        [self reloadData];
    }
    else if ([NSArray isEmpty:self.visibleCells]) {
        // Il primo reloadData deve essere senza animazioni
        [self reloadData];
    }
    else {
        @try {
            [self reloadSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, [self numberOfSections])]];
        }
        @catch (NSException *exception) {
            [self reloadData];
        }
    }
}

@end


// MARK: - ScrollView

@implementation UIScrollView (DBViewExtensions)

- (CGRect)visibleRect
{
    CGRect visibleRect = CGRectZero;
    visibleRect.origin = self.contentOffset;
    visibleRect.size = self.contentSize;
    return visibleRect;
}

+ (UIScrollView *)creaScrollViewConVista:(UIView *)vista
{
    UIScrollView *scrollView = [UIScrollView new];
    scrollView.frame = vista.bounds;
    scrollView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    scrollView.contentSize = scrollView.bounds.size;
    
    // Sposto tutte le subview nella nuova ScrollView
    [vista spostaSubviewsSullaVista:scrollView];
    
    // Adatto l'altezza in base al contenuto
    [scrollView adattaAltezzaContenuto];
    
    return scrollView;
}

- (CGSize)contentSizeIdeale
{
    // Nascondo gli ScrollIndicator per non falsare il contenuto della ScrollView
    BOOL showsHorizontalScrollIndicator = self.showsHorizontalScrollIndicator;
    BOOL showsVerticalScrollIndicator = self.showsVerticalScrollIndicator;
    
    self.showsHorizontalScrollIndicator = NO;
    self.showsVerticalScrollIndicator = NO;
    
    CGSize contentSize = CGSizeMake(self.frame.size.width, 0.0);
    
    for (UIView *subview in self.subviews) {
        // Se la subview è più alta del contentSize attuale
        if (contentSize.height < subview.puntoInferiore) {
            contentSize.height = subview.puntoInferiore;
        }
        
        // Se la subview è più larga del contentSize attuale
        if (contentSize.width < subview.puntoDestro) {
            contentSize.width = subview.puntoDestro;
        }
    }
    
    self.showsHorizontalScrollIndicator = showsHorizontalScrollIndicator;
    self.showsVerticalScrollIndicator = showsVerticalScrollIndicator;
    
    return contentSize;
}

- (void)adattaContenuto
{
    self.contentSize = self.contentSizeIdeale;
}

- (void)adattaAltezzaContenuto
{
    self.contentSize = CGSizeMake(self.contentSize.width, self.contentSizeIdeale.height);
}

- (void)adattaLarghezzaContenuto
{
    self.contentSize = CGSizeMake(self.contentSizeIdeale.width, self.contentSize.height);
}

- (void)scrollToTop:(BOOL)animated
{
    CGPoint topOffset = CGPointMake(0, -self.contentInset.top);
    [self setContentOffset:topOffset animated:animated];
}

- (void)scrollToBottom:(BOOL)animated
{
    CGPoint bottomOffset = CGPointMake(0, self.contentSize.height - self.bounds.size.height + self.contentInset.bottom);
    [self setContentOffset:bottomOffset animated:animated];
}

@end


// MARK: - WebView

@implementation UIWebView (DBViewExtensions)

- (CGRect)visibleRect
{
    CGRect visibleRect = CGRectZero;
    visibleRect.origin = self.scrollView.contentOffset;
    visibleRect.size = self.scrollView.contentSize;
    return visibleRect;
}

- (CGSize)contentSizeIdeale
{
    return [self sizeThatFits:CGSizeZero];
}

- (void)adattaContenuto
{
    self.dimensioni = self.contentSizeIdeale;
}

- (void)adattaAltezzaContenuto
{
    self.dimensioni = CGSizeMake(self.frame.size.width, self.contentSizeIdeale.height);
}

- (void)adattaLarghezzaContenuto
{
    self.dimensioni = CGSizeMake(self.contentSizeIdeale.width, self.frame.size.height);
}

- (void)scrollToTop:(BOOL)animazione
{
    CGPoint offset = CGPointZero;
    
    if ([DBDevice isiOSMin:7.0])
        offset = CGPointMake(-self.scrollView.contentInset.left, -self.scrollView.contentInset.top);
    
    [self.scrollView setContentOffset:offset animated:animazione];
}

@end


// MARK: - Label

@implementation UILabel (DBViewExtensions)

/// Metodo per adattare il font della UILabel se l'altezza è più piccola del font attuale
- (void)adattaAltezzaFont
{
    // Da implementare meglio!
    
    UIFont *fontAttuale = self.font;
    NSString *stringa = self.text;
    
    CGFloat altezzaMassima = self.frame.size.height;
    
    CGFloat dimensioneMassima = self.font.pointSize;
    CGFloat dimensioneAttuale = 0.0;
    
    while (dimensioneAttuale < dimensioneMassima) {
        UIFont *font = [fontAttuale fontWithSize:dimensioneAttuale];
        CGSize dimensioniFont = [stringa sizeWithFont:font constrainedToSize:self.frame.size];
        // CGSize dimensioniFont = [stringa sizeWithFont:font forWidth:self.frame.size.width lineBreakMode:self.lineBreakMode];
        
        if (dimensioniFont.height > altezzaMassima) {
            // L'altezza del font supera quella della Label
            break;
        }
        
        // DBLog($(dimensioneAttuale), @": ", NSStringFromCGSize(dimensioniFont));
        dimensioneAttuale++;
        
        //        difference = labelSize.height - [testString sizeWithFont:tempFont].height;
        //
        //        if (mid == tempMin || mid == tempMax) {
        //            if (difference < 0) {
        //                return [UIFont fontWithName:fontName size:(mid - 1)];
        //            }
        //
        //            return [UIFont fontWithName:fontName size:mid];
        //        }
        //
        //        if (difference < 0) {
        //            tempMax = mid - 1;
        //        } else if (difference > 0) {
        //            tempMin = mid + 1;
        //        } else {
        //            return [UIFont fontWithName:fontName size:mid];
        //        }
    }
    
    self.font = [fontAttuale fontWithSize:dimensioneAttuale];
}

// DA CONTROLLARE E IMPLEMENTARE
- (void)adjustFontSizeToFit
{
    UIFont *font = self.font;
    CGSize size = self.frame.size;
    
    for (CGFloat maxSize = self.font.pointSize; maxSize >= self.minimumFontSize; maxSize -= 1.f)
    {
        font = [font fontWithSize:maxSize];
        CGSize constraintSize = CGSizeMake(size.width, MAXFLOAT);
        CGSize labelSize = [self.text sizeWithFont:font constrainedToSize:constraintSize lineBreakMode:UILineBreakModeWordWrap];
        if(labelSize.height <= size.height)
        {
            self.font = font;
            [self setNeedsLayout];
            break;
        }
    }
    // set the font to the minimum size anyway
    self.font = font;
    [self setNeedsLayout];
}

/// Metodo per ridimensionare una UILabel in base al testo, e riposizionarla sotto una UIView
- (void)ridimensionaConAltezzaMassima:(CGFloat)altezzaMassima riposizionaSotto:(UIView *)viewSopra conMargine:(CGFloat)margine
{
    // Riposiziono la label in base a se è stata passata la viewSopra o no
    CGRect frameView = (viewSopra) ? viewSopra.frame : self.frame;
    if (viewSopra) frameView.origin.y += frameView.size.height + margine;
    
    // Calcolo l'altezza in base al testo
    frameView.size.height = [self.text altezzaConFont:self.font conDimensioniMassime:CGSizeMake(frameView.size.width, altezzaMassima)];
    
    // Applico il nuovo frame
    self.frame = frameView;
}

- (void)setAttributedTextWithHTMLString:(NSString *)htmlString
{
    NSString *fontFamily = self.font.familyName;
    CGFloat fontSize = self.font.pointSize;
    UIColor *fontColor = self.textColor;
    
    NSMutableString *style = [NSMutableString string];
    // [style appendString:@"html {-webkit-text-size-adjust:none;}\n"];
    [style appendString:@"* { \n"];
    [style appendString:@"padding: 0; margin: 0; \n"];
    [style appendFormat:@"font-family: '%@'; \n", fontFamily];
    [style appendFormat:@"font-size: %.2f px; \n", fontSize];
    [style appendFormat:@"color: rgba(%.f, %.f, %.f, %.2f); \n", fontColor.R * 255, fontColor.G * 255, fontColor.B * 255, fontColor.alpha];
    [style appendString:@"}"];
    
    NSString *html = [NSString stringWithFormat:@"<html><head><style>%@</style></head><body>%@</body></html>", style, htmlString];
    
    NSAttributedString *attributedString = [NSAttributedString attributedStringWithHTML:html];
    self.attributedText = attributedString;
}

- (void)setAttributedTextConStringa:(NSString *)stringa
                      conColoreBase:(UIColor *)coloreBase
               conStringaAttributed:(NSString *)stringaAttributed
                conColoreAttributed:(UIColor *)coloreAttributed
{
    stringa = [NSString safeString:stringa];
    stringaAttributed = [NSString safeString:stringaAttributed];
    if (!coloreBase) coloreBase = self.textColor;
    if (!coloreAttributed) coloreAttributed = coloreBase;
    
    NSDictionary *base = @{NSForegroundColorAttributeName:coloreBase, NSFontAttributeName:self.font};
    NSDictionary *attributed = @{NSForegroundColorAttributeName:coloreAttributed, NSFontAttributeName:self.font};
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:stringa attributes:base];
    
    NSRange range = [stringa rangeOfString:stringaAttributed];
    [attributedString setAttributes:attributed range:range];
    
    self.attributedText = attributedString;
}

- (void)setAttributedTextConStringa:(NSString *)stringa
                        conFontBase:(UIFont *)fontBase
                      conColoreBase:(UIColor *)coloreBase
               conStringaAttributed:(NSString *)stringaAttributed
                  conFontAttributed:(UIFont *)fontAttributed
                conColoreAttributed:(UIColor *)coloreAttributed
{
    stringa = [NSString safeString:stringa];
    stringaAttributed = [NSString safeString:stringaAttributed];
    if (!fontBase) fontBase = self.font;
    if (!coloreBase) coloreBase = self.textColor;
    if (!fontAttributed) fontAttributed = fontBase;
    if (!coloreAttributed) coloreAttributed = coloreBase;
    
    NSDictionary *base = @{NSForegroundColorAttributeName:coloreBase, NSFontAttributeName:fontBase};
    NSDictionary *attributed = @{NSForegroundColorAttributeName:coloreAttributed, NSFontAttributeName:fontAttributed};
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:stringa attributes:base];
    
    NSRange range = [stringa rangeOfString:stringaAttributed];
    [attributedString setAttributes:attributed range:range];
    
    self.attributedText = attributedString;
}

- (void)applicaInterlinea:(CGFloat)interlinea
{
    NSMutableAttributedString *attributedString = [NSMutableAttributedString attributedStringConStringa:self.text];
    
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = self.lineBreakMode;
    paragraphStyle.lineSpacing = interlinea;
    paragraphStyle.alignment = self.textAlignment;
    
    NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
    [attributes safeSetObject:paragraphStyle forKey:NSParagraphStyleAttributeName];
    [attributes safeSetObject:self.textColor forKey:NSForegroundColorAttributeName];
    [attributes safeSetObject:self.font forKey:NSFontAttributeName];
    
    [attributedString setAttributes:attributes range:NSMakeRange(0, self.text.length)];
    
    self.attributedText = attributedString;
}

@end

#endif


// MARK: - TextField

@implementation UITextField (DBViewExtensions)

#if DBFRAMEWORK_TARGET_IOS

- (UIColor *)placeholderTextColor
{
    return [self oggettoAssociatoConSelector:@selector(placeholderTextColor)];
}

- (void)setPlaceholderTextColor:(UIColor *)placeholderTextColor
{
    [self associaOggettoStrong:placeholderTextColor conSelector:@selector(placeholderTextColor)];
    
    // Aggiorno il placeholder
    [self setPlaceholder:self.placeholder];
}

- (UIFont *)placeholderTextFont
{
    return [self oggettoAssociatoConSelector:@selector(placeholderTextFont)];
}

- (void)setPlaceholderTextFont:(UIFont *)placeholderTextFont
{
    [self associaOggettoStrong:placeholderTextFont conSelector:@selector(placeholderTextFont)];
    
    // Aggiorno il placeholder
    [self setPlaceholder:self.placeholder];
}

- (void)setPlaceholder:(NSString *)placeholder
{
    NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
    if (self.placeholderTextFont)  [attributes setObject:self.placeholderTextFont forKey:NSFontAttributeName];
    if (self.placeholderTextColor) [attributes setObject:self.placeholderTextColor forKey:NSForegroundColorAttributeName];
    self.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString safeString:placeholder] attributes:attributes];
}

+ (void)correzioneLagiOS8
{
    DBFRAMEWORK_DEPRECATED_LOG(0.6.3, "utilizza il metodo +[DBUtility correzioneLagInputFields]");
    [DBUtility correzioneLagInputFields];
}

#else

- (NSString *)text
{
    return self.stringValue;
}

- (void)setText:(NSString *)text
{
    self.stringValue = text;
    
    switch (self.lineBreakMode) {
        case NSLineBreakByCharWrapping:
        case NSLineBreakByWordWrapping:
            break;
        default:
            [self aggiornaDimensioneFont];
            break;
    }
}

- (NSAttributedString *)attributedText
{
    return self.attributedStringValue;
}

- (void)setAttributedText:(NSAttributedString *)attributedText
{
    self.attributedStringValue = attributedText;
}

- (NSString *)placeholder
{
    return self.placeholderString;
}

- (void)setPlaceholder:(NSString *)placeholder
{
    self.placeholderString = placeholder;
}

- (void)aggiornaDimensioneFont
{
    // NSLog(@"Dimensioni '%@': %@ -> %@", self.text, NSStringFromCGSize(self.frame.size), NSStringFromCGSize([self dimensioniTesto]));
    
    CGFloat padding = [NSTextView new].textContainer.lineFragmentPadding;
    padding += 4.0;
    
    CGFloat pointSize = self.font.pointSize;
    NSFont *font = [NSFont fontWithName:self.font.familyName size:pointSize];
    NSString *text = self.text;
    CGSize dimensioni = [text sizeWithAttributes:@{NSFontAttributeName:font}];
    
    while (dimensioni.width > (self.frame.size.width - padding) || dimensioni.height > self.frame.size.height) {
        pointSize -= 0.2;
        
        font = [NSFont fontWithName:self.font.familyName size:pointSize];
        dimensioni = [text sizeWithAttributes:@{NSFontAttributeName:font}];
        
        if (pointSize <= (self.font.pointSize - 5.0)) {
            break;
        }
    }
    
    self.font = font;
    [self setNeedsLayout];
}

- (NSSize)dimensioniTestoConFont:(NSFont *)font
{
    NSSize answer = NSZeroSize;
    NSTypesetterBehavior typesetterBehavior = NSTypesetterLatestBehavior;
    
    if ([self.text length] > 0) {
        // Checking for empty string is necessary since Layout Manager will give the nominal
        // height of one line if length is 0.  Our API specifies 0.0 for an empty string.
        NSSize size = self.frame.size;
        
        NSTextContainer *textContainer = [[NSTextContainer alloc] initWithContainerSize:size];
        NSTextStorage *textStorage = [[NSTextStorage alloc] initWithAttributedString:self.attributedText];
        [textStorage setAttributes:@{NSFontAttributeName:font} range:NSRangeFromString(self.text)];
        
        NSLayoutManager *layoutManager = [NSLayoutManager new];
        [layoutManager addTextContainer:textContainer];
        [textStorage addLayoutManager:layoutManager];
        [layoutManager setHyphenationFactor:0.0];
        if (typesetterBehavior != NSTypesetterLatestBehavior) {
            [layoutManager setTypesetterBehavior:typesetterBehavior];
        }
        
        // NSLayoutManager is lazy, so we need the following kludge to force layout:
        [layoutManager glyphRangeForTextContainer:textContainer];
        
        answer = [layoutManager usedRectForTextContainer:textContainer].size;
        
        // Adjust if there is extra height for the cursor
        NSSize extraLineSize = [layoutManager extraLineFragmentRect].size;
        if (extraLineSize.height > 0) {
            answer.height -= extraLineSize.height;
        }
        
        //        // In case we changed it above, set typesetterBehavior back
        //        // to the default value.
        typesetterBehavior = NSTypesetterLatestBehavior;
    }
    
    return answer;
}

- (void)applicaInterlinea:(CGFloat)interlinea
{
    NSMutableAttributedString *attributedString = [NSMutableAttributedString attributedStringConStringa:self.stringValue];
    [attributedString applicaInterlinea:interlinea];
    [attributedString setAlignment:self.alignment range:NSMakeRange(0, self.stringValue.length)];
    self.attributedText = attributedString;
}

#endif

@end


// MARK: - TextView

@implementation UITextView (DBViewExtensions)

#if DBFRAMEWORK_TARGET_IOS

- (void)applicaPaddingMinimo
{
    [self applicaPaddingInset:UIEdgeInsetsMake(12.0, 12.0, 12.0, 12.0)];
}

- (void)applicaPadding:(CGFloat)padding
{
    self.textContainer.lineFragmentPadding = 0.0;
    self.textContainerInset = UIEdgeInsetsMake(padding, padding, padding, padding);
}

- (void)applicaPaddingInset:(UIEdgeInsets)paddingInset
{
    self.textContainer.lineFragmentPadding = 0.0;
    self.textContainerInset = paddingInset;
}

- (void)applicaInterlinea:(CGFloat)interlinea
{
    if ([NSString isNotEmpty:self.text]) {
        NSMutableAttributedString *attributedString = [NSMutableAttributedString attributedStringConStringa:self.text];
        
        NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
        paragraphStyle.lineSpacing = interlinea;
        paragraphStyle.alignment = self.textAlignment;
        
        NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
        [attributes safeSetObject:paragraphStyle forKey:NSParagraphStyleAttributeName];
        [attributes safeSetObject:self.textColor forKey:NSForegroundColorAttributeName];
        [attributes safeSetObject:self.font forKey:NSFontAttributeName];
        
        [attributedString setAttributes:attributes range:NSMakeRange(0, self.text.length)];
        
        self.attributedText = attributedString;
    }
    
    if ([self isKindOfClass:[DBTextView class]]) {
        UILabel *placeholderLabel = [self safePerformSelector:NSSelectorFromString(@"placeholderLabel")];
        [placeholderLabel applicaInterlinea:interlinea];
    }
}

#else

- (NSString *)text
{
    return self.string;
}

- (void)setText:(NSString *)text
{
    self.string = text;
}

#endif

@end


// MARK: - Button

@implementation UIButton (DBViewExtensions)

- (NSString *)text
{
#if DBFRAMEWORK_TARGET_IOS
    return [self titleForState:UIControlStateNormal];
#else
    return self.title;
#endif
}

- (void)setText:(NSString *)text
{
#if DBFRAMEWORK_TARGET_IOS
    [UIView performWithoutAnimation:^{
        [self setTitle:text forState:UIControlStateNormal & UIControlStateHighlighted & UIControlStateSelected];
        [self layoutIfNeeded];
    }];
#else
    self.title = text;
#endif
}

- (NSAttributedString *)attributedText
{
#if DBFRAMEWORK_TARGET_IOS
    return [self attributedTitleForState:UIControlStateNormal];
#else
    return self.attributedTitle;
#endif
}

- (void)setAttributedText:(NSAttributedString *)attributedText
{
#if DBFRAMEWORK_TARGET_IOS
    [UIView performWithoutAnimation:^{
        [self setAttributedTitle:attributedText forState:UIControlStateNormal & UIControlStateHighlighted & UIControlStateSelected];
        [self layoutIfNeeded];
    }];
#else
    self.attributedTitle = attributedText;
#endif
}

#if DBFRAMEWORK_TARGET_IOS
- (UIColor *)textColor
{
    return [self titleColorForState:UIControlStateNormal];
}

- (void)setTextColor:(UIColor *)textColor
{
    [UIView performWithoutAnimation:^{
        [self setTitleColor:textColor forState:UIControlStateNormal & UIControlStateHighlighted & UIControlStateSelected];
        [self layoutIfNeeded];
    }];
}

- (UIImage *)image
{
    return [self imageForState:UIControlStateNormal];
}

- (void)setImage:(UIImage *)image
{
    [self setImage:image forState:UIControlStateNormal & UIControlStateHighlighted & UIControlStateSelected];
}

- (UIImage *)backgroundImage
{
    return [self backgroundImageForState:UIControlStateNormal];
}

- (void)setBackgroundImage:(UIImage *)backgroundImage
{
    [self setBackgroundImage:backgroundImage forState:UIControlStateNormal & UIControlStateHighlighted & UIControlStateSelected];
}
#endif

@end


// MARK: - GestureRecognizer

#if DBFRAMEWORK_TARGET_IOS

@implementation UIGestureRecognizer (DBViewExtensions)

- (void)invalidateGesture
{
    BOOL isEnabled = self.isEnabled;
    self.enabled = NO;
    self.enabled = isEnabled;
}

@end

#endif


// MARK: - Storyboard

@implementation UIStoryboard (DBViewExtensions)

+ (UIStoryboard *)safeStoryboardWithName:(NSString *)name bundle:(NSBundle *)storyboardBundleOrNil
{
    @try {
        return [UIStoryboard storyboardWithName:name bundle:storyboardBundleOrNil];
    } @catch (NSException *exception) {
        return nil;
    }
}

- (__kindof UIViewController * __nullable)instantiateViewControllerWithClass:(nullable Class)klass
{
    @try {
#if DBFRAMEWORK_TARGET_IOS
        return [self instantiateViewControllerWithIdentifier:NSStringFromClass(klass)];
#else
        return [self instantiateControllerWithIdentifier:NSStringFromClass(klass)];
#endif
    }
    @catch (NSException *exception) {
        NSString *identifier = NSStringFromClass(klass);
        NSString *storyboard = [self description];
        DBExtendedLog(@"Error: couldn't find a ViewController with identifier '", identifier, @"' in Storyboard ", storyboard);
        return nil;
    }
}

@end


// MARK: - Font

@implementation UIFont (DBViewExtensions)

/// Returns the system font if a font with the specified name doesn't exist.
+ (UIFont *)safeFontWithName:(NSString *)fontName size:(CGFloat)fontSize
{
    @try {
        UIFont *font = [UIFont fontWithName:fontName size:fontSize];
        
        if (font) {
            return font;
        } else {
            return [UIFont systemFontOfSize:fontSize];
        }
        
    } @catch(NSException *exception) {
        return [UIFont systemFontOfSize:fontSize];
    }
}

@end
