//
//  DBObjectExtensions.m
//  File version: 1.0.0
//  Last modified: 04/09/2016
//
//  Created by Davide Balistreri on 04/09/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBObjectExtensions.h"
#import "DBDataSource.h"

@implementation NSObject (DBObjectExtensions)

+ (BOOL)isEmpty:(id)object
{
    if ([object isKindOfClass:[NSString class]])
        return [self stringIsEmpty:object];
    
    if ([object isKindOfClass:[NSArray class]])
        return [self arrayIsEmpty:object];
    
    if ([object isKindOfClass:[NSDictionary class]])
        return [self dictionaryIsEmpty:object];
    
    if ([object isKindOfClass:[DBDataSource class]])
        return [self dataSourceIsEmpty:object];
    
    return [self isNull:object];
}

+ (BOOL)isNotEmpty:(id)object
{
    return ![self isEmpty:object];
}

+ (BOOL)isNull:(id)object
{
    return (!object || [object isKindOfClass:[NSNull class]]) ? YES : NO;
}

+ (BOOL)isNotNull:(id)object
{
    return ![self isNull:object];
}

+ (BOOL)stringIsEmpty:(NSString *)string
{
    if ([self isNull:string])
        return YES;
    
    // Pulizia dei caratteri vuoti
    NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *stringCheck = [string stringByTrimmingCharactersInSet:set];
    return (stringCheck.length > 0) ? NO : YES;
}

+ (BOOL)arrayIsEmpty:(NSArray *)array
{
    if ([self isNull:array])
        return YES;
    
    return (array.count > 0) ? NO : YES;
}

+ (BOOL)dictionaryIsEmpty:(NSDictionary *)dictionary
{
    if ([self isNull:dictionary])
        return YES;
    
    return (dictionary.count > 0) ? NO : YES;
}

+ (BOOL)dataSourceIsEmpty:(DBDataSource *)dataSource
{
    if ([self isNull:dataSource])
        return YES;
    
    return (dataSource.numeroOggetti > 0) ? NO : YES;
}

- (BOOL)isNotEqual:(id)object
{
    return ![self isEqual:object];
}

- (BOOL)isNotMemberOfClass:(Class)aClass
{
    return ![self isMemberOfClass:aClass];
}

- (BOOL)isNotKindOfClass:(Class)aClass
{
    return ![self isKindOfClass:aClass];
}

- (BOOL)isMemberOfClasses:(NSArray<Class> *)classes
{
    for (Class aClass in classes) {
        if ([self isMemberOfClass:aClass]) {
            return YES;
        }
    }
    
    return NO;
}

- (BOOL)isKindOfClasses:(NSArray<Class> *)classes
{
    for (Class aClass in classes) {
        if ([self isKindOfClass:aClass]) {
            return YES;
        }
    }
    
    return NO;
}

- (BOOL)isNotMemberOfClasses:(NSArray<Class> *)classes
{
    return ![self isMemberOfClasses:classes];
}

- (BOOL)isNotKindOfClasses:(NSArray<Class> *)classes
{
    return ![self isKindOfClasses:classes];
}

/// This method lets you safely call a selector, public or private (that may be not known to the compiler)
- (id)safePerformSelector:(SEL)aSelector
{
    return [self safePerformSelector:aSelector withObject:nil];
}

/// This method lets you safely call a selector, public or private (that may be not known to the compiler)
- (id)safePerformSelector:(SEL)aSelector withObject:(id)object
{
    @synchronized(self) {
        // NSInvocation will copy the bytes of the return value into the given memory buffer, regardless of type.
        // ARC will assume that any returned value has been retained and will release it when it goes out of scope.
        // This is why I am setting the pointer to a non-retained type, otherwise it will crash.
        __unsafe_unretained id returnValue = nil;
        
        Class klass = [self class];
        NSMethodSignature *methodSignature = [klass instanceMethodSignatureForSelector:aSelector];
        
        if (methodSignature) {
            NSInteger numberOfArguments = methodSignature.numberOfArguments; // Min: 2 arguments
            BOOL hasAtLeastOneArgument = (numberOfArguments > 2) ? YES : NO;
            BOOL hasReturnValue = NO;
            
            // strcmp: compares the contents, not the pointers
            if (strcmp(methodSignature.methodReturnType, "v")) {
                // Type encoding "v" = void
                hasReturnValue = YES;
            }
            
            NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:methodSignature];
            [invocation setTarget:self];
            [invocation setSelector:aSelector];
            
            if (hasAtLeastOneArgument && object) {
                [invocation setArgument:&object atIndex:2];
            }
            
            [invocation invoke];
            
            if (hasReturnValue) {
                [invocation getReturnValue:&returnValue];
            }
        }
        
        return returnValue;
    }
}

@end
