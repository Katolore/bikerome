//
//  DBObjectExtensions.h
//  File version: 1.0.0
//  Last modified: 04/09/2016
//
//  Created by Davide Balistreri on 04/09/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

@interface NSObject (DBObjectExtensions)

+ (BOOL)isEmpty:(id)object;
+ (BOOL)isNotEmpty:(id)object;

+ (BOOL)isNull:(id)object;
+ (BOOL)isNotNull:(id)object;

- (BOOL)isNotEqual:(id)object;

- (BOOL)isNotMemberOfClass:(Class)aClass;
- (BOOL)isNotKindOfClass:(Class)aClass;

- (BOOL)isMemberOfClasses:(NSArray<Class> *)classes;
- (BOOL)isKindOfClasses:(NSArray<Class> *)classes;

- (BOOL)isNotMemberOfClasses:(NSArray<Class> *)classes;
- (BOOL)isNotKindOfClasses:(NSArray<Class> *)classes;

/// This method lets you safely call a selector, public or private (that may be not known to the compiler)
- (id)safePerformSelector:(SEL)aSelector;

/// This method lets you safely call a selector, public or private (that may be not known to the compiler)
- (id)safePerformSelector:(SEL)aSelector withObject:(id)object;

@end
