//
//  DBDateExtensions.m
//  File version: 1.0.1
//  Last modified: 04/14/2016
//
//  Created by Davide Balistreri on 03/01/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBDateExtensions.h"
#import "DBObjectExtensions.h"
#import "DBLocalization.h"

// MARK: - NSDateFormatter

@implementation NSDateFormatter (DBDateExtensions)

+ (NSDateFormatter *)dateFormatterConFormato:(NSString *)formato
{
    if ([NSString isEmpty:formato]) {
        return [NSDateFormatter new];
    }
    
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    dateFormatter.locale = [NSLocale localeWithLocaleIdentifier:[DBLocalization identifierLinguaCorrente]];
    dateFormatter.dateFormat = formato;
    return dateFormatter;
}

+ (NSString *)stringaConData:(NSDate *)data conFormato:(NSString *)formato
{
    if (!data || [NSString isEmpty:formato]) {
        return @"";
    }
    
    NSDateFormatter *dateFormatter = [NSDateFormatter dateFormatterConFormato:formato];
    return [dateFormatter stringFromDate:data];
}

+ (NSDate *)dataConStringa:(NSString *)stringa conFormato:(NSString *)formato
{
    if ([NSString isEmpty:stringa] || [NSString isEmpty:formato]) {
        return nil;
    }
    
    NSDateFormatter *dateFormatter = [NSDateFormatter dateFormatterConFormato:formato];
    return [dateFormatter dateFromString:stringa];
}

+ (NSString *)stringaFormatoStandardData:(NSDate *)data
{
    if (!data) {
        return @"";
    }
    
    return [NSDateFormatter localizedStringFromDate:data dateStyle:NSDateFormatterLongStyle timeStyle:NSDateFormatterNoStyle];
}

+ (NSString *)stringaFormatoStandardOra:(NSDate *)data
{
    if (!data) {
        return @"";
    }
    
    return [NSDateFormatter localizedStringFromDate:data dateStyle:NSDateFormatterNoStyle timeStyle:NSDateFormatterMediumStyle];
}

+ (NSString *)stringaFormatoStandardDataOra:(NSDate *)data
{
    if (!data) {
        return @"";
    }
    
    return [NSDateFormatter localizedStringFromDate:data dateStyle:NSDateFormatterLongStyle timeStyle:NSDateFormatterMediumStyle];
}

@end


// MARK: - NSDate

@implementation NSDate (DBDateExtensions)

/// 1 = Sunday, 2 = Monday
- (NSInteger)weekday
{
    NSCalendar *calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents* dateComponents = [calendar components:NSCalendarUnitWeekday fromDate:self];
    return [dateComponents weekday];
}

/// 7 = Sunday, 1 = Monday
- (NSInteger)italianWeekday
{
    NSInteger weekday = self.weekday - 1;
    return (weekday == 0) ? 7 : weekday;
}

- (NSInteger)secondo
{
    NSString *stringa = [NSDateFormatter stringaConData:self conFormato:@"ss"];
    return stringa.integerValue;
}

- (NSInteger)minuto
{
    NSString *stringa = [NSDateFormatter stringaConData:self conFormato:@"mm"];
    return stringa.integerValue;
}

- (NSInteger)ora
{
    NSString *stringa = [NSDateFormatter stringaConData:self conFormato:@"HH"];
    return stringa.integerValue;
}

- (NSInteger)giorno
{
    NSString *stringa = [NSDateFormatter stringaConData:self conFormato:@"dd"];
    return stringa.integerValue;
}

- (NSInteger)mese
{
    NSString *stringa = [NSDateFormatter stringaConData:self conFormato:@"MM"];
    return stringa.integerValue;
}

- (NSInteger)anno
{
    NSString *stringa = [NSDateFormatter stringaConData:self conFormato:@"yyyy"];
    return stringa.integerValue;
}

+ (NSDate *)dataConAnno:(NSInteger)anno mese:(NSInteger)mese giorno:(NSInteger)giorno ore:(NSInteger)ore minuti:(NSInteger)minuti secondi:(NSInteger)secondi
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *dateComponents = [NSDateComponents new];
    [dateComponents setYear:anno];
    [dateComponents setMonth:mese];
    [dateComponents setDay:giorno];
    [dateComponents setHour:ore];
    [dateComponents setMinute:minuti];
    [dateComponents setSecond:secondi];
    
    return [calendar dateFromComponents:dateComponents];
}

+ (NSDate *)dataConAnno:(NSInteger)anno mese:(NSInteger)mese giorno:(NSInteger)giorno
{
    return [self dataConAnno:anno mese:mese giorno:giorno ore:0 minuti:0 secondi:0];
}

+ (NSTimeInterval)timeIntervalConGiorni:(CGFloat)giorni
{
    return (24 * 60 * 60 * giorni);
}

- (NSDate *)aggiungiAnni:(NSInteger)anni
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents = [NSDateComponents new];
    [dateComponents setYear:anni];
    return [calendar dateByAddingComponents:dateComponents toDate:self options:0];
}

- (NSDate *)aggiungiMese:(NSInteger)mesi
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents = [NSDateComponents new];
    [dateComponents setMonth:mesi];
    return [calendar dateByAddingComponents:dateComponents toDate:self options:0];
}

- (NSDate *)aggiungiGiorni:(NSInteger)giorni
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents = [NSDateComponents new];
    [dateComponents setDay:giorni];
    return [calendar dateByAddingComponents:dateComponents toDate:self options:0];
}

- (NSDate *)aggiungiOre:(NSInteger)ore
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents = [NSDateComponents new];
    [dateComponents setHour:ore];
    return [calendar dateByAddingComponents:dateComponents toDate:self options:0];
}

- (NSDate *)aggiungiMinuti:(NSInteger)minuti
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents = [NSDateComponents new];
    [dateComponents setMinute:minuti];
    return [calendar dateByAddingComponents:dateComponents toDate:self options:0];
}

- (NSDate *)aggiungiSecondi:(NSInteger)secondi
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents = [NSDateComponents new];
    [dateComponents setSecond:secondi];
    return [calendar dateByAddingComponents:dateComponents toDate:self options:0];
}

- (NSDate *)impostaAnno:(NSInteger)anno
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *data = [calendar dateBySettingUnit:NSCalendarUnitYear value:anno ofDate:self options:0];
    return (data) ? data : self;
}

- (NSDate *)impostaMese:(NSInteger)mese
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *data = [calendar dateBySettingUnit:NSCalendarUnitMonth value:mese ofDate:self options:0];
    return (data) ? data : self;
}

- (NSDate *)impostaGiorno:(NSInteger)giorno
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *data = [calendar dateBySettingUnit:NSCalendarUnitDay value:giorno ofDate:self options:0];
    return (data) ? data : self;
}

- (NSDate *)impostaOre:(NSInteger)ore
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *data = [calendar dateBySettingUnit:NSCalendarUnitHour value:ore ofDate:self options:0];
    return (data) ? data : self;
}

- (NSDate *)impostaMinuti:(NSInteger)minuti
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *data = [calendar dateBySettingUnit:NSCalendarUnitMinute value:minuti ofDate:self options:0];
    return (data) ? data : self;
}

- (NSDate *)impostaSecondi:(NSInteger)secondi
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *data = [calendar dateBySettingUnit:NSCalendarUnitSecond value:secondi ofDate:self options:0];
    return (data) ? data : self;
}

+ (NSDate *)adesso
{
    return [NSDate date];
}

+ (NSDate *)oggi
{
    return [[NSDate adesso] dataGiorno];
}

+ (NSDate *)ieri
{
    return [[NSDate oggi] aggiungiGiorni:-1];
}

+ (NSDate *)domani
{
    return [[NSDate oggi] aggiungiGiorni:1];
}

- (NSDate *)dataGiorno
{
    NSString *formato = @"dd/MM/yyyy";
    NSString *oggi = [NSDateFormatter stringaConData:self conFormato:formato];
    return [NSDateFormatter dataConStringa:oggi conFormato:formato];
}

- (NSDate *)dataFineGiorno
{
    NSDate *domani = [self aggiungiGiorni:1];
    NSDate *giornoDomani = [domani dataGiorno];
    NSDate *fineGiorno = [giornoDomani aggiungiSecondi:-1];
    return fineGiorno;
}

- (NSDate *)firstDayOfMonth
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit) fromDate:self];
    [components setDay:1];
    
    NSDate *date = [calendar dateFromComponents:components];
    return date;
}

- (BOOL)isGiorno:(NSDate *)giorno
{
    NSString *stringSelf = [NSDateFormatter stringaConData:self conFormato:@"dd/MM/yyyy"];
    NSString *stringData = [NSDateFormatter stringaConData:giorno conFormato:@"dd/MM/yyyy"];
    return ([stringSelf isEqualToString:stringData]) ? YES : NO;
}

- (BOOL)isPrimaDiGiorno:(NSDate *)giorno
{
    return [[self dataGiorno] isPrimaDi:[giorno dataGiorno]];
}

- (BOOL)isDopoDiGiorno:(NSDate *)giorno
{
    return [[self dataGiorno] isDopoDi:[giorno dataGiorno]];
}

- (BOOL)isPrimaDi:(NSDate *)data
{
    return (data && [self secondiMancantiAllaData:data] > 0) ? YES : NO;
}

- (BOOL)isDopoDi:(NSDate *)data
{
    return (data && [self secondiPassatiDallaData:data] > 0) ? YES : NO;
}

- (BOOL)isOggi
{
    return ([self isGiorno:[NSDate oggi]]) ? YES : NO;
}

- (BOOL)isPrimaDiOggi
{
    return ([self isPrimaDiGiorno:[NSDate oggi]]) ? YES : NO;
}

- (BOOL)isDopoDiOggi
{
    return ([self isDopoDiGiorno:[NSDate oggi]]) ? YES : NO;
}

- (BOOL)isPrimaDiAdesso
{
    return ([self isPrimaDi:[NSDate date]]) ? YES : NO;
}

- (BOOL)isDopoDiAdesso
{
    return ([self isDopoDi:[NSDate date]]) ? YES : NO;
}

- (NSInteger)anniPassatiDallaData:(NSDate *)data
{
    if (!data) {
        return 0;
    }
    
    NSInteger annoSelf = [[NSDateFormatter stringaConData:self conFormato:@"yyyy"] integerValue];
    NSInteger annoData = [[NSDateFormatter stringaConData:data conFormato:@"yyyy"] integerValue];
    return annoSelf - annoData;
}

- (NSInteger)anniMancantiAllaData:(NSDate *)data
{
    return -[self anniPassatiDallaData:data];
}

- (NSInteger)anniPassati
{
    return [[NSDate adesso] anniPassatiDallaData:self];
}

- (NSInteger)anniMancanti
{
    return -[self anniPassati];
}

- (CGFloat)giorniPassatiDallaData:(NSDate *)data
{
    NSTimeInterval secondiDifferenza = [self secondiPassatiDallaData:data];
    NSTimeInterval unGiorno = [NSDate timeIntervalConGiorni:1];
    return secondiDifferenza / unGiorno;
}

- (CGFloat)giorniMancantiAllaData:(NSDate *)data
{
    return -[self giorniPassatiDallaData:data];
}

- (CGFloat)giorniPassati
{
    return [self giorniPassatiDallaData:[NSDate date]];
}

- (CGFloat)giorniMancanti
{
    return -[self giorniPassati];
}

+ (NSTimeInterval)secondiPassatiDallaData:(NSDate *)data
{
    return [[NSDate adesso] secondiPassatiDallaData:data];
}

+ (NSTimeInterval)secondiMancantiAllaData:(NSDate *)data
{
    return -[self secondiPassatiDallaData:data];
}

- (NSTimeInterval)secondiPassatiDallaData:(NSDate *)data
{
    return [self timeIntervalSinceDate:data];
}

- (NSTimeInterval)secondiMancantiAllaData:(NSDate *)data
{
    return -[self secondiPassatiDallaData:data];
}

- (NSTimeInterval)secondiPassati
{
    return [self secondiPassatiDallaData:[NSDate adesso]];
}

- (NSTimeInterval)secondiMancanti
{
    return -[self secondiPassati];
}

@end
