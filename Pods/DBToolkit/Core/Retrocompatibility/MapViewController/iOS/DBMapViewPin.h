//
//  DBMapViewPin.h
//  File version: 1.0.0
//  Last modified: 12/01/2015
//
//  Created by Davide Balistreri on 28/05/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBObject.h"
#import <MapKit/MapKit.h>

@interface DBMapViewPin : DBObject

/// Le coordinate del Pin sulla mappa
@property (nonatomic) CLLocationCoordinate2D coordinate;

/// L'icona del Pin sulla mappa
@property (strong, nonatomic) UIImage *icona;

/// Il punto di ancoraggio dell'icona sulla mappa (valori da 0.0 a 1.0, default 0.5)
@property (nonatomic) CGPoint anchorPoint;

/// This is a weak reference to the annotation view displayed on the map view, if present
@property (weak, nonatomic) MKAnnotationView *annotationView;

/// La didascalia che viene aperta una volta cliccato sul Pin
@property (strong, nonatomic) UIView *calloutView;

/// Il titolo del Pin, che viene mostrato se non esiste una calloutView
@property (strong, nonatomic) NSString *titolo;

/// Il sottotitolo del Pin, che viene mostrato se non esiste una calloutView
@property (strong, nonatomic) NSString *sottotitolo;

/// Se il Pin è selezionabile, quando viene selezionato viene eseguita una callback
@property (nonatomic) BOOL selezionabile;

/// Variabile per associare un oggetto a piacere al Pin
@property (strong, nonatomic) id oggetto;

/// An integer that you can use to identify map view pin objects in your application
@property (nonatomic) NSInteger tag;

@end
