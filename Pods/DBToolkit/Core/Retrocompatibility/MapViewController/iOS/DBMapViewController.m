//
//  DBMapViewController.m
//  File version: 1.0.1
//  Last modified: 04/01/2016
//
//  Created by Davide Balistreri on 10/30/2014
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBMapViewController.h"
#import "DBFramework.h"

// #import "SMCalloutView.h"

@interface DBMapViewAnnotationDeprecated : NSObject <MKAnnotation>

@property (nonatomic) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;

@end

@implementation DBMapViewAnnotationDeprecated

@end


@interface DBMapViewController () <MKMapViewDelegate /* , SMCalloutViewDelegate */ >

@property (strong, nonatomic) NSArray *pins;
@property (strong, nonatomic) NSArray *annotations;

@property (nonatomic) BOOL shouldNotifyRegionChanges;
@property (nonatomic) BOOL didCenterUserLocation;

@property (strong, nonatomic) MKMapView *mkMapView;
// @property (strong, nonatomic) SMCalloutView *smCalloutView;
@property (strong, nonatomic) DBMapViewPin *pinSelezionato;
@property (weak, nonatomic) MKAnnotationView *userAnnotationView;

// Se l'inizializzazione della Mappa è completa, attivo le animazioni
@property (nonatomic) BOOL mapDidAppear;

@end


@implementation DBMapViewController

+ (instancetype)mapViewControllerConDelegate:(id<DBMapViewControllerDelegate>)delegate conVista:(UIView *)vista
{
    DBMapViewController *mapViewController = [DBMapViewController new];
    mapViewController.delegate = delegate;
    mapViewController.view.frame = vista.bounds;
//    mapViewController.view.autoresizingMask = DBViewAdattaDimensioni;
    [vista insertSubview:mapViewController.view atIndex:0];
    [vista setAutolayoutEqualMarginsForSubview:mapViewController.view];
    
    return mapViewController;
}

+ (instancetype)mapViewControllerConVista:(UIView *)vista
{
    return [self mapViewControllerConDelegate:nil conVista:vista];
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        // Inizializzazione
        self.riposizionaPinVicini = YES;
        self.centraCameraSuPosizioneUtente = YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self impostaSchermata];
}

- (void)impostaSchermata
{
    self.mkMapView = [MKMapView new];
    self.mkMapView.translatesAutoresizingMaskIntoConstraints = NO;
    self.mapView.delegate = self;
    self.mapView.tintColor = self.tintColor;
    // self.mapView.frame = self.view.bounds;
    // self.mapView.autoresizingMask = DBViewAdattaDimensioni;
    [self.view addSubview:self.mapView];
    [self.view setAutolayoutEqualMarginsForSubview:self.mapView];
    
    [self aggiornaCameraMappaConCoordinate:[DBGeolocation posizioneCorrente] conAnimazione:NO];
    
    // Animazione fade-in
    self.view.alpha = 0.0;
    
    [DBAnimation performAfterDelay:0.6 withDuration:0.4 finalState:^{
        self.view.alpha = 1.0;
        self.mapDidAppear = YES;
    } completionBlock:nil];
}

// MARK: - Public methods

- (MKMapView *)mapView
{
    return self.mkMapView;
}

@synthesize tintColor = _tintColor;
- (void)setTintColor:(UIColor *)tintColor
{
    _tintColor = tintColor;
    
    if (self.mapView) {
        self.mapView.tintColor = tintColor;
    }
}

- (UIColor *)tintColor
{
    return (_tintColor) ? _tintColor : [DBUtility tintColor];
}

- (void)setMapType:(MKMapType)mapType
{
    _mapType = mapType;
    
    self.mapView.mapType = mapType;
}

- (void)setSeguiDirezioneUtente:(BOOL)seguiDirezioneUtente
{
    _seguiDirezioneUtente = seguiDirezioneUtente;
    self.centraCameraSuPosizioneUtente = NO;
    
    if (self.mostraPosizioneUtente && seguiDirezioneUtente) {
        [self.mapView setUserTrackingMode:MKUserTrackingModeFollowWithHeading animated:self.mapDidAppear];
    }
    else {
        [self.mapView setUserTrackingMode:MKUserTrackingModeNone animated:self.mapDidAppear];
    }
}

- (void)setMostraPosizioneUtente:(BOOL)mostraPosizioneUtente
{
    _mostraPosizioneUtente = mostraPosizioneUtente;
    
    // Aggiorno la visualizzazione dell'utente sulla mappa
    self.mapView.showsUserLocation = mostraPosizioneUtente;
    
    // Aggiorno la modalità di tracking dell'utente
    self.seguiDirezioneUtente = self.seguiDirezioneUtente;
    self.centraCameraSuPosizioneUtente = mostraPosizioneUtente;
}

- (void)aggiungiPin:(DBMapViewPin *)pin
{
    [self aggiungiPin:pin aggiornaPosizioneCamera:YES];
}

- (void)aggiungiPin:(DBMapViewPin *)pin aggiornaPosizioneCamera:(BOOL)aggiornaPosizioneCamera
{
    if (!pin) return;
    if (![DBGeolocation isPosizioneValida:pin.coordinate]) return;
    
    NSMutableArray *pins = [NSMutableArray arrayWithArray:self.pins];
    [pins addObject:pin];
    
    [self impostaPins:pins aggiornaPosizioneCamera:aggiornaPosizioneCamera];
}

- (void)impostaPins:(NSArray *)pins
{
    [self impostaPins:pins aggiornaPosizioneCamera:YES];
}

- (void)impostaPins:(NSArray *)pins aggiornaPosizioneCamera:(BOOL)aggiornaPosizioneCamera
{
    NSMutableArray *annotations = [NSMutableArray array];
    
    // Popolo la mappa con le informazioni ricevute sui Pin
    for (DBMapViewPin *pin in pins) {
        DBMapViewAnnotationDeprecated *annotation = [DBMapViewAnnotationDeprecated new];
        
        // Se il Pin è selezionabile, al click verrà aperta la calloutView, il titolo assegnato è indifferente
        annotation.title = (pin.calloutView) ? @"title" : [NSString safeString:pin.titolo];
        annotation.subtitle = (pin.calloutView) ? nil : [NSString safeString:pin.sottotitolo];
        
        if ([DBGeolocation isPosizioneValida:pin.coordinate])
            annotation.coordinate = pin.coordinate;
        
        [annotations addObject:annotation];
    }
    
    if (self.riposizionaPinVicini) {
        // Algoritmo per spostare i Pin che hanno coordinate sovrapponibili
        [DBMapViewController mutateCoordinatesOfClashingAnnotations:annotations];
    }
    
    // Rimuovo i vecchi Pin
    if ([NSArray isNotEmpty:self.annotations])
        [self.mapView removeAnnotations:self.annotations];
    
    // Salvo in memoria i nuovi Pin
    if ([NSArray isNotEmpty:pins]) {
        self.pins = pins;
        self.annotations = annotations;
        [self.mapView addAnnotations:annotations];
        
        if (aggiornaPosizioneCamera && !self.seguiDirezioneUtente) {
            [self zoomMapViewToFitAnnotations:self.mapView animated:YES];
        }
    }
}

// MARK: - Metodi

- (CLLocationDistance)raggioPuntoCentrale
{
    CLLocationCoordinate2D coordinateLato   = [self.mapView convertPoint:CGPointZero toCoordinateFromView:self.mapView];
    CLLocationCoordinate2D coordinateCentro = [self.mapView convertPoint:CGPointMake(self.mapView.frame.size.width / 2.0, 0.0) toCoordinateFromView:self.mapView];
    return [DBGeolocation distanzaDa:coordinateLato a:coordinateCentro];
}

- (CLLocationCoordinate2D)coordinatePuntoCentrale
{
    return self.mapView.centerCoordinate;
}

- (void)impostaCoordinatePuntoCentrale:(CLLocationCoordinate2D)coordinate conAnimazione:(BOOL)animazione
{
    CGFloat raggio = self.mapView.camera.altitude / 1000.0;
    [self impostaCoordinatePuntoCentrale:coordinate conRaggioKM:raggio conAnimazione:animazione];
}

- (void)impostaCoordinatePuntoCentrale:(CLLocationCoordinate2D)coordinate conRaggioKM:(CGFloat)raggioKM conAnimazione:(BOOL)animazione
{
    [self stopUpdatingUserLocation];
    
    MKMapCamera *camera = [self.mapView.camera copy];
    camera.centerCoordinate = coordinate;
    camera.altitude = raggioKM * 1000.0;
    
    [self.mapView setCamera:camera animated:animazione];
}

- (void)aggiornaCameraMappaConCoordinate:(CLLocationCoordinate2D)coordinate conAnimazione:(BOOL)animazione
{
    if (![DBGeolocation isPosizioneValida:coordinate]) {
        return;
    }
    
    // if (self.didCenterUserLocation) {
    [self stopUpdatingUserLocation];
    [self zoomMapViewToFitAnnotations:self.mapView animated:animazione];
    
    //    } else {
    //        MKCoordinateRegion mapRegion;
    //        mapRegion.center = coordinate;
    //        mapRegion.span.latitudeDelta = 0.075;
    //        mapRegion.span.longitudeDelta = 0.075;
    //        [self.mapView setRegion:mapRegion animated:NO];
    //
    //        self.didCenterUserLocation = YES;
    //    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    [self stopUpdatingUserLocation];
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesMoved:touches withEvent:event];
    self.shouldNotifyRegionChanges = YES;
}

- (void)stopUpdatingUserLocation
{
    self.centraCameraSuPosizioneUtente = NO;
    self.seguiDirezioneUtente = NO;
}

#define MINIMUM_ZOOM_ARC 0.014 //approximately 1 miles (1 degree of arc ~= 69 miles)
#define ANNOTATION_REGION_PAD_FACTOR 1.50
#define MAX_DEGREES_ARC 360
//size the mapView region to fit its annotations
- (void)zoomMapViewToFitAnnotations:(MKMapView *)mapView animated:(BOOL)animated
{
    NSArray *annotations = mapView.annotations;
    NSInteger count = annotations.count;
    
    if (count == 0) {
        // Non ci sono punti sulla mappa
        return;
    }
    
    // Convert NSArray of id <MKAnnotation> into an MKCoordinateRegion that can be used to set the map size
    // Can't use NSArray with MKMapPoint because MKMapPoint is not an id
    MKMapPoint points[count]; // C array of MKMapPoint struct
    for (int i = 0; i < count; i++) {
        // Load points C array by converting coordinates to points
        CLLocationCoordinate2D coordinate = [(id <MKAnnotation>)[annotations objectAtIndex:i] coordinate];
        points[i] = MKMapPointForCoordinate(coordinate);
    }
    
    // Create MKMapRect from array of MKMapPoint
    MKMapRect mapRect = [[MKPolygon polygonWithPoints:points count:count] boundingMapRect];
    
    // Convert MKCoordinateRegion from MKMapRect
    MKCoordinateRegion regionFit = MKCoordinateRegionForMapRect(mapRect);
    
    // Add padding so pins aren't scrunched on the edges
    regionFit.span.latitudeDelta  *= ANNOTATION_REGION_PAD_FACTOR;
    regionFit.span.longitudeDelta *= ANNOTATION_REGION_PAD_FACTOR;
    
    // But padding can't be bigger than the world
    if (regionFit.span.latitudeDelta  > MAX_DEGREES_ARC) regionFit.span.latitudeDelta  = MAX_DEGREES_ARC;
    if (regionFit.span.longitudeDelta > MAX_DEGREES_ARC) regionFit.span.longitudeDelta = MAX_DEGREES_ARC;
    
    // And don't zoom in stupid-close on small samples
    if (regionFit.span.latitudeDelta  < MINIMUM_ZOOM_ARC) regionFit.span.latitudeDelta  = MINIMUM_ZOOM_ARC;
    if (regionFit.span.longitudeDelta < MINIMUM_ZOOM_ARC) regionFit.span.longitudeDelta = MINIMUM_ZOOM_ARC;
    
    // And if there is a sample of 1 we want the max zoom-in instead of max zoom-out
    if (count == 1) {
        regionFit.span.latitudeDelta  = MINIMUM_ZOOM_ARC;
        regionFit.span.longitudeDelta = MINIMUM_ZOOM_ARC;
    }
    
    animated = (self.mapDidAppear) ? animated : NO;
    [mapView setRegion:regionFit animated:animated];
}

// MARK: - Delegate

- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated
{
    // DEBUG: Tolto
    // if (!self.shouldNotifyRegionChanges) return;
    
    if ([self.delegate respondsToSelector:@selector(mapViewWillChangeRegion:)]) {
        [self.delegate mapViewWillChangeRegion:self];
    }
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    // DEBUG: Tolto
    // if (!self.shouldNotifyRegionChanges) return;
    
    if ([self.delegate respondsToSelector:@selector(mapViewDidChangeRegion:conCoordinate:)]) {
        [self.delegate mapViewDidChangeRegion:self conCoordinate:[self coordinatePuntoCentrale]];
    }
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    if (self.centraCameraSuPosizioneUtente) {
        [self aggiornaCameraMappaConCoordinate:userLocation.coordinate conAnimazione:self.mapDidAppear];
    }
}

- (void)mapView:(MKMapView *)mapView didChangeUserTrackingMode:(MKUserTrackingMode)mode animated:(BOOL)animated
{
    self.seguiDirezioneUtente = (mode == MKUserTrackingModeFollowWithHeading) ? YES : NO;
    
    if (self.userAnnotationView) {
        self.userAnnotationView.image = (self.seguiDirezioneUtente) ? self.immaginePosizioneUtenteConHeading : self.immaginePosizioneUtente;
    }
}

- (MKAnnotationView *)nuovoAnnotationViewConMapView:(MKMapView *)mapView conAnnotation:(id<MKAnnotation>)annotation
{
    NSString *identifier = @"DBDefaultPinID";
    
    MKAnnotationView *annotationView = nil;
    annotationView = [mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
    
    if (!annotationView) {
        annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
        annotationView.tintColor = self.tintColor;
    }
    
    return annotationView;
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    // Pin posizione utente
    if ([annotation isEqual:mapView.userLocation]) {
        mapView.userLocation.title = @"";
        
        if (self.immaginePosizioneUtente) {
            // Pin utente personalizzato
            MKAnnotationView *annotationView = [self nuovoAnnotationViewConMapView:mapView conAnnotation:annotation];
            annotationView.image = (self.seguiDirezioneUtente) ? self.immaginePosizioneUtenteConHeading : self.immaginePosizioneUtente;
            self.userAnnotationView = annotationView;
            return annotationView;
        }
        
        return nil;
    }
    
    MKAnnotationView *annotationView = [self nuovoAnnotationViewConMapView:mapView conAnnotation:annotation];
    NSInteger pinNumero = [self.annotations indexOfObject:annotation];
    DBMapViewPin *pin = [self.pins safeObjectAtIndex:pinNumero];
    pin.annotationView = annotationView; // Weak reference
    
    annotationView.tag = pinNumero;
    annotationView.enabled = YES;
    annotationView.canShowCallout = YES;
    annotationView.image = pin.icona;
    
    UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    
    if ([DBDevice isiOSMajor:8]) {
        infoButton.larghezza += 10.0;
        infoButton.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
    }
    
    annotationView.rightCalloutAccessoryView = infoButton;
    return annotationView;
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    // Prendo il Pin associato all'AnnotationView
    DBMapViewPin *pin = [self.pins safeObjectAtIndex:view.tag];
    self.pinSelezionato = pin;
    
    if ([self.delegate respondsToSelector:@selector(mapView:apertaDidascaliaPin:)])
        [self.delegate mapView:self apertaDidascaliaPin:self.pinSelezionato];
    
    // CODICE SMCALLOUTVIEW PER ORA NON UTILIZZATO
    //    // Tolgo la CalloutView precedente
    //    [self.calloutView dismissCalloutAnimated:YES];
    //    self.calloutView = nil;
    //
    //    self.calloutView = [SMCalloutView platformCalloutView];
    //    self.calloutView.delegate = self;
    //    self.calloutView.contentViewInset = UIEdgeInsetsZero;
    //    self.calloutView.contentView = pin.calloutView;
    //
    //    [self.calloutView presentCalloutFromRect:view.bounds inView:view constrainedToView:self.mapView animated:YES];
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view
{
    self.pinSelezionato = nil;
    
    // CODICE SMCALLOUTVIEW PER ORA NON UTILIZZATO
    //    // Tolgo la CalloutView
    //    [self.calloutView dismissCalloutAnimated:YES];
    //    self.calloutView = nil;
}

// MARK: - SMCalloutView Delegate

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    if ([self.delegate respondsToSelector:@selector(mapView:cliccataDidascaliaPin:)])
        [self.delegate mapView:self cliccataDidascaliaPin:self.pinSelezionato];
}

// MARK: - Codice AnnotationCoordinateUtility

+ (void)mutateCoordinatesOfClashingAnnotations:(NSArray *)annotations
{
    NSDictionary *coordinateValuesToAnnotations = [self groupAnnotationsByLocationValue:annotations];
    
    for (NSValue *coordinateValue in coordinateValuesToAnnotations.allKeys) {
        NSMutableArray *outletsAtLocation = coordinateValuesToAnnotations[coordinateValue];
        
        if (outletsAtLocation.count > 1) {
            CLLocationCoordinate2D coordinate;
            [coordinateValue getValue:&coordinate];
            [self repositionAnnotations:outletsAtLocation toAvoidClashAtCoordination:coordinate];
        }
    }
}

+ (NSDictionary *)groupAnnotationsByLocationValue:(NSArray *)annotations
{
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    
    for (id<MKAnnotation> pin in annotations) {
        
        // Arrotondo le coordinate
        CLLocationDegrees latitudine = [[NSString stringWithFormat:@"%.3f", pin.coordinate.latitude] doubleValue];
        CLLocationDegrees longitudine = [[NSString stringWithFormat:@"%.3f", pin.coordinate.longitude] doubleValue];
        pin.coordinate = CLLocationCoordinate2DMake(latitudine, longitudine);
        
        CLLocationCoordinate2D coordinate = pin.coordinate;
        NSValue *coordinateValue = [NSValue valueWithBytes:&coordinate objCType:@encode(CLLocationCoordinate2D)];
        
        NSMutableArray *annotationsAtLocation = result[coordinateValue];
        if (!annotationsAtLocation) {
            annotationsAtLocation = [NSMutableArray array];
            result[coordinateValue] = annotationsAtLocation;
        }
        
        [annotationsAtLocation addObject:pin];
    }
    
    return result;
}

+ (void)repositionAnnotations:(NSMutableArray *)annotations toAvoidClashAtCoordination:(CLLocationCoordinate2D)coordinate
{
    double distance = 8 * annotations.count;
    double radiansBetweenAnnotations = (M_PI * 2) / annotations.count;
    
    for (int i = 0; i < annotations.count; i++) {
        double heading = radiansBetweenAnnotations * i;
        CLLocationCoordinate2D newCoordinate = [self calculateCoordinateFrom:coordinate onBearing:heading atDistance:distance];
        
        id <MKAnnotation> annotation = annotations[i];
        annotation.coordinate = newCoordinate;
    }
}

+ (CLLocationCoordinate2D)calculateCoordinateFrom:(CLLocationCoordinate2D)coordinate onBearing:(double)bearingInRadians atDistance:(double)distanceInMetres
{
    double coordinateLatitudeInRadians = coordinate.latitude * M_PI / 180;
    double coordinateLongitudeInRadians = coordinate.longitude * M_PI / 180;
    
    double distanceComparedToEarth = distanceInMetres / 6378100;
    
    double resultLatitudeInRadians = asin(sin(coordinateLatitudeInRadians) * cos(distanceComparedToEarth) + cos(coordinateLatitudeInRadians) * sin(distanceComparedToEarth) * cos(bearingInRadians));
    double resultLongitudeInRadians = coordinateLongitudeInRadians + atan2(sin(bearingInRadians) * sin(distanceComparedToEarth) * cos(coordinateLatitudeInRadians), cos(distanceComparedToEarth) - sin(coordinateLatitudeInRadians) * sin(resultLatitudeInRadians));
    
    CLLocationCoordinate2D result;
    result.latitude = resultLatitudeInRadians * 180 / M_PI;
    result.longitude = resultLongitudeInRadians * 180 / M_PI;
    
    return result;
}

@end
