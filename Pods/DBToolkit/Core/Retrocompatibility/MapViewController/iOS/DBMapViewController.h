//
//  DBMapViewController.h
//  File version: 1.0.1
//  Last modified: 04/12/2015
//
//  Created by Davide Balistreri on 10/30/2014
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBViewController.h"
#import "DBMapViewPin.h"

#import <MapKit/MapKit.h>

@class DBMapViewController;

@protocol DBMapViewControllerDelegate <NSObject>

@optional

/// Callback quando l'utente tappa sul Pin e viene aperta la didascalia
- (void)mapView:(DBMapViewController *)mapView apertaDidascaliaPin:(DBMapViewPin *)pin;

/// Callback quando l'utente tappa sulla didascalia del Pin aperto
- (void)mapView:(DBMapViewController *)mapView cliccataDidascaliaPin:(DBMapViewPin *)pin;

- (void)mapViewWillChangeRegion:(DBMapViewController *)mapView;
- (void)mapViewDidChangeRegion:(DBMapViewController *)mapView conCoordinate:(CLLocationCoordinate2D)coordinate;

@end


@interface DBMapViewController : UIViewController

+ (instancetype)mapViewControllerConVista:(UIView *)vista;
+ (instancetype)mapViewControllerConDelegate:(id<DBMapViewControllerDelegate>)delegate conVista:(UIView *)vista;

@property (weak, nonatomic) id<DBMapViewControllerDelegate> delegate;

@property (readonly) MKMapView *mapView;
@property (strong, nonatomic) UIColor *tintColor;

@property (nonatomic) BOOL riposizionaPinVicini;
@property (nonatomic) BOOL mostraPosizioneUtente;
@property (nonatomic) BOOL centraCameraSuPosizioneUtente;
@property (nonatomic) BOOL seguiDirezioneUtente;

@property (nonatomic) MKMapType mapType;
@property (strong, nonatomic) UIImage *immaginePosizioneUtente;
@property (strong, nonatomic) UIImage *immaginePosizioneUtenteConHeading;

/// La distanza dal lato al punto centrale della mappa, in metri
@property (readonly) CLLocationDistance raggioPuntoCentrale;

/// Le coordinate del punto centrale della mappa
@property (readonly) CLLocationCoordinate2D coordinatePuntoCentrale;

/// Modifica le coordinate del punto centrale della mappa, eventualmente effettuando lo spostamento con un'animazione
- (void)impostaCoordinatePuntoCentrale:(CLLocationCoordinate2D)coordinate conAnimazione:(BOOL)animazione;

/// Modifica le coordinate del punto centrale della mappa e lo zoom (in KM), eventualmente effettuando lo spostamento con un'animazione
- (void)impostaCoordinatePuntoCentrale:(CLLocationCoordinate2D)coordinate conRaggioKM:(CGFloat)raggioKM conAnimazione:(BOOL)animazione;

/// Metodo per posizionare un nuovo Pin sulla mappa
- (void)aggiungiPin:(DBMapViewPin *)pin;
- (void)aggiungiPin:(DBMapViewPin *)pin aggiornaPosizioneCamera:(BOOL)aggiornaPosizioneCamera;

/// Metodo per reimpostare tutti i Pin presenti sulla mappa
- (void)impostaPins:(NSArray *)pins;
- (void)impostaPins:(NSArray *)pins aggiornaPosizioneCamera:(BOOL)aggiornaPosizioneCamera;

@end
