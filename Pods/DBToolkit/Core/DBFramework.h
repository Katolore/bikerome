//
//  DBFramework.h
//  File version: 0.7.3
//  Last modified: 02/06/2017
//
//  Created by Davide Balistreri on 03/07/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

/// Core

#import "DBFoundation.h"
#import "DBCompatibility.h"
#import "DBUtility.h"
#import "DBApplication.h"


/// Retrocompatibility

#import "DBDeprecated.h"
#import "DBToolkit.h"


#if DBFRAMEWORK_TARGET_IOS

// UIKit Classes (View Controllers)
#import "DBNavigationController.h"
#import "DBNavigationStoryController.h"
#import "DBTableViewController.h"
#import "DBTabBarController.h"
#import "DBTutorialViewController.h"
#import "DBPageController.h"
#import "DBPopupViewController.h"

// UIKit Classes (Views)
#import "DBView.h"
#import "DBScrollView.h"
#import "DBTableView.h"
#import "DBDraggableView.h"
#import "DBParallaxView.h"
#import "DBZoomableImageView.h"
#import "DBSlideMenu.h"
#import "DBSlideshowView.h"
#import "DBButton.h"
#import "DBTextView.h"
#import "DBSingleSlider.h"
#import "DBDoubleSlider.h"
#import "DBPagerView.h"
#import "DBWebView.h"
#import "DBMapView.h"
#import "DBAccessoryTextField.h"
#import "DBImageView.h"
#import "DBAutoLayoutScrollView.h"
#import "DBAutoLayoutTableView.h"

// UIKit Classes (Cells)
#import "DBTableViewCell.h"
#import "DBTableViewGenericCell.h"
#import "DBTableViewMessageCell.h"
#import "DBTableViewSpinnerCell.h"

// Handlers
#import "DBKeyboardHandler.h"
#import "DBPickerHandler.h"
#import "DBPDFHandler.h"
#import "DBTableViewHandler.h"
#import "DBCameraHandler.h"

// NSKit Classes
#import "DBDevice.h"
#import "DBGeolocation.h"
#import "DBCoreData.h"
#import "DBDataSource.h"
#import "DBSocialKit.h"
#import "DBFileManager.h"

// UIKit Classes
#import "DBAssetLibrary.h"

#endif


/// iOS and OSX

// Extensions
#import "DBArrayExtensions.h"
#import "DBColorExtensions.h"
#import "DBDateExtensions.h"
#import "DBDictionaryExtensions.h"
#import "DBImageExtensions.h"
#import "DBObjectExtensions.h"
#import "DBStringExtensions.h"
#import "DBViewExtensions.h"

// UIKit Classes
#import "DBAlertView.h"
#import "DBProgressHUD.h"
#import "DBShareController.h"
#import "DBAutoLayoutView.h"
#import "DBAutoLayoutImageView.h"
#import "DBAnimation.h"

// Objects
#import "DBObject.h"
#import "DBManagedObject.h"

// NSKit Classes
#import "DBLocalization.h"
#import "DBLocalizationExtensions.h"
#import "DBGeometry.h"
#import "DBThread.h"
#import "DBNetworking.h"
#import "DBNotificationCenter.h"

// Others
#import "Objectify.h"
