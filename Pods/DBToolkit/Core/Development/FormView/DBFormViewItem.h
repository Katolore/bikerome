//
//  DBFormViewItem.h
//  Versione file: 1.0.0
//  Ultimo aggiornamento: 29/03/2016
//
//  Creato da Davide Balistreri il 29/03/2016
//  Copyright 2013-2016 © Davide Balistreri. Tutti i diritti riservati.
//

#import "DBObject.h"

// CLASSE IN VIA DI SVILUPPO

@class DBFormView;

@interface DBFormViewItem : DBObject

@property (strong, nonatomic) UIView *campo;

@property (weak, nonatomic) DBFormView *form;
@property (nonatomic) CGFloat altezzaFissata;

+ (instancetype)itemConCampo:(UIView *)campo;

@end
