//
//  DBFormHandler.m
//  Versione file: 0.1.0
//  Ultimo aggiornamento: 28/03/2016
//
//  Creato da Davide Balistreri il 28/03/2016
//  Copyright 2013-2016 © Davide Balistreri. Tutti i diritti riservati.
//

#import "DBFormHandler.h"
#import "DBTableViewHandler.h"
#import "DBGeometry.h"
#import "DBViewExtensions.h"


/**
 * Classe in sviluppo.
 */


#pragma mark - DBFormHandlerManager dichiarazione

@interface DBFormHandlerManager : NSObject

+ (DBFormHandler *)handlerConTableView:(UITableView *)tableView;

@end


#pragma mark - DBFormHandlerItem implementazione

@interface DBFormHandlerItem : DBObject

@property (weak, nonatomic) DBFormHandler *form;

@property (strong, nonatomic) UIView *vista;
@property (nonatomic) CGFloat altezzaFissata;

@end


@implementation DBFormHandlerItem

+ (instancetype)itemConVista:(UIView *)vista
{
    DBFormHandlerItem *item = [self new];
    item.vista = vista;
    item.altezzaFissata = vista.altezza;
    return item;
}

- (void)rimuoviDalForm
{
    [self.form rimuoviVista:self];
}

@end


#pragma mark - DBFormHandler implementazione

@interface DBFormHandler () <DBTableViewHandlerDelegate>

@property (strong, nonatomic) DBDataSource *dataSource;

@end


@implementation DBFormHandler

#pragma mark Metodi di classe

+ (instancetype)formConTableView:(UITableView *)tableView
{
    // Questo metodo crea o riutilizza un Handler
    return [DBFormHandlerManager handlerConTableView:tableView];
}

#pragma mark Metodi d'istanza

@synthesize tableView = _tableView;
- (void)setTableView:(UITableView *)tableView
{
    if (_tableView) {
        // Rimuovo i riferimenti alla vecchia TableView
        _tableView.delegate = nil;
        _tableView.dataSource = nil;
    }
    
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    _tableView = tableView;
}

- (void)distruggiFormHandler
{
    // Da fare
}

#pragma mark Gestione campi del Form

- (void)aggiungiVista:(UIView *)vista
{
    DBFormHandlerItem *oggetto = [DBFormHandlerItem itemConVista:vista];
    oggetto.form = self;
    
    [self.dataSource aggiungiOggetto:oggetto];
    [self.tableView reloadData];
}

- (void)rimuoviVista:(UIView *)vista
{
    // Da fare
}

#pragma mark TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.dataSource numeroSezioni];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.dataSource numeroOggettiNellaSezioneConIndice:section];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DBFormHandlerItem *oggetto = [self.dataSource oggettoConIndexPath:indexPath];
    
    if ([oggetto.vista isKindOfClass:[UITableViewCell class]]) {
        return UITableViewAutomaticDimension;
    }
    
    return oggetto.altezzaFissata;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DBFormHandlerItem *oggetto = [self.dataSource oggettoConIndexPath:indexPath];
    
    if ([oggetto.vista isKindOfClass:[UITableViewCell class]]) {
        // La vista è già una UITableViewCell
        return oggetto.vista;
    }
    
    UITableViewCell *cella = [UITableViewCell new];
    cella.selectionStyle = UITableViewCellSelectionStyleNone;
    cella.clipsToBounds = YES;
    
    // Quando le celle non sono più visibili sullo schermo vengono deallocate
    // La superview di ogni vista viene quindi automaticamente resettata
    UIView *vista = oggetto.vista;
    [cella.contentView addSubview:vista];
    
    return cella;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    DBFormHandlerItem *oggetto = [self.dataSource oggettoConIndexPath:indexPath];
    UIView *vista = oggetto.vista;
    
    if ([oggetto.vista isKindOfClass:[UITableViewCell class]] == NO) {
        vista.larghezza = tableView.larghezza;
        vista.altezza = [self tableView:tableView heightForRowAtIndexPath:indexPath];
    }
}

@end


#pragma mark - DBFormHandlerManager implementazione

@interface DBFormHandlerManager ()

@property (strong, nonatomic) NSMutableArray *handlers;

@end


@implementation DBFormHandlerManager

+ (instancetype)sharedInstance
{
    @synchronized(self) {
        DBFormHandlerManager *sharedInstance = [DBCentralManager getRisorsa:@"DBSharedFormHandler"];
        
        if (!sharedInstance) {
            sharedInstance = [DBFormHandlerManager new];
            sharedInstance.handlers = [NSMutableArray array];
            [DBCentralManager setRisorsa:sharedInstance conIdentifier:@"DBSharedFormHandler"];
        }
        
        return sharedInstance;
    }
}

+ (DBFormHandler *)handlerConTableView:(UITableView *)tableView // conDelegate:(id<DBFormHandlerDelegate>)delegate
{
    // Controllo se già è stato instanziato l'handler per la TableView o il delegate specificati
    DBFormHandlerManager *sharedInstance = [DBFormHandlerManager sharedInstance];
    
    for (DBFormHandler *handler in sharedInstance.handlers) {
        if (tableView && [handler.tableView isEqual:tableView]) {
            return handler;
        }
        //        if (delegate && [handler.delegate isEqual:delegate]) {
        //            return handler;
        //        }
    }
    
    // Non l'ho trovato, instanzio un nuovo Handler
    DBFormHandler *handler = [DBFormHandler new];
    handler.tableView = tableView;
    handler.dataSource = [DBDataSource new];
    
    [sharedInstance.handlers addObject:handler];
    
    return handler;
}

@end
