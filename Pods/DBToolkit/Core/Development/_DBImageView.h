//
//  DBImageView.h
//  File version: 1.0.0
//  Last modified: 11/20/2016
//
//  Created by Davide Balistreri on 11/20/2016
//  Copyright 2013-2016 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

@class DBAnimation;


@interface DBImageView : UIImageView

- (void)setImage:(UIImage *)image withAnimation:(DBAnimation *)animation;

@end
