//
//  DBIndicatorToastView.h
//  File version: 1.0.0 beta
//  Last modified: 04/30/2017
//
//  Created by Davide Balistreri on 04/30/2017
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

/**
 * IMPORTANT DISCLAIMER:
 * This is a preliminary version for a technology in development.
 * It is not final, and it is subject to change.
 * Newer versions may break compatibility of software implemented using this API.
 */

#import "DBIndicatorView.h"


// MARK: - Class define

/**
 * Toast view similar to Android toasts.
 *
 * It appears on the bottom part of the screen.
 */
@interface DBIndicatorToastView : DBIndicatorView

// MARK: Initialization

+ (instancetype)toastViewWithMessage:(NSString *)message;

+ (instancetype)localizedToastViewWithMessage:(NSString *)message;


// MARK: Content setup

@property (weak, nonatomic) UILabel *messageLabel;


// MARK: Behavior setup

@property (nonatomic) NSTimeInterval duration;

@property (nonatomic) BOOL touchToDismiss;

@end
