//
//  DBIndicatorToastView.m
//  File version: 1.0.0 beta
//  Last modified: 04/30/2017
//
//  Created by Davide Balistreri on 04/30/2017
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

/**
 * IMPORTANT DISCLAIMER:
 * This is a preliminary version for a technology in development.
 * It is not final, and it is subject to change.
 * Newer versions may break compatibility of software implemented using this API.
 */

#import "DBIndicatorToastView.h"


// MARK: - DBIndicatorToastView class

@implementation DBIndicatorToastView

@end
