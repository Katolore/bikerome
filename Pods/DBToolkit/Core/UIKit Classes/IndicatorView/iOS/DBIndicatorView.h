//
//  DBIndicatorView.h
//  File version: 1.0.0 beta
//  Last modified: 04/30/2017
//
//  Created by Davide Balistreri on 04/30/2017
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

/**
 * IMPORTANT DISCLAIMER:
 * This is a preliminary version for a technology in development.
 * It is not final, and it is subject to change.
 * Newer versions may break compatibility of software implemented using this API.
 */

#import "DBView.h"

#import "DBIndicatorViewDefines.h"

#import "DBAnimation.h"
#import "DBViewExtensions.h"


// MARK: - Class define

/**
 * This is the main structure for DBIndicators.
 *
 * @note You can't directly instantiate this class: you must choose and instantiate an indicator between
 * DBIndicatorBannerView, DBIndicatorProgressView and DBIndicatorToastView.
 */

@interface DBIndicatorView : DBView

// MARK: Style setup

@property (nonatomic) DBIndicatorViewBackgroundStyle backgroundStyle;


// MARK: Behavior setup

/// When enabled indicators are displayed one at a time. Default: enabled.
@property (nonatomic) BOOL userInteraction;

/// When enabled indicators are displayed one at a time. Default: enabled.
@property (nonatomic) BOOL dismissesOtherIndicators;


// MARK: Presentation

- (void)showWithAnimation:(DBAnimationTransition)animation;

- (void)hideWithAnimation:(DBAnimationTransition)animation;

@end
