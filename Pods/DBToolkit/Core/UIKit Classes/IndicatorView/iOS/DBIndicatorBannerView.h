//
//  DBIndicatorBannerView.h
//  File version: 1.0.0 beta
//  Last modified: 04/30/2017
//
//  Created by Davide Balistreri on 04/30/2017
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

/**
 * IMPORTANT DISCLAIMER:
 * This is a preliminary version for a technology in development.
 * It is not final, and it is subject to change.
 * Newer versions may break compatibility of software implemented using this API.
 */

#import "DBIndicatorView.h"


// MARK: - Class define

/**
 * Banner view similar to iOS notification banners.
 *
 * It appears on top of the StatusBar.
 */
@interface DBIndicatorBannerView : DBIndicatorView

// MARK: Initialization

+ (instancetype)bannerViewWithTitle:(NSString *)title message:(NSString *)message icon:(UIImage *)icon;

+ (instancetype)localizedBannerViewWithTitle:(NSString *)title message:(NSString *)message icon:(UIImage *)icon;


// MARK: Content setup

@property (weak, nonatomic) UILabel *titleLabel;

@property (weak, nonatomic) UILabel *messageLabel;

@property (weak, nonatomic) UIImageView *imageView;


// MARK: Behavior setup

@property (nonatomic) NSTimeInterval duration;

@property (nonatomic) BOOL swipeToDismiss;

@end
