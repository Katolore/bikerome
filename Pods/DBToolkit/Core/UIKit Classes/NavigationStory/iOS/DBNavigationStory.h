//
//  DBNavigationStory.h
//  File version: 1.1.0
//  Last modified: 03/28/2017
//
//  Created by Davide Balistreri on 05/05/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

#import "DBObject.h"


/**
 * A NavigationStory its an object that represents a precise navigation stack of ViewControllers.
 *
 * It is used inside a NavigationStoryController.
 */

@interface DBNavigationStory : DBObject

/**
 * Creates a new NavigationStory.
 *
 * The Story will begin with the specified ViewController, encapsulated in a NavigationController if needed.
 * When pushed inside a NavigationStoryController, it will be retained in memory by using its identifier.
 *
 * If no identifier is specified, the class name will be used instead.
 */
+ (instancetype)navigationStoryWithViewController:(__kindof UIViewController *)viewController identifier:(NSString *)identifier;


/// A NavigationStory always begins with a NavigationController.
@property (strong, nonatomic, readonly) __kindof UINavigationController *navigationController;

/// The ViewController used to instantiate the NavigationStory.
@property (strong, nonatomic, readonly) __kindof UIViewController *rootViewController;

/// The unique identifier that distinguishes this Story from the others.
@property (strong, nonatomic, readonly) NSString *identifier;


/// The ViewControllers currently on the navigation stack of the NavigationStory.
@property (nonatomic, readonly) NSArray<__kindof UIViewController *> *viewControllers;

/// The ViewController on top of the navigation stack of the NavigationStory.
@property (nonatomic, readonly) __kindof UIViewController *presentedViewController;

@end


@interface DBNavigationStory (Deprecated)

+ (DBNavigationStory *)navigationStoryWithNavigationController:(UINavigationController *)navigationController
    DBFRAMEWORK_DEPRECATED(0.7.2, "please use +[DBNavigationStory navigationStoryWithViewController:identifier:]");

@end
