//
//  DBShareController.m
//  File version: 1.1.0
//  Last modified: 02/14/2016
//
//  Created by Davide Balistreri on 04/21/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBShareController.h"
#import "DBFramework.h"

#import <Social/Social.h>

#if DBFRAMEWORK_TARGET_IOS
#import <MessageUI/MessageUI.h>
#import <MobileCoreServices/MobileCoreServices.h>
#endif

@interface DBShareController ()
#if DBFRAMEWORK_TARGET_IOS
<MFMailComposeViewControllerDelegate>
#else
<NSSharingServiceDelegate>
#endif

@property (weak, nonatomic) UIViewController *rootViewController;
@property (copy) DBShareControllerCompletionBlock completionBlock;

@end


@implementation DBShareController

+ (BOOL)isMailDisponibile
{
#if DBFRAMEWORK_TARGET_IOS
    return [MFMailComposeViewController canSendMail];
#else
    NSSharingService *sharingService = [NSSharingService sharingServiceNamed:NSSharingServiceNameComposeEmail];
    return (sharingService) ? YES : NO;
#endif
}

+ (BOOL)isFacebookDisponibile
{
#if DBFRAMEWORK_TARGET_IOS
    return [SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook];
#else
    NSSharingService *sharingService = [NSSharingService sharingServiceNamed:NSSharingServiceNamePostOnFacebook];
    return (sharingService) ? YES : NO;
#endif
}

+ (BOOL)isTwitterDisponibile
{
#if DBFRAMEWORK_TARGET_IOS
    return [SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter];
#else
    NSSharingService *sharingService = [NSSharingService sharingServiceNamed:NSSharingServiceNamePostOnTwitter];
    return (sharingService) ? YES : NO;
#endif
}

+ (BOOL)isInstagramDisponibile
{
#if DBFRAMEWORK_TARGET_IOS
    NSURL *instagramURL = [NSURL URLWithString:@"instagram://app"];
    return [[UIApplication sharedApplication] canOpenURL:instagramURL];
#else
    return NO;
#endif
}

// MARK: - DBShareController

NSMutableArray *dbSharedShareControllers;
+ (DBShareController *)creaShareController
{
    @synchronized(self) {
        DBShareController *shareController = [DBShareController new];
        shareController.rootViewController = [DBApplication presentedViewController];
        
        if (!dbSharedShareControllers) {
            // Inizializzazione
            dbSharedShareControllers = [NSMutableArray new];
        }
        
        [dbSharedShareControllers addObject:shareController];
        
        return shareController;
    }
}

+ (void)rimuoviShareController:(DBShareController *)shareController
{
    @synchronized(self) {
        if (dbSharedShareControllers) {
            [dbSharedShareControllers removeObject:shareController];
        }
    }
}

// MARK: - Mail

+ (NSString *)determinaMimeTypeFile:(NSString *)nomeFile
{
    CFStringRef extensionRef = (__bridge CFStringRef)[nomeFile pathExtension];
    CFStringRef uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, extensionRef, NULL);
    
    CFStringRef mimeTypeRef = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType);
    CFRelease(uti);
    
    return CFBridgingRelease(mimeTypeRef);
}

+ (BOOL)creaMail
{
    return [self creaMailConOggetto:nil messaggio:nil isHTML:NO allegati:nil destinatari:nil suViewController:nil conCompletionBlock:nil];
}

+ (BOOL)creaMailConAllegati:(NSArray *)allegati
{
    return [self creaMailConOggetto:nil messaggio:nil isHTML:NO allegati:allegati destinatari:nil suViewController:nil conCompletionBlock:nil];
}

+ (BOOL)creaMailConDestinatari:(NSArray *)destinatari conOggetto:(NSString *)oggetto conMessaggio:(NSString *)messaggio isHTML:(BOOL)isHTML
{
    return [self creaMailConOggetto:oggetto messaggio:messaggio isHTML:isHTML allegati:nil destinatari:destinatari suViewController:nil conCompletionBlock:nil];
}

+ (BOOL)creaMailConDestinatari:(NSArray *)destinatari conOggetto:(NSString *)oggetto conMessaggio:(NSString *)messaggio isHTML:(BOOL)isHTML conAllegati:(NSArray *)allegati
{
    return [self creaMailConOggetto:oggetto messaggio:messaggio isHTML:isHTML allegati:allegati destinatari:destinatari suViewController:nil conCompletionBlock:nil];
}

+ (BOOL)creaMailConOggetto:(NSString *)oggetto messaggio:(NSString *)messaggio isHTML:(BOOL)isHTML allegati:(NSArray *)allegati destinatari:(NSArray *)destinatari suViewController:(UIViewController *)viewController
{
    return [self creaMailConOggetto:oggetto messaggio:messaggio isHTML:isHTML allegati:allegati destinatari:destinatari suViewController:viewController conCompletionBlock:nil];
}

+ (BOOL)creaMailConOggetto:(NSString *)oggetto messaggio:(NSString *)messaggio isHTML:(BOOL)isHTML allegati:(NSArray *)allegati destinatari:(NSArray *)destinatari suViewController:(UIViewController *)viewController conCompletionBlock:(DBShareControllerCompletionBlock)completionBlock
{
    // Creo lo ShareController
    DBShareController *shareController = [DBShareController creaShareController];
    if (viewController)  shareController.rootViewController = viewController;
    if (completionBlock) shareController.completionBlock = completionBlock;
    
    // Check disponibilità servizio
    if (![DBShareController isMailDisponibile]) {
        // Notifico il ViewController che non è stato mostrato lo ShareController
        [shareController notificaDelegateConResult:DBShareControllerResultFailure];
        return NO;
    }
    
#if DBFRAMEWORK_TARGET_IOS
    MFMailComposeViewController *mailComposer = [MFMailComposeViewController new];
    [mailComposer setSubject:oggetto];
    [mailComposer setMessageBody:messaggio isHTML:isHTML];
    [mailComposer setToRecipients:destinatari];
    [mailComposer setMailComposeDelegate:shareController];
    
    for (NSString *directoryAllegato in allegati) {
        NSURL *URL = [NSURL fileURLWithPath:directoryAllegato];
        NSData *data = [NSData dataWithContentsOfURL:URL];
        
        NSString *nomeFile = [directoryAllegato lastPathComponent];
        NSString *mimeType = [self determinaMimeTypeFile:nomeFile];
        
        [mailComposer addAttachmentData:data mimeType:mimeType fileName:nomeFile];
    }
    
    DBProgressHUD *progressHUD = [DBProgressHUD progressHUD];
    [shareController.rootViewController presentViewController:mailComposer animated:YES completion:^{
        [DBProgressHUD rimuoviProgressHUD:progressHUD];
    }];
    
    return YES;
#else
    NSMutableArray *items = [NSMutableArray array];
    [items safeAddObject:messaggio];
    [items addObjectsFromArray:allegati];
    
    NSSharingService *mailComposer = [NSSharingService sharingServiceNamed:NSSharingServiceNameComposeEmail];
    [mailComposer setSubject:oggetto];
    [mailComposer setRecipients:destinatari];
    [mailComposer setDelegate:shareController];
    [mailComposer performWithItems:items];
    
    return YES;
#endif
}

// MARK: - Mail Delegate

#if DBFRAMEWORK_TARGET_IOS
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultSent:
            [self chiudiShareControllerConResult:DBShareControllerResultSuccess];
            break;
            
        default:
            [self chiudiShareControllerConResult:DBShareControllerResultFailure];
            break;
    }
}
#else
- (void)sharingService:(NSSharingService *)sharingService didShareItems:(NSArray *)items
{
    [self chiudiShareControllerConResult:DBShareControllerResultSuccess];
}

- (void)sharingService:(NSSharingService *)sharingService didFailToShareItems:(NSArray *)items error:(NSError *)error
{
    [self chiudiShareControllerConResult:DBShareControllerResultFailure];
}
#endif

// MARK: - Facebook

+ (BOOL)creaFacebookPost
{
    return [self creaFacebookPostConTesto:nil URL:nil immagine:nil suViewController:nil conCompletionBlock:nil];
}

+ (BOOL)creaFacebookPostConTesto:(NSString *)testo URL:(NSURL *)URL immagine:(UIImage *)immagine
{
    return [self creaFacebookPostConTesto:testo URL:URL immagine:immagine suViewController:nil conCompletionBlock:nil];
}

+ (BOOL)creaFacebookPostConTesto:(NSString *)testo URL:(NSURL *)URL immagine:(UIImage *)immagine suViewController:(UIViewController *)viewController
{
    return [self creaFacebookPostConTesto:testo URL:URL immagine:immagine suViewController:viewController conCompletionBlock:nil];
}

+ (BOOL)creaFacebookPostConTesto:(NSString *)testo URL:(NSURL *)URL immagine:(UIImage *)immagine suViewController:(UIViewController *)viewController conCompletionBlock:(DBShareControllerCompletionBlock)completionBlock
{
    // Creo lo ShareController
    DBShareController *shareController = [DBShareController creaShareController];
    if (viewController) shareController.rootViewController = viewController;
    if (completionBlock) shareController.completionBlock = completionBlock;
    
    // Check disponibilità servizio
    if (![DBShareController isFacebookDisponibile]) {
        // Notifico il ViewController che non è stato mostrato lo ShareController
        [shareController notificaDelegateConResult:DBShareControllerResultFailure];
        return NO;
    }
    
#if DBFRAMEWORK_TARGET_IOS
    SLComposeViewController *composer = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    if (testo)    [composer setInitialText:testo];
    if (URL)      [composer addURL:URL];
    if (immagine) [composer addImage:immagine];
    
    [composer setCompletionHandler:^(SLComposeViewControllerResult result) {
        [shareController handleShareComposerCompletionResult:result];
    }];
    
    DBProgressHUD *progressHUD = [DBProgressHUD progressHUD];
    [shareController.rootViewController presentViewController:composer animated:YES completion:^{
        [DBProgressHUD rimuoviProgressHUD:progressHUD];
    }];
#endif
    
    return YES;
}

// MARK: - Twitter

+ (BOOL)creaTwitterPost
{
    return [self creaTwitterPostConTesto:nil URL:nil immagine:nil suViewController:nil conCompletionBlock:nil];
}

+ (BOOL)creaTwitterPostConTesto:(NSString *)testo URL:(NSURL *)URL immagine:(UIImage *)immagine
{
    return [self creaTwitterPostConTesto:testo URL:URL immagine:immagine suViewController:nil conCompletionBlock:nil];
}

+ (BOOL)creaTwitterPostConTesto:(NSString *)testo URL:(NSURL *)URL immagine:(UIImage *)immagine suViewController:(UIViewController *)viewController
{
    return [self creaTwitterPostConTesto:testo URL:URL immagine:immagine suViewController:viewController conCompletionBlock:nil];
}

+ (BOOL)creaTwitterPostConTesto:(NSString *)testo URL:(NSURL *)URL immagine:(UIImage *)immagine suViewController:(UIViewController *)viewController conCompletionBlock:(DBShareControllerCompletionBlock)completionBlock
{
    // Creo lo ShareController
    DBShareController *shareController = [DBShareController creaShareController];
    if (viewController) shareController.rootViewController = viewController;
    if (completionBlock) shareController.completionBlock = completionBlock;
    
    // Check disponibilità servizio
    if (![DBShareController isTwitterDisponibile]) {
        // Notifico il ViewController che non è stato mostrato lo ShareController
        [shareController notificaDelegateConResult:DBShareControllerResultFailure];
        return NO;
    }
    
#if DBFRAMEWORK_TARGET_IOS
    SLComposeViewController *composer = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    if (testo)    [composer setInitialText:testo];
    if (URL)      [composer addURL:URL];
    if (immagine) [composer addImage:immagine];
    
    [composer setCompletionHandler:^(SLComposeViewControllerResult result) {
        [shareController handleShareComposerCompletionResult:result];
    }];
    
    DBProgressHUD *progressHUD = [DBProgressHUD progressHUD];
    [shareController.rootViewController presentViewController:composer animated:YES completion:^{
        [DBProgressHUD rimuoviProgressHUD:progressHUD];
    }];
#endif
    
    return YES;
}

// MARK: - Delegate

#if DBFRAMEWORK_TARGET_IOS
- (void)handleShareComposerCompletionResult:(SLComposeViewControllerResult)result
{
    switch (result) {
        case SLComposeViewControllerResultDone:
            [self chiudiShareControllerConResult:DBShareControllerResultSuccess];
            break;
            
        default:
            [self chiudiShareControllerConResult:DBShareControllerResultFailure];
            break;
    }
}
#endif

- (void)notificaDelegateConResult:(DBShareControllerResult)result
{
    // Da fare meglio
    id delegate = (id)self.rootViewController;
    if ([delegate respondsToSelector:@selector(shareControllerDidFinishWithResult:)]) {
        [delegate shareControllerDidFinishWithResult:result];
    }
    
    if (self.completionBlock) {
        self.completionBlock(result);
    }
}

- (void)chiudiShareControllerConResult:(DBShareControllerResult)result
{
    [self notificaDelegateConResult:result];
    
#if DBFRAMEWORK_TARGET_IOS
    [self.rootViewController dismissViewControllerAnimated:YES completion:nil];
#endif
    
    [DBShareController rimuoviShareController:self];
}

@end
