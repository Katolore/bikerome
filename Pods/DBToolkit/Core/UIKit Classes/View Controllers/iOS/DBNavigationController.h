//
//  DBNavigationController.h
//  File version: 1.1.1
//  Last modified: 09/11/2016
//
//  Created by Davide Balistreri on 03/30/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

#if DBFRAMEWORK_VERSION_EARLIER_THAN(__DBFRAMEWORK_0_6_2)
    #warning Bisogna adattare il codice DBNavigationController alla nuova versione del Framework.
#endif


/**
 * NavigationController esteso seguendo i principi di funzionamento del DBViewController.
 * <p>Permette di gestire il Push&Pop in maniera più completa (es: CompletionBlock a fine operazione).</p>
 */
@interface DBNavigationController : UINavigationController

/**
 * Boolean che indica se la schermata è stata inizializzata completamente da quando è stata creata.
 *
 * Viene impostato a <b>YES</b> quando il ViewController ha terminato l'inizializzazione, è stato
 * localizzato e aggiornato, ed è visibile sullo schermo.
 */
@property (nonatomic, readonly) BOOL inizializzazioneCompletata;

/**
 * Metodo da utilizzare per inizializzare la schermata.
 *
 * Viene richiamato automaticamente quando il ViewController ha terminato l'inizializzazione.
 *
 * Deve essere eseguito un'unica volta.
 */
- (void)inizializzaSchermata;

/**
 * Metodo da utilizzare per impostare tutti i componenti della schermata:
 * ad esempio i frame delle subviews, o le immagini e i testi che non verranno cambiati.
 *
 * Viene richiamato automaticamente quando il ViewController sta per essere mostrato.
 *
 * Deve essere eseguito un'unica volta.
 */
- (void)impostaSchermata;

/**
 * Metodo per distruggere la schermata e le eventuali risorse attive.
 *
 * Viene richiamato automaticamente quando il ViewController ha iniziato la deallocazione.
 *
 * Deve essere eseguito un'unica volta.
 */
- (void)distruggiSchermata;

/**
 * Metodo da utilizzare per la localizzazione di tutti i testi della schermata.
 *
 * Viene richiamato automaticamente dopo l'esecuzione del metodo <b>impostaSchermata</b>,
 * e verrà richiamato ogni volta che verrà cambiata la lingua attarverso la classe <i>DBLocalization</i>,
 * per facilitare l'aggiornamento di tutti i testi con la nuova lingua impostata.
 */
- (void)localizzaSchermata;

/**
 * Metodo da utilizzare quando bisogna aggiornare i dati visualizzati sulla schermata, con o senza animazione.
 *
 * Viene richiamato automaticamente quando il ViewController ha terminato l'inizializzazione,
 * dopo l'esecuzione dei metodi <b>impostaSchermata</b> e <b>localizzaSchermata</b>.
 */
- (void)aggiornaSchermata:(BOOL)animazione;

/**
 * Metodo da utilizzare quando bisogna aggiornare i dati che saranno mostrati nella schermata: ad esempio
 * per scaricare nuove informazioni da un web service.
 */
- (void)aggiornaDataSource;


// MARK: - Gestione della navigazione estesa

/**
 * Block da eseguire al termine di un'operazione.
 */
typedef void (^DBNavigationControllerCompletionBlock)(void);

/**
 * Metodo esteso per poter richiamare un blocco di codice al termine dell'operazione.
 */
- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated conCompletionBlock:(DBNavigationControllerCompletionBlock)completionBlock;

/**
 * Metodo esteso per poter richiamare un blocco di codice al termine dell'operazione.
 */
- (NSArray<UIViewController *> *)popToRootViewControllerAnimated:(BOOL)animated conCompletionBlock:(DBNavigationControllerCompletionBlock)completionBlock;

/**
 * Metodo esteso per poter richiamare un blocco di codice al termine dell'operazione.
 */
- (NSArray<UIViewController *> *)popToViewController:(UIViewController *)viewController animated:(BOOL)animated conCompletionBlock:(DBNavigationControllerCompletionBlock)completionBlock;

/**
 * Metodo esteso per poter richiamare un blocco di codice al termine dell'operazione.
 */
- (UIViewController *)popViewControllerAnimated:(BOOL)animated conCompletionBlock:(DBNavigationControllerCompletionBlock)completionBlock;

/**
 * Chiude tutti i View Controller del Navigation Controller corrente anteponendo al primo un nuovo View Controller.
 */
- (NSArray<UIViewController *> *)popToNuovoRootViewController:(UIViewController *)rootViewController animated:(BOOL)animated;

/**
 * Metodo esteso per poter richiamare un blocco di codice al termine dell'operazione.
 */
- (NSArray<UIViewController *> *)popToNuovoRootViewController:(UIViewController *)rootViewController animated:(BOOL)animated conCompletionBlock:(DBNavigationControllerCompletionBlock)completionBlock;

@end
