//
//  DBPageController.m
//  File version: 1.0.1
//  Last modified: 12/02/2015
//
//  Created by Davide Balistreri on 11/30/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBPageController.h"

#import "DBFramework.h"

@interface DBPageController () <UIPageViewControllerDelegate, UIPageViewControllerDataSource, UIGestureRecognizerDelegate>

@property (strong, nonatomic) UIPageViewController *pageController;
@property (strong, nonatomic) UIPageControl *indicatorePagina;

@property (nonatomic) NSUInteger indexPendingMostraPagina;

@property (strong, nonatomic) UIGestureRecognizer *gestureRecognizer;
@property (weak, nonatomic) UIScrollView *scrollView;

@end


@implementation DBPageController

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self impostaPageController];
    }
    return self;
}

- (void)impostaPageController
{
    // Spazio tra le pagine
    NSDictionary *options = @{UIPageViewControllerOptionInterPageSpacingKey:[NSNumber numberWithDouble:20.0]};
    
    // Inizializzo il PageViewController
    self.pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:options];
    self.pageController.delegate = self;
    
    self.view = self.pageController.view;
    
    [self impostaGestureRecognizer];
    
    for (id subview in self.pageController.view.subviews) {
        if ([subview isKindOfClass:[UIScrollView class]]) {
            self.scrollView = subview;
        }
    }
    
    [self mostraViewControllerConIndex:self.indexPendingMostraPagina animazione:NO];
}

@synthesize delegate = _delegate;
- (void)setDelegate:(id<DBPageControllerDelegate>)delegate
{
    _delegate = delegate;
    [self impostaGestureRecognizer];
}

- (void)impostaGestureRecognizer
{
    // Se il Gesture Recognizer non è ancora stato inizializzato e le condizioni ci sono
    if (self.view.superview && self.delegate && !self.gestureRecognizer) {
        self.gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(azioneGestureRecognizer:)];
        self.gestureRecognizer.delegate = self;
        self.gestureRecognizer.cancelsTouchesInView = NO;
        [self.view addGestureRecognizer:self.gestureRecognizer];
    }
}

@synthesize viewControllers = _viewControllers;
- (void)setViewControllers:(NSArray *)viewControllers
{
    _viewControllers = viewControllers;
    
    if (self.isScorrimentoInvertito) {
        // Inverto l'ordine degli elementi nell'array
        _viewControllers = [viewControllers reverseObjectEnumerator].allObjects;
        self.selectedIndex = viewControllers.count - 1;
    }
    
    self.scrollView.bounces = (viewControllers.count < 2) ? NO : YES;
    
    if (viewControllers.count == 0) {
        self.pageController.dataSource = nil;
        return;
    }
    
    self.pageController.dataSource = self;
    
    [self aggiornaIndicatorePagina];
    
    // Seleziono il primo ViewController
    NSInteger indexPrimoViewController = (self.isScorrimentoInvertito) ? self.viewControllers.count - 1 : 0;
    UIViewController *viewController = [self viewControllerAtIndex:indexPrimoViewController];
    [self.pageController setViewControllers:@[viewController] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
}

@synthesize tintColor = _tintColor;
- (void)setTintColor:(UIColor *)tintColor
{
    _tintColor = tintColor;
    
    if (self.mostraIndicatorePagina) {
        self.indicatorePagina.currentPageIndicatorTintColor = self.tintColor;
    }
}

- (UIColor *)tintColor
{
    if (_tintColor) {
        return _tintColor;
    }
    else if ([DBUtility tintColor]) {
        return [DBUtility tintColor];
    }
    else {
        return [UIColor whiteColor];
    }
}

@synthesize mostraIndicatorePagina = _mostraIndicatorePagina;
- (void)setMostraIndicatorePagina:(BOOL)mostraIndicatorePagina
{
    _mostraIndicatorePagina = mostraIndicatorePagina;
    
    if (mostraIndicatorePagina && !self.indicatorePagina) {
        // Abilito il PageControl
        self.indicatorePagina = [UIPageControl new];
        [self.pageController.view addSubview:self.indicatorePagina];
        
        self.indicatorePagina.currentPageIndicatorTintColor = self.tintColor;
        self.indicatorePagina.hidesForSinglePage = YES;
        
        [self.indicatorePagina addTarget:self action:@selector(indicatorePaginaTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
        
        CGRect frameIndicatorePagina = CGRectZero;
        frameIndicatorePagina.size.height = 34.0;
        frameIndicatorePagina.size.width = self.view.bounds.size.width;
        frameIndicatorePagina.origin.x = 0.0;
        frameIndicatorePagina.origin.y = self.view.bounds.size.height - frameIndicatorePagina.size.height - 6.0; // Margine
        self.indicatorePagina.frame = frameIndicatorePagina;
        self.indicatorePagina.autoresizingMask = UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleWidth;
        
        [self aggiornaIndicatorePagina];
    }
    else if (!mostraIndicatorePagina && self.indicatorePagina) {
        // Rimuovo il PageControl
        [self.indicatorePagina removeFromSuperview];
        [self.indicatorePagina removeTarget:self action:@selector(indicatorePaginaTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
    }
}

@synthesize scorrimentoInvertito = _scorrimentoInvertito;
- (void)setScorrimentoInvertito:(BOOL)scorrimentoInvertito
{
    _scorrimentoInvertito = scorrimentoInvertito;
    
    // Aggiorno le posizioni dell'array di ViewControllers
    self.viewControllers = self.viewControllers;
}

- (void)aggiornaIndicatorePagina
{
    if (self.indicatorePagina) {
        self.indicatorePagina.numberOfPages = self.viewControllers.count;
        self.indicatorePagina.currentPage = self.selectedIndex;
    }
}

- (void)aggiungiViewController:(UIViewController *)viewController
{
    NSMutableArray *viewControllers = [NSMutableArray arrayWithArray:self.viewControllers];
    
    if (self.isScorrimentoInvertito) {
        // Inverto l'ordine degli elementi dell'array
        viewControllers = [[viewControllers reverseObjectEnumerator].allObjects mutableCopy];
    }
    
    [viewControllers addObject:viewController];
    self.viewControllers = viewControllers;
}

- (UIImageView *)aggiungiImageViewControllerConImmagine:(UIImage *)immagine
{
    UIViewController *viewController = [UIViewController new];
    viewController.view.frame = self.view.bounds;
    
    UIImageView *imageView = [UIImageView new];
    imageView.frame = self.view.bounds;
    imageView.autoresizingMask = DBViewAdattaDimensioni;
    imageView.image = immagine;
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.clipsToBounds = YES;
    [viewController.view addSubview:imageView];
    
    [self aggiungiViewController:viewController];
    
    return imageView;
}

- (void)rimuoviViewControllers
{
    self.viewControllers = [NSArray array];
}

@synthesize selectedIndex = _selectedIndex;
- (void)setSelectedIndex:(NSUInteger)selectedIndex
{
    _selectedIndex = selectedIndex;
    [self aggiornaIndicatorePagina];
}

- (void)indicatorePaginaTouchUpInside:(UIPageControl *)sender
{
    NSLog(@"Current Page: %d", (int)sender.currentPage);
    [self mostraViewControllerConIndex:sender.currentPage animazione:!self.isSenzaAnimazione];
}

- (void)mostraViewControllerConIndex:(NSUInteger)index animazione:(BOOL)animazione
{
    // Check
    if (self.viewControllers.count == 0) return;
    if (![self.viewControllers objectAtIndexExists:index]) return;
    
    NSUInteger trueIndex = index;
    if (self.isScorrimentoInvertito)
        trueIndex = self.viewControllers.count - 1 - trueIndex;
    
    UIPageViewControllerNavigationDirection direction = (self.selectedIndex < trueIndex) ? UIPageViewControllerNavigationDirectionForward : UIPageViewControllerNavigationDirectionReverse;
    
    UIViewController *viewController = [self viewControllerAtIndex:trueIndex];
    [self.pageController setViewControllers:@[viewController] direction:direction animated:animazione completion:nil];
    self.selectedIndex = trueIndex;
    
    if ([self.delegate respondsToSelector:@selector(pageControllerDidShowItem:atIndex:)])
        [self.delegate pageControllerDidShowItem:viewController atIndex:trueIndex];
}

// MARK: - Page view controller data source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSInteger currentIndex = [self.viewControllers indexOfObject:viewController];
    NSInteger nextIndex = currentIndex - 1;
    
    if (![self.viewControllers objectAtIndexExists:nextIndex]) return nil;
    return [self viewControllerAtIndex:nextIndex];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSInteger currentIndex = [self.viewControllers indexOfObject:viewController];
    NSInteger nextIndex = currentIndex + 1;
    
    if (![self.viewControllers objectAtIndexExists:nextIndex]) return nil;
    return [self viewControllerAtIndex:nextIndex];
}

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed
{
    UIViewController *current = [pageViewController.viewControllers firstObject];
    self.selectedIndex = [self.viewControllers indexOfObject:current];
    
    // Indice che tiene conto della storyboard invertita
    NSUInteger trueIndex = self.selectedIndex;
    if (self.isScorrimentoInvertito)
        trueIndex = self.viewControllers.count - 1 - trueIndex;
    
    if ([self.delegate respondsToSelector:@selector(pageControllerDidShowItem:atIndex:)])
        [self.delegate pageControllerDidShowItem:current atIndex:trueIndex];
}

- (UIViewController *)viewControllerAtIndex:(NSUInteger)index
{
    UIViewController *viewController = [self.viewControllers safeObjectAtIndex:index];
    viewController.view.frame = self.view.bounds;
    viewController.view.autoresizingMask = DBViewAdattaDimensioni;
    viewController.view.layer.masksToBounds = YES;
    return viewController;
}

- (void)azioneGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
{
    if (self.viewControllers.count == 0) return;
    id item = [self.viewControllers safeObjectAtIndex:self.selectedIndex];
    
    if ([self.delegate respondsToSelector:@selector(pageControllerDidSelectItem:atIndex:)])
        [self.delegate pageControllerDidSelectItem:item atIndex:self.selectedIndex];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

@end
