//
//  DBTabBarController.h
//  File version: 1.1.0
//  Last modified: 02/25/2016
//
//  Created by Davide Balistreri on 03/30/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

#if DBFRAMEWORK_VERSION_EARLIER_THAN(__DBFRAMEWORK_0_6_0)
    #warning Bisogna adattare il codice DBTabBarController alla nuova versione del Framework.
#endif


/**
 * TabBarController esteso seguendo i principi di funzionamento del DBViewController.
 */
@interface DBTabBarController : UITabBarController <UITabBarControllerDelegate>

/**
 * Boolean che indica se la schermata è stata inizializzata completamente da quando è stata creata.
 *
 * Viene impostato a <b>YES</b> quando il ViewController ha terminato l'inizializzazione, è stato
 * localizzato e aggiornato, ed è visibile sullo schermo.
 */
@property (nonatomic, readonly) BOOL inizializzazioneCompletata;

/**
 * Metodo da utilizzare per inizializzare la schermata.
 *
 * Viene richiamato automaticamente quando il ViewController ha terminato l'inizializzazione.
 *
 * Deve essere eseguito un'unica volta.
 */
- (void)inizializzaSchermata;

/**
 * Metodo da utilizzare per impostare tutti i componenti della schermata:
 * ad esempio i frame delle subviews, o le immagini e i testi che non verranno cambiati.
 *
 * Viene richiamato automaticamente quando il ViewController sta per essere mostrato.
 *
 * Deve essere eseguito un'unica volta.
 */
- (void)impostaSchermata;

/**
 * Metodo per distruggere la schermata e le eventuali risorse attive.
 *
 * Viene richiamato automaticamente quando il ViewController ha iniziato la deallocazione.
 *
 * Deve essere eseguito un'unica volta.
 */
- (void)distruggiSchermata;

/**
 * Metodo da utilizzare per la localizzazione di tutti i testi della schermata.
 *
 * Viene richiamato automaticamente dopo l'esecuzione del metodo <b>impostaSchermata</b>,
 * e verrà richiamato ogni volta che verrà cambiata la lingua attarverso la classe <i>DBLocalization</i>,
 * per facilitare l'aggiornamento di tutti i testi con la nuova lingua impostata.
 */
- (void)localizzaSchermata;

/**
 * Metodo da utilizzare quando bisogna aggiornare i dati visualizzati sulla schermata, con o senza animazione.
 *
 * Viene richiamato automaticamente quando il ViewController ha terminato l'inizializzazione,
 * dopo l'esecuzione dei metodi <b>impostaSchermata</b> e <b>localizzaSchermata</b>.
 */
- (void)aggiornaSchermata:(BOOL)animazione;

/**
 * Metodo da utilizzare quando bisogna aggiornare i dati che saranno mostrati nella schermata: ad esempio
 * per scaricare nuove informazioni da un web service.
 */
- (void)aggiornaDataSource;

@end
