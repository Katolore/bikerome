//
//  DBViewController.m
//  File version: 1.1.2
//  Last modified: 02/27/2016
//
//  Created by Davide Balistreri on 02/17/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBViewController.h"
#import "DBLocalization.h"

@interface DBViewController ()

@property (nonatomic, readonly) BOOL impostazioneInizialeCompletata;

@end


@implementation DBViewController

// MARK: - Override dei metodi di sistema

- (void)viewDidLoad
{
    // Il ViewController ancora non è stato inizializzato completamente
    [super viewDidLoad];
    
    // Eseguo i metodi implementati dallo sviluppatore
    [self privateInizializzaSchermata];
}

#if DBFRAMEWORK_TARGET_IOS
- (void)viewWillAppear:(BOOL)animated
{
    if (self.impostazioneInizialeCompletata == NO) {
        // L'inizializzazione del ViewController è quasi completata
        
        // Eseguo i metodi implementati dallo sviluppatore
        [self privateImpostaSchermata];
        [self privateLocalizzaSchermata];
        [self privateAggiornaSchermata:NO];
    }
    else {
        [self privateAggiornaSchermata:animated];
    }
    
    _impostazioneInizialeCompletata = YES;
    
    [super viewWillAppear:animated];
}
#else
- (void)viewWillAppear
{
    if (self.impostazioneInizialeCompletata == NO) {
        // L'inizializzazione del ViewController è quasi completata
        
        // Eseguo i metodi implementati dallo sviluppatore
        [self privateImpostaSchermata];
        [self privateLocalizzaSchermata];
        [self privateAggiornaSchermata:NO];
    }
    else {
        [self privateAggiornaSchermata:NO];
    }
    
    _impostazioneInizialeCompletata = YES;
    
    [super viewWillAppear];
}
#endif

#if DBFRAMEWORK_TARGET_IOS
- (void)viewDidAppear:(BOOL)animated
{
    // Inizializzazione del ViewController completata
    _inizializzazioneCompletata = YES;
    
    [super viewDidAppear:animated];
}
#else
- (void)viewDidAppear
{
    // Inizializzazione del ViewController completata
    _inizializzazioneCompletata = YES;
    
    [super viewDidAppear];
}
#endif

- (void)dealloc
{
    [self privateDistruggiSchermata];
}

// MARK: - Metodi privati

- (void)privateInizializzaSchermata
{
    if (self.impostazioneInizialeCompletata == NO) {
        // Ascolto le notifiche per i cambi di localizzazione
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(privateLocalizzaSchermata) name:kNotificaLinguaCorrenteAggiornata object:nil];
        
        // Perform the method implemented by the developer
        [self inizializzaSchermata];
    }
}

- (void)privateImpostaSchermata
{
    if (self.impostazioneInizialeCompletata == NO) {
        // Aggiorno i frame delle subviews
        [self.view layoutIfNeeded];
        
        // Prevengo la fuoriuscita dei contenuti dalla vista
        // self.view.clipsToBounds = YES;
        
        // Perform the method implemented by the developer
        [self impostaSchermata];
    }
}

- (void)privateDistruggiSchermata
{
    // Rimuovo il listener per i cambi di localizzazione
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificaLinguaCorrenteAggiornata object:nil];
    
    // Perform the method implemented by the developer
    [self distruggiSchermata];
}

- (void)privateLocalizzaSchermata
{
    // Perform the method implemented by the developer
    [self localizzaSchermata];
}

- (void)privateAggiornaSchermata:(BOOL)animazione
{
    // Perform the method implemented by the developer
    [self aggiornaSchermata:animazione];
}

// MARK: - Metodi da implementare dallo sviluppatore

- (void)inizializzaSchermata
{
    // Developers may override this method and provide custom behavior
}

- (void)impostaSchermata
{
    // Developers may override this method and provide custom behavior
}

- (void)distruggiSchermata
{
    // Developers may override this method and provide custom behavior
}

- (void)localizzaSchermata
{
    // Developers may override this method and provide custom behavior
}

- (void)aggiornaSchermata:(BOOL)animazione
{
    // Developers may override this method and provide custom behavior
}

- (void)aggiornaDataSource
{
    // Developers may override this method and provide custom behavior
}

@end
