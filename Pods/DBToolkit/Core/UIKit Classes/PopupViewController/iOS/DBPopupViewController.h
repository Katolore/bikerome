//
//  DBPopupViewController.h
//  File version: 1.0.0
//  Last modified: 11/15/2016
//
//  Created by Davide Balistreri on 09/24/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

/**
 * IMPORTANT DISCLAIMER:
 * This is a preliminary version for a technology in development.
 * It is not final, and it is subject to change.
 * Newer versions may break compatibility of software implemented using this API.
 */


#import "DBFoundation.h"

#import "DBAnimation.h"

@protocol DBPopupViewControllerDelegate;


// MARK: - PopupViewController declaration

@interface DBPopupViewController : NSObject

NS_ASSUME_NONNULL_BEGIN

// MARK: Initialization

/**
 * Designed initializer.
 *
 * This creates a new PopupViewController that will host the specified view controller.
 *
 * After the creation you may need to setup graphics and behaviors of the PopupViewController.
 *
 * When everything is set, present it from any view controller by calling:
 * -[UIViewController presentPopupViewController:animated:completionBlock].
 *
 * When you're done with it, you can dismiss either from the view controller presented,
 * or from the presenting view controller, by calling:
 * -[UIViewController dismissPopupViewControllerAnimated:completionBlock].
 */
+ (DBPopupViewController *)popupWithViewController:(UIViewController *)viewController
    NS_SWIFT_NAME(popup(withViewController:));

/**
 * This creates a new PopupViewController that will host the specified view controller.
 *
 * After the creation you may need to setup graphics and behaviors of the PopupViewController.
 *
 * When everything is set, present it from any view controller by calling:
 * -[UIViewController presentPopupViewController:animated:completionBlock].
 *
 * When you're done with it, you can dismiss either from the view controller presented,
 * or from the presenting view controller, by calling:
 * -[UIViewController dismissPopupViewControllerAnimated:completionBlock].
 */
- (void)initWithViewController:(UIViewController *)viewController;


/// The view controller whose view is displayed in the popup view.
@property (strong, nonatomic, nullable) UIViewController *viewController;

/// The delegate class which will be informed about the popup state changes.
@property (nonatomic, nullable) id<DBPopupViewControllerDelegate> delegate;


// MARK: Defines

/// Generic completion block.
typedef void (^DBPopupViewControllerCompletionBlock)(void);


// MARK: Style setup

/**
 * The PopupViewController background view: use this property to change its style as you want.
 *
 * Remember that if you want to change the look of the content view controller,
 * for example by adding rounded corners and shadows, you can apply these effects
 * directly to the view controller which you used to create the PopupViewController.
 *
 * Default: dark semi-transparent background color.
 */
@property (strong, nonatomic, readonly) UIView *backgroundView;


// MARK: Size setup

// Some layout size constants

/// The popup will fill the size of the window, respecting the contentInset, and compress its width if needed.
extern const CGSize DBPopupViewControllerCompressWidth;
/// The popup will fill the size of the window, respecting the contentInset, and compress its height if needed.
extern const CGSize DBPopupViewControllerCompressHeight;
/// The popup will fill the size of the window, respecting the contentInset, and compress both width and height if needed.
extern const CGSize DBPopupViewControllerCompressSize;
/// The popup will fill the size of the window, respecting the contentInset.
extern const CGSize DBPopupViewControllerExpandSize;

/**
 * Use this property to setup the behavior of the popup view.
 *
 * Default: DBPopupViewControllerExpandSize
 *
 * Note: if the layout of the popup view doesn't respect this size, please review your view constraints.
 * You may need to lower the priority of those at the edges.
 * If the issue still persist check that you don't change the view frame programmatically,
 * as it will break autolayout rules.
 */
@property (nonatomic) CGSize contentSize;

/**
 * The padding from the edges of the device screen.
 * The content size of the popup view will never exceed this padding.
 *
 * Default: 20, 20, 20, 20
 */
@property (nonatomic) UIEdgeInsets contentInsets;


// MARK: Behavior setup

/**
 * If enabled, touching the blank padding area will dismiss the popup.
 *
 * Default: disabled.
 */
@property (nonatomic) BOOL touchOutsideDismissesPopup;

NS_ASSUME_NONNULL_END

@end


// MARK: - ViewController extension

@interface UIViewController (DBPopupViewController)

NS_ASSUME_NONNULL_BEGIN

/**
 * Use this to present a popup view controller.
 *
 * You can call this selector from any view controller: only the popup view controller and
 * its presenting view controller may dismiss a presented popup.
 */
- (void)presentPopupViewController:(DBPopupViewController *)popupViewController withTransition:(DBAnimationTransition)transition completionBlock:(nullable DBPopupViewControllerCompletionBlock)completionBlock;

/**
 * Use this to dismiss a presented popup view controller.
 *
 * You can call this selector from the popup view controller or from its presenting view controller.
 */
- (void)dismissPopupViewControllerWithTransition:(DBAnimationTransition)transition completionBlock:(nullable DBPopupViewControllerCompletionBlock)completionBlock;

NS_ASSUME_NONNULL_END

@end


// MARK: - Protocol declaration

@protocol DBPopupViewControllerDelegate <NSObject>

@optional

NS_ASSUME_NONNULL_BEGIN

- (void)popupViewControllerWillAppear:(DBPopupViewController *)popupViewController withAnimation:(DBAnimation *)animation;
- (void)popupViewControllerDidAppear:(DBPopupViewController *)popupViewController;

- (void)popupViewControllerWillDisappear:(DBPopupViewController *)popupViewController withAnimation:(DBAnimation *)animation;
- (void)popupViewControllerDidDisappear:(DBPopupViewController *)popupViewController;

NS_ASSUME_NONNULL_END

@end
