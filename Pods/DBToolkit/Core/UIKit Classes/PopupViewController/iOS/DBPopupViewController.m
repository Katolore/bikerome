//
//  DBPopupViewController.m
//  File version: 1.0.0
//  Last modified: 11/15/2016
//
//  Created by Davide Balistreri on 09/24/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBPopupViewController.h"

#import "DBAnimation.h"
#import "DBGeometry.h"
#import "DBColorExtensions.h"
#import "DBViewExtensions.h"


// MARK: - DBPopupViewController class implementation

@interface DBPopupViewController () <UIGestureRecognizerDelegate>

/// The view controller that presented this popup view controller.
@property (strong, nonatomic) UIViewController *presentingViewController;

/// The window that hosts the popup view, created at status bar level.
@property (strong, nonatomic) UIWindow *window;

/// Constraints added to the content view.
@property (strong, nonatomic) NSArray<NSLayoutConstraint *> *constraints;

@property (nonatomic) BOOL isPresented;

@end


@implementation DBPopupViewController

+ (NSMutableArray *)sharedInstances
{
    @synchronized(self) {
        NSMutableArray *sharedInstances = [DBCentralManager getRisorsa:@"DBSharedPopupViewControllers"];
        
        if (!sharedInstances) {
            sharedInstances = [NSMutableArray array];
            [DBCentralManager setRisorsa:sharedInstances conIdentifier:@"DBSharedPopupViewControllers"];
        }
        
        return sharedInstances;
    }
}

// MARK: - Initialization

+ (instancetype)popupWithViewController:(UIViewController *)viewController
{
    DBPopupViewController *instance = [DBPopupViewController new];
    [instance setViewController:viewController];
    return instance;
}

- (void)initWithViewController:(UIViewController *)viewController
{
    [self setViewController:viewController];
}

// MARK: - Defines

const CGFloat DBPopupViewControllerCompress = 12345;
const CGFloat DBPopupViewControllerExpand   = 56789;

const CGSize DBPopupViewControllerCompressHeight = {DBPopupViewControllerExpand,   DBPopupViewControllerCompress};
const CGSize DBPopupViewControllerCompressWidth  = {DBPopupViewControllerCompress, DBPopupViewControllerExpand};
const CGSize DBPopupViewControllerCompressSize   = {DBPopupViewControllerCompress, DBPopupViewControllerCompress};
const CGSize DBPopupViewControllerExpandSize     = {DBPopupViewControllerExpand,   DBPopupViewControllerExpand};

// MARK: - Private setup

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setupDefaults];
        [self basicSetup];
    }
    return self;
}

- (void)setupDefaults
{
    _contentSize = DBPopupViewControllerExpandSize;
    _contentInsets = UIEdgeInsetsMake(20.0, 20.0, 20.0, 20.0);
}

- (void)basicSetup
{
    // Popup window
    UIWindow *window = [UIWindow new];
    window.frame = [[UIScreen mainScreen] bounds];
    window.backgroundColor = [UIColor clearColor];
    window.windowLevel = UIWindowLevelStatusBar;
    self.window = window;
    
    // Window root view controller
    UIViewController *rootViewController = [UIViewController new];
    UIView *superview = rootViewController.view;
    // The next line breaks the layout: do not use it!
    // superview.translatesAutoresizingMaskIntoConstraints = NO;
    superview.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.8];
    window.rootViewController = rootViewController;
    
    // Gesture recognizer (root view controller)
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapRecognizer:)];
    [tapRecognizer setDelegate:self];
    [superview addGestureRecognizer:tapRecognizer];
}

- (void)updateAutolayout
{
    UIWindow *window = self.window;
    UIViewController *rootViewController = window.rootViewController;
    UIView *superview = rootViewController.view;
    UIViewController *viewController = self.viewController;
    UIView *view = viewController.view;
    
    if (!viewController || !view || !superview || !self.isPresented) {
        // Not valid
        return;
    }
    
    CGFloat insetWidth  = self.contentInsets.left + self.contentInsets.right;
    CGFloat insetHeight = self.contentInsets.top + self.contentInsets.bottom;
    
    // Remove the old constraints
    [superview removeConstraints:self.constraints];
    
    NSMutableArray *constraints = [NSMutableArray array];
    
    // Centered layout
    NSLayoutConstraint *centerX = [NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:superview attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0];
    NSLayoutConstraint *centerY = [NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:superview attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0];
    
    // Maximum size
    NSLayoutConstraint *maxWidth = [NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationLessThanOrEqual toItem:superview attribute:NSLayoutAttributeWidth multiplier:1.0 constant:-insetWidth];
    NSLayoutConstraint *maxHeight = [NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationLessThanOrEqual toItem:superview attribute:NSLayoutAttributeHeight multiplier:1.0 constant:-insetHeight];
    
    [constraints addObjectsFromArray:@[centerX, centerY, maxWidth, maxHeight]];
    
    // Minimum or exact width
    if (self.contentSize.width <= 0.0) {
        // Not valid
    }
    else if (self.contentSize.width == DBPopupViewControllerCompress) {
        // Already setup
    }
    else if (self.contentSize.width == DBPopupViewControllerExpand) {
        // Match the superview
        NSLayoutConstraint *minWidth = [NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:superview attribute:NSLayoutAttributeWidth multiplier:1.0 constant:-insetWidth];
        minWidth.priority = 900;
        [constraints addObject:minWidth];
    }
    else {
        // User defined
        NSLayoutConstraint *minWidth = [NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:self.contentSize.width];
        minWidth.priority = 900;
        [constraints addObject:minWidth];
    }
    
    // Minimum or exact height
    if (self.contentSize.height <= 0.0) {
        // Not valid
    }
    else if (self.contentSize.height == DBPopupViewControllerCompress) {
        // Already setup
    }
    else if (self.contentSize.height == DBPopupViewControllerExpand) {
        // Match the superview
        NSLayoutConstraint *minHeight = [NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:superview attribute:NSLayoutAttributeHeight multiplier:1.0 constant:-insetHeight];
        minHeight.priority = 900;
        [constraints addObject:minHeight];
    }
    else {
        // User defined
        NSLayoutConstraint *minHeight = [NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:self.contentSize.height];
        minHeight.priority = 900;
        [constraints addObject:minHeight];
    }
    
    // Add the new constraints
    [superview addConstraints:constraints];
    self.constraints = constraints;
    
    [view setNeedsLayout];
    [view layoutIfNeeded];
}


// MARK: - Setup

@synthesize backgroundView = _backgroundView;
@synthesize contentSize = _contentSize;
@synthesize contentInsets = _contentInsets;
@synthesize touchOutsideDismissesPopup = _touchOutsideDismissesPopup;

- (void)setContentSize:(CGSize)contentSize
{
    _contentSize = contentSize;
    [self updateAutolayout];
}

- (void)setContentInsets:(UIEdgeInsets)contentInsets
{
    _contentInsets = contentInsets;
    [self updateAutolayout];
}

- (UIView *)backgroundView
{
    UIWindow *window = self.window;
    UIViewController *rootViewController = window.rootViewController;
    UIView *superview = rootViewController.view;
    return superview;
}


// MARK: - Control

- (void)presentWithTransition:(DBAnimationTransition)transition completionBlock:(DBPopupViewControllerCompletionBlock)completionBlock
{
    self.isPresented = YES;
    
    UIWindow *window = self.window;
    UIViewController *rootViewController = window.rootViewController;
    UIView *superview = rootViewController.view;
    
    // Content view controller
    UIViewController *viewController = self.viewController;
    UIView *view = viewController.view;
    view.translatesAutoresizingMaskIntoConstraints = NO;
    
    [superview addSubview:view];
    [viewController willMoveToParentViewController:rootViewController];
    [rootViewController addChildViewController:viewController];
    [viewController didMoveToParentViewController:rootViewController];
    
    // Layout
    [self updateAutolayout];
    
    // Animation
    DBAnimation *animation = [DBAnimation new];
    animation.duration = (transition == DBAnimationTransitionNone) ? 0.0 : 0.6;
    animation.userInteractionEnabled = YES;
    animation.useSpringEffect = YES;
    
    if ([self.delegate respondsToSelector:@selector(popupViewControllerWillAppear:withAnimation:)]) {
        [self.delegate popupViewControllerWillAppear:self withAnimation:animation];
    }
    
    [animation setInitialState:^{
        [window makeKeyAndVisible];
        window.alpha = 0.0;
        
        if (transition == DBAnimationTransitionZoom) {
            view.transform = CGAffineTransformMakeScale(0.9, 0.9);
        }
    }];
    
    [animation setFinalState:^{
        window.alpha = 1.0;
        
        if (transition == DBAnimationTransitionZoom) {
            view.transform = CGAffineTransformIdentity;
        }
    }];
    
    [animation setCompletionBlock:^{
        if (completionBlock) {
            completionBlock();
        }
        
        if ([self.delegate respondsToSelector:@selector(popupViewControllerDidAppear:)]) {
            [self.delegate popupViewControllerDidAppear:self];
        }
    }];
    
    [animation start];
    
    // Instance memory persist
    NSMutableArray *sharedInstances = [DBPopupViewController sharedInstances];
    [sharedInstances addObject:self];
}

- (void)dismissWithTransition:(DBAnimationTransition)transition completionBlock:(DBPopupViewControllerCompletionBlock)completionBlock
{
    self.isPresented = NO;
    
    UIWindow *window = self.window;
    UIViewController *viewController = self.viewController;
    UIView *view = viewController.view;
    
    // Animation
    DBAnimation *animation = [DBAnimation new];
    animation.duration = (transition == DBAnimationTransitionNone) ? 0.0 : 0.3;
    animation.userInteractionEnabled = YES;
    
    if ([self.delegate respondsToSelector:@selector(popupViewControllerWillDisappear:withAnimation:)]) {
        [self.delegate popupViewControllerWillDisappear:self withAnimation:animation];
    }
    
    [animation setFinalState:^{
        window.alpha = 0.0;
        
        if (transition == DBAnimationTransitionZoom) {
            view.transform = CGAffineTransformMakeScale(0.9, 0.9);
        }
    }];
    
    [animation setCompletionBlock:^{
        [viewController willMoveToParentViewController:nil];
        [viewController removeFromParentViewController];
        [viewController didMoveToParentViewController:nil];
        
        window.hidden = YES;
        
        // Instance memory removal
        NSMutableArray *sharedInstances = [DBPopupViewController sharedInstances];
        [sharedInstances removeObject:self];
        
        if (completionBlock) {
            completionBlock();
        }
        
        if ([self.delegate respondsToSelector:@selector(popupViewControllerDidDisappear:)]) {
            [self.delegate popupViewControllerDidDisappear:self];
        }
    }];
    
    [animation start];
}

// MARK: - GestureRecognizer delegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    // Accetto solo tocchi sulla view del root view controller
    return ([touch.view isEqual:gestureRecognizer.view]) ? YES : NO;
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    // Accetto il riconoscimento solo se il flag è attivo
    return self.touchOutsideDismissesPopup;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    // Di default lascio eseguire l'azione all'altro gesture recognizer
    return NO;
}

- (void)tapRecognizer:(UITapGestureRecognizer *)tapRecognizer
{
    // Tocco riconosciuto: chiudo il popup
    [self dismissWithTransition:DBAnimationTransitionFade completionBlock:nil];
}

@end


@implementation UIViewController (DBPopupViewController)

- (void)presentPopupViewController:(DBPopupViewController *)popupViewController withTransition:(DBAnimationTransition)transition completionBlock:(DBPopupViewControllerCompletionBlock)completionBlock
{
    popupViewController.presentingViewController = self;
    
    [popupViewController presentWithTransition:transition completionBlock:^{
        if (completionBlock) {
            completionBlock();
        }
    }];
}

- (void)dismissPopupViewControllerWithTransition:(DBAnimationTransition)transition completionBlock:(DBPopupViewControllerCompletionBlock)completionBlock
{
    NSMutableArray *sharedInstances = [DBPopupViewController sharedInstances];
    
    for (DBPopupViewController *popupViewController in sharedInstances) {
        if ([popupViewController.presentingViewController isEqual:self]) {
            // The view controller that presented the popup is asking to dismiss it
            [popupViewController dismissWithTransition:transition completionBlock:completionBlock];
            break;
        }
        
        if ([popupViewController.viewController isEqual:self]) {
            // The popup view controller is asking to be dismissed
            [popupViewController dismissWithTransition:transition completionBlock:completionBlock];
            break;
        }
        
        if ([popupViewController.viewController isEqual:self.navigationController]) {
            // The popup view controller is a navigation controller
            // and one of its child view controllers is asking to dismiss it
            [popupViewController dismissWithTransition:transition completionBlock:completionBlock];
            break;
        }
    }
}

@end
