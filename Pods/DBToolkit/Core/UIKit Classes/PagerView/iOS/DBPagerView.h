//
//  DBPagerView.h
//  File version: 1.0.0 beta
//  Last modified: 03/20/2018
//
//  Created by Davide Balistreri on 08/03/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

/**
 * IMPORTANT DISCLAIMER:
 * This is a preliminary version for a technology in development.
 * It is not final, and it is subject to change.
 * Newer versions may break compatibility of software implemented using this API.
 */

#import "DBView.h"

// Delegate protocol and other stuff
#import "DBPagerViewDefines.h"

#import "DBPagerViewTabItem.h"
#import "DBPagerViewPageIndicator.h"
#import "DBPagerViewPage.h"


// MARK: - Class define

NS_ASSUME_NONNULL_BEGIN

/**
 * API DESCRIPTION:
 * Use this class when you need to present content ---
 *
 *
 */
@interface DBPagerView : DBView

// MARK: - Setup

/// Delegate
@property (weak, nonatomic, nullable) id<DBPagerViewDelegate> delegate;

/// Data Source
@property (strong, nonatomic) NSArray<DBPagerViewPage *> *pages;

- (void)removeAllPages;

- (void)reloadData;


@property (weak, nonatomic) DBPagerViewPage *presentedPage;
- (void)setPresentedPage:(DBPagerViewPage *)page animated:(BOOL)animated;

@property (nonatomic) NSInteger presentedPageIndex;
- (void)setPresentedPageIndex:(NSInteger)index animated:(BOOL)animated;

- (void)gotoPreviousPage:(BOOL)animated;

- (void)gotoNextPage:(BOOL)animated;


// MARK: - Graphic setup

/**
 * The tint color for selected/focused tab items.
 * It's used to style tab items and page indicator, if present.
 */
@property (strong, nonatomic) UIColor *selectedColor;

/**
 * The tint color for unselected tab items.
 * It's used to style tab items and page indicator, if present.
 */
@property (strong, nonatomic) UIColor *unselectedColor;

/**
 * The color of the focused page indicator.
 * It's used to style the page indicator, if present.
 */
@property (strong, nonatomic) UIColor *pageIndicatorColor;

/**
 * The font of the text.
 * It's used to style the tab items, if present.
 */
@property (strong, nonatomic) UIFont *font;


@property (nonatomic) BOOL animationsEnabled; // Default: YES
@property (nonatomic) BOOL scrollEnabled; // Default: YES
@property (nonatomic) BOOL bounces; // Default: NO

@property (nonatomic) DBPagerViewLayoutDirection layoutDirection;

// Tab view
@property (nonatomic) UIEdgeInsets                 tabViewInset;
@property (nonatomic) DBPagerViewLayoutHeight      tabViewHeight; // Default: 50px
@property (nonatomic) DBPagerViewTabViewPosition   tabViewPosition;
@property (nonatomic, getter=isTabViewHidden) BOOL tabViewHidden;

// Tab view items
@property (nonatomic) DBPagerViewTabItemStyle tabItemStyle;

// Page indicator
@property (nonatomic) DBPagerViewLayoutHeight pageIndicatorHeight; // Default: DBPagerViewLayoutHeightAutomatic
@property (nonatomic) DBPagerViewPageIndicatorPosition pageIndicatorPosition;
@property (nonatomic, getter=isPageIndicatorHidden) BOOL pageIndicatorHidden;

// Content view
@property (nonatomic) UIEdgeInsets                     contentViewInset;
@property (nonatomic, getter=isContentViewHidden) BOOL contentViewHidden;


/**
 * The view used as page indicator.
 *
 * You can use some of the default indicators provided in this bundle,
 * or subclass DBPagerViewPageIndicator and create yours.
 *
 * Default: DBPagerViewLinePageIndicator
 */
@property (strong, nonatomic) __kindof DBPagerViewPageIndicator *pageIndicator;

@end


@interface DBPagerView (Extended)

// Methods extended from DBPagerViewPage (to simplify page creation and presentation)

- (nullable DBPagerViewPage *)addPageWithView:(nullable __kindof UIView *)view withTitle:(nullable NSString *)title;

- (nullable DBPagerViewPage *)addPageWithView:(nullable __kindof UIView *)view withLocalizedTitle:(nullable NSString *)localizedTitle;

- (nullable DBPagerViewPage *)addPageWithViewController:(nullable __kindof UIViewController *)viewController;

@end

NS_ASSUME_NONNULL_END
