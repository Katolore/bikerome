//
//  DBPagerViewPageIndicator.m
//  File version: 1.0.0 beta
//  Last modified: 10/04/2016
//
//  Created by Davide Balistreri on 10/03/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

/**
 * IMPORTANT DISCLAIMER:
 * This is a preliminary version for a technology in development.
 * It is not final, and it is subject to change.
 * Newer versions may break compatibility of software implemented using this API.
 */

#import "DBPagerViewPageIndicator.h"

#import "DBPagerView.h"
#import "DBPagerViewPage.h"

#import "DBArrayExtensions.h"
#import "DBObjectExtensions.h"
#import "DBGeometry.h"


// MARK: - DBPagerViewPageIndicator define

@implementation DBPagerViewPageIndicator

+ (instancetype)indicator
{
    return [[self class] new];
}

- (void)setupObject
{
    [super setupObject];
    self.translatesAutoresizingMaskIntoConstraints = NO;
}

- (DBPagerViewPageIndicatorPosition)preferredPosition
{
    // Override this getter to change the default position of your custom page indicator
    // If you want, you can query all parent pager view properties and react to other elements position
    
    // Default
    return DBPagerViewPageIndicatorPositionNearTabView;
}

- (void)reloadLayout
{
    // Implementare nelle classi che ereditano da questa
}

- (void)updateLayoutWithFocusedPageOffset:(CGFloat)currentPageOffset
{
    // Implementare nelle classi che ereditano da questa
}

@end


// MARK: - DBPagerViewLinePageIndicator define

@implementation DBPagerViewLinePageIndicator

- (void)setupObject
{
    [super setupObject];
    
    self.lineView = [UIView new];
    self.lineView.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:self.lineView];
    
    // Constraints
    UIView *lineView = self.lineView;
    NSDictionary *views = NSDictionaryOfVariableBindings(lineView);
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[lineView(10)]" options:0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[lineView(3@50)]|" options:0 metrics:nil views:views]];
}

- (void)reloadLayout
{
    self.backgroundColor = self.parentPagerView.backgroundColor;
    self.lineView.backgroundColor = self.indicatorColor;
}

- (DBPagerViewPageIndicatorPosition)preferredPosition
{
    if (self.parentPagerView.isTabViewHidden == NO) {
        return DBPagerViewPageIndicatorPositionInsideTabView;
    }
    else if (self.parentPagerView.tabViewPosition == DBPagerViewTabViewPositionTop) {
        return DBPagerViewPageIndicatorPositionTop;
    }
    else {
        return DBPagerViewPageIndicatorPositionBottom;
    }
}

- (void)updateLayoutWithCurrentPageOffset:(CGFloat)currentPageOffset
{
    if (!self.parentPagerView || [NSArray isEmpty:self.parentPagerView.pages]) {
        // Not valid
        return;
    }
    
    if (self.parentPagerView.isTabViewHidden) {
        [self updateLayoutUsingFullWidthWithCurrentPageOffset:currentPageOffset];
    }
    else {
        switch (self.parentPagerView.pageIndicatorPosition) {
            case DBPagerViewPageIndicatorPositionAutomatic:
            case DBPagerViewPageIndicatorPositionInsideTabView:
            case DBPagerViewPageIndicatorPositionNearTabView:
                [self updateLayoutUsingRealWidthWithCurrentPageOffset:currentPageOffset];
                break;
            default:
                [self updateLayoutUsingFullWidthWithCurrentPageOffset:currentPageOffset];
                break;
        }
    }
}

- (void)updateLayoutUsingFullWidthWithCurrentPageOffset:(CGFloat)currentPageOffset
{
    NSArray *pages = self.parentPagerView.pages;
    
    NSInteger currentPageIndex = floor(currentPageOffset);
    currentPageIndex = mantieniAllInterno(currentPageIndex, 0, pages.count - 1);
    
    CGFloat focusPercentage = fmod(currentPageOffset, 1.0);
    
    // Actual size
    CGSize size = CGSizeZero;
    size.width = (self.frame.size.width - self.contentInset.left - self.contentInset.right) / pages.count;
    
    // Current page origin
    CGPoint currentPageOrigin = CGPointZero;
    currentPageOrigin.x = size.width * currentPageIndex;
    
    // Actual origin
    CGPoint origin = CGPointZero;
    origin.x = currentPageOrigin.x + (size.width * focusPercentage);
    
    self.lineView.constraintLeading = origin.x;
    self.lineView.constraintLarghezza = size.width;
    [self.lineView layoutIfNeeded];
}

- (void)updateLayoutUsingRealWidthWithCurrentPageOffset:(CGFloat)currentPageOffset
{
    NSArray *pages = self.parentPagerView.pages;
    
    NSInteger currentPageIndex = floor(currentPageOffset);
    NSInteger nextPageIndex = currentPageIndex + 1;
    
    // Current page origin
    CGPoint currentPageOrigin = CGPointZero;
    
    for (NSInteger counter = 0; counter < pages.count; counter++) {
        DBPagerViewPage *page = [pages safeObjectAtIndex:counter];
        
        if (counter < currentPageIndex) {
            // Current page not reached yet
            currentPageOrigin.x += page.tabItem.displaySize.width;
        }
        else if (counter >= currentPageIndex) {
            // Current page origin calculated
            break;
        }
    }
    
    DBPagerViewPage *currentPage = [pages safeObjectAtIndex:currentPageIndex];
    DBPagerViewPage *nextPage = [pages safeObjectAtIndex:nextPageIndex];
    
    CGFloat focusPercentage = (nextPage) ? nextPage.focusPercentage : 1.0 - currentPage.focusPercentage;
    
    // Size calculation
    CGSize currentPageSize = currentPage.tabItem.displaySize;
    CGSize nextPageSize = nextPage.tabItem.displaySize;
    
    CGSize sizeDifference = CGSizeZero;
    sizeDifference.width = currentPageSize.width - nextPageSize.width;
    
    // Actual origin
    CGPoint origin = CGPointZero;
    origin.x = currentPageOrigin.x + (currentPageSize.width * focusPercentage);
    
    // Actual size
    CGSize size = CGSizeZero;
    size.width  = currentPageSize.width - (sizeDifference.width * focusPercentage);
    
    // Debug: NSLog(@"^ CurrentPage %d Width: %.2f  NextPage %d Width: %.2f", currentPageIndex, currentPageSize.width, nextPageIndex, nextPageSize.width);
    // Debug: NSLog(@"^ Origin: %.2f  SizeDiff: %.2f  Size: %.2f", origin.x, sizeDifference.width, size.width);
    
    self.lineView.constraintLeading = origin.x;
    self.lineView.constraintLarghezza = size.width;
    [self.lineView layoutIfNeeded];
}

@synthesize indicatorColor = _indicatorColor;
- (void)setIndicatorColor:(UIColor *)indicatorColor
{
    _indicatorColor = indicatorColor;
    self.lineView.backgroundColor = self.indicatorColor;
}

- (UIColor *)indicatorColor
{
    UIColor *color = (_indicatorColor) ? _indicatorColor : self.parentPagerView.pageIndicatorColor;
    return color;
}

@end


// MARK: - DBPagerViewDotsPageIndicator define

@implementation DBPagerViewDotsPageIndicator

- (void)setupObject
{
    [super setupObject];
    
    self.pageControl = [UIPageControl new];
    self.pageControl.translatesAutoresizingMaskIntoConstraints = NO;
    self.pageControl.hidesForSinglePage = YES;
    [self addSubview:self.pageControl];
    
    // Constraints
    UIView *pageControl = self.pageControl;
    NSDictionary *views = NSDictionaryOfVariableBindings(pageControl);
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[pageControl]|" options:0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[pageControl]|" options:0 metrics:nil views:views]];
}

- (DBPagerViewPageIndicatorPosition)preferredPosition
{
    if (self.parentPagerView.tabViewPosition == DBPagerViewTabViewPositionTop) {
        return DBPagerViewPageIndicatorPositionBottom;
    }
    else {
        return DBPagerViewPageIndicatorPositionNearTabView;
    }
}

- (void)reloadLayout
{
    NSArray *pages = self.parentPagerView.pages;
    self.pageControl.numberOfPages = pages.count;
    self.pageControl.currentPageIndicatorTintColor = self.selectedColor;
    self.pageControl.pageIndicatorTintColor = self.unselectedColor;
}

- (void)updateLayoutWithCurrentPageOffset:(CGFloat)currentPageOffset
{
    if (!self.parentPagerView || [NSArray isEmpty:self.parentPagerView.pages]) {
        // Not valid
        return;
    }
    
    NSInteger currentPageIndex = parseInt(currentPageOffset + 0.5);
    currentPageIndex = mantieniAllInterno(currentPageIndex, 0, self.pageControl.numberOfPages - 1);
    
    self.pageControl.currentPage = currentPageIndex;
}

@synthesize selectedColor = _selectedColor;
- (void)setSelectedColor:(UIColor *)selectedColor
{
    _selectedColor = selectedColor;
    self.pageControl.currentPageIndicatorTintColor = self.selectedColor;
}

- (UIColor *)selectedColor
{
    UIColor *color = (_selectedColor) ? _selectedColor : self.parentPagerView.pageIndicatorColor;
    return color;
}

@synthesize unselectedColor = _unselectedColor;
- (void)setUnselectedColor:(UIColor *)unselectedColor
{
    _unselectedColor = unselectedColor;
    self.pageControl.pageIndicatorTintColor = self.unselectedColor;
}

- (UIColor *)unselectedColor
{
    UIColor *color = (_unselectedColor) ? _unselectedColor : self.parentPagerView.unselectedColor;
    return color;
}

@end
