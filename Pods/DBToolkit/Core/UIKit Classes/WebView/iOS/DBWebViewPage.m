//
//  DBWebViewPage.m
//  File version: 1.0.0
//  Last modified: 11/21/2016
//
//  Created by Davide Balistreri on 11/21/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBWebViewPage.h"

@implementation DBWebViewPage

@synthesize title = _title;
- (void)setTitle:(NSString *)title
{
    _title = title;
}

@synthesize URL = _URL;
- (void)setURL:(NSURL *)URL
{
    _URL = URL;
}

@end
