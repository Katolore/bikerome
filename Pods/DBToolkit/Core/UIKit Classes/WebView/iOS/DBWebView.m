//
//  DBWebView.m
//  File version: 1.0.1
//  Last modified: 05/22/2018
//
//  Created by Davide Balistreri on 11/21/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

/**
 * IMPORTANT DISCLAIMER:
 * This is a preliminary version for a technology in development.
 * It is not final, and it is subject to change.
 * Newer versions may break compatibility of software implemented using this API.
 */


#import "DBWebView.h"
#import "DBObjectExtensions.h"
#import "DBGeometry.h"

#if __has_include(<WebKit/WebKit.h>)
// WebKit import available
#import <WebKit/WebKit.h>
#define kDBWebViewAvailable 1
#else
#define kDBWebViewAvailable 0
#endif


@interface DBWebView ()
#if kDBWebViewAvailable
<WKNavigationDelegate, WKUIDelegate, UIWebViewDelegate>
#else
<UIWebViewDelegate>
#endif

@property (strong, nonatomic) UIWebView *uiWebView;

#if kDBWebViewAvailable
@property (strong, nonatomic) WKWebView *wkWebView;
#else
@property (strong, nonatomic) id wkWebView;
#endif

@end


@interface DBWebViewPage (PrivateMethods)

- (void)setTitle:(NSString *)title;
- (void)setURL:(NSURL *)URL;

@end


@implementation DBWebView

- (void)setupObject
{
    // It will automatically set up the WebView
    [self setUseWebKitWebView:kDBWebViewAvailable];
}

@synthesize useWebKitWebView = _useWebKitWebView;
- (void)setUseWebKitWebView:(BOOL)useWebKitWebView
{
    if (_useWebKitWebView != useWebKitWebView) {
        // Setup only if needed
        _useWebKitWebView = useWebKitWebView;
        [self setupWebView];
    }
}

- (void)setupWebView
{
    if (self.webView) {
        // Remove old WebView
        [self.webView removeFromSuperview];
        self.wkWebView = nil;
        self.uiWebView = nil;
    }
    
    // Set up new WebView
    if (self.useWebKitWebView && NSClassFromString(@"WKWebView")) {
#if kDBWebViewAvailable
        // WebKit web view, supported only on iOS 8+
        self.wkWebView = [WKWebView new];
        self.wkWebView.navigationDelegate = self;
        self.wkWebView.UIDelegate = self;
#endif
    }
    else {
        // UIKit fallback, much slower but supports previous iOS releases
        self.uiWebView = [UIWebView new];
        self.uiWebView.delegate = self;
        self.uiWebView.backgroundColor = [UIColor clearColor];
    }
    
    UIView *webView = (self.wkWebView) ? self.wkWebView : self.uiWebView;
    [webView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [webView setOpaque:NO];
    
    [webView copyContentLayoutPrioritiesFrom:self];
    
    [self addSubview:webView];
    [self setAutolayoutEqualMarginsForSubview:webView];
}

// MARK: - Public methods

- (id)webView
{
    if (self.wkWebView) {
        return self.wkWebView;
    }
    else if (self.uiWebView) {
        return self.uiWebView;
    }
    
    return nil;
}

- (UIScrollView *)scrollView
{
    if (self.wkWebView) {
#if kDBWebViewAvailable
        return self.wkWebView.scrollView;
#endif
    }
    else if (self.uiWebView) {
        return self.uiWebView.scrollView;
    }
    
    return nil;
}

- (BOOL)isScrollEnabled
{
    return self.scrollView.isScrollEnabled;
}

- (void)setScrollEnabled:(BOOL)scrollEnabled
{
    self.scrollView.scrollEnabled = scrollEnabled;
}

- (DBWebViewPage *)currentPage
{
    if (self.wkWebView) {
#if kDBWebViewAvailable
        if (self.wkWebView.URL) {
            DBWebViewPage *currentPage = [DBWebViewPage new];
            currentPage.title = self.wkWebView.title;
            currentPage.URL = self.wkWebView.URL;
            return currentPage;
        }
#endif
    }
    else if (self.uiWebView && self.uiWebView.request.URL) {
        DBWebViewPage *currentPage = [DBWebViewPage new];
        currentPage.title = [self.uiWebView stringByEvaluatingJavaScriptFromString:@"document.title"];
        currentPage.URL = self.uiWebView.request.URL;
        return currentPage;
    }
    
    return nil;
}

- (void)loadHTMLString:(NSString *)HTMLString
{
    [self loadHTMLString:HTMLString baseURL:nil];
}

- (void)loadHTMLString:(NSString *)HTMLString baseURL:(NSURL *)baseURL
{
    if ([NSObject isNotEmpty:HTMLString]) {
        // Load HTMLString
        if (self.wkWebView) {
            [self.wkWebView loadHTMLString:HTMLString baseURL:baseURL];
        }
        else if (self.uiWebView) {
            [self.uiWebView loadHTMLString:HTMLString baseURL:baseURL];
        }
    }
}

- (void)loadURLString:(NSString *)URLString
{
    if ([NSObject isNotEmpty:URLString]) {
        // Load URLString
        NSURL *URL = [NSURL URLWithString:URLString];
        NSURLRequest *URLRequest = [NSURLRequest requestWithURL:URL];
        [self loadURLRequest:URLRequest];
    }
}

- (void)loadURL:(NSURL *)URL
{
    if ([NSObject isNotEmpty:URL]) {
        // Load URL
        NSURLRequest *URLRequest = [NSURLRequest requestWithURL:URL];
        [self loadURLRequest:URLRequest];
    }
}

- (void)loadURLRequest:(NSURLRequest *)URLRequest
{
    if ([NSObject isNotEmpty:URLRequest]) {
        // Load URLRequest
        if (self.wkWebView) {
            [self.wkWebView loadRequest:URLRequest];
        }
        else if (self.uiWebView) {
            [self.uiWebView loadRequest:URLRequest];
        }
    }
}

- (void)reload
{
    if (self.wkWebView) {
        [self.wkWebView reload];
    }
    else if (self.uiWebView) {
        [self.uiWebView reload];
    }
}

- (void)stopLoading
{
    if (self.wkWebView) {
        [self.wkWebView stopLoading];
    }
    else if (self.uiWebView) {
        [self.uiWebView stopLoading];
    }
}

- (BOOL)canGoBack
{
    if (self.wkWebView) {
        return [self.wkWebView canGoBack];
    }
    else if (self.uiWebView) {
        return [self.uiWebView canGoBack];
    }
    
    return NO;
}

- (BOOL)canGoForward
{
    if (self.wkWebView) {
        return [self.wkWebView canGoForward];
    }
    else if (self.uiWebView) {
        return [self.uiWebView canGoForward];
    }
    
    return NO;
}

- (void)goBack
{
    if (self.wkWebView) {
        [self.wkWebView goBack];
    }
    else if (self.uiWebView) {
        [self.uiWebView goBack];
    }
}

- (void)goForward
{
    if (self.wkWebView) {
        [self.wkWebView goForward];
    }
    else if (self.uiWebView) {
        [self.uiWebView goForward];
    }
}

// MARK: - Private methods

#if kDBWebViewAvailable
- (DBWebViewNavigationType)navigationTypeFromWKWebView:(WKNavigationType)navigationType
{
    switch (navigationType) {
        case WKNavigationTypeLinkActivated:
            return DBWebViewNavigationTypeLinkActivated;
        case WKNavigationTypeFormSubmitted:
            return DBWebViewNavigationTypeFormSubmitted;
        case WKNavigationTypeBackForward:
            return DBWebViewNavigationTypeBackForward;
        case WKNavigationTypeReload:
            return DBWebViewNavigationTypeReload;
        case WKNavigationTypeFormResubmitted:
            return DBWebViewNavigationTypeFormResubmitted;
        default:
            return DBWebViewNavigationTypeOther;
    }
}
#endif

- (DBWebViewNavigationType)navigationTypeFromUIWebView:(UIWebViewNavigationType)navigationType
{
    switch (navigationType) {
        case UIWebViewNavigationTypeLinkClicked:
            return DBWebViewNavigationTypeLinkActivated;
        case UIWebViewNavigationTypeFormSubmitted:
            return DBWebViewNavigationTypeFormSubmitted;
        case UIWebViewNavigationTypeBackForward:
            return DBWebViewNavigationTypeBackForward;
        case UIWebViewNavigationTypeReload:
            return DBWebViewNavigationTypeReload;
        case UIWebViewNavigationTypeFormResubmitted:
            return DBWebViewNavigationTypeFormResubmitted;
        default:
            return DBWebViewNavigationTypeOther;
    }
}

#if kDBWebViewAvailable

// MARK: - WKWebView delegate

- (WKWebView *)webView:(WKWebView *)webView createWebViewWithConfiguration:(nonnull WKWebViewConfiguration *)configuration forNavigationAction:(nonnull WKNavigationAction *)navigationAction windowFeatures:(nonnull WKWindowFeatures *)windowFeatures
{
    if (navigationAction.targetFrame.isMainFrame == NO) {
        [webView loadRequest:navigationAction.request];
    }
    
    return nil;
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler
{
    if ([self.delegate respondsToSelector:@selector(webView:shouldLoadRequest:withNavigationType:)]) {
        DBWebViewNavigationType realNavigationType = [self navigationTypeFromWKWebView:navigationAction.navigationType];
        BOOL result = [self.delegate webView:self shouldLoadRequest:navigationAction.request withNavigationType:realNavigationType];
        decisionHandler((result) ? WKNavigationActionPolicyAllow : WKNavigationActionPolicyCancel);
    }
    else {
        // Default
        NSURL *URL = navigationAction.request.URL;
        NSString *scheme = URL.scheme;
        
        if ([scheme containsString:@"tel"] || [scheme containsString:@"mailto"]) {
            if ([[UIApplication sharedApplication] canOpenURL:URL]) {
                [[UIApplication sharedApplication] openURL:URL];
                decisionHandler(WKNavigationActionPolicyCancel);
                return;
            }
        }
        
        decisionHandler(WKNavigationActionPolicyAllow);
    }
}

- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(null_unspecified WKNavigation *)navigation
{
    if ([self.delegate respondsToSelector:@selector(webViewDidStartLoad:)]) {
        [self.delegate webViewDidStartLoad:self];
    }
}

- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error
{
    if ([self.delegate respondsToSelector:@selector(webView:didFailLoadWithError:)]) {
        [self.delegate webView:self didFailLoadWithError:error];
    }
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
{
    if ([self.delegate respondsToSelector:@selector(webViewDidFinishLoad:)]) {
        [self.delegate webViewDidFinishLoad:self];
    }
}

#endif


// MARK: - UIWebView delegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if ([self.delegate respondsToSelector:@selector(webView:shouldLoadRequest:withNavigationType:)]) {
        DBWebViewNavigationType realNavigationType = [self navigationTypeFromUIWebView:navigationType];
        return [self.delegate webView:self shouldLoadRequest:request withNavigationType:realNavigationType];
    }
    else {
        // Default
        return YES;
    }
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    if ([self.delegate respondsToSelector:@selector(webViewDidStartLoad:)]) {
        [self.delegate webViewDidStartLoad:self];
    }
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    if ([self.delegate respondsToSelector:@selector(webView:didFailLoadWithError:)]) {
        [self.delegate webView:self didFailLoadWithError:error];
    }
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    if ([self.delegate respondsToSelector:@selector(webViewDidFinishLoad:)]) {
        [self.delegate webViewDidFinishLoad:self];
    }
}

@end


@implementation DBWebView (Extended)

- (void)currentPageSize:(void(^)(CGSize currentPageSize))completionBlock
{
    NSString *documentHeight = @"\
    Math.max( \
    document.body.scrollHeight, \
    document.body.offsetHeight, \
    document.documentElement.clientHeight, \
    document.documentElement.scrollHeight, \
    document.documentElement.offsetHeight \
    )";
    
#if kDBWebViewAvailable
    if (self.wkWebView) {
        [self.wkWebView evaluateJavaScript:documentHeight completionHandler:^(id _Nullable result, NSError * _Nullable error) {
            CGSize currentPageSize = CGSizeMake(self.wkWebView.frame.size.width, [result doubleValue]);
            
            if (completionBlock) {
                completionBlock(currentPageSize);
            }
        }];
    }
#endif
    
    if (self.uiWebView) {
        NSString *result = [self.uiWebView stringByEvaluatingJavaScriptFromString:documentHeight];
        CGSize currentPageSize = CGSizeMake(self.uiWebView.frame.size.width, [result doubleValue]);
        
        if (completionBlock) {
            completionBlock(currentPageSize);
        }
    }
}

@end
