//
//  DBSingleSlider.m
//  File version: 1.0.0
//  Last modified: 05/11/2016
//
//  Created by Davide Balistreri on 05/11/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBSingleSlider.h"

#import "DBViewExtensions.h"
#import "DBGeometry.h"


@interface DBSingleSlider () <UIGestureRecognizerDelegate>

@property (strong, nonatomic) UIPanGestureRecognizer *sliderRecognizer;
@property (nonatomic) CGFloat sliderRecognizerOriginX;

@end


@implementation DBSingleSlider

// MARK: - Inizializzazione

- (void)setupObject
{
    // Init
    self.slider = [UIImageView new];
    self.slider.contentMode = UIViewContentModeScaleAspectFit;
    self.slider.clipsToBounds = YES;
    self.slider.translatesAutoresizingMaskIntoConstraints = NO;
    self.slider.userInteractionEnabled = YES;
    
    self.fillBar = [UIImageView new];
    self.fillBar.clipsToBounds = YES;
    self.fillBar.translatesAutoresizingMaskIntoConstraints = NO;
    
    self.trackBar = [UIImageView new];
    self.trackBar.clipsToBounds = YES;
    self.trackBar.translatesAutoresizingMaskIntoConstraints = NO;
    
    // Subviews
    [self addSubview:self.trackBar];
    [self addSubview:self.fillBar];
    [self addSubview:self.slider];
    
    // Constraints TrackBar
    NSLayoutConstraint *layoutTrackBarAltezza = [NSLayoutConstraint constraintWithItem:self.trackBar attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:0.0];
    NSLayoutConstraint *layoutTrackBarLeading = [NSLayoutConstraint constraintWithItem:self.trackBar attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0];
    NSLayoutConstraint *layoutTrackBarTrailing = [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.trackBar attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0];
    NSLayoutConstraint *layoutTrackBarCentroY = [NSLayoutConstraint constraintWithItem:self.trackBar attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0];
    [NSLayoutConstraint activateConstraints:@[layoutTrackBarAltezza, layoutTrackBarLeading, layoutTrackBarTrailing, layoutTrackBarCentroY]];
    
    // Constraints FillBar
    NSLayoutConstraint *layoutFillBarAltezza = [NSLayoutConstraint constraintWithItem:self.fillBar attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:0.0];
    NSLayoutConstraint *layoutFillBarLeading = [NSLayoutConstraint constraintWithItem:self.fillBar attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.trackBar attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0];
    NSLayoutConstraint *layoutFillBarTrailing = [NSLayoutConstraint constraintWithItem:self.slider attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.fillBar attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0];
    NSLayoutConstraint *layoutFillBarCentroY = [NSLayoutConstraint constraintWithItem:self.fillBar attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.trackBar attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0];
    [NSLayoutConstraint activateConstraints:@[layoutFillBarAltezza, layoutFillBarLeading, layoutFillBarTrailing, layoutFillBarCentroY]];
    
    // Constraints Slider
    NSLayoutConstraint *layoutSliderLarghezza = [NSLayoutConstraint constraintWithItem:self.slider attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:0.0];
    NSLayoutConstraint *layoutSliderAltezza = [NSLayoutConstraint constraintWithItem:self.slider attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:0.0];
    NSLayoutConstraint *layoutSliderLeading = [NSLayoutConstraint constraintWithItem:self.slider attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.trackBar attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0];
    NSLayoutConstraint *layoutSliderCentroY = [NSLayoutConstraint constraintWithItem:self.slider attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.trackBar attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0];
    [NSLayoutConstraint activateConstraints:@[layoutSliderLarghezza, layoutSliderAltezza, layoutSliderLeading, layoutSliderCentroY]];
    
    // GestureRecognizer
    self.sliderRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(sliderRecognizer:)];
    self.sliderRecognizer.delegate = self;
    [self addGestureRecognizer:self.sliderRecognizer];
    
    // Style
    self.sliderStyle = DBSliderStyleDefault;
    
    // Valori iniziali
    self.sliderPercentage = 0.0;
}

- (CGSize)intrinsicContentSize
{
    return CGSizeMake(100.0, 44.0);
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    switch (self.sliderStyle) {
        case DBSliderStyleDefault:
            // Stile di default
            [self.slider aggiungiAngoliArrotondatiCerchio];
            [self.fillBar aggiungiAngoliArrotondatiCerchio];
            [self.trackBar aggiungiAngoliArrotondatiCerchio];
            break;
        default:
            break;
    }
}


// MARK: - Configurazione funzionale

@synthesize delegate = _delegate;
- (void)setDelegate:(id<DBSliderDelegate>)delegate
{
    _delegate = delegate;
    
    // Aggiorno il delegate sulla posizione attuale
    if ([delegate respondsToSelector:@selector(sliderDidChangeValue:)]) {
        [delegate sliderDidChangeValue:self];
    }
}


// MARK: - Configurazione grafica

@synthesize sliderStyle = _sliderStyle;
- (void)setSliderStyle:(DBSliderStyle)sliderStyle
{
    _sliderStyle = sliderStyle;
    [self configureWithStyle:sliderStyle];
}

@synthesize sliderSize = _sliderSize;
- (void)setSliderSize:(CGSize)sliderSize
{
    _sliderSize = sliderSize;
    
    self.slider.constraintLarghezza = sliderSize.width;
    self.slider.constraintAltezza = sliderSize.height;
    [self.slider layoutIfNeeded];
    
    [self moveSlider:self.slider.constraintLeading];
}

@synthesize barHeight = _barHeight;
- (void)setBarHeight:(CGFloat)barHeight
{
    _barHeight = barHeight;
    
    self.trackBar.constraintAltezza = barHeight;
    self.fillBar.constraintAltezza = barHeight;
    [self layoutIfNeeded];
}

- (void)configureWithStyle:(DBSliderStyle)style
{
    UIColor *sliderColor = nil;
    UIColor *fillBarColor = nil;
    UIColor *trackBarColor = nil;
    
    CGSize  sliderSize = CGSizeZero;
    CGFloat barHeight = 0.0;
    
    switch (style) {
        case DBSliderStyleDefault:
            // Stile di default
            sliderColor   = [UIColor colorWithWhite:0.4 alpha:1.0];
            fillBarColor  = [UIColor colorWithWhite:0.5 alpha:1.0];
            trackBarColor = [UIColor colorWithWhite:0.8 alpha:1.0];
            
            sliderSize = CGSizeMake(28.0, 28.0);
            barHeight = 4.0;
            break;
            
        default:
            // Stile assente
            self.slider.layer.cornerRadius = 0.0;
            self.fillBar.layer.cornerRadius = 0.0;
            self.trackBar.layer.cornerRadius = 0.0;
            break;
    }
    
    // Slider
    self.slider.backgroundColor = sliderColor;
    self.slider.constraintLarghezza = sliderSize.width;
    self.slider.constraintAltezza = sliderSize.height;
    
    // FillBar
    self.fillBar.backgroundColor = fillBarColor;
    self.fillBar.constraintAltezza = barHeight;
    
    // TrackBar
    self.trackBar.backgroundColor = trackBarColor;
    self.trackBar.constraintAltezza = barHeight;
    
    [self layoutIfNeeded];
}


// MARK: - Configurazione grafica manuale

@synthesize slider = _slider;
- (void)setSlider:(UIImageView *)slider
{
    _slider = slider;
}

@synthesize fillBar = _fillBar;
- (void)setFillBar:(UIImageView *)fillBar
{
    _fillBar = fillBar;
}

@synthesize trackBar = _trackBar;
- (void)setTrackBar:(UIImageView *)trackBar
{
    _trackBar = trackBar;
}


// MARK: - Metodi pubblici

- (NSInteger)sliderStep
{
    return [self stepWithPosition:self.slider.constraintLeading];
}

- (void)setSliderStep:(NSInteger)sliderStep
{
    [self moveSlider:[self positionWithStep:sliderStep]];
}

- (CGFloat)sliderPercentage
{
    CGFloat width = [self trackBarUsableWidth];
    CGFloat leading = self.slider.constraintLeading;
    return (leading / width);
}

- (void)setSliderPercentage:(CGFloat)sliderPercentage
{
    CGFloat percentage = mantieniAllInterno(sliderPercentage, 0.0, 1.0);
    CGFloat width = [self trackBarUsableWidth];
    CGFloat position = width * percentage;
    [self moveSlider:position];
}


// MARK: - Metodi privati

- (CGFloat)trackBarWidth
{
    CGFloat width = self.trackBar.constraintLarghezza;
    return (width > 0.0) ? width : self.trackBar.larghezza;
}

- (CGFloat)trackBarUsableWidth
{
    CGFloat width = [self trackBarWidth];
    CGFloat larghezzaSlider = self.slider.constraintLarghezza;
    return (width - larghezzaSlider);
}

- (void)moveSlider:(CGFloat)pixel
{
    // CGFloat larghezzaSlider = self.slider.constraintLarghezza;
    CGFloat larghezzaTotale = [self trackBarUsableWidth];
    
    NSInteger step = [self stepWithPosition:pixel];
    if (step != NSNotFound) {
        CGFloat larghezzaStep = larghezzaTotale / self.numberOfSteps;
        pixel = larghezzaStep * step;
    }
    
    pixel = mantieniAllInterno(pixel, 0.0, larghezzaTotale);
    
    if (self.numberOfSteps > 0) {
        CGFloat oldPixel = self.slider.constraintLeading;
        if (oldPixel != pixel) {
            // Animazione
            [UIView animateWithDuration:0.12 animations:^{
                self.slider.constraintLeading = pixel;
                [self layoutIfNeeded];
            }];
        }
    }
    else {
        self.slider.constraintLeading = pixel;
        [self layoutIfNeeded];
    }
    
    if ([self.delegate respondsToSelector:@selector(sliderDidChangeValue:)]) {
        [self.delegate sliderDidChangeValue:self];
    }
}

- (BOOL)isStepped
{
    return (self.numberOfSteps < 1) ? NO : YES;
}

- (CGFloat)stepWidth
{
    CGFloat larghezzaTotale = [self trackBarUsableWidth];
    
    if ([self isStepped]) {
        return safeDouble(larghezzaTotale / parseDouble(self.numberOfSteps));
    }
    else {
        return larghezzaTotale / 10.0;
    }
}

- (NSInteger)stepWithPosition:(CGFloat)position
{
    if ([self isStepped] == NO) {
        return NSNotFound;
    }
    
    CGFloat stepWidth = [self stepWidth];
    CGFloat offset = 0.5;
    
    CGFloat step = (position / stepWidth) + offset;
    return parseInt(step);
}

- (CGFloat)positionWithStep:(NSInteger)step
{
    if ([self isStepped] == NO) {
        return 0.0;
    }
    
    CGFloat stepWidth = [self stepWidth];
    return stepWidth * parseDouble(step);
}


// MARK: - Gesture recognizer

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)sender shouldReceiveTouch:(UITouch *)touch
{
    if ([self.delegate respondsToSelector:@selector(sliderShouldBeginChangingValue:)]) {
        if ([self.delegate sliderShouldBeginChangingValue:self] == NO) {
            return NO;
        }
    }
    
    // Aumento l'area trascinabile dello slider
    CGRect touchableFrame = self.slider.frame;
    touchableFrame.origin.x -= 20.0;
    touchableFrame.origin.y -= 20.0;
    touchableFrame.size.width += 40.0;
    touchableFrame.size.height += 40.0;
    
    CGPoint point = [touch locationInView:sender.view];
    return CGRectContainsPoint(touchableFrame, point);
}

- (void)sliderRecognizer:(UIPanGestureRecognizer *)recognizer
{
    switch (recognizer.state) {
        case UIGestureRecognizerStateBegan: {
            // Salvo il punto di origine iniziale
            self.sliderRecognizerOriginX = self.slider.constraintLeading;
            [self bringSubviewToFront:self.slider];
            
            if ([self.delegate respondsToSelector:@selector(sliderDidBeginChangingValue:)]) {
                [self.delegate sliderDidBeginChangingValue:self];
            }
            
            break;
        }
        case UIGestureRecognizerStateChanged: {
            CGPoint translation = [recognizer translationInView:self];
            CGFloat originX = self.sliderRecognizerOriginX + translation.x;
            [self moveSlider:originX];
            break;
        }
        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateFailed:
        case UIGestureRecognizerStateCancelled: {
            if ([self.delegate respondsToSelector:@selector(sliderDidEndChangingValue:)]) {
                [self.delegate sliderDidEndChangingValue:self];
            }
            
            break;
        }
        default:
            break;
    }
}

@end
