//
//  DBMapViewAnnotation.m
//  File version: 1.0.0
//  Last modified: 04/11/2017
//
//  Created by Davide Balistreri on 11/22/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBMapViewAnnotation.h"

#import "DBMapView.h"


@interface DBMapView (PrivateMethods)

- (nonnull __kindof DBMapViewAnnotation *)annotationWithClass:(Class)klass coordinate:(CLLocationCoordinate2D)coordinate;

@end


@interface DBMapViewAnnotationLayer : CALayer

@property (nonatomic) CGFloat fixedPosition;

@end


@implementation DBMapViewAnnotation

+ (instancetype)annotationWithMapView:(DBMapView *)mapView
{
    Class klass = [self class];
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(0, 0);
    return [mapView annotationWithClass:klass coordinate:coordinate];
}

+ (instancetype)annotationWithMapView:(DBMapView *)mapView coordinate:(CLLocationCoordinate2D)coordinate
{
    Class klass = [self class];
    return [mapView annotationWithClass:klass coordinate:coordinate];
}


+ (Class)layerClass
{
    return [DBMapViewAnnotationLayer class];
}

- (CGFloat)zPosition
{
    return self.layer.zPosition;
}

- (void)setZPosition:(CGFloat)zPosition
{
    DBMapViewAnnotationLayer *zLayer = (id) self.layer;
    zLayer.fixedPosition = zPosition;
}

@end


@implementation DBMapViewAnnotationLayer

- (CGFloat)zPosition
{
    if (self.fixedPosition > 0) {
        // NSLog(@"^ Fixed Z-position: %.2f", self.fixedPosition);
        return self.fixedPosition;
    }
    
    // NSLog(@"^ Standard Z-position: %.2f", super.zPosition);
    return super.zPosition;
}

@synthesize fixedPosition = _fixedPosition;
- (void)setFixedPosition:(CGFloat)fixedPosition
{
    _fixedPosition = fixedPosition;
    [super setZPosition:fixedPosition];
}

@end
