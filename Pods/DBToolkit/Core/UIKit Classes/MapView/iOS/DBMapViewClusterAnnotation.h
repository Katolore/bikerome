//
//  DBMapViewClusterAnnotation.h
//  File version: 1.0.0
//  Last modified: 04/18/2017
//
//  Created by Davide Balistreri on 04/11/2017
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

/**
 * IMPORTANT DISCLAIMER:
 * This is a preliminary version for a technology in development.
 * It is not final, and it is subject to change.
 * Newer versions may break compatibility of software implemented using this API.
 */


#import "DBMapViewAnnotation.h"

@interface DBMapViewClusterAnnotation : DBMapViewAnnotation

@property (nonatomic, readonly) NSArray<DBMapViewAnnotation *> *annotations;

@end
