//
//  DBTableViewSpinnerCell.h
//  File version: 1.0.0
//  Last modified: 12/01/2015
//
//  Created by Davide Balistreri on 11/09/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBTableViewCell.h"

@interface DBTableViewSpinnerCell : DBTableViewCell

+ (instancetype)cella;

@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;

@end
