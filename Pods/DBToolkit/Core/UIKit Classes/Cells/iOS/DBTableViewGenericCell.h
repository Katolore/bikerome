//
//  DBTableViewGenericCell.h
//  File version: 1.0.0
//  Last modified: 12/01/2015
//
//  Created by Davide Balistreri on 09/07/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBTableViewCell.h"

/**
 Una cella generica con alcune IBOutlet pensata per evitare di creare nuove classi per TableViewCell semplici.
 */
@interface DBTableViewGenericCell : DBTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titolo;
@property (weak, nonatomic) IBOutlet UILabel *descrizione;

@property (weak, nonatomic) IBOutlet UIImageView *immagine;

@property (weak, nonatomic) IBOutlet UILabel *linea;
@property (weak, nonatomic) IBOutlet UILabel *background;

@end
