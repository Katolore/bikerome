//
//  DBTableViewSpinnerCell.m
//  File version: 1.0.0
//  Last modified: 12/01/2015
//
//  Created by Davide Balistreri on 11/09/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBTableViewSpinnerCell.h"
#import "DBFramework.h"

@implementation DBTableViewSpinnerCell

+ (instancetype)cella
{
    return [DBTableViewSpinnerCell new];
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self loadView];
    }
    return self;
}

- (void)loadView
{
    self.activityIndicator = [UIActivityIndicatorView new];
    self.activityIndicator.dimensioni = self.contentView.dimensioni;
    self.activityIndicator.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    self.activityIndicator.color = [DBUtility tintColor];
    [self.contentView addSubview:self.activityIndicator];
    [self.activityIndicator startAnimating];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

@end
