//
//  DBAccessoryViewDefines.m
//  File version: 1.0.0 beta
//  Last modified: 01/05/2017
//
//  Created by Davide Balistreri on 12/05/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBAccessoryTextFieldDefines.h"

// MARK: - Defines

const CGFloat DBAccessoryViewExpand    = 12345;
const CGFloat DBAccessoryViewAutomatic = 56789;

const CGSize DBAccessoryViewExpandSize    = {DBAccessoryViewExpand,    DBAccessoryViewExpand};
const CGSize DBAccessoryViewAutomaticSize = {DBAccessoryViewAutomatic, DBAccessoryViewAutomatic};
