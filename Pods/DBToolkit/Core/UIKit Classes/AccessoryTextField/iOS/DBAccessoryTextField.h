//
//  DBAccessoryTextField.h
//  File version: 1.0.0 beta
//  Last modified: 01/05/2017
//
//  Created by Davide Balistreri on 12/05/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

/**
 * IMPORTANT DISCLAIMER:
 * This is a preliminary version for a technology in development.
 * It is not final, and it is subject to change.
 * Newer versions may break compatibility of software implemented using this API.
 */


#import "DBTextField.h"
#import "DBAnimation.h"
#import "DBAccessoryTextFieldDefines.h"

@class DBKeyboardHandler;


/**
 * DBAccessoryTextField is a TextField that handles an attached View, the AccessoryView, which is
 * shown and hidden when the it gains or loses focus.
 *
 * TIP: If you want to be informed about the TextField focus or text changes, respond to its delegate
 * as you would with a standard text field.
 */

@interface DBAccessoryTextField : DBTextField

/**
 * This is the View that will be shown or hidden when the TextField gains or loses the focus.
 *
 * You can change its positioning rules, content size and appearing transitions by setting the properties of this class.
 * You can also style this AccessoryView as you would with a normal View, because it is!
 *
 * NB: If you want to use a ViewController's view as AccessoryView, please use the
 * 'accessoryViewController' property.
 */
@property (strong, nonatomic, nullable) __kindof UIView *accessoryView;

/**
 * If you want to use a ViewController's view as AccessoryView, this is the right way to do it.
 *
 * Please check out the 'accessoryView' description for further informations.
 *
 * NB: Don't anchor any item of the view to its ViewController's layout guides (top and bottom) 
 * in the Interface Builder, because if you do that the view won't be displayed,
 * and it will complain about 'ambiguous vertical position'.
 *
 * You must anchor the top and bottom constraints to the view, not the ViewController layout guides.
 */
@property (strong, nonatomic, nullable) __kindof UIViewController *accessoryViewController;


/**
 * Appearance transitions when the TextField gains and loses focus.
 * (Not used yet)
 */
@property (nonatomic) DBAnimationTransition appearanceTransitions;


/**
 * Padding from the edges of the available space in the superview.
 * The content size of the AccessoryView will never exceed this padding.
 *
 * Default: zero
 */
@property (nonatomic) UIEdgeInsets accessoryViewInsets;

/**
 * Use this property to setup the layout size of the AccessoryView.
 *
 * Default: DBAccessoryViewExpandSize
 */
@property (nonatomic) CGSize accessoryViewSize;


/**
 * This is the real View that will be added to the screen and attached to the TextField.
 * It is shown and hidden when the Keyboard opens and closes.
 *
 * The AccessoryView will be placed inside this ContainerView.
 */
@property (strong, nonatomic, readonly, nonnull) UIView *containerView;


/**
 * Use this property if you want to change the View that will host the ContainerView.
 * By default this is nil, and the PresentingView will automatically be the TextField's superview.
 */
@property (weak, nonatomic, nullable) UIView *presentingView;


/**
 * Linking this text field to a DBKeyboardHandler makes things easier by providing some automatisms:
 * - Automatic AccessoryView positioning when the Keyboard opens (and closes)
 * - Automatic AccessoryView content size based on available space in the superview
 *
 * The text field is automatically linked when it's added to a DBKeyboardHandler,
 * but if you prefer you can disable this behavior by replacing this property with a nil value.
 */
@property (weak, nonatomic, nullable) DBKeyboardHandler *keyboardHandler;

@end
