//
//  DBAccessoryTextField.m
//  File version: 1.0.0 beta
//  Last modified: 01/05/2017
//
//  Created by Davide Balistreri on 12/05/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBAccessoryTextField.h"
#import "DBAccessoryTextFieldDefines.h"
#import "DBKeyboardHandler.h"
#import "DBViewExtensions.h"
#import "DBGeometry.h"
#import "DBThread.h"

// MARK: - DBAccessoryTextFieldContainerView subclass

/**
 * A simple subclassed View created to simplify identification and debugging purposes.
 */
@interface DBAccessoryTextFieldContainerView : UIView

@end

@implementation DBAccessoryTextFieldContainerView

@end


// MARK: - DBAccessoryTextField class

@interface DBAccessoryTextField ()

@property (nonatomic) BOOL isActive;

@property (strong, nonatomic) DBAnimation *animation;

// MARK: ContainerView
@property (strong, nonatomic) NSArray *customConstraints;
@property (nonatomic) CGFloat availableSpace;
@property (nonatomic) CGFloat anchorMargin;

@end


@implementation DBAccessoryTextField

- (void)setupObject
{
    // Setup the container view
    self.containerView = [DBAccessoryTextFieldContainerView new];
    self.containerView.translatesAutoresizingMaskIntoConstraints = NO;
    self.containerView.clipsToBounds = YES; // Override this if you want to add shadows!
    
    // Defaults
    self.accessoryViewSize = DBAccessoryViewExpandSize;
}

@synthesize containerView = _containerView;
- (void)setContainerView:(UIView *)containerView
{
    _containerView = containerView;
}

@synthesize accessoryView = _accessoryView;
- (void)setAccessoryView:(__kindof UIView *)accessoryView
{
    if (_accessoryView) {
        // Remove old accessory view
        [_accessoryView removeFromSuperview];
    }
    
    _accessoryView = accessoryView;
    
    // Setup
    [accessoryView setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    // Add new accessory view
    [self.containerView addSubview:accessoryView];
    
    // Constraints
    [self.containerView setAutolayoutEqualMarginsForSubview:accessoryView];
}

@synthesize accessoryViewController = _accessoryViewController;
- (void)setAccessoryViewController:(__kindof UIViewController *)accessoryViewController
{
    _accessoryViewController = accessoryViewController;
    self.accessoryView = accessoryViewController.view;
}

@synthesize presentingView = _presentingView;
- (UIView *)presentingView
{
    return (_presentingView) ? _presentingView : self.superview;
}

@synthesize keyboardHandler = _keyboardHandler;
- (void)setKeyboardHandler:(DBKeyboardHandler *)keyboardHandler
{
    _keyboardHandler = keyboardHandler;
    [self updateSettings];
}

- (BOOL)becomeFirstResponder
{
    self.isActive = YES;
    return [super becomeFirstResponder];
}

- (BOOL)resignFirstResponder
{
    self.isActive = NO;
    return [super resignFirstResponder];
}

@synthesize isActive = _isActive;
- (void)setIsActive:(BOOL)isActive
{
    if (_isActive != isActive) {
        _isActive = isActive;
        [self updateLayout];
    }
}

- (void)updateLayout
{
    if (self.animation) {
        [self.animation forceStop];
    }
    
    DBAnimation *animation = [DBAnimation new];
    animation.delay = 0.01; // Piccolo ritardo per non interferire con altre animazioni
    animation.duration = 0.3;
    // animation.useSpringEffect = YES;
    
    BOOL animateInitialAlpha = (self.keyboardHandler.isKeyboardOpen) ? NO : YES;
    
    [animation setInitialState:^{
        if (self.isActive) {
            [self.presentingView addSubview:self.containerView];
            [self reloadConstraints];
        }
        
        if (animateInitialAlpha && self.isActive) {
            self.containerView.alpha = 0.0;
        }
        else {
            self.containerView.alpha = 1.0;
        }
    }];
    
    [animation setFinalState:^{
        if (self.isActive) {
            self.containerView.alpha = 1.0;
        }
        else {
            self.containerView.alpha = 0.0;
        }
    }];
    
    [animation setCompletionBlock:^{
        if (self.isActive == NO) {
            [self.containerView removeFromSuperview];
        }
        
        self.animation = nil;
    }];
    
    self.animation = animation;
    [animation start];
}

// MARK: - ContainerView

- (void)updateSettings
{
    DBKeyboardHandler *keyboardHandler = self.keyboardHandler;
    [keyboardHandler inputField:self setAnchorPoint:DBKeyboardHandlerAnchorPointTop];
    [keyboardHandler inputField:self setForceAnchorPoint:YES];
}

- (void)reloadConstraints
{
    if (!self.presentingView) {
        // Not ready
        return;
    }
    
    if (self.customConstraints) {
        // Remove old constraints
        [self removeConstraints:self.customConstraints];
    }
    
    // Constraints
    NSMutableArray *constraints = [NSMutableArray array];
    
    UIView *textField = self;
    UIView *containerView = self.containerView;
    UIView *presentingView = self.presentingView;
    
    // Anchors and content inset
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:containerView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:textField attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0];
    top.constant = self.accessoryViewInsets.top;
    top.priority = 800;
    [constraints addObject:top];
    
    NSLayoutConstraint *left = [NSLayoutConstraint constraintWithItem:containerView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:presentingView attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0];
    left.constant = self.accessoryViewInsets.left;
    left.priority = 800;
    [constraints addObject:left];
    
    NSLayoutConstraint *right = [NSLayoutConstraint constraintWithItem:presentingView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:containerView attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0];
    right.constant = self.accessoryViewInsets.right;
    right.priority = 800;
    [constraints addObject:right];
    
    // Width constant
    if (self.accessoryViewSize.width == DBAccessoryViewAutomaticSize.width) {
        // ContainerView width linked to TextField
        CGRect superviewFrame = presentingView.frame;
        CGRect viewFrame = textField.frame;
        viewFrame.origin = [DBGeometry calcolaOrigineReale:textField conVistaParent:presentingView];
        
        NSLayoutConstraint *leftInset = [NSLayoutConstraint constraintWithItem:containerView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:presentingView attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0];
        leftInset.constant = viewFrame.origin.x;
        leftInset.priority = 900;
        
        NSLayoutConstraint *rightInset = [NSLayoutConstraint constraintWithItem:presentingView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:containerView attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0];
        rightInset.constant = superviewFrame.size.width - viewFrame.origin.x - viewFrame.size.width;
        rightInset.priority = 900;
        
        [constraints addObjectsFromArray:@[leftInset, rightInset]];
    }
    else if (self.accessoryViewSize.width == DBAccessoryViewExpandSize.width) {
        // ContainerView width equals to TextField's superview
        // Already set up
    }
    else {
        // ContainerView custom width (X centered to TextField)
        // FIXME: Centering to the TextField is wrong as it won't respect the contentInset!
        NSLayoutConstraint *center = [NSLayoutConstraint constraintWithItem:containerView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:textField attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0];
        center.priority = 800;
        
        NSLayoutConstraint *width = [NSLayoutConstraint constraintWithItem:containerView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:0.0];
        width.constant = self.accessoryViewSize.width;
        width.priority = 800; // Must be equal or less than left and right constraints
        
        [constraints addObjectsFromArray:@[center, width]];
    }
    
    // Height constant
    NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:containerView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:0.0];
    height.constant = [self calculateHeightConstant];
    height.priority = 900;
    [constraints addObject:height];
    
    // Constraints ready
    [presentingView addConstraints:constraints];
    self.customConstraints = constraints;
}

- (CGFloat)calculateHeightConstant
{
    CGFloat heightConstant = 0.0;
    
    if (self.accessoryViewSize.height == DBAccessoryViewAutomaticSize.height) {
        // ContainerView height linked to TextField
        heightConstant = self.availableSpace - fmax(self.anchorMargin, self.accessoryViewInsets.bottom);
    }
    else if (self.accessoryViewSize.height == DBAccessoryViewExpandSize.height) {
        // ContainerView height fills available space
        heightConstant = self.availableSpace - self.accessoryViewInsets.bottom;
    }
    else {
        // ContainerView custom height (maxed to available space)
        heightConstant = fmin(self.accessoryViewSize.height, self.availableSpace);
        heightConstant = fmin(heightConstant, self.availableSpace - self.accessoryViewInsets.bottom);
    }
    
    heightConstant -= self.accessoryViewInsets.top;
    heightConstant = fmax(0.0, heightConstant);
    
    if (heightConstant == 0.0) {
        if (self.presentingView == self.superview && self.isActive) {
            // Helper log
            DBLog(@"Current focused AccessoryTextField AccessoryView's height is zero, it will not be displayed. One of the reasons could be that the TextField's superview isn't the main view. To fix this, please specify the main view by setting the AccessoryTextField 'presentingView' property.");
        }
    }
    
    return heightConstant;
}

// MARK: - Private KeyboardHandler delegate

- (void)keyboardHandlerDidUpdateAvailableSpace:(CGFloat)availableSpace withAnchorMargin:(CGFloat)anchorMargin
{
    self.availableSpace = availableSpace;
    self.anchorMargin = anchorMargin;
    
    UIView *containerView = self.containerView;
    NSLayoutConstraint *height = [containerView constraintConAttributo:NSLayoutAttributeHeight];
    height.constant = [self calculateHeightConstant];
    
    [self setNeedsUpdateConstraints];
    [self updateConstraintsIfNeeded];
}

@end
