//
//  DBAutoLayoutImageView.h
//  File version: 1.0.0
//  Last modified: 02/21/2016
//
//  Created by Davide Balistreri on 02/21/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

@interface DBAutoLayoutImageView : UIImageView

@end
