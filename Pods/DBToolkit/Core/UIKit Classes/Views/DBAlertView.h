//
//  DBAlertView.h
//  File version: 1.1.0
//  Last modified: 02/14/2016
//
//  Created by Davide Balistreri on 04/15/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"
#import "DBCompatibility.h"

@interface DBAlertView : NSObject

/**
 * CompletionBlock per eseguire un'azione quando l'utente seleziona un pulsante
 */
typedef void (^DBAlertViewCompletionBlock)(void);


// MARK: - Metodi comuni

/**
 * Chiude tutte le UIAlertView e gli UIActionSheet eventualmente aperti, senza eseguire il completionBlock.
 */
+ (void)chiudiTutto;


// MARK: - Alert View

/// Pulsante chiudi
+ (void)alertConTitolo:(NSString *)titolo conMessaggio:(NSString *)messaggio conPulsanteChiudi:(NSString *)chiudi conChiudiBlock:(DBAlertViewCompletionBlock)chiudiBlock;

/// Pulsanti accetta e rifiuta
+ (void)alertConTitolo:(NSString *)titolo conMessaggio:(NSString *)messaggio conPulsanteAccetta:(NSString *)accetta conAccettaBlock:(DBAlertViewCompletionBlock)accettaBlock conPulsanteRifiuta:(NSString *)rifiuta conRifiutaBlock:(DBAlertViewCompletionBlock)rifiutaBlock;

/// Tre pulsanti
+ (void)alertConTitolo:(NSString *)titolo conMessaggio:(NSString *)messaggio conPrimoPulsante:(NSString *)primoPulsante conPrimoPulsanteBlock:(DBAlertViewCompletionBlock)primoPulsanteBlock conSecondoPulsante:(NSString *)secondoPulsante conSecondoPulsanteBlock:(DBAlertViewCompletionBlock)secondoPulsanteBlock conPulsanteChiudi:(NSString *)chiudi conChiudiBlock:(DBAlertViewCompletionBlock)chiudiBlock;

/// Quattro pulsanti
+ (void)alertConTitolo:(NSString *)titolo conMessaggio:(NSString *)messaggio conPrimoPulsante:(NSString *)primoPulsante conPrimoPulsanteBlock:(DBAlertViewCompletionBlock)primoPulsanteBlock conSecondoPulsante:(NSString *)secondoPulsante conSecondoPulsanteBlock:(DBAlertViewCompletionBlock)secondoPulsanteBlock conTerzoPulsante:(NSString *)terzoPulsante conTerzoPulsanteBlock:(DBAlertViewCompletionBlock)terzoPulsanteBlock conPulsanteChiudi:(NSString *)chiudi conChiudiBlock:(DBAlertViewCompletionBlock)chiudiBlock;

/// Pulsante chiudi (versione localizzata)
+ (void)alertLocalizzatoConTitolo:(NSString *)titolo conMessaggio:(NSString *)messaggio conPulsanteChiudi:(NSString *)chiudi conChiudiBlock:(DBAlertViewCompletionBlock)chiudiBlock;

/// Pulsanti accetta e rifiuta (versione localizzata)
+ (void)alertLocalizzatoConTitolo:(NSString *)titolo conMessaggio:(NSString *)messaggio conPulsanteAccetta:(NSString *)accetta conAccettaBlock:(DBAlertViewCompletionBlock)accettaBlock conPulsanteRifiuta:(NSString *)rifiuta conRifiutaBlock:(DBAlertViewCompletionBlock)rifiutaBlock;

/// Tre pulsanti (versione localizzata)
+ (void)alertLocalizzatoConTitolo:(NSString *)titolo conMessaggio:(NSString *)messaggio conPrimoPulsante:(NSString *)primoPulsante conPrimoPulsanteBlock:(DBAlertViewCompletionBlock)primoPulsanteBlock conSecondoPulsante:(NSString *)secondoPulsante conSecondoPulsanteBlock:(DBAlertViewCompletionBlock)secondoPulsanteBlock conPulsanteChiudi:(NSString *)chiudi conChiudiBlock:(DBAlertViewCompletionBlock)chiudiBlock;

/// Quattro pulsanti (versione localizzata)
+ (void)alertLocalizzatoConTitolo:(NSString *)titolo conMessaggio:(NSString *)messaggio conPrimoPulsante:(NSString *)primoPulsante conPrimoPulsanteBlock:(DBAlertViewCompletionBlock)primoPulsanteBlock conSecondoPulsante:(NSString *)secondoPulsante conSecondoPulsanteBlock:(DBAlertViewCompletionBlock)secondoPulsanteBlock conTerzoPulsante:(NSString *)terzoPulsante conTerzoPulsanteBlock:(DBAlertViewCompletionBlock)terzoPulsanteBlock conPulsanteChiudi:(NSString *)chiudi conChiudiBlock:(DBAlertViewCompletionBlock)chiudiBlock;


// MARK: - Action Sheet

// Da adattare per OSX: NSBeginAlertSheet

#if DBFRAMEWORK_TARGET_IOS

/**
 * CompletionBlock che permette all'utente di mostrare l'ActionSheet su una vista.
 */
typedef void (^DBActionSheetBlock)(UIActionSheet *actionSheet);

/// Pulsanti accetta e rifiuta
+ (void)actionSheetConTitolo:(NSString *)titolo conPulsanteAccetta:(NSString *)accetta conAccettaBlock:(DBAlertViewCompletionBlock)accettaBlock conPulsanteRifiuta:(NSString *)rifiuta conRifiutaBlock:(DBAlertViewCompletionBlock)rifiutaBlock conActionSheet:(DBActionSheetBlock)actionSheetBlock;

/// Tre pulsanti
+ (void)actionSheetConTitolo:(NSString *)titolo conPrimoPulsante:(NSString *)primoPulsante conPrimoPulsanteBlock:(DBAlertViewCompletionBlock)primoPulsanteBlock conSecondoPulsante:(NSString *)secondoPulsante conSecondoPulsanteBlock:(DBAlertViewCompletionBlock)secondoPulsanteBlock conPulsanteChiudi:(NSString *)chiudi conChiudiBlock:(DBAlertViewCompletionBlock)chiudiBlock conActionSheet:(DBActionSheetBlock)actionSheetBlock;

/// Quattro pulsanti
+ (void)actionSheetConTitolo:(NSString *)titolo conPrimoPulsante:(NSString *)primoPulsante conPrimoPulsanteBlock:(DBAlertViewCompletionBlock)primoPulsanteBlock conSecondoPulsante:(NSString *)secondoPulsante conSecondoPulsanteBlock:(DBAlertViewCompletionBlock)secondoPulsanteBlock conTerzoPulsante:(NSString *)terzoPulsante conTerzoPulsanteBlock:(DBAlertViewCompletionBlock)terzoPulsanteBlock conPulsanteChiudi:(NSString *)chiudi conChiudiBlock:(DBAlertViewCompletionBlock)chiudiBlock conActionSheet:(DBActionSheetBlock)actionSheetBlock;

/// Cinque pulsanti
+ (void)actionSheetConTitolo:(NSString *)titolo conPrimoPulsante:(NSString *)primoPulsante conPrimoPulsanteBlock:(DBAlertViewCompletionBlock)primoPulsanteBlock conSecondoPulsante:(NSString *)secondoPulsante conSecondoPulsanteBlock:(DBAlertViewCompletionBlock)secondoPulsanteBlock conTerzoPulsante:(NSString *)terzoPulsante conTerzoPulsanteBlock:(DBAlertViewCompletionBlock)terzoPulsanteBlock conQuartoPulsante:(NSString *)quartoPulsante conQuartoPulsanteBlock:(DBAlertViewCompletionBlock)quartoPulsanteBlock conPulsanteChiudi:(NSString *)chiudi conChiudiBlock:(DBAlertViewCompletionBlock)chiudiBlock conActionSheet:(DBActionSheetBlock)actionSheetBlock;


/// Pulsanti accetta e rifiuta (versione localizzata)
+ (void)actionSheetLocalizzatoConTitolo:(NSString *)titolo conPulsanteAccetta:(NSString *)accetta conAccettaBlock:(DBAlertViewCompletionBlock)accettaBlock conPulsanteRifiuta:(NSString *)rifiuta conRifiutaBlock:(DBAlertViewCompletionBlock)rifiutaBlock conActionSheet:(DBActionSheetBlock)actionSheetBlock;

/// Tre pulsanti (versione localizzata)
+ (void)actionSheetLocalizzatoConTitolo:(NSString *)titolo conPrimoPulsante:(NSString *)primoPulsante conPrimoPulsanteBlock:(DBAlertViewCompletionBlock)primoPulsanteBlock conSecondoPulsante:(NSString *)secondoPulsante conSecondoPulsanteBlock:(DBAlertViewCompletionBlock)secondoPulsanteBlock conPulsanteChiudi:(NSString *)chiudi conChiudiBlock:(DBAlertViewCompletionBlock)chiudiBlock conActionSheet:(DBActionSheetBlock)actionSheetBlock;

/// Quattro pulsanti (versione localizzata)
+ (void)actionSheetLocalizzatoConTitolo:(NSString *)titolo conPrimoPulsante:(NSString *)primoPulsante conPrimoPulsanteBlock:(DBAlertViewCompletionBlock)primoPulsanteBlock conSecondoPulsante:(NSString *)secondoPulsante conSecondoPulsanteBlock:(DBAlertViewCompletionBlock)secondoPulsanteBlock conTerzoPulsante:(NSString *)terzoPulsante conTerzoPulsanteBlock:(DBAlertViewCompletionBlock)terzoPulsanteBlock conPulsanteChiudi:(NSString *)chiudi conChiudiBlock:(DBAlertViewCompletionBlock)chiudiBlock conActionSheet:(DBActionSheetBlock)actionSheetBlock;

/// Cinque pulsanti (versione localizzata)
+ (void)actionSheetLocalizzatoConTitolo:(NSString *)titolo conPrimoPulsante:(NSString *)primoPulsante conPrimoPulsanteBlock:(DBAlertViewCompletionBlock)primoPulsanteBlock conSecondoPulsante:(NSString *)secondoPulsante conSecondoPulsanteBlock:(DBAlertViewCompletionBlock)secondoPulsanteBlock conTerzoPulsante:(NSString *)terzoPulsante conTerzoPulsanteBlock:(DBAlertViewCompletionBlock)terzoPulsanteBlock conQuartoPulsante:(NSString *)quartoPulsante conQuartoPulsanteBlock:(DBAlertViewCompletionBlock)quartoPulsanteBlock conPulsanteChiudi:(NSString *)chiudi conChiudiBlock:(DBAlertViewCompletionBlock)chiudiBlock conActionSheet:(DBActionSheetBlock)actionSheetBlock;

#endif

@end
