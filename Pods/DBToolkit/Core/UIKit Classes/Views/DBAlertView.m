//
//  DBAlertView.m
//  File version: 1.1.0
//  Last modified: 02/14/2016
//
//  Created by Davide Balistreri on 04/15/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBAlertView.h"
#import "DBFramework.h"

@interface DBAlertView ()

#if DBFRAMEWORK_TARGET_IOS
<UIAlertViewDelegate, UIActionSheetDelegate>
#else
<NSAlertDelegate>
#endif

@property (strong, nonatomic) id object;

@property (copy) DBAlertViewCompletionBlock primoPulsanteBlock;
@property (copy) DBAlertViewCompletionBlock secondoPulsanteBlock;
@property (copy) DBAlertViewCompletionBlock terzoPulsanteBlock;
@property (copy) DBAlertViewCompletionBlock quartoPulsanteBlock;
@property (copy) DBAlertViewCompletionBlock quintoPulsanteBlock;

@end


@implementation DBAlertView

// MARK: - DBAlertView

+ (NSMutableArray *)sharedInstance
{
    @synchronized(self) {
        NSMutableArray *sharedInstance = [DBCentralManager getRisorsa:@"DBSharedAlertView"];
        
        if (!sharedInstance) {
            sharedInstance = [NSMutableArray array];
            [DBCentralManager setRisorsa:sharedInstance conIdentifier:@"DBSharedAlertView"];
        }
        
        return sharedInstance;
    }
}

+ (DBAlertView *)nuovaAlertView
{
    @synchronized(self) {
        DBAlertView *alertView = [DBAlertView new];
        NSMutableArray *sharedInstance = [self sharedInstance];
        [sharedInstance addObject:alertView];
        
        return alertView;
    }
}

+ (BOOL)isAlertViewValida:(DBAlertView *)alertView
{
    @synchronized(self) {
        NSMutableArray *sharedInstance = [self sharedInstance];
        return [sharedInstance containsObject:alertView];
    }
}

+ (void)rimuoviAlertView:(DBAlertView *)alertView
{
    @synchronized(self) {
        // Check
        if (!alertView || ![alertView isKindOfClass:[DBAlertView class]])
            return;
        
        NSMutableArray *sharedInstance = [self sharedInstance];
        
        if (sharedInstance) {
            // Libero la memoria e i puntatori occupati
            alertView.primoPulsanteBlock = nil;
            alertView.secondoPulsanteBlock = nil;
            alertView.terzoPulsanteBlock = nil;
            alertView.quartoPulsanteBlock = nil;
            alertView.quintoPulsanteBlock = nil;
            
            [sharedInstance removeObject:alertView];
        }
    }
}

// MARK: - Metodi comuni

/**
 * Chiude tutte le UIAlertView e gli UIActionSheet eventualmente aperti, senza eseguire il completionBlock.
 */
+ (void)chiudiTutto
{
    NSMutableArray *sharedInstance = [self sharedInstance];
    
    while (sharedInstance.count > 0) {
        DBAlertView *alertView = [sharedInstance firstObject];
        
        // Chiudo l'AlertView senza eseguire alcun completionBlock
        if ([alertView.object respondsToSelector:@selector(dismissWithClickedButtonIndex:animated:)]) {
            [alertView.object dismissWithClickedButtonIndex:-1 animated:YES];
        }
        
        [DBAlertView rimuoviAlertView:alertView];
    }
}


// MARK: - Alert View

+ (void)alertConTitolo:(NSString *)titolo conMessaggio:(NSString *)messaggio conPulsanteChiudi:(NSString *)chiudi conChiudiBlock:(DBAlertViewCompletionBlock)chiudiBlock
{
    if ([DBDevice isiOSMin:8.0]) {
        // Utilizzo il nuovo AlertController
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleAlert];
        if ([NSString isNotEmpty:titolo])    alertController.title   = [NSString cleanString:titolo];
        if ([NSString isNotEmpty:messaggio]) alertController.message = [NSString cleanString:messaggio];
        
        if (chiudi) {
            [alertController addAction:[UIAlertAction actionWithTitle:chiudi style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                if (chiudiBlock) chiudiBlock();
            }]];
        }
        
        UIViewController *viewController = [DBApplication presentedViewController];
        [viewController presentViewController:alertController animated:YES completion:nil];
    }
    else {
        DBAlertView *delegate = [self nuovaAlertView];
        delegate.primoPulsanteBlock = chiudiBlock;
        
        UIAlertView *alertView = [UIAlertView new];
        alertView.delegate = delegate;
        alertView.title = [NSString cleanString:titolo];
        if (messaggio) alertView.message = [NSString cleanString:messaggio];
        [alertView addButtonWithTitle:[NSString cleanString:chiudi]];
        [alertView setCancelButtonIndex:0];
        
        [alertView show];
        delegate.object = alertView;
    }
}

+ (void)alertConTitolo:(NSString *)titolo conMessaggio:(NSString *)messaggio conPulsanteAccetta:(NSString *)accetta conAccettaBlock:(DBAlertViewCompletionBlock)accettaBlock conPulsanteRifiuta:(NSString *)rifiuta conRifiutaBlock:(DBAlertViewCompletionBlock)rifiutaBlock
{
    if ([DBDevice isiOSMin:8.0]) {
        // Utilizzo il nuovo AlertController
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleAlert];
        if ([NSString isNotEmpty:titolo])    alertController.title   = [NSString cleanString:titolo];
        if ([NSString isNotEmpty:messaggio]) alertController.message = [NSString cleanString:messaggio];
        
        if (accetta) {
            [alertController addAction:[UIAlertAction actionWithTitle:accetta style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                if (accettaBlock) accettaBlock();
            }]];
        }
        
        if (rifiuta) {
            [alertController addAction:[UIAlertAction actionWithTitle:rifiuta style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                if (rifiutaBlock) rifiutaBlock();
            }]];
        }
        
        UIViewController *viewController = [DBApplication presentedViewController];
        [viewController presentViewController:alertController animated:YES completion:nil];
    }
    else {
        DBAlertView *delegate = [self nuovaAlertView];
        delegate.primoPulsanteBlock = accettaBlock;
        delegate.secondoPulsanteBlock = rifiutaBlock;
        
        UIAlertView *alertView = [UIAlertView new];
        alertView.delegate = delegate;
        alertView.title = [NSString cleanString:titolo];
        alertView.message = [NSString cleanString:messaggio];
        [alertView addButtonWithTitle:[NSString cleanString:accetta]];
        [alertView addButtonWithTitle:[NSString cleanString:rifiuta]];
        [alertView setCancelButtonIndex:1];
        
        [alertView show];
        delegate.object = alertView;
    }
}

+ (void)alertConTitolo:(NSString *)titolo conMessaggio:(NSString *)messaggio conPrimoPulsante:(NSString *)primoPulsante conPrimoPulsanteBlock:(DBAlertViewCompletionBlock)primoPulsanteBlock conSecondoPulsante:(NSString *)secondoPulsante conSecondoPulsanteBlock:(DBAlertViewCompletionBlock)secondoPulsanteBlock conPulsanteChiudi:(NSString *)chiudi conChiudiBlock:(DBAlertViewCompletionBlock)chiudiBlock
{
    if ([DBDevice isiOSMin:8.0]) {
        // Utilizzo il nuovo AlertController
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleAlert];
        if ([NSString isNotEmpty:titolo])    alertController.title   = [NSString cleanString:titolo];
        if ([NSString isNotEmpty:messaggio]) alertController.message = [NSString cleanString:messaggio];
        
        if (primoPulsante) {
            [alertController addAction:[UIAlertAction actionWithTitle:primoPulsante style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                if (primoPulsanteBlock) primoPulsanteBlock();
            }]];
        }
        
        if (secondoPulsante) {
            [alertController addAction:[UIAlertAction actionWithTitle:secondoPulsante style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                if (secondoPulsanteBlock) secondoPulsanteBlock();
            }]];
        }
        
        if (chiudi) {
            [alertController addAction:[UIAlertAction actionWithTitle:chiudi style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                if (chiudiBlock) chiudiBlock();
            }]];
        }
        
        UIViewController *viewController = [DBApplication presentedViewController];
        [viewController presentViewController:alertController animated:YES completion:nil];
    }
    else {
        DBAlertView *delegate = [self nuovaAlertView];
        delegate.primoPulsanteBlock = primoPulsanteBlock;
        delegate.secondoPulsanteBlock = secondoPulsanteBlock;
        delegate.terzoPulsanteBlock = chiudiBlock;
        
        UIAlertView *alertView = [UIAlertView new];
        alertView.delegate = delegate;
        alertView.title = [NSString cleanString:titolo];
        alertView.message = [NSString cleanString:messaggio];
        [alertView addButtonWithTitle:[NSString cleanString:primoPulsante]];
        [alertView addButtonWithTitle:[NSString cleanString:secondoPulsante]];
        
        if ([NSString isNotEmpty:chiudi]) {
            [alertView addButtonWithTitle:[NSString cleanString:chiudi]];
            [alertView setCancelButtonIndex:2];
        }
        
        [alertView show];
        delegate.object = alertView;
    }
}

+ (void)alertConTitolo:(NSString *)titolo conMessaggio:(NSString *)messaggio conPrimoPulsante:(NSString *)primoPulsante conPrimoPulsanteBlock:(DBAlertViewCompletionBlock)primoPulsanteBlock conSecondoPulsante:(NSString *)secondoPulsante conSecondoPulsanteBlock:(DBAlertViewCompletionBlock)secondoPulsanteBlock conTerzoPulsante:(NSString *)terzoPulsante conTerzoPulsanteBlock:(DBAlertViewCompletionBlock)terzoPulsanteBlock conPulsanteChiudi:(NSString *)chiudi conChiudiBlock:(DBAlertViewCompletionBlock)chiudiBlock
{
    DBAlertView *delegate = [self nuovaAlertView];
    delegate.primoPulsanteBlock = primoPulsanteBlock;
    delegate.secondoPulsanteBlock = secondoPulsanteBlock;
    delegate.terzoPulsanteBlock = terzoPulsanteBlock;
    delegate.quartoPulsanteBlock = chiudiBlock;
    
    UIAlertView *alertView = [UIAlertView new];
    alertView.delegate = delegate;
    alertView.title = [NSString cleanString:titolo];
    alertView.message = [NSString cleanString:messaggio];
    [alertView addButtonWithTitle:[NSString cleanString:primoPulsante]];
    [alertView addButtonWithTitle:[NSString cleanString:secondoPulsante]];
    [alertView addButtonWithTitle:[NSString cleanString:terzoPulsante]];
    
    if ([NSString isNotEmpty:chiudi]) {
        [alertView addButtonWithTitle:[NSString cleanString:chiudi]];
        [alertView setCancelButtonIndex:3];
    }
    
    [alertView show];
    delegate.object = alertView;
}

+ (void)alertLocalizzatoConTitolo:(NSString *)titolo conMessaggio:(NSString *)messaggio conPulsanteChiudi:(NSString *)chiudi conChiudiBlock:(DBAlertViewCompletionBlock)chiudiBlock
{
    [self alertConTitolo:[titolo stringaLocalizzata] conMessaggio:[messaggio stringaLocalizzata] conPulsanteChiudi:[chiudi stringaLocalizzata] conChiudiBlock:chiudiBlock];
}

+ (void)alertLocalizzatoConTitolo:(NSString *)titolo conMessaggio:(NSString *)messaggio conPulsanteAccetta:(NSString *)accetta conAccettaBlock:(DBAlertViewCompletionBlock)accettaBlock conPulsanteRifiuta:(NSString *)rifiuta conRifiutaBlock:(DBAlertViewCompletionBlock)rifiutaBlock
{
    [self alertConTitolo:[titolo stringaLocalizzata] conMessaggio:[messaggio stringaLocalizzata] conPulsanteAccetta:[accetta stringaLocalizzata] conAccettaBlock:accettaBlock conPulsanteRifiuta:[rifiuta stringaLocalizzata] conRifiutaBlock:rifiutaBlock];
}

+ (void)alertLocalizzatoConTitolo:(NSString *)titolo conMessaggio:(NSString *)messaggio conPrimoPulsante:(NSString *)primoPulsante conPrimoPulsanteBlock:(DBAlertViewCompletionBlock)primoPulsanteBlock conSecondoPulsante:(NSString *)secondoPulsante conSecondoPulsanteBlock:(DBAlertViewCompletionBlock)secondoPulsanteBlock conPulsanteChiudi:(NSString *)chiudi conChiudiBlock:(DBAlertViewCompletionBlock)chiudiBlock
{
    [self alertConTitolo:[titolo stringaLocalizzata] conMessaggio:[messaggio stringaLocalizzata] conPrimoPulsante:[primoPulsante stringaLocalizzata] conPrimoPulsanteBlock:primoPulsanteBlock conSecondoPulsante:[secondoPulsante stringaLocalizzata] conSecondoPulsanteBlock:secondoPulsanteBlock conPulsanteChiudi:[chiudi stringaLocalizzata] conChiudiBlock:chiudiBlock];
}

+ (void)alertLocalizzatoConTitolo:(NSString *)titolo conMessaggio:(NSString *)messaggio conPrimoPulsante:(NSString *)primoPulsante conPrimoPulsanteBlock:(DBAlertViewCompletionBlock)primoPulsanteBlock conSecondoPulsante:(NSString *)secondoPulsante conSecondoPulsanteBlock:(DBAlertViewCompletionBlock)secondoPulsanteBlock conTerzoPulsante:(NSString *)terzoPulsante conTerzoPulsanteBlock:(DBAlertViewCompletionBlock)terzoPulsanteBlock conPulsanteChiudi:(NSString *)chiudi conChiudiBlock:(DBAlertViewCompletionBlock)chiudiBlock
{
    [self alertConTitolo:[titolo stringaLocalizzata] conMessaggio:[messaggio stringaLocalizzata] conPrimoPulsante:[primoPulsante stringaLocalizzata] conPrimoPulsanteBlock:primoPulsanteBlock conSecondoPulsante:[secondoPulsante stringaLocalizzata] conSecondoPulsanteBlock:secondoPulsanteBlock conTerzoPulsante:[terzoPulsante stringaLocalizzata] conTerzoPulsanteBlock:terzoPulsanteBlock conPulsanteChiudi:[chiudi stringaLocalizzata] conChiudiBlock:chiudiBlock];
}


// MARK: - Action Sheet

#if DBFRAMEWORK_TARGET_IOS

+ (void)actionSheetConTitolo:(NSString *)titolo conPulsanteAccetta:(NSString *)accetta conAccettaBlock:(DBAlertViewCompletionBlock)accettaBlock conPulsanteRifiuta:(NSString *)rifiuta conRifiutaBlock:(DBAlertViewCompletionBlock)rifiutaBlock conActionSheet:(DBActionSheetBlock)actionSheetBlock
{
    // Check iniziale
    if (!actionSheetBlock) return;
    
    DBAlertView *delegate = [self nuovaAlertView];
    delegate.primoPulsanteBlock = accettaBlock;
    delegate.secondoPulsanteBlock = rifiutaBlock;
    
    UIActionSheet *actionSheet = [UIActionSheet new];
    actionSheet.delegate = delegate;
    
    if ([NSString isNotEmpty:titolo]) {
        actionSheet.title = [NSString cleanString:titolo];
    }
    
    [actionSheet addButtonWithTitle:[NSString cleanString:accetta]];
    [actionSheet addButtonWithTitle:[NSString cleanString:rifiuta]];
    [actionSheet setCancelButtonIndex:1];
    
    actionSheetBlock(actionSheet);
    delegate.object = actionSheet;
}

+ (void)actionSheetConTitolo:(NSString *)titolo conPrimoPulsante:(NSString *)primoPulsante conPrimoPulsanteBlock:(DBAlertViewCompletionBlock)primoPulsanteBlock conSecondoPulsante:(NSString *)secondoPulsante conSecondoPulsanteBlock:(DBAlertViewCompletionBlock)secondoPulsanteBlock conPulsanteChiudi:(NSString *)chiudi conChiudiBlock:(DBAlertViewCompletionBlock)chiudiBlock conActionSheet:(DBActionSheetBlock)actionSheetBlock
{
    // Check iniziale
    if (!actionSheetBlock) return;
    
    DBAlertView *delegate = [self nuovaAlertView];
    delegate.primoPulsanteBlock = primoPulsanteBlock;
    delegate.secondoPulsanteBlock = secondoPulsanteBlock;
    delegate.terzoPulsanteBlock = chiudiBlock;
    
    UIActionSheet *actionSheet = [UIActionSheet new];
    actionSheet.delegate = delegate;
    
    if ([NSString isNotEmpty:titolo]) {
        actionSheet.title = [NSString cleanString:titolo];
    }
    
    [actionSheet addButtonWithTitle:[NSString cleanString:primoPulsante]];
    [actionSheet addButtonWithTitle:[NSString cleanString:secondoPulsante]];
    
    if ([NSString isNotEmpty:chiudi]) {
        [actionSheet addButtonWithTitle:[NSString cleanString:chiudi]];
        [actionSheet setCancelButtonIndex:2];
    }
    
    actionSheetBlock(actionSheet);
    delegate.object = actionSheet;
}

+ (void)actionSheetConTitolo:(NSString *)titolo conPrimoPulsante:(NSString *)primoPulsante conPrimoPulsanteBlock:(DBAlertViewCompletionBlock)primoPulsanteBlock conSecondoPulsante:(NSString *)secondoPulsante conSecondoPulsanteBlock:(DBAlertViewCompletionBlock)secondoPulsanteBlock conTerzoPulsante:(NSString *)terzoPulsante conTerzoPulsanteBlock:(DBAlertViewCompletionBlock)terzoPulsanteBlock conPulsanteChiudi:(NSString *)chiudi conChiudiBlock:(DBAlertViewCompletionBlock)chiudiBlock conActionSheet:(DBActionSheetBlock)actionSheetBlock
{
    // Check iniziale
    if (!actionSheetBlock) return;
    
    DBAlertView *delegate = [self nuovaAlertView];
    delegate.primoPulsanteBlock = primoPulsanteBlock;
    delegate.secondoPulsanteBlock = secondoPulsanteBlock;
    delegate.terzoPulsanteBlock = terzoPulsanteBlock;
    delegate.quartoPulsanteBlock = chiudiBlock;
    
    UIActionSheet *actionSheet = [UIActionSheet new];
    actionSheet.delegate = delegate;
    
    if ([NSString isNotEmpty:titolo]) {
        actionSheet.title = [NSString cleanString:titolo];
    }
    
    [actionSheet addButtonWithTitle:[NSString cleanString:primoPulsante]];
    [actionSheet addButtonWithTitle:[NSString cleanString:secondoPulsante]];
    [actionSheet addButtonWithTitle:[NSString cleanString:terzoPulsante]];
    
    if ([NSString isNotEmpty:chiudi]) {
        [actionSheet addButtonWithTitle:[NSString cleanString:chiudi]];
        [actionSheet setCancelButtonIndex:3];
    }
    
    actionSheetBlock(actionSheet);
    delegate.object = actionSheet;
}

+ (void)actionSheetConTitolo:(NSString *)titolo conPrimoPulsante:(NSString *)primoPulsante conPrimoPulsanteBlock:(DBAlertViewCompletionBlock)primoPulsanteBlock conSecondoPulsante:(NSString *)secondoPulsante conSecondoPulsanteBlock:(DBAlertViewCompletionBlock)secondoPulsanteBlock conTerzoPulsante:(NSString *)terzoPulsante conTerzoPulsanteBlock:(DBAlertViewCompletionBlock)terzoPulsanteBlock conQuartoPulsante:(NSString *)quartoPulsante conQuartoPulsanteBlock:(DBAlertViewCompletionBlock)quartoPulsanteBlock conPulsanteChiudi:(NSString *)chiudi conChiudiBlock:(DBAlertViewCompletionBlock)chiudiBlock conActionSheet:(DBActionSheetBlock)actionSheetBlock
{
    // Check iniziale
    if (!actionSheetBlock) return;
    
    DBAlertView *delegate = [self nuovaAlertView];
    delegate.primoPulsanteBlock = primoPulsanteBlock;
    delegate.secondoPulsanteBlock = secondoPulsanteBlock;
    delegate.terzoPulsanteBlock = terzoPulsanteBlock;
    delegate.quartoPulsanteBlock = quartoPulsanteBlock;
    delegate.quintoPulsanteBlock = chiudiBlock;
    
    UIActionSheet *actionSheet = [UIActionSheet new];
    actionSheet.delegate = delegate;
    
    if ([NSString isNotEmpty:titolo]) {
        actionSheet.title = [NSString cleanString:titolo];
    }
    
    [actionSheet addButtonWithTitle:[NSString cleanString:primoPulsante]];
    [actionSheet addButtonWithTitle:[NSString cleanString:secondoPulsante]];
    [actionSheet addButtonWithTitle:[NSString cleanString:terzoPulsante]];
    [actionSheet addButtonWithTitle:[NSString cleanString:quartoPulsante]];
    
    if ([NSString isNotEmpty:chiudi]) {
        [actionSheet addButtonWithTitle:[NSString cleanString:chiudi]];
        [actionSheet setCancelButtonIndex:4];
    }
    
    actionSheetBlock(actionSheet);
    delegate.object = actionSheet;
}

+ (void)actionSheetLocalizzatoConTitolo:(NSString *)titolo conPulsanteAccetta:(NSString *)accetta conAccettaBlock:(DBAlertViewCompletionBlock)accettaBlock conPulsanteRifiuta:(NSString *)rifiuta conRifiutaBlock:(DBAlertViewCompletionBlock)rifiutaBlock conActionSheet:(DBActionSheetBlock)actionSheetBlock
{
    [self actionSheetConTitolo:[titolo stringaLocalizzata] conPulsanteAccetta:[accetta stringaLocalizzata] conAccettaBlock:accettaBlock conPulsanteRifiuta:[rifiuta stringaLocalizzata] conRifiutaBlock:rifiutaBlock conActionSheet:actionSheetBlock];
}

+ (void)actionSheetLocalizzatoConTitolo:(NSString *)titolo conPrimoPulsante:(NSString *)primoPulsante conPrimoPulsanteBlock:(DBAlertViewCompletionBlock)primoPulsanteBlock conSecondoPulsante:(NSString *)secondoPulsante conSecondoPulsanteBlock:(DBAlertViewCompletionBlock)secondoPulsanteBlock conPulsanteChiudi:(NSString *)chiudi conChiudiBlock:(DBAlertViewCompletionBlock)chiudiBlock conActionSheet:(DBActionSheetBlock)actionSheetBlock
{
    [self actionSheetConTitolo:[titolo stringaLocalizzata] conPrimoPulsante:[primoPulsante stringaLocalizzata] conPrimoPulsanteBlock:primoPulsanteBlock conSecondoPulsante:[secondoPulsante stringaLocalizzata] conSecondoPulsanteBlock:secondoPulsanteBlock conPulsanteChiudi:[chiudi stringaLocalizzata] conChiudiBlock:chiudiBlock conActionSheet:actionSheetBlock];
}

+ (void)actionSheetLocalizzatoConTitolo:(NSString *)titolo conPrimoPulsante:(NSString *)primoPulsante conPrimoPulsanteBlock:(DBAlertViewCompletionBlock)primoPulsanteBlock conSecondoPulsante:(NSString *)secondoPulsante conSecondoPulsanteBlock:(DBAlertViewCompletionBlock)secondoPulsanteBlock conTerzoPulsante:(NSString *)terzoPulsante conTerzoPulsanteBlock:(DBAlertViewCompletionBlock)terzoPulsanteBlock conPulsanteChiudi:(NSString *)chiudi conChiudiBlock:(DBAlertViewCompletionBlock)chiudiBlock conActionSheet:(DBActionSheetBlock)actionSheetBlock
{
    [self actionSheetConTitolo:[titolo stringaLocalizzata] conPrimoPulsante:[primoPulsante stringaLocalizzata] conPrimoPulsanteBlock:primoPulsanteBlock conSecondoPulsante:[secondoPulsante stringaLocalizzata] conSecondoPulsanteBlock:secondoPulsanteBlock conTerzoPulsante:[terzoPulsante stringaLocalizzata] conTerzoPulsanteBlock:terzoPulsanteBlock conPulsanteChiudi:[chiudi stringaLocalizzata] conChiudiBlock:chiudiBlock conActionSheet:actionSheetBlock];
}

+ (void)actionSheetLocalizzatoConTitolo:(NSString *)titolo conPrimoPulsante:(NSString *)primoPulsante conPrimoPulsanteBlock:(DBAlertViewCompletionBlock)primoPulsanteBlock conSecondoPulsante:(NSString *)secondoPulsante conSecondoPulsanteBlock:(DBAlertViewCompletionBlock)secondoPulsanteBlock conTerzoPulsante:(NSString *)terzoPulsante conTerzoPulsanteBlock:(DBAlertViewCompletionBlock)terzoPulsanteBlock conQuartoPulsante:(NSString *)quartoPulsante conQuartoPulsanteBlock:(DBAlertViewCompletionBlock)quartoPulsanteBlock conPulsanteChiudi:(NSString *)chiudi conChiudiBlock:(DBAlertViewCompletionBlock)chiudiBlock conActionSheet:(DBActionSheetBlock)actionSheetBlock
{
    [self actionSheetConTitolo:[titolo stringaLocalizzata] conPrimoPulsante:[primoPulsante stringaLocalizzata] conPrimoPulsanteBlock:primoPulsanteBlock conSecondoPulsante:[secondoPulsante stringaLocalizzata] conSecondoPulsanteBlock:secondoPulsanteBlock conTerzoPulsante:[terzoPulsante stringaLocalizzata] conTerzoPulsanteBlock:terzoPulsanteBlock conQuartoPulsante:[quartoPulsante stringaLocalizzata] conQuartoPulsanteBlock:quartoPulsanteBlock conPulsanteChiudi:[chiudi stringaLocalizzata] conChiudiBlock:chiudiBlock conActionSheet:actionSheetBlock];
}

#endif

// MARK: - Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    @synchronized(self) {
        switch (buttonIndex) {
            case 0:
                // Primo pulsante
                if (self.primoPulsanteBlock) self.primoPulsanteBlock();
                break;
                
            case 1:
                // Secondo pulsante
                if (self.secondoPulsanteBlock) self.secondoPulsanteBlock();
                break;
                
            case 2:
                // Terzo pulsante
                if (self.terzoPulsanteBlock) self.terzoPulsanteBlock();
                break;
                
            case 3:
                // Quarto pulsante
                if (self.quartoPulsanteBlock) self.quartoPulsanteBlock();
                break;
                
            case 4:
                // Chiudi
                if (self.quintoPulsanteBlock) self.quintoPulsanteBlock();
                break;
        }
        
        [DBAlertView rimuoviAlertView:self];
    }
}

#if DBFRAMEWORK_TARGET_IOS

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    @synchronized(self) {
        [self alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:buttonIndex];
    }
}

#endif

@end
