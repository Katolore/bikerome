//
//  NSImageView+AspectFill.m
//  Versione file: 1.0.0
//  Ultimo aggiornamento: 14/02/2016
//
//  Creato da Davide Balistreri il 14/02/2016
//  Copyright 2013-2016 © Davide Balistreri. Tutti i diritti riservati.
//

#import "NSImageView+AspectFill.h"
#import "DBGeometry.h"

@implementation NSImageViewAspectFill

- (id)init
{
    self = [super init];
    if (self) {
        [self inizializza];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self inizializza];
    }
    return self;
}

- (id)initWithFrame:(NSRect)frameRect
{
    self = [super initWithFrame:frameRect];
    if (self) {
        [self inizializza];
    }
    return self;
}

- (void)inizializza
{
    [self setImageScaling:self.imageScaling];
    [self setImage:self.image];
}

- (void)setImageScaling:(NSImageScaling)newScaling
{
    // That's necessary to use nothing but NSImageScaleAxesIndependently
    [super setImageScaling:NSImageScaleAxesIndependently];
}

- (NSSize)intrinsicContentSize
{
    // Fix Autolayout
    return [DBGeometry dimensioniSchermo];
}

- (void)setImage:(NSImage *)image
{
    if (image == nil) {
        [super setImage:image];
        return;
    }
    
    NSImage *scaleToFillImage = [NSImage imageWithSize:self.bounds.size flipped:NO drawingHandler:^BOOL(NSRect dstRect) {
        
        NSSize imageSize = [image size];
        NSSize imageViewSize = self.bounds.size; // Yes, do not use dstRect.
        
        NSSize newImageSize = imageSize;
        
        CGFloat imageAspectRatio = imageSize.height/imageSize.width;
        CGFloat imageViewAspectRatio = imageViewSize.height/imageViewSize.width;
        
        if (imageAspectRatio < imageViewAspectRatio) {
            // Image is more horizontal than the view. Image left and right borders need to be cropped.
            newImageSize.width = imageSize.height / imageViewAspectRatio;
        }
        else {
            // Image is more vertical than the view. Image top and bottom borders need to be cropped.
            newImageSize.height = imageSize.width * imageViewAspectRatio;
        }
        
        NSRect srcRect = NSMakeRect(imageSize.width/2.0-newImageSize.width/2.0,
                                    imageSize.height/2.0-newImageSize.height/2.0,
                                    newImageSize.width,
                                    newImageSize.height);
        
        [[NSGraphicsContext currentContext] setImageInterpolation:NSImageInterpolationMedium];
        
        [image drawInRect:dstRect // Interestingly, here needs to be dstRect and not self.bounds
                 fromRect:srcRect
                operation:NSCompositeCopy
                 fraction:1.0
           respectFlipped:YES
                    hints:@{NSImageHintInterpolation:@(NSImageInterpolationMedium)}];
        
        return YES;
    }];
    
    [scaleToFillImage setCacheMode:NSImageCacheNever]; // Hence it will automatically redraw with new frame size of the image view.
    
    [super setImage:scaleToFillImage];
}

@end
