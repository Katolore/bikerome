//
//  DBView.m
//  File version: 1.0.1
//  Last modified: 11/26/2016
//
//  Created by Davide Balistreri on 05/11/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBView.h"

@interface DBView (PrivateDeprecatedMethods)

- (void)setupView;

- (void)inizializza;

@end


@implementation DBView

// MARK: - Setup

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self privateSetupObject];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self privateSetupObject];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self privateSetupObject];
    }
    return self;
}

@synthesize setupCompleted = _setupCompleted;
- (void)privateSetupObject
{
    if (self.setupCompleted == NO) {
        // Prevent further initialization requests
        _setupCompleted = YES;
        
        // Perform the method implemented by the developer
        [self setupObject];
        
        if ([self respondsToSelector:@selector(setupView)]) {
            DBFRAMEWORK_DEPRECATED_CUSTOM_LOG("-[DBView setupView]", 0.7.1, "please use -[DBView setupObject]");
            [self performSelector:@selector(setupView)];
        }
        
        if ([self respondsToSelector:@selector(inizializza)]) {
            DBFRAMEWORK_DEPRECATED_CUSTOM_LOG("-[DBView inizializza]", 0.7.1, "please use -[DBView setupObject]");
            [self performSelector:@selector(inizializza)];
        }
    }
}

@synthesize viewDidAppear = _viewDidAppear;
- (void)didMoveToSuperview
{
    [super didMoveToSuperview];
    _viewDidAppear = YES;
}

- (BOOL)inizializzazioneCompletata
{
    DBFRAMEWORK_DEPRECATED_LOG(0.7.1, "please use -[DBView setupCompleted]");
    return self.setupCompleted;
}

- (void)setupObject
{
    // Developers may override this method and provide custom behavior
}

@end
