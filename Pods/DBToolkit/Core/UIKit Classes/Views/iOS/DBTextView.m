//
//  DBTextView.m
//  File version: 1.0.3
//  Last modified: 04/27/2017
//
//  Created by Davide Balistreri on 03/15/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBTextView.h"
#import "DBGeometry.h"
#import "DBNotificationCenter.h"

// MARK: - DBTextViewPlaceholderLabel subclass

@interface DBTextViewPlaceholderLabel : UILabel

@property (weak, nonatomic) NSLayoutConstraint *layoutMinHeight;

@end

@implementation DBTextViewPlaceholderLabel

@end


// MARK: - DBTextView class

@interface DBTextView ()

@property (strong, nonatomic) DBTextViewPlaceholderLabel *placeholderLabel;

@end


@implementation DBTextView

// MARK: - Setup

- (instancetype)init
{
    self = [super init];
    
    if (self) {
        [self privateSetupObject];
    }
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    
    if (self) {
        [self privateSetupObject];
    }
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
        [self privateSetupObject];
    }
    
    return self;
}

@synthesize setupCompleted = _setupCompleted;
- (void)privateSetupObject
{
    if (self.setupCompleted == NO) {
        // Prevent further initialization requests
        _setupCompleted = YES;
        
        // Setup placeholder label
        DBTextViewPlaceholderLabel *placeholderLabel = [DBTextViewPlaceholderLabel new];
        placeholderLabel.translatesAutoresizingMaskIntoConstraints = NO;
        placeholderLabel.numberOfLines = 0;
        self.placeholderLabel = placeholderLabel;
        
        // Perform the method implemented by the developer
        [self setupObject];
    }
}

- (void)setupObject
{
    // Developers may override this method and provide custom behavior
}


// MARK: - Private methods

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if (self.placeholderLabel) {
        CGRect caretRect = [self caretRectForPosition:self.beginningOfDocument];
        self.placeholderLabel.constraintTop     = caretRect.origin.y;
        self.placeholderLabel.constraintLeft    = caretRect.origin.x;
        self.placeholderLabel.constraintRight   = caretRect.origin.x;
        
        self.placeholderLabel.layoutMinHeight.constant = caretRect.size.height;
        [self.placeholderLabel layoutIfNeeded];
    }
}

- (void)setupPlaceholder
{
    if (self.placeholder) {
        [self loadPlaceholder];
    }
    else {
        [self unloadPlaceholder];
    }
    
    [self updatePlaceholderVisibility];
    [self updatePlaceholderStyle];
}

- (void)updatePlaceholderVisibility
{
    if (self.placeholderLabel) {
        // Mostro il placeholder solo quando non c'è del testo sulla TextView
        if (self.text.length == 0 && self.placeholderLabel.hidden) {
            self.placeholderLabel.hidden = NO;
        }
        else if (self.text.length > 0 && self.placeholderLabel.hidden == NO) {
            self.placeholderLabel.hidden = YES;
        }
    }
}

- (void)updatePlaceholderStyle
{
    if (self.placeholderLabel) {
        // Aggiorno lo stile
        self.placeholderLabel.font = self.placeholderFont;
        self.placeholderLabel.textColor = self.placeholderColor;
        self.placeholderLabel.text = self.placeholder;
    }
}

- (void)loadPlaceholder
{
    [self insertSubview:self.placeholderLabel atIndex:0];
    
    // Autolayout
    NSMutableArray *constraints = [NSMutableArray array];
    
    DBTextViewPlaceholderLabel *placeholder = self.placeholderLabel;
    
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:placeholder attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0];
    [constraints addObject:top];
    
    NSLayoutConstraint *left = [NSLayoutConstraint constraintWithItem:placeholder attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0];
    [constraints addObject:left];
    
    NSLayoutConstraint *maxWidth = [NSLayoutConstraint constraintWithItem:placeholder attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0.0];
    [maxWidth setPriority:900];
    [constraints addObject:maxWidth];
    
    NSLayoutConstraint *minHeight = [NSLayoutConstraint constraintWithItem:placeholder attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:10.0];
    [minHeight setPriority:900];
    [constraints addObject:minHeight];
    [placeholder setLayoutMinHeight:minHeight];
    
    [self addConstraints:constraints];
    [self layoutIfNeeded];
    
    [DBNotificationCenter removeObserversOnTarget:self forName:UITextViewTextDidChangeNotification];
    [DBNotificationCenter addObserverOnTarget:self forName:UITextViewTextDidChangeNotification withSelector:@selector(updatePlaceholderVisibility) object:nil];
    
    [self updatePlaceholderVisibility];
}

- (void)unloadPlaceholder
{
    [DBNotificationCenter removeObserversOnTarget:self forName:UITextViewTextDidChangeNotification];
    [self.placeholderLabel removeFromSuperview];
}

- (void)dealloc
{
    [self unloadPlaceholder];
}

- (CGSize)intrinsicContentSize
{
    CGSize placeholderSize = [self placeholderSize];
    CGSize textViewSize = [super intrinsicContentSize];
    
    CGSize size = textViewSize;
    size.height = fmax(textViewSize.height, placeholderSize.height);
    return size;
}

- (CGSize)placeholderSize
{
    CGSize size = self.placeholderLabel.intrinsicContentSize;
    size.width  += self.textContainerInset.left + self.textContainerInset.right;
    size.height += self.textContainerInset.top + self.textContainerInset.bottom;
    return size;
}


// MARK: - Public methods

@synthesize placeholder = _placeholder;
- (void)setPlaceholder:(NSString *)placeholder
{
    _placeholder = placeholder;
    [self setupPlaceholder];
}

@synthesize placeholderColor = _placeholderColor;
- (UIColor *)placeholderColor
{
    return (_placeholderColor) ? _placeholderColor : self.textColor;
}

- (void)setPlaceholderColor:(UIColor *)placeholderColor
{
    _placeholderColor = placeholderColor;
    [self updatePlaceholderStyle];
}

@synthesize placeholderFont = _placeholderFont;
- (UIFont *)placeholderFont
{
    return (_placeholderFont) ? _placeholderFont : self.font;
}

- (void)setPlaceholderFont:(UIFont *)placeholderFont
{
    _placeholderFont = placeholderFont;
    [self updatePlaceholderStyle];
}

- (void)setTextColor:(UIColor *)textColor
{
    [super setTextColor:textColor];
    [self updatePlaceholderStyle];
}

- (void)setFont:(UIFont *)font
{
    [super setFont:font];
    [self updatePlaceholderStyle];
}

- (void)setText:(NSString *)text
{
    [super setText:text];
    [self updatePlaceholderVisibility];
}

- (void)setAttributedText:(NSAttributedString *)attributedText
{
    [super setAttributedText:attributedText];
    [self updatePlaceholderVisibility];
}

@end
