//
//  DBDraggableView.h
//  File version: 1.0.1
//  Last modified: 03/21/2016
//
//  Created by Davide Balistreri on 07/07/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DBDraggableViewDelegate;


/**
 * DraggableView è una View con possibilità di spostamento.
 */
@interface DBDraggableView : UIView

/// Come deve comportarsi la DraggableView durante lo spostamento.
typedef NS_ENUM(NSUInteger, DBDraggableViewMode) {
    /// Default. Spostamento libero.
    DBDraggableViewModeLibero,
    /// Mantiene i bordi della View all'interno della Superview.
    DBDraggableViewModeBordiAllInterno,
    /// Mantiene il centro della View all'interno della Superview.
    DBDraggableViewModeCentroAllInterno,
    /// Impedisce lo spostamento della View.
    DBDraggableViewModeNessuno
};

@property (nonatomic) DBDraggableViewMode mode;

@property (nonatomic) BOOL animazioneTouchDown;

/**
 * Permette di disabilitare gli spostamenti della DraggableView.
 */
@property (nonatomic, getter = isEnabled) BOOL enabled;

@property (weak, nonatomic) id<DBDraggableViewDelegate> delegate;

@end


@protocol DBDraggableViewDelegate <NSObject>

- (void)draggableViewDidMove:(DBDraggableView *)draggableView;

@end
