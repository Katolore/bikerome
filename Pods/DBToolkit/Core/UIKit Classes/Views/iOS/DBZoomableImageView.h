//
//  DBZoomableImageView.h
//  File version: 1.0.0
//  Last modified: 12/01/2015
//
//  Created by Davide Balistreri on 11/26/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

@class DBZoomableImageView;

@protocol DBZoomableImageViewDelegate <NSObject>

- (void)zoomableImageViewTouchUpInside:(DBZoomableImageView *)imageView;

@end


@interface DBZoomableImageView : UIControl

@property (nonatomic) id<DBZoomableImageViewDelegate> delegate;

@property (strong, nonatomic) UIImage *image;

@property (nonatomic) CGFloat minimumZoomScale;
@property (nonatomic) CGFloat maximumZoomScale;

@end
