//
//  DBControl.h
//  File version: 1.0.0
//  Last modified: 05/11/2016
//
//  Created by Davide Balistreri on 05/11/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

@interface DBControl : UIControl

/// Boolean che indica se l'oggetto è stato inizializzato completamente da quando è stato creato.
@property (nonatomic, readonly) BOOL setupCompleted;

/**
 * Metodo da utilizzare per inizializzare l'oggetto.
 * <p>Viene richiamato automaticamente appena l'oggetto viene istanziato.</p>
 * <p>Deve essere eseguito un'unica volta.</p>
 */
- (void)setupObject;

@end
