//
//  DBSlideshowView.m
//  File version: 1.0.1
//  Last modified: 03/10/2016
//
//  Created by Davide Balistreri on 02/22/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBSlideshowView.h"

#import "DBAutoLayoutImageView.h"
#import "DBFramework.h"

@interface DBSlideshowView ()

@property (strong, nonatomic) DBAutoLayoutImageView *imageSotto;
@property (strong, nonatomic) DBAutoLayoutImageView *imageSopra;

@property (strong, nonatomic) NSArray *dataSource;

@property (nonatomic) BOOL inEsecuzione;
@property (nonatomic) NSInteger index;

@end


@implementation DBSlideshowView

// MARK: - Inizializzazione

- (void)setupObject
{
    if (self.setupCompleted == NO) {
        self.imageSotto = [DBAutoLayoutImageView new];
        self.imageSotto.contentMode = UIViewContentModeScaleAspectFill;
        self.imageSotto.frame = self.bounds;
        self.imageSotto.autoresizingMask = DBViewAdattaDimensioni;
        [self addSubview:self.imageSotto];
        
        self.imageSopra = [DBAutoLayoutImageView new];
        self.imageSopra.contentMode = UIViewContentModeScaleAspectFill;
        self.imageSopra.frame = self.bounds;
        self.imageSopra.autoresizingMask = DBViewAdattaDimensioni;
        [self addSubview:self.imageSopra];
        
        // Configurazione di default
        self.durataVisualizzazione = 5.0;
        self.durataTransizione = 5.0;
        self.animazioneVisualizzazione = DBSlideshowAnimazioneVisualizzazioneStatica;
        self.animazioneTransizione = DBSlideshowAnimazioneTransizioneDissolvenza;
        
        self.userInteractionEnabled = NO;
    }
    
    [super setupObject];
}

// MARK: - Gestione slideshow

- (void)avviaSlideshow
{
    self.inEsecuzione = YES;
    [self eseguiSlideshow];
}

- (void)eseguiSlideshow
{
    if (self.inEsecuzione == NO) {
        // Slideshow interrotto
        return;
    }
    
    if (self.index >= self.dataSource.count) {
        // Ricomincio lo slideshow
        self.index = 0;
    }
    
    // Prendo la prossima foto
    UIImage *prossima = [self.dataSource safeObjectAtIndex:self.index];
    self.index++;
    
    if (!self.imageSotto.image) {
        // Carico la prima immagine
        self.imageSotto.image = prossima;
        
        [DBThread eseguiBlockConRitardo:self.durataVisualizzazione block:^{
            [self eseguiSlideshow];
        }];
    }
    else {
        self.imageSopra.alpha = 0.0;
        self.imageSopra.image = prossima;
        
        [UIView animateWithDuration:self.durataTransizione animations:^{
            self.imageSopra.alpha = 1.0;
            
        } completion:^(BOOL finished) {
            self.imageSotto.image = prossima;
            self.imageSopra.image = nil;
            
            [DBThread eseguiBlockConRitardo:self.durataVisualizzazione block:^{
                [self eseguiSlideshow];
            }];
        }];
    }
}

- (void)interrompiSlideshow
{
    self.inEsecuzione = NO;
}

// MARK: - Gestione DataSource

- (void)setImmagini:(NSArray *)immagini
{
    _immagini = immagini;
    self.dataSource = [immagini copy];
}

@end
