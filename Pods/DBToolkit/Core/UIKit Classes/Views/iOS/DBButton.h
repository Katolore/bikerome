//
//  DBButton.h
//  File version: 1.1.0
//  Last modified: 03/13/2016
//
//  Created by Davide Balistreri on 02/23/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

#if DBFRAMEWORK_VERSION_LATER_THAN(__DBFRAMEWORK_0_6_0) && DBFRAMEWORK_VERSION_EARLIER_THAN(__DBFRAMEWORK_0_6_1)
    #warning Bisogna adattare il codice DBButton alla nuova versione del Framework.
#endif


/**
 * UIButton object extended for an easier setup process and later use.
 * The setup sequence is the same as other objects, views and controls inside this framework.
 */

@interface DBButton : UIButton

/// Boolean che indica se l'oggetto è stato inizializzato completamente da quando è stato creato.
@property (nonatomic, readonly) BOOL setupCompleted;

/**
 * Metodo da utilizzare per inizializzare l'oggetto.
 * <p>Viene richiamato automaticamente appena l'oggetto viene istanziato.</p>
 * <p>Deve essere eseguito un'unica volta.</p>
 */
- (void)setupObject;


/**
 * Le animazioni disponibili per il pulsante.
 */
typedef NS_ENUM(NSUInteger, DBButtonMode) {
    /// Nessuna animazione. Impostazione di default.
    DBButtonModeNessunaAnimazione,
    /// Alla pressione verrà eseguito uno zoom-out del contenuto del pulsante.
    DBButtonModeZoomOut
};


/**
 * L'animazione da eseguire quando il pulsante viene selezionato.
 * Default: nessuna.
 */
@property (nonatomic) DBButtonMode mode;

/**
 * Attiva o disattiva il cambio di stile quando il pulsante viene selezionato.
 * Default: disattivo.
 */
@property (nonatomic) BOOL selezionatoAggiornaStile;

@property (weak, nonatomic) UIImage *immagineNormale;
@property (weak, nonatomic) UIImage *immagineSelezionato;
@property (weak, nonatomic) UIColor *coloreNormale;
@property (weak, nonatomic) UIColor *coloreSelezionato;

@end
