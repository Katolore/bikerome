//
//  DBSlideshowView.h
//  File version: 1.0.1
//  Last modified: 03/10/2016
//
//  Created by Davide Balistreri on 02/22/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBAutoLayoutView.h"

@interface DBSlideshowView : DBAutoLayoutView

/**
 * Le varie modalità di esecuzione dello slideshow.
 */
typedef NS_ENUM(NSUInteger, DBSlideshowAnimazioneVisualizzazione) {
    /// Il dataSource verrà visualizzato con un effetto dissolvenza.
    DBSlideshowAnimazioneVisualizzazioneStatica
};

/**
 * Le varie modalità di esecuzione dello slideshow.
 */
typedef NS_ENUM(NSUInteger, DBSlideshowAnimazioneTransizione) {
    /// Il dataSource verrà visualizzato senza alcun effetto.
    DBSlideshowAnimazioneTransizioneStatica,
    /// Il dataSource verrà visualizzato con un effetto dissolvenza.
    DBSlideshowAnimazioneTransizioneDissolvenza
};


/**
 * L'animazione quando si visualizza un elemento.
 */
@property (nonatomic) DBSlideshowAnimazioneVisualizzazione animazioneVisualizzazione;

/**
 * L'animazione quando si passa da un elemento all'altro.
 */
@property (nonatomic) DBSlideshowAnimazioneTransizione animazioneTransizione;

/**
 * La durata della visualizzazione di un elemento.
 */
@property (nonatomic) NSTimeInterval durataVisualizzazione;

/**
 * La durata del passaggio da un elemento all'altro.
 */
@property (nonatomic) NSTimeInterval durataTransizione;


/**
 * Array di immagini salvate nella memoria locale del dispositivo.
 */
@property (strong, nonatomic) NSArray *immagini;


/**
 * Avvia lo slideshow.
 */
- (void)avviaSlideshow;

/**
 * Interrompe lo slideshow.
 */
- (void)interrompiSlideshow;

@end
