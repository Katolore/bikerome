//
//  DBZoomableImageView.m
//  File version: 1.0.0
//  Last modified: 12/01/2015
//
//  Created by Davide Balistreri on 11/26/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBZoomableImageView.h"

#import "DBGeometry.h"

@interface DBZoomableImageViewDelegate : UIViewController <UIScrollViewDelegate>

@property (weak, nonatomic) DBZoomableImageView *sender;

@end


@interface DBZoomableImageView () <UIGestureRecognizerDelegate>

@property (strong, nonatomic) DBZoomableImageViewDelegate *scrollDelegate;

@property (strong, nonatomic) UIImageView *imageView;
@property (strong, nonatomic) UIScrollView *scrollView;

@property (strong, nonatomic) UITapGestureRecognizer *recognizerTapSingolo;
@property (strong, nonatomic) UITapGestureRecognizer *recognizerTapDoppio;

@property (nonatomic) CGRect frameOriginale;

@end


@implementation DBZoomableImageView

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.scrollDelegate = [DBZoomableImageViewDelegate new];
        self.scrollDelegate.sender = self;
        self.minimumZoomScale = 1.0;
        self.maximumZoomScale = 4.0;
    }
    return self;
}

- (void)dealloc
{
    if (self.imageView) {
        [self.imageView removeFromSuperview];
        [self.scrollView removeFromSuperview];
        [self.scrollView removeGestureRecognizer:self.recognizerTapSingolo];
        [self.scrollView removeGestureRecognizer:self.recognizerTapDoppio];
        self.recognizerTapSingolo = nil;
        self.recognizerTapDoppio = nil;
        self.scrollDelegate = nil;
        self.imageView = nil;
        self.scrollView = nil;
    }
}

- (void)impostaView
{
    if (!self.imageView) {
        // Inizializzazione
        UIScrollView *scrollView = [UIScrollView new];
        scrollView.frame = self.bounds;
        scrollView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
        scrollView.minimumZoomScale = self.minimumZoomScale;
        scrollView.maximumZoomScale = self.maximumZoomScale;
        scrollView.delegate = self.scrollDelegate;
        scrollView.clipsToBounds = YES;
        self.scrollView = scrollView;
        
        UIImageView *imageView = [UIImageView new];
        imageView.image = self.image;
        self.imageView = imageView;
        
        [scrollView addSubview:imageView];
        [self addSubview:self.scrollView];
    }
    
    if (self.delegate && !self.recognizerTapSingolo) {
        self.recognizerTapSingolo = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(azioneRecognizerTapSingolo:)];
        self.recognizerTapSingolo.numberOfTapsRequired = 1;
        self.recognizerTapSingolo.delegate = self;
        [self.scrollView addGestureRecognizer:self.recognizerTapSingolo];
    }
    
    if (!self.recognizerTapDoppio) {
        self.recognizerTapDoppio = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(azioneRecognizerTapDoppio:)];
        self.recognizerTapDoppio.numberOfTapsRequired = 2;
        self.recognizerTapDoppio.delegate = self;
        [self.scrollView addGestureRecognizer:self.recognizerTapDoppio];
    }
    
    if (self.recognizerTapSingolo && self.recognizerTapDoppio)
        [self.recognizerTapSingolo requireGestureRecognizerToFail:self.recognizerTapDoppio];
    
    self.scrollView.frame = self.bounds;
}

- (void)setImage:(UIImage *)image
{
    _image = image;
    self.imageView.image = image;
    [self adattaDimensioneAllImmagine];
}

- (void)adattaDimensioneAllImmagine
{
    if (!self.image) {
        // Not valid
        return;
    }
    
    CGRect frame = self.bounds;
    
    // Se è cambiato l'orientamento, bisogna calcolare il nuovo contentOffset per centrare l'immagine
    // sulla stessa porzione che era visibile con l'orientamento precedente
    //    CGPoint centro = self.imageView.center;
    //    CGFloat centroX = (self.scrollView.visibleRect.size.width  / 2.0) + self.scrollView.visibleRect.origin.x;
    //    CGFloat centroY = (self.scrollView.visibleRect.size.height / 2.0) + self.scrollView.visibleRect.origin.y;
    //    centroX /= self.scrollView.zoomScale;
    //    centroY /= self.scrollView.zoomScale;
    //    NSLog(@"Centro: %.2f %.2f", centroX, centroY);
    
    UIImage *immagine = self.image;
    CGSize dimensioniImmagine = immagine.size;
    
    CGFloat riduzioneLarghezza = frame.size.width  / dimensioniImmagine.width;
    CGFloat riduzioneAltezza   = frame.size.height / dimensioniImmagine.height;
    riduzioneLarghezza *= self.scrollView.zoomScale;
    riduzioneAltezza   *= self.scrollView.zoomScale;
    
    CGFloat riduzione = riduzioneLarghezza;
    if (riduzioneAltezza < riduzioneLarghezza)
        riduzione = riduzioneAltezza;
    
    dimensioniImmagine.width  *= riduzione;
    dimensioniImmagine.height *= riduzione;
    
    self.imageView.dimensioni = dimensioniImmagine;
    self.scrollView.contentSize = dimensioniImmagine;
    
    //    CGPoint centro = CGPointMake(centroX, centroY);
    //    centroX *= riduzioneLarghezza;
    //    centroY *= riduzioneAltezza;
    //    centro = CGPointMake(centroX, centroY);
    // self.scrollView.contentOffset = centro;
    
    [self centraImageView];
}

- (void)centraImageView
{
    [self calcolaContentInset];
}

- (void)calcolaContentInset
{
    // Calcolo le dimensioni degli spazi vuoti per mantenere centrata l'ImageView
    CGFloat spazioX = 0.0;
    CGFloat spazioY = 0.0;
    
    if (self.imageView.larghezza < self.scrollView.larghezza) {
        spazioX = (self.scrollView.larghezza - self.imageView.larghezza) / 2.0;
    }
    if (self.imageView.altezza < self.scrollView.altezza) {
        spazioY = (self.scrollView.altezza - self.imageView.altezza) / 2.0;
    }
    
    // NSLog(@"Centro: %.2f %.2f", spazioX, spazioY);
    self.scrollView.contentInset = UIEdgeInsetsMake(spazioY, spazioX, spazioY, spazioX);
}

- (void)didMoveToSuperview
{
    [super didMoveToSuperview];
    
    if (self.superview) {
        [self impostaView];
    }
}

- (void)azioneRecognizerTapSingolo:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(zoomableImageViewTouchUpInside:)]) {
        [self.delegate zoomableImageViewTouchUpInside:self];
    }
}

- (void)azioneRecognizerTapDoppio:(UITapGestureRecognizer *)sender
{
    if (self.scrollView.zoomScale > self.scrollView.minimumZoomScale) {
        // Zoom out
        [self.scrollView setZoomScale:self.scrollView.minimumZoomScale animated:YES];
    }
    else {
        // Zoom in
        CGPoint centro = [sender locationInView:sender.view];
        CGRect zoomRect = [self zoomRectForScale:self.scrollView.maximumZoomScale withCenter:centro];
        [self.scrollView zoomToRect:zoomRect animated:YES];
    }
}

- (CGRect)zoomRectForScale:(CGFloat)scale withCenter:(CGPoint)center
{
    CGRect zoomRect;
    
    zoomRect.size.height = [self.imageView frame].size.height / scale;
    zoomRect.size.width  = [self.imageView frame].size.width  / scale;
    
    center = [self.imageView convertPoint:center fromView:self.scrollView];
    
    zoomRect.origin.x = center.x - ((zoomRect.size.width  / 2.0));
    zoomRect.origin.y = center.y - ((zoomRect.size.height / 2.0));
    
    return zoomRect;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if (CGRectEqualToRect(self.frame, self.frameOriginale) == NO) {
        // Il frame è cambiato, bisogna adattare l'ImageView
        self.frameOriginale = self.frame;
        [self adattaDimensioneAllImmagine];
    }
}

@end


@implementation DBZoomableImageViewDelegate

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return [scrollView.subviews firstObject];
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale
{
    // Bisogna dichiarare questo metodo per far funzionare lo zoom
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    [self.sender centraImageView];
}

@end
