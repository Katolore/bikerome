//
//  DBSlideMenu.h
//  File version: 1.2.2
//  Last modified: 08/05/2016
//
//  Created by Davide Balistreri on 08/19/2015
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"

// Protocol defined at the bottom.
@protocol DBSlideMenuDelegate;


// MARK: - Class define

/**
 * DBSlideMenu allows you to add and manage side view controllers for your application.
 * Use +[DBSlideMenu sharedInstance] to setup everything.
 */
@interface DBSlideMenu : NSObject

// MARK: Core methods

/**
 * DBSlideMenu shared instance. Designated initializer.
 * Use this to setup menus for your application.
 */
+ (DBSlideMenu *)sharedInstance;


// MARK: Defines

/// Generic CompletionBlock.
typedef void (^DBSlideMenuBlock)(void);

/**
 * Available behaviors when menus are opening.
 */
typedef NS_ENUM(NSUInteger, DBSlideMenuOpeningMode) {
    /// Menu will open remaining attached to its opening side.
    DBSlideMenuOpeningModeAlongOpeningSide,
    /// Menu will open remaining attached to the center (MainController).
    DBSlideMenuOpeningModeAlongMainController,
    /// Menu will open centered with a parallax effect.
    DBSlideMenuOpeningModeParallax,
    /// Menu will open above the MainController.
    DBSlideMenuOpeningModeAboveMainController,
    /// Default mode. Menu will open remaining attached to its opening side.
    DBSlideMenuOpeningModeDefault = DBSlideMenuOpeningModeAlongOpeningSide
};

/**
 * Available behaviors for the StatusBar when menus are opening.
 */
typedef NS_ENUM(NSUInteger, DBSlideMenuStatusBarMode) {
    /// Default mode. StatusBar untouched.
    DBSlideMenuStatusBarModeDefault,
    /// StatusBar background will gradually become tinted with DBSlideMenu tintColor.
    DBSlideMenuStatusBarModeOverlay,
    /// StatusBar will follow the opening/closing movement of the menu.
    DBSlideMenuStatusBarModeAlongOpeningSide
};


// MARK: Class methods

/// Boolean which determines whether menus can be opened or not.
+ (void)setEnabled:(BOOL)enabled;

/// Returns YES when the left menu is open.
+ (BOOL)isLeftMenuOpen;

/// Returns YES when the right menu is open.
+ (BOOL)isRightMenuOpen;


/// Opens the left menu, with or without animation.
+ (void)openLeftMenu:(BOOL)animated;

/// Opens the left menu, with or without animation.
+ (void)openLeftMenu:(BOOL)animated completionBlock:(DBSlideMenuBlock)completionBlock;


/// Opens the right menu, with or without animation.
+ (void)openRightMenu:(BOOL)animated;

/// Opens the right menu, with or without animation.
+ (void)openRightMenu:(BOOL)animated completionBlock:(DBSlideMenuBlock)completionBlock;


/// Closes all menus, with or without animation.
+ (void)closeMenu:(BOOL)animated;

/// Closes all menus, with or without animation.
+ (void)closeMenu:(BOOL)animated completionBlock:(DBSlideMenuBlock)completionBlock;


// MARK: Main controller

/**
 * Main view controller of the application. Mandatory.
 */
@property (strong, nonatomic) UIViewController *mainController;


// MARK: Left menu controller

/// View controller for the left menu.
@property (strong, nonatomic) UIViewController *leftMenuController;

/// Behavior of the left menu when is opening.
@property (nonatomic) DBSlideMenuOpeningMode leftMenuOpeningMode;

/// This configuration allows you to change the behavior of the StatusBar when the left menu is opening (iPhone only).
@property (nonatomic) DBSlideMenuStatusBarMode leftMenuStatusBarMode;


// MARK: Right menu controller

/// View controller for the right menu.
@property (strong, nonatomic) UIViewController *rightMenuController;

/// Behavior of the right menu when is opening.
@property (nonatomic) DBSlideMenuOpeningMode rightMenuOpeningMode;

/// This configuration allows you to change the behavior of the StatusBar when the right menu is opening (iPhone only).
@property (nonatomic) DBSlideMenuStatusBarMode rightMenuStatusBarMode;


// MARK: Instance methods

/// Boolean which determines whether menus can be opened or not.
@property (nonatomic) BOOL enabled;

/// Indicates whether a menu is open.
@property (nonatomic, readonly) BOOL isMenuOpen;

/// It is YES when the left menu is open.
@property (nonatomic, readonly) BOOL isLeftMenuOpen;

/// It is YES when the right menu is open.
@property (nonatomic, readonly) BOOL isRightMenuOpen;

/**
 * Tint color to be used for graphics elements.
 * At the moment it's only used to color the background of the StatusBar overlay (if present).
 */
@property (strong, nonatomic) UIColor *tintColor;

/**
 * By default a menu can be opened also on view controllers that don't implement the DBSlideMenuDelegate protocol.
 * If this property is set to YES, the menu will be opened ONLY on view controllers that implements the protocol.
 */
@property (nonatomic) BOOL requireProtocolImplementation;

@end


// MARK: - Protocol define

/**
 * This protocol allows you to manage the opening behavior of menus in view controllers that implement it.
 */
@protocol DBSlideMenuDelegate <NSObject>

@optional

/**
 * Determines whether the left menu can be opened or not in the presented view controller,
 * if it implements this method and returns NO.
 */
- (BOOL)shouldOpenLeftMenu;

/**
 * Determines whether the right menu can be opened or not in the presented view controller,
 * if it implements this method and returns NO.
 */
- (BOOL)shouldOpenRightMenu;

@end
