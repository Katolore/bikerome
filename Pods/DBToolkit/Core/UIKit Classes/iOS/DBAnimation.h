//
//  DBAnimation.h
//  File version: 1.0.1
//  Last modified: 09/11/2016
//
//  Created by Davide Balistreri on 06/24/2016
//  Copyright 2013-2017 © Davide Balistreri. All rights reserved.
//

#import "DBFoundation.h"
#import "DBObject.h"

/**
 * Classe per creare e gestire animazioni in maniera avanzata.
 */
@interface DBAnimation : DBObject <NSCopying>

// MARK: Defines

/// Generic block object.
typedef void (^DBAnimationBlock)(void);

/// Some useful preconfigured transitions.
typedef NS_ENUM(NSUInteger, DBAnimationTransition) {
    /// No transition.
    DBAnimationTransitionNone = 0,
    /// Fade transition.
    DBAnimationTransitionFade,
    /// Fade and zoom transition.
    DBAnimationTransitionZoom,
    /// Default: fade transition.
    DBAnimationTransitionAutomatic = DBAnimationTransitionFade
};


// MARK: Base configuration

/// The total duration of the animations, measured in seconds. If you specify a negative value or 0, the changes are made without animating them. Default is 0.2.
@property (nonatomic) NSTimeInterval duration;

/// The amount of time (measured in seconds) to wait before beginning the animations. Specify a value of 0 to begin the animations immediately.
@property (nonatomic) NSTimeInterval delay;

/// If enabled, the animation will be executed backwards from end to beginning state.
@property (nonatomic, getter = isReverse) BOOL reverse;

/// If enabled, the animation will have a small spring effect.
@property (nonatomic) BOOL useSpringEffect;

/// If enabled, the animation will end with a small bouncing effect.
@property (nonatomic) BOOL useSpringWithDampingEffect;

/// If enabled, user can interact with animated objects during the animation. Default: disabled.
@property (nonatomic) BOOL userInteractionEnabled;


// MARK: State configuration

/// A block object containing the beginning state of the animation sequence.
@property (copy) DBAnimationBlock initialState;
- (void)setInitialState:(DBAnimationBlock)initialState;

/// A block object containing the ending state of the animation sequence.
@property (copy) DBAnimationBlock finalState;
- (void)setFinalState:(DBAnimationBlock)finalState;

/// A block object to be executed when the animation sequence begins.
@property (copy) DBAnimationBlock completionBlock;
- (void)setCompletionBlock:(DBAnimationBlock)completionBlock;


// MARK: Advanced configuration

/**
 * If set, the animation will be executed by calling the states as follows: initial-final-initial.
 * The animation will respect its total duration.
 */
@property (nonatomic) BOOL bouncesBack;

/**
 * Determines the point of the total duration in which the animation will be inverted.
 * Default is 0.5.
 */
@property (nonatomic) CGFloat bouncePoint;


// MARK: Loop configuration

/**
 * If set, the animation will run repeatedly until manually stopped.
 * It will not consider -numberOfReps value.
 */
@property (nonatomic) BOOL repeatsAutomatically;

/**
 * Number of repetitions.
 * This is not considered if -repeatsAutomatically is enabled.
 * Default is 1.
 */
@property (nonatomic) NSUInteger numberOfRepetitions;


// MARK: State control

@property (readonly, getter = isRunning)   BOOL running;
@property (readonly, getter = isFinished)  BOOL finished;
@property (readonly, getter = isStopped)   BOOL stopped;
@property (readonly, getter = isCancelled) BOOL cancelled;

/// Starts the animation with specified params after waiting for the specified delay time.
- (void)start;

/// Starts the animation with specified params ignoring the delay time.
- (void)forceStart;

/// Waits until the animation arrives to its final state then executes its completion block (if declared).
- (void)stop;

/// Brings the animation to the final state and executes its completion block (if declared).
- (void)forceStop;

/// Invalidates the animation.
- (void)cancel;

@end


@interface DBAnimation (DBAnimationHelpers)

+ (DBAnimation *)performWithDuration:(NSTimeInterval)duration finalState:(DBAnimationBlock)finalState;
+ (DBAnimation *)performWithDuration:(NSTimeInterval)duration finalState:(DBAnimationBlock)finalState completionBlock:(DBAnimationBlock)completionBlock;

+ (DBAnimation *)performWithDuration:(NSTimeInterval)duration useSpringEffect:(BOOL)useSpringEffect finalState:(DBAnimationBlock)finalState completionBlock:(DBAnimationBlock)completionBlock;
+ (DBAnimation *)performWithDuration:(NSTimeInterval)duration useSpringWithDampingEffect:(BOOL)useSpringWithDampingEffect finalState:(DBAnimationBlock)finalState completionBlock:(DBAnimationBlock)completionBlock;

// MARK: Delay

+ (DBAnimation *)performAfterDelay:(NSTimeInterval)delay withDuration:(NSTimeInterval)duration finalState:(DBAnimationBlock)finalState completionBlock:(DBAnimationBlock)completionBlock;
+ (DBAnimation *)performAfterDelay:(NSTimeInterval)delay withDuration:(NSTimeInterval)duration useSpringEffect:(BOOL)useSpringEffect finalState:(DBAnimationBlock)finalState completionBlock:(DBAnimationBlock)completionBlock;
+ (DBAnimation *)performAfterDelay:(NSTimeInterval)delay withDuration:(NSTimeInterval)duration useSpringWithDampingEffect:(BOOL)useSpringWithDampingEffect finalState:(DBAnimationBlock)finalState completionBlock:(DBAnimationBlock)completionBlock;

@end
