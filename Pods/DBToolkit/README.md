# DBToolkit

## Installation

DBToolkit is available through [CocoaPods](http://cocoapods.org).
To install it, simply add the following lines to your Podfile:

```ruby
use_frameworks!
pod 'DBToolkit', :git => 'https://bitbucket.org/davidebalistreri/dbtoolkit.git'
```

To use it in your projects, import the root class of the framework:

```
#!objective-c
#import "DBToolkit.h"
```

(Optional) Specify which version you are using, to monitor future changes of the framework:

```
#!objective-c
#define DBFRAMEWORK_CURRENT_VERSION __DBFRAMEWORK_0_7_1
```

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Author

Davide Balistreri, d.balistreri@apptoyou.it

## License

DBToolkit is available under the MIT license. See the LICENSE file for more info.
