//
//  UIUtility.swift
//  Main Screen
//
//  Created by Lorenzo Antista on 11/01/2018.
//  Copyright © 2018 Lorenzo Antista. All rights reserved.
//

import UIKit
import DBToolkit

class UIUtility: NSObject {
    
    //Helper per ottenere il font bold da utilizzare nell'app, VERIFICA LA PRESENZA DEL FONT
    static func fontBold(_ size: CGFloat) -> UIFont {
        if let font = UIFont(name: "OpenSans-Bold", size: size){
        
            return font
        } else{
        //Failback sul font di sistema
            return UIFont.systemFont(ofSize: size)
        }
        
    }
    
    //Helper per ottenere il font regular da utilizzare nell'app
    static func fontRegular(_ size: CGFloat) -> UIFont {
        return UIFont(name: "OpenSans-Regular", size: size)!
    }

    //Helper per ottenere il font Light da utilizzare nell'app
    static func fontLight(_ size: CGFloat) -> UIFont {
        return UIFont(name: "OpenSans-Light", size: size)!
    }
    
    //Funzione semplificata per mostrare un avviso sull'app
    static func mostraAlert(titolo: String?, messaggio: String?, viewController: UIViewController) {
        
        let alert = UIAlertController(title: titolo, message: messaggio, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Chiudi", style: .cancel, handler: nil))
        
        viewController.present(alert, animated: true, completion: nil)
    }
    
    //Funzione semplificata per mostrare un avviso di conferma sull'app e gestire la scelta dell'utente
    static func mostraAlertDiConferma(titolo: String?, messaggio: String?, pulsanteSi: String?, pulsanteNo: String?, viewController: UIViewController, completion: ((Bool) ->Void)?) //Il bool ci dirà se l'utente ha premuto "Si" o "No"
    {
        
        let titoloLocalizzato = NSLocalizedString(titolo ?? "", comment: "")
        
        let messaggioLocalizzato = NSLocalizedString(messaggio ?? "", comment: "")
        
        //Creo l'alert
        let alert = UIAlertController(title: titoloLocalizzato, message: messaggioLocalizzato, preferredStyle: .alert)
        
        //Se la variabile "pulsanteNo" è valorizzata, usiamo quela stringa altrimenti di default mostriamo "titolo.no"
        let titoloPulsanteNo = pulsanteNo ?? "titolo.no"
        
        //Utilizziamo la stringa come chiave per localizzare il titolo del pulsante
        let titoloPulsanteNoLocalizzato = NSLocalizedString(titoloPulsanteNo, comment: "")
        //
        alert.addAction(UIAlertAction(title: titoloPulsanteNoLocalizzato, style: .cancel){
            (action) in
            
            //Comunico che l'utente ha premuto "No" a chi ha chiamato questa funzione
            completion?(false)
            
        })
        
        let titoloPulsanteSi = pulsanteSi ?? "titolo.si"
        
        let titoloPulsanteSiLocalizzato = NSLocalizedString(titoloPulsanteSi, comment: "")
        //
        alert.addAction(UIAlertAction(title: titoloPulsanteSiLocalizzato, style: .default){
            (action) in
            
            //Comunico che l'utente ha premuto "No" a chi ha chiamato questa funzione
            completion?(true)
            
        })
        
        viewController.present(alert, animated: true, completion: nil)
    
    }
    
    static func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        
        if widthRatio > heightRatio {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.draw(in: rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    //Funzione semplificata per mostrare un avviso di conferma sull'app e gestire la scelta dell'utente
    static func mostraActionSheetDiConferma(titolo: String?, messaggio: String?, pulsanteSopra: String?, pulsanteSotto: String?, viewController: UIViewController, completion: ((Bool) ->Void)?) //Il bool ci dirà se l'utente ha premuto "Si" o "No"
    {
        
        let titoloLocalizzato = NSLocalizedString(titolo ?? "", comment: "")
        
        let messaggioLocalizzato = NSLocalizedString(messaggio ?? "", comment: "")
        
        //Creo l'alert
        let alert = UIAlertController(title: titoloLocalizzato, message: messaggioLocalizzato, preferredStyle: .actionSheet)
        
        //Se la variabile "pulsanteNo" è valorizzata, usiamo quela stringa altrimenti di default mostriamo "titolo.no"
        let titoloPulsanteSotto = pulsanteSotto ?? "titolo.no"
        
        //Utilizziamo la stringa come chiave per localizzare il titolo del pulsante
        let titoloPulsanteSottoLocalizzato = NSLocalizedString(titoloPulsanteSotto, comment: "")
        //
        alert.addAction(UIAlertAction(title: titoloPulsanteSottoLocalizzato, style: .default){
            (action) in
            
            //Comunico che l'utente ha premuto "No" a chi ha chiamato questa funzione
            completion?(false)
            
        })
        
        let titoloPulsanteSopra = pulsanteSopra ?? "titolo.si"
        let titoloPulsanteSopraLocalizzato = NSLocalizedString(titoloPulsanteSopra, comment: "")
        //
        alert.addAction(UIAlertAction(title: titoloPulsanteSopraLocalizzato, style: .default){
            (action) in
            
            //Comunico che l'utente ha premuto "No" a chi ha chiamato questa funzione
            completion?(true)
            
        })
        
        let pulsanteChiudiLocalizzato = NSLocalizedString("titolo.chiudi", comment: "")
        
        alert.addAction(UIAlertAction(title: pulsanteChiudiLocalizzato, style: .cancel, handler: nil))
        
        viewController.present(alert, animated: true, completion: nil)
        
    }

    
    static func mostraProgressHUD() {
        _ = DBProgressHUD(conMessaggio: "Caricamento...")
    }
    
    static func rimuoviProgressHUD() {
        DBProgressHUD.rimuoviProgressHUDs()
    }
    
}
