//
//  NetworkParser.swift
//  Main Screen
//
//  Created by Lorenzo Antista on 11/12/2017.
//  Copyright © 2017 Lorenzo Antista. All rights reserved.
//

import UIKit
//Classe che si occuperà di trasfromare le risposte dei servizi web in oggetti utilizzabili dell'app

class NetworkParser {
    
    static func parseListaOpere(data: Data) -> [Opera] {
        
        var listaOpere: [Opera] = []
        
        do {
            //Converte il messaggio inviato dal server in un oggetto su cui è possibile operare
            let jsonObject = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
            
            if let jsonListaOpere = jsonObject as? [NSDictionary] {
                
                for jsonOpera in jsonListaOpere {
                    
                    //Parse dell'oggetto Opera ricevuto
                    let nuovaOpera = Opera()
                    
                    nuovaOpera.nome = jsonOpera["title"] as! String
                    
                    nuovaOpera.descrizioneBreve = jsonOpera["description"] as! String
                    
                    nuovaOpera.urlCopertina = jsonOpera["image_url"] as! String
                    
                    listaOpere.append(nuovaOpera)
                }
            }
        } catch let errore {
            print("YOU BAD!")
            print(errore)
        }
        return listaOpere
    }
    
    static func parseUtente(data: IEDDictionary) -> Utente {
    
        let utente = Utente()
        
        //Assegno i dati inviata dal server all'istanza creata
        //...
        utente.email = data["email"] as? String
        utente.cognome = data["cognome"] as? String
        utente.nome = data["nome"] as? String
        utente.urlAvatar = data["avatar_url"] as? String
        utente.citta = data["citta"] as? String
        utente.data_nascita = data["data_nascita"] as? String
        utente.token = data["auth_token"] as? String
        utente.timestamp = data["timestamp"] as? String
    
        
        //Restituisco l'istanza dell'utente
        return utente
    }
    
    static func parseListaOpere(data: [NSDictionary]) -> [Opera] {
        //creo la lista di news che verrà restituita dopo il parse dei dati
        //inviati dal server
        var listaOpere: [Opera] = []
        
        //Prendo ogni articolo inviato dal server in formato JSON
        for jsonOpera in data {
            let opera = Opera()
            
            let jsonUtente = jsonOpera["creatore"] as! NSDictionary
            opera.creatore = parseUtente(data: jsonUtente as! IEDDictionary)
            
            opera.nome = (jsonOpera["nome"] as? String)!
            opera.descrizione = (jsonOpera["descrizione"] as? String)!
            opera.indirizzo = (jsonOpera["indirizzo"] as? String)!
            opera.latitudine = (jsonOpera["latitudine"] as? String)!
            opera.cover_url = (jsonOpera["cover_url"] as? String)!
            opera.web_url = (jsonOpera["web_url"] as? String)!
        
            listaOpere.append(opera)
            
        }
        
        return listaOpere
    }
}
