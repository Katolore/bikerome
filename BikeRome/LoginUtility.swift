//
//  LoginUtility.swift
//  Main Screen
//
//  Created by Lorenzo Antista on 11/01/2018.
//  Copyright © 2018 Lorenzo Antista. All rights reserved.
//

import UIKit

class LoginUtility {
    
    static var utenteConnesso: Utente?
    
    // Costante per la chiave di memorizzazione del database
    static private let keyUtenteConnesso = "KeyUtenteConnesso"
    
    
    // Questa funzione restituisce l'eventuale utente connesso e salvato sul database dell'app
    // Il punto interrogativo serve per il nil
    static func caricaUtenteConnesso() {
        
        let defaults = UserDefaults.standard
        
        let data = defaults.value(forKey: keyUtenteConnesso)
        
        if let dataUtente = data as? Data {
            
            let utenteNonArchiviato = NSKeyedUnarchiver.unarchiveObject(with: dataUtente)
            
            // Mi assicuro che i dati convertiti sono esattamente della classe "Utente"
            if let utente = utenteNonArchiviato as? Utente {
                // Restituisco l'utente connesso
                utenteConnesso = utente
            }
        }
        else {
            utenteConnesso = nil
        }
    }
    
    static func salvaUtenteConnesso() {
        let defaults = UserDefaults.standard
        
        if let utenteDaSalvare = utenteConnesso {
            // Aggiorno l'utente connesso
            
            // Converto l'istanza della nostra classe "Utente" in un insieme di byte (di tipo "Data") che posso salvare tranquillamente nelle UserDefaults
            let dataUtente = NSKeyedArchiver.archivedData(withRootObject: utenteDaSalvare)
            defaults.set(dataUtente, forKey: keyUtenteConnesso)
            
            // Sincronizza i dati nel database
            defaults.synchronize()
        }
        else {
            // Rimuovo l'utente connesso
            defaults.removeObject(forKey: keyUtenteConnesso)
            defaults.synchronize()
        }
    }
    
}
