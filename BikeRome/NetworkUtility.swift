//
//  NetworkUtility.swift
//  Main Screen
//
//  Created by Lorenzo Antista on 14/12/2017.
//  Copyright © 2017 Lorenzo Antista. All rights reserved.
//

import UIKit

class NetworkUtility {
    
    static func scaricaImmagine(conIndirizzoWeb indirizzoWeb: String, perImageView imageView: UIImageView) {
    
        if indirizzoWeb == "" {
            return
        }
        
        //Rimuovo l'immagine precendentemente impostata
        imageView.image = nil
        
        DispatchQueue.global().async {
            do{
                //Scarichiamo l'immagine da internet 
                
                //Converto la stringa dell'indirizzo web nel formato 'URL'
                let urlImmagine = URL.init(string: indirizzoWeb)!
            
                //Scarico i dati dell'immagine presenti all'indirizzo web
                let datiImmagine = try Data.init(contentsOf: urlImmagine)
                
                let immagine = UIImage.init(data: datiImmagine)
                
                DispatchQueue.main.async {
                    
                    //Assegniamo l'iimagine all'ImageView specificata
                    imageView.image = immagine
                    
                }
                
            }catch let errore {
                //Si è verificata un'eccezione
                print("Si è verificata un'eccezione")
                print(errore)
            
            }
            
            
        }
        
    }

}
