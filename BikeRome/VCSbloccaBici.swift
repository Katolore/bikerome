//
//  VCSbloccaBici.swift
//  BikeRome
//
//  Created by Riccardo Piccialuti on 10/05/18.
//  Copyright © 2018 IED. All rights reserved.
//

import UIKit
import DBToolkit

class VCSbloccaBici : ViewController, DBCameraHandlerDelegate {
    
    var delegate: VCSbloccaBiciDelegate?
    
    var cameraHandler: DBCameraHandler?
    
    var qrCodeDaSbloccare = "www.sbloccabici.com"
    
    
    // controlli del VCSBLOCCABICI
    
    // MARK - Outlets
    
    @IBOutlet weak var containerCamera: UIView!
    
    @IBOutlet weak var buttonIndietro: UIButton!
    
    @IBOutlet weak var textCodiceSblocco: UITextField!
    
    
    // MARK - Setup
    
    override func setupGrafica() {
        cameraHandler = DBCameraHandler(view: containerCamera, cameraDevice: .rearCamera, outputQuality: .high)
        cameraHandler?.delegate = self
        
        cameraHandler?.startCapturingMetadataObjectTypes([AVMetadataObjectTypeQRCode])
    }
    
    override func setupContenuti() {
        
    }
    
    
    // MARK: - Functions
    
    func analizza(qrCode: String) {
        // Check se la stringa "qrCode" è quella da noi ricercata
        
        /*if (<condizione>) {
            <if_istruzioni>
        } else {
            <else_istruzioni>
        }*/
        
        // Se è quella ricercata
        if qrCode == qrCodeDaSbloccare {
            // 1. Interrompiamo la cattura di cameraHandler.stopCapturingAllMetadataObjectTypes()
            cameraHandler?.stopCapturingAllMetadataObjectTypes()

            // 2. Mostriamo un alert con scritto "Bici sbloccata"
            print("Hai sbloccato la tua bicicletta")
            
            UIUtility.mostraAlert(titolo: "Conferma Sblocco", messaggio: "Hai sbloccato la tua bicicletta", viewController: self)
            
            // Informo il view controller della mappa che è stata sbloccata una bici
            delegate?.biciSbloccata(qrCode: qrCode)
        }
        
    }
    
    
    // MARK: - CameraHandler delegate
    
    func handler(_ handler: DBCameraHandler!, didCapture metadataObjects: [AVMetadataObject]!) {
        if let qrCode = metadataObjects.first as? AVMetadataMachineReadableCodeObject {
            analizza(qrCode: qrCode.stringValue)
        }
    }
    
    
    // MARK - Actions
    
    @IBAction func buttonIndietro(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        
    }
    
    
    
}


protocol VCSbloccaBiciDelegate {
    
    func biciSbloccata(qrCode: String)
    
}
