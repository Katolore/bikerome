//
//  VCWelcome.swift
//  Main Screen
//
//  Created by Lorenzo Antista on 11/01/2018.
//  Copyright © 2018 Lorenzo Antista. All rights reserved.
//

import UIKit

class VCWelcome: ViewController {
    
    // MARK: - Outlets
    
    @IBOutlet weak var buttonRegistrazione: UIButton!
    
    @IBOutlet weak var buttonLogin: UIButton!
    
    
    // MARK: - Setup
    
    override func setupGrafica() {
        
        buttonRegistrazione.clipsToBounds = true
        
        buttonRegistrazione.layer.cornerRadius = 10.0
        
        buttonRegistrazione.layer.shadowOpacity = 0.3
        
        buttonRegistrazione.layer.shadowOffset = CGSize(width: 0, height: 5.0)
        
        buttonLogin.layer.cornerRadius = 10.0
        buttonLogin.clipsToBounds = true
        
        buttonLogin.layer.shadowOpacity = 0.3
        
        buttonLogin.layer.shadowOffset = CGSize(width: 0, height: 5.0)
        
    }
    
    override func setupContenuti() {
        
    }
    
    
    // MARK: - Actions
    
    @IBAction func buttonRegistrazione(_ sender: Any) {
        
        let next = StoryboardUtility.registrazioneController()
        navigationController?.pushViewController(next, animated: true)
    }
    
    @IBAction func buttonLogin(_ sender: Any) {
        
        let next = StoryboardUtility.loginController()
        navigationController?.pushViewController(next, animated: true)
    }
    
}
