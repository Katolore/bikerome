//
//  VCRegistrazione.swift
//  BikeRome
//
//  Created by Lorenzo Antista on 12/04/2018.
//  Copyright © 2018 IED. All rights reserved.
//

import UIKit

class VCRegistrazione : ViewController {
    
    // MARK: - Outlets
    
    
    @IBOutlet weak var textNomeCognome: UITextField!
    @IBOutlet weak var textEmail: UITextField!
    
    @IBOutlet weak var textPassword: UITextField!
    
    
    // MARK: - Collections
    
    
    
    // MARK: - Setup della schermata
    
    override func setupGrafica() {
        super.setupGrafica()
        
    }
    
    override func setupContenuti() {
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        _ = DBKeyboardHandler(view: view, inputFields: [textNomeCognome, textEmail, textPassword], delegate: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        DBKeyboardHandler.destroy(with: view)
    }
    
    
    
    // MARK: - Funzioni
    
    func validazioneDatiInseriti() -> Bool {
        
        if textNomeCognome.text == nil || textNomeCognome.text == "" {
            UIUtility.mostraAlert(titolo: "Ops!", messaggio: "Inserisci il tuo nome e cognome", viewController: self)
            return false
        }
        
        if textEmail.text == nil || textEmail.text == "" {
            UIUtility.mostraAlert(titolo: "Ops!", messaggio: "Inserisci una email valida", viewController: self)
            return false
        }
        
        if textPassword.text == nil || textPassword.text == "" {
            UIUtility.mostraAlert(titolo: "Ops!", messaggio: "Inserisci una password valida", viewController: self)
            return false
        }
        
        return true
    }
    
    
    // MARK: - Actions
    
    @IBAction func buttonRegistrazione(_ sender: Any) {
        
        guard validazioneDatiInseriti() else {
            // Validazione fallita
            return
        }
        
        // Validazione riuscita
        
        guard let nomeCognome = textNomeCognome.text, let email = textEmail.text, let password = textPassword.text else {
            return
        }
        
        // Mostriamo il progress HUD di caricamento
        UIUtility.mostraProgressHUD()
        
        
        
        
        // Effettuiamo la richiesta web
        Network.registrazione(connomeCognome: nomeCognome, conEmail: email, password: password) {
            (data, error) in
            
            // Nascondiamo il progress HUD di caricamento
            UIUtility.rimuoviProgressHUD()
            
            if error {
                // Si è verificato un errore
                
                // Controlliamo se il server ha risposto con una stringa di errore (dentro "data")
                if let messaggioErrore = data as? String {
                    UIUtility.mostraAlert(titolo: "OPS!", messaggio: messaggioErrore, viewController: self)
                }
            } else {
                // Il login è andato a buon fine
                
                // Controlliamo se il server ha risposto con un utente (dentro "data")
                if let utente = data as? Utente {
                    // Salviamo l'utente connesso
                    LoginUtility.utenteConnesso = utente
                    LoginUtility.salvaUtenteConnesso()
                    
                    // Andiamo alla home
                    MainController.gotoHome(animated: true)
                }
            }

        }
    }
    
    @IBAction func buttonBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}

