//
//  VCMappaBici.swift
//  BikeRome
//
//  Created by Peppe on 10/05/18.
//  Copyright © 2018 IED. All rights reserved.
//

import UIKit
import MapKit

class VCMappaBici : ViewController, MKMapViewDelegate {
    
    var posizioniRicevute: [CLLocationCoordinate2D] = []
    
    var ultimaLineaDisegnata: MKPolyline?
    
    var listaBici: [Bicicletta] = []
    
    let locationManager = CLLocationManager()
    
    // Qui memorizzo la bicicletta attualmente selezionata dall'utente, in attesa di essere prenotata
    var biciclettaSelezionata: Bicicletta?
    
    
    
    
    // MARK: - Outlets
    
    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var buttonAssistenza: UIButton!
    
    @IBOutlet weak var buttonMenu: UIButton!
    
    @IBOutlet weak var buttonFiltri: UIButton!
    
    @IBOutlet weak var buttonPosizioneUtente: UIButton!
    
    @IBOutlet weak var buttonSbloccaBici: UIButton!
    
    @IBOutlet weak var containerBiciSelezionata: UIView!
    
    @IBOutlet weak var labelIndirizzoBiciSelezionata: UILabel!
    
    @IBOutlet weak var labelDistanza: UILabel!
    
    @IBOutlet weak var labelDistanzaInMetri: UILabel!
    
    @IBOutlet weak var labelCosto: UILabel!
    
    @IBOutlet weak var labelCostoInEuro: UILabel!
    
    @IBOutlet weak var labelTempo: UILabel!
    
    @IBOutlet weak var labelTempoInMinuti: UILabel!
    
    @IBOutlet weak var buttonPrenota: UIButton!
    
    @IBOutlet weak var labelNumeroPrenotazione: UILabel!
    
    @IBOutlet weak var labelIndirizzoBiciPrenotata: UILabel!
    
    @IBOutlet weak var labelTempoPrenotazione: UILabel!
    
    @IBOutlet weak var buttonAnnullaPrenotazione: UIButton!
    
    @IBOutlet weak var containerPrenotaBici: UIView!
    
    @IBOutlet weak var containerBiciPrenotata: UIView!
    
    
    
    
    
    // MARK: - Setup
    
    override func setupGrafica() {
        navigationController?.isNavigationBarHidden = true
        
        mapView.showsUserLocation = true
        mapView.showsCompass = false
        
        // Richiedo la posizione dell'utente
        GpsUtility.richiediAutorizzazione()
        
        // Stato iniziale: container nascosti
        containerPrenotaBici.collapseLayout()
        containerBiciPrenotata.collapseLayout()
        
        // Sfondi dei container
        containerBiciSelezionata.backgroundColor = UIColor.white.withAlphaComponent(0.8)
        containerPrenotaBici.backgroundColor = nil
        containerBiciPrenotata.backgroundColor = nil
        
        mapView.delegate = self
    }
    
    override func setupContenuti() {
        // Qui impostiamo i contenuti da visualizzare sul view controller
        
        // SIMONE:
        
        // Biciclette di prova
        let biciUno = Bicicletta()
        biciUno.nome = "bicicletta,0001"
        biciUno.indirizzo = "Via Alcamo 11, Roma"
        biciUno.coordinate = CLLocationCoordinate2DMake(41.8863958, 12.5238831)
        biciUno.iconaPin = UIUtility.resizeImage(image: #imageLiteral(resourceName: "icona bici"), targetSize: CGSize.init(width: 50.0, height: 50.0))
        listaBici.append(biciUno)
        
        let biciDue = Bicicletta()
        biciDue.nome = "bicicletta,0002"
        biciDue.indirizzo = "Via Tiburtina 912, Roma"
        biciDue.coordinate = CLLocationCoordinate2DMake(41.9241238, 12.5705689)
        biciDue.iconaPin = UIUtility.resizeImage(image: #imageLiteral(resourceName: "icona bici"), targetSize: CGSize.init(width: 50.0, height: 50.0))
        listaBici.append(biciDue)
        
        let biciTre = Bicicletta()
        biciTre.nome = "bicicletta,003"
        biciTre.indirizzo = "via portuense 579, Roma"
        biciTre.coordinate = CLLocationCoordinate2DMake(41.872487,12.477949)
        biciTre.iconaPin = UIUtility.resizeImage(image: #imageLiteral(resourceName: "icona bici"), targetSize: CGSize.init(width: 50.0, height: 50.0))
        listaBici.append(biciTre)
        
        let biciQuattro = Bicicletta()
        biciQuattro.nome = "bicicletta,004"
        biciQuattro.indirizzo = "Via Ostiense 7, Roma"
        biciQuattro.coordinate = CLLocationCoordinate2DMake(41.874831, 12.480852)
        biciQuattro.iconaPin = UIUtility.resizeImage(image: #imageLiteral(resourceName: "icona bici"), targetSize: CGSize.init(width: 50.0, height: 50.0))
        listaBici.append(biciQuattro)
        
        let biciCinque = Bicicletta()
        biciCinque.nome = "bicicletta,005"
        biciCinque.indirizzo = "Via Branca 122, Roma"
        biciCinque.coordinate = CLLocationCoordinate2DMake(41.878851, 12.472592)
        biciCinque.iconaPin = UIUtility.resizeImage(image: #imageLiteral(resourceName: "icona bici"), targetSize: CGSize.init(width: 50.0, height: 50.0))
        listaBici.append(biciCinque)
        
        let biciSei = Bicicletta()
        biciSei.nome = "bicicletta,006"
        biciSei.indirizzo = "via Alessandro volta 8, Roma"
        biciSei.coordinate = CLLocationCoordinate2DMake(45.587814, 9.270257)
        biciSei.iconaPin = UIUtility.resizeImage(image: #imageLiteral(resourceName: "icona bici"), targetSize: CGSize.init(width: 50.0, height: 50.0))
        listaBici.append(biciSei)
        
        let biciSette = Bicicletta()
        biciSette.nome = "bicicletta,007"
        biciSette.indirizzo = "via del porto fluviale 22, Roma"
        biciSette.coordinate = CLLocationCoordinate2DMake(41.872487, 12.477949)
        biciSette.iconaPin = UIUtility.resizeImage(image: #imageLiteral(resourceName: "icona bici"), targetSize: CGSize.init(width: 50.0, height: 50.0))
        listaBici.append(biciSette)
        
        let biciOtto = Bicicletta()
        biciOtto.nome = "bicicletta,008"
        biciOtto.indirizzo = "via marmorata 169, Roma"
        biciOtto.coordinate = CLLocationCoordinate2DMake(41.882062, 12.476503)
        biciOtto.iconaPin = UIUtility.resizeImage(image: #imageLiteral(resourceName: "icona bici"), targetSize: CGSize.init(width: 50.0, height: 50.0))
        listaBici.append(biciOtto)
        
        let biciNove = Bicicletta()
        biciNove.nome = "bicicletta,009"
        biciNove.indirizzo = "Parco savello, Roma"
        biciNove.coordinate = CLLocationCoordinate2DMake(41.885129, 12.480336)
        biciNove.iconaPin = UIUtility.resizeImage(image: #imageLiteral(resourceName: "icona bici"), targetSize: CGSize.init(width: 50.0, height: 50.0))
        listaBici.append(biciNove)
        
        
        let mattatoio = Bicicletta()
        mattatoio.nome = "Ex mattatoio"
        mattatoio.indirizzo = "Piazza Orazio Giustiniani 4, Roma"
        mattatoio.coordinate = CLLocationCoordinate2DMake(41.876159, 12.473844)
        mattatoio.iconaPin = UIUtility.resizeImage(image: #imageLiteral(resourceName: "icona mattatoi"), targetSize: CGSize.init(width: 60.0, height: 60.0))
        listaBici.append(mattatoio)
        
        let totem = Bicicletta()
        totem.nome = "totem"
        totem.indirizzo = "piazza dei cavalieri di malta, Roma"
        totem.coordinate = CLLocationCoordinate2DMake(41.882674, 12.478100)
        totem.iconaPin = UIUtility.resizeImage(image: #imageLiteral(resourceName: "icona totem"), targetSize: CGSize.init(width: 60.0, height: 60.0))
        
        listaBici.append(totem)
        
        
        // Metto i pin delle biciclette sulla mappa
        for biciDaRappresentare in listaBici {
            let pin = PinMappa(bici: biciDaRappresentare)
            mapView.addAnnotation(pin)
        }
        
        // GIUSEPPE:
        // let coordinateBici = CLLocationCoordinate2D(latitude: 41.882212, longitude: 12.476650)
        // disegnaPercorso(conCoordinate: coordinateBici)
        
        //centro mappa su posizione utente
        
        let coordinateUtente = mapView.userLocation.coordinate
        
        centerMapOnLocation(location: coordinateUtente)
        
        
    }
    
    func aggiornaContenuti() {
        // Questa funzione si occupa di prendere i contenuti aggiornati da mostrare sul view controller
        
    }
    
    
    // MARK: - Funzioni
    
    //funzione per centrare mappa
    
    let regionRadius: CLLocationDistance = 1000
    func centerMapOnLocation(location: CLLocationCoordinate2D) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location, regionRadius, regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    //disegna percorso
    func disegnaPercorso(conCoordinate destinationLocation: CLLocationCoordinate2D) {
        let sourceLocation = mapView.userLocation.coordinate
        
        let sourcePlacemark = MKPlacemark(coordinate: sourceLocation, addressDictionary: nil)
        let destinationPlacemark = MKPlacemark(coordinate: destinationLocation, addressDictionary: nil)
        
        let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
        let destinationMapItem = MKMapItem(placemark: destinationPlacemark)
        
        let sourceAnnotation = MKPointAnnotation()
        sourceAnnotation.title = "La tua Posizione "
        
        if let location = sourcePlacemark.location {
            sourceAnnotation.coordinate = location.coordinate
            
            let destinationAnnotation = MKPointAnnotation()
            destinationAnnotation.title = "Bicicletta Libera"
            
            if let location = destinationPlacemark.location {
                destinationAnnotation.coordinate = location.coordinate
                
                self.mapView.showAnnotations([sourceAnnotation,destinationAnnotation], animated: true )
            }
            
            let directionRequest = MKDirectionsRequest()
            directionRequest.source = sourceMapItem
            directionRequest.destination = destinationMapItem
            directionRequest.transportType = .walking
            
            //calcolo direzione
            let directions = MKDirections(request: directionRequest)
            
            directions.calculate {
                (response, error) in
                
                guard let response = response else {
                    if let error = error {
                        print("Error: \(error)")
                    }
                    
                    return
                }
                
                let route = response.routes[0]
                self.mapView.add((route.polyline), level: MKOverlayLevel.aboveRoads)
                
                let rect = route.polyline.boundingMapRect
                self.mapView.setRegion(MKCoordinateRegionForMapRect(rect), animated: true)
            }
        }
    }
    
    func disegnaPercorso() {
        
        // Elimino la vecchia linea
        if let linea = ultimaLineaDisegnata {
            mapView.remove(linea)
        }
        
        // Disegno la linea che collega tutte le coordinate ricevute
        let lineaDaDisegnare = MKPolyline.init(coordinates: &posizioniRicevute, count: posizioniRicevute.count)
        mapView.add(lineaDaDisegnare)
        
        // Salvo la nuova linea
        ultimaLineaDisegnata = lineaDaDisegnare
    }
    
    func centraMappaSuPosizioneUtente() {
        let coordinateUtente = mapView.userLocation.coordinate
        
        // Controllo se le coordinate dell'utente sono valide
        if CLLocationCoordinate2DIsValid(coordinateUtente) {
            
            // Controllo se le coordinate non sono nell'oceano atlantico
            if coordinateUtente.latitude != 0 && coordinateUtente.longitude != 0 {
                // Centro la mappa sulle coordinate
                mapView.setCenter(coordinateUtente, animated: true)
            }
        }
    }
    
    /// Questa funzione si occupa di gestire la selezione di una bicicletta sulla mappa
    func biciclettaTappata(_ bicicletta: Bicicletta) {
        
        // Memorizzo la bicicletta selezionata nella sessione corrente
        self.biciclettaSelezionata = bicicletta
        
        // Mostro/nascondo i container
        containerPrenotaBici.expandLayout()
        containerBiciPrenotata.collapseLayout()
        
        // Applico i dati della bicicletta selezionata sugli elementi della view
        labelIndirizzoBiciSelezionata.text = bicicletta.indirizzo
        labelIndirizzoBiciPrenotata.text = bicicletta.indirizzo
        
    }
    
    
    // MARK: - Actions
    
    @IBAction func buttonMenu(_ sender: Any) {
        DBSlideMenu.openLeftMenu(true)
    }
    
    @IBAction func buttonFiltri(_ sender: Any) {
        
    }
    
    @IBAction func buttonPosizioneUtente(_ sender: Any) {
        
        // DA SPOSTARE
        GpsUtility.gestore = {
            (posizione) in
            
            // Questa parte di codice viene richiamata ogni volta che l'utente si sposta, ci permette di ottenere le coordinate GPS di ogni spostamento
            print("Coordinate: \(posizione.latitude)  \(posizione.longitude)")
            
            // Aggiungo la posizione appena ricevuta all'array di tutte le posizioni
            self.posizioniRicevute.append(posizione)
        }
        
        GpsUtility.avviaTracciamento()
    }
    
    @IBAction func buttonSbloccaBici(_ sender: Any) {
        let next = StoryboardUtility.cameraController()
        navigationController?.pushViewController(next, animated: true)
    }
    
    @IBAction func buttonAssistenza(_ sender: Any) {
        
    }
    
    @IBAction func buttonPrenota(_ sender: Any) {
        
        containerPrenotaBici.collapseLayout()
        containerBiciPrenotata.expandLayout()
    }
    
    @IBAction func buttonAnnullaPrenotazione(_ sender: Any) {
        UIUtility.mostraAlertDiConferma(titolo: "Annulla Prenotazione", messaggio: "Sei sicuro di voelr annullare la prenotazione?", pulsanteSi: "Si", pulsanteNo: "No", viewController: self) {
            (sceltaUtente) in
            
            if sceltaUtente {
                self.containerPrenotaBici.collapseLayout()
                self.containerBiciPrenotata.collapseLayout()
            }
        }
    }
    
    
    
    // qua
    
    // MARK: - MapView delegate
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = UIColor.red
        renderer.lineWidth = 4.0
        
        return renderer
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {

        if annotation.isEqual(mapView.userLocation) {
            // Il pallino della posizione utente non deve essere personalizzato
            return nil
        }
        
        let annotationView = MKAnnotationView()

        if let pinMappa = annotation as? PinMappa {
            annotationView.image = pinMappa.biciclettaAssociata?.iconaPin
            annotationView.canShowCallout = true
        }

        return annotationView
    }
    
    var posizioneUtenteGiaCentrata = false
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        if posizioneUtenteGiaCentrata == false {
            centerMapOnLocation(location: userLocation.coordinate)
            posizioneUtenteGiaCentrata = true
        }
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        // Controllo se l'annotation view ha l'oggetto PinMappa
        if let annotation = view.annotation as? PinMappa {
            // Controllo se il PinMappa ha una bicicletta associato
            if let biciclettaSelezionata = annotation.biciclettaAssociata {
                biciclettaTappata(biciclettaSelezionata)
            }
        }
    }
    
}
