//
//  CellOpera.swift
//  Main Screen
//
//  Created by Lorenzo Antista on 30/11/2017.
//  Copyright © 2017 Lorenzo Antista. All rights reserved.
//

import UIKit

class CellOpera: UITableViewCell {

   
    
    @IBOutlet weak var labelNome: UILabel!
    
    
    @IBOutlet weak var imageNuoveOpere: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setupConOpera(operaDaRappresentare: Opera){
    //Questa funzione si occupeà di prendere i dati dall'oggetto Opera e metterli sugli elementi grafici della cella
        
        labelNome.text = operaDaRappresentare.nome
    
        NetworkUtility.scaricaImmagine(conIndirizzoWeb: operaDaRappresentare.cover_url , perImageView: imageNuoveOpere)
    }
   
}
