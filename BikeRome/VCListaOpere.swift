//
//  VCListaArticoli.swift
//  Main Screen
//
//  Created by Lorenzo Antista on 15/02/2018.
//  Copyright © 2018 Lorenzo Antista. All rights reserved.
//

import UIKit

class VCListaOpere : UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
  
    var lista: [Opera] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        tableView.delegate = self
        tableView.dataSource = self
        
        setupGrafica()
        setupContenuti()
        
    }
    
    func setupGrafica() {
    navigationItem.title = "Crea Post"

    }
    
    func setupContenuti() {
        
        aggiornaContenuti()
    }
    
    func aggiornaContenuti() {
    
        Network.getListaNewsAggiornata {
            (data, error) in
            if let listaOpereAggiornate = data as? [Opera] {
                self.lista = listaOpereAggiornate
                self.tableView.reloadData()
                
            }
        }
    
    
    }
    
    
    @IBAction func buttonAggiungiOpera(_ sender: Any) {
        
        let next = StoryboardUtility.aggiungiOpera()
        navigationController?.pushViewController(next, animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lista.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellOpera") as! CellOpera
        
        //Metto i dati sulla cella
        let operaCorrente = lista[indexPath.row]
        cell.setupConOpera(operaDaRappresentare: operaCorrente)
        
        return cell
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        aggiornaContenuti()
    }
    
    
    
}
