//
//  Opera.swift
//  Main Screen
//
//  Created by Lorenzo Antista on 16/11/2017.
//  Copyright © 2017 Lorenzo Antista. All rights reserved.
//

import UIKit

class Opera {

    var id: String = ""

    var nome: String = ""
    
    var indirizzo: String = ""
    
    var stile = Stile()
    
    var descrizioneBreve: String = ""
    
    var aggiunto = false
    
    var urlCopertina: String = ""
    
    var descrizione: String = ""
    
    var latitudine: String = ""
    
    var longitudine: String = ""
    
    var web_url: String = ""

    var creatore: Utente?
    
    var cover_url: String = ""
    
    

}
