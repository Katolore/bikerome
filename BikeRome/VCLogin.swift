//
//  VCLogin.swift
//  BikeRome
//
//  Created by Riccardo Piccialuti on 14/06/18.
//  Copyright © 2018 IED. All rights reserved.
//

import UIKit

class VCLogin : ViewController {
    
    // MARK: - Outlets
    
    @IBOutlet weak var buttonLogin: UIButton!
    
    @IBOutlet weak var textEmail: UITextField!
    
    @IBOutlet weak var textPassword: UITextField!
    
    
    // MARK: - Setup
    
    override func setupGrafica() {
        
        buttonLogin.layer.cornerRadius = 10.0
        buttonLogin.clipsToBounds = true
    }
    
    override func setupContenuti() {
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        _ = DBKeyboardHandler(view: view, inputFields: [textEmail, textPassword], delegate: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        DBKeyboardHandler.destroy(with: view)
    }
    
    
    // MARK: - Funzioni
    
    func validazioneDatiInseriti() -> Bool {
        
        if textEmail.text == nil || textEmail.text == "" {
            UIUtility.mostraAlert(titolo: "Ops!", messaggio: "Non hai inserito la tua email!", viewController: self)
            return false
        }
        
        
        if textPassword.text == nil || textPassword.text == "" {
            UIUtility.mostraAlert(titolo: "Ops!", messaggio: "Non hai inserito la tua password!", viewController: self)
            return false
        }
        
        return true
    }
    
    
    // MARK: - Actions
    
    @IBAction func buttonLogin(_ sender: Any) {
        
        guard validazioneDatiInseriti() else {
            // Validazione fallita
            return
        }
        
        // Validazione riuscita
        
        guard let email = textEmail.text, let password = textPassword.text else {
            return
        }
        
        // Easter egg
        if email == "a", password == "a" {
            MainController.gotoHome(animated: true)
            return
        }
        
        if email == "a", password == "a" {
            MainController.gotoHome(animated: true)
            return
        }
        
        // Mostriamo il progress HUD di caricamento
        UIUtility.mostraProgressHUD()
        
        // Effettuiamo la richiesta web
        Network.login(conEmail: textEmail.text!,password: textPassword.text!) {
            (data, error) in
            
            // Nascondiamo il progress HUD di caricamento
            UIUtility.rimuoviProgressHUD()
            
            if error {
                // Si è verificato un errore
                
                // Controlliamo se il server ha risposto con una stringa di errore (dentro "data")
                if let messaggioErrore = data as? String {
                    UIUtility.mostraAlert(titolo: "OPS!", messaggio: messaggioErrore, viewController: self)
                } else {
                    UIUtility.mostraAlert(titolo: "OPS!", messaggio: "Login non riuscito!", viewController: self)
                }
            } else {
                // Il login è andato a buon fine
                
                // Controlliamo se il server ha risposto con un utente (dentro "data")
                if let utente = data as? Utente {
                    // Salviamo l'utente connesso
                    LoginUtility.utenteConnesso = utente
                    LoginUtility.salvaUtenteConnesso()
                    
                    // Andiamo alla home
                    MainController.gotoHome(animated: true)
                }
            }
        }
    }
    
    @IBAction func buttonBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
