//
//  VCCreaOpera.swift
//  Main Screen
//
//  Created by Lorenzo Antista on 14/02/2018.
//  Copyright © 2018 Lorenzo Antista. All rights reserved.
//

import UIKit
import CoreLocation


class VCCreaOpera: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    
    @IBOutlet weak var textNome: UITextField!
    
    @IBOutlet weak var textIndirizzo: UITextField!
    
    @IBOutlet weak var textDescrizione: UITextField!
    
    @IBOutlet weak var labelLatitudine: UILabel!
   
    @IBOutlet weak var labelLongitudine: UILabel!
    
    @IBOutlet weak var textPagineWeb: UITextField!
    
    @IBOutlet weak var buttonOperaNuova: UIButton!
    
    @IBOutlet weak var imageOperaNuova: UIImageView!
    
    @IBOutlet weak var buttonCreaOpera: UIButton!
    
    var coordinate: CLLocationCoordinate2D!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupGrafica()
        setupContenuti()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupGrafica() {
 
        imageOperaNuova.layer.cornerRadius = 50
        
        buttonOperaNuova.layer.shadowOffset = CGSize (width: 0, height: 5.0)
        buttonOperaNuova.layer.shadowOpacity = 0.3
        buttonOperaNuova.layer.cornerRadius = 24
        
        buttonCreaOpera.layer.shadowOffset = CGSize (width: 0, height: 0)
        
        buttonCreaOpera.layer.shadowOpacity = 0.3
        
        buttonCreaOpera.layer.cornerRadius = 20

        
        navigationItem.title = "Crea Post"
    }
    
    func setupContenuti() {
        
        if let utenteConnesso = LogInUtility.utenteConnesso() {
        
            if let urlAvatar = utenteConnesso.urlAvatar{
                NetworkUtility.scaricaImmagine(conIndirizzoWeb: urlAvatar, perImageView: imageOperaNuova)
            }
            
        }
       
    }
    
    func inviaRichiestaCreaOpera() {
    
        if imageOperaNuova.image == nil {
            UIUtility.mostraAlert(titolo: "Warning", messaggio: "You didn't load any image", viewController: self)
            return
        }
        
 
        if textNome.text == ""{
            UIUtility.mostraAlert(titolo: "Warning", messaggio: "You didn't load any image", viewController: self)
            return
        }
        
        if textDescrizione.text == "" {
            UIUtility.mostraAlert(titolo: "Warning", messaggio: "You didn't load any image", viewController: self)
            return
        }
        
        if textPagineWeb.text == "" {
            UIUtility.mostraAlert(titolo: "Warning", messaggio: "You didn't load any image", viewController: self)
            return
        }
        
        if textDescrizione.text == "" {
            UIUtility.mostraAlert(titolo: "Warning", messaggio: "You didn't load any image", viewController: self)
            return
        }
        
        let latitudine = "" // labelLatitudine.text
        let longitudine = "" // labelLatitudine.text
        
        Network.creaOpera(nome: textNome.text, descrizione: textDescrizione.text, cover_url: imageOperaNuova.image, indirizzo: textIndirizzo.text, latitudine: latitudine, longitudine: longitudine, web_url: textPagineWeb.text) { (data, error) in
            if error {
                
                UIUtility.mostraAlert(titolo: "Warning", messaggio: "An error occured", viewController: self)
            } else {
            
                self.navigationController?.popViewController(animated: true)
                
            }
        }
    
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        picker.dismiss(animated: true)
        
        if let immagine = info[UIImagePickerControllerOriginalImage] as?
            UIImage{
            //Ridimensioniamo l'immagine da inviare
            let immagineRidimensionata = UIUtility.resizeImage(image: immagine, targetSize: CGSize(width: 500.0, height: 500.0))
            
            //inviamo l'immagine al server
            
            Network.modificaAvatar(nuovoAvatar: immagineRidimensionata, completionBlock: {
                (data, error) in
                //Controllo se il server ha restituito l'utente aggiornato
                if let utenteAggiornato = data as? Utente {
                    //Aggiorno l'indirizzo web dell'avatar dell'utente connesso
                    let utenteConnesso = LogInUtility.utenteConnesso()
                    utenteConnesso?.urlAvatar = utenteAggiornato.urlAvatar
                    LogInUtility.setUtenteConnesso(utenteConnesso)
                    
                    self.setupContenuti()
                }
            })
        }
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        picker.dismiss(animated: true)
        
    }
    
    @IBAction func buttonModificaImmagine(_ sender: Any) {
          UIUtility.mostraActionSheetDiConferma(titolo: "actionSheet.modificaAvatar.titolo", messaggio: "actionSheet.modificaAvatar.messaggio", pulsanteSopra: "actionSheet.modificaAvatar.fotocamera", pulsanteSotto: "actionSheet.modificaAvatar.galleria", viewController: self) {
            (sceltaUtente) in
            
            if sceltaUtente {
                if UIImagePickerController.isSourceTypeAvailable(.camera){
                    //Fotocamera
                    let imagePickerController = UIImagePickerController()
                    imagePickerController.sourceType = .camera
                    imagePickerController.delegate = self
                    self.present(imagePickerController, animated: true)
                }
                
            } else {
                //Galleria
                let imagePickerController = UIImagePickerController()
                imagePickerController.sourceType = .photoLibrary
                imagePickerController.delegate = self
                self.present(imagePickerController, animated: true)
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func buttonCreaOpera(_ sender: Any) {
    
        inviaRichiestaCreaOpera()
    
    }

}
