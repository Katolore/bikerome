//
//  VCSplash.swift
//  Main Screen
//
//  Created by Lorenzo Antista on 11/01/2018.
//  Copyright © 2018 Lorenzo Antista. All rights reserved.
//

import UIKit

class VCSplash : UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // Si richiama la funzione della classe Utility per aggiungere dei secondi alla splash screen
        Utility.esegui(dopo: 2.0) {
            self.vaiAllaProssimaSchermata()
        }
    }
    
    private func vaiAllaProssimaSchermata() {
        
        //        // Per debug vado direttamente sulla home
        //        MainController.gotoHome(animated: false)
        //        return
        
        LoginUtility.caricaUtenteConnesso()
        
        if LoginUtility.utenteConnesso != nil {
            // Siamo loggati
            MainController.gotoHome(animated: false)
        }
        else {
            // Non siamo loggati
            MainController.gotoWelcome(animated: false)
        }
    }
    
}
