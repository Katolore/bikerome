//
//  VCMenu.swift
//  BikeRome
//
//  Created by Lorenzo Antista on 10/05/2018.
//  Copyright © 2018 IED. All rights reserved.
//

import UIKit

class VCMenu : ViewController, UITableViewDelegate, UITableViewDataSource {
    
    var listaElementiMenu: [ElementoMenu] = []
    
    
    // MARK: - Outlets
    
    @IBOutlet weak var labelNomeCognome: UILabel!
    
    @IBOutlet weak var imageAvatar: UIImageView!
    
    @IBOutlet weak var labelPunti: UILabel!
    
    @IBOutlet weak var labelDistanza: UILabel!
    
    @IBOutlet weak var labelPercorsi: UILabel!
    
    @IBOutlet weak var labelCalorie: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var buttonLogout: UIButton!
    
    @IBOutlet weak var buttonProfiloUtente: UIButton!
    
    
    // MARK: - Setup
    
    override func setupGrafica() {
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 44.0
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    override func setupContenuti() {
        
        // Metto i dati dell'utente connesso sull'intestazione del menu
        if let utenteConnesso = LoginUtility.utenteConnesso {
            
            labelNomeCognome.text = utenteConnesso.nome
            
        }
        
        
        // Popolo la lista degli elementi
        let portafoglio = ElementoMenu(icona: #imageLiteral(resourceName: "IconaPortafoglio"), titolo: "Portafoglio")
        portafoglio.isPortafoglio = true
        portafoglio.valore = "17€"
        listaElementiMenu.append(portafoglio)
        
        let mappa = ElementoMenu(icona: #imageLiteral(resourceName: "IconaMappa"), titolo: "Mappa")
        listaElementiMenu.append(mappa)
        
        let percorsi = ElementoMenu(icona:#imageLiteral(resourceName: "IconaPercorsi"), titolo: "Percorsi")
        listaElementiMenu.append(percorsi)
        
        let eventi = ElementoMenu(icona:#imageLiteral(resourceName: "IconaCalendario"), titolo: "Calendario Eventi")
        listaElementiMenu.append(eventi)
        
        let prenotazione = ElementoMenu(icona:#imageLiteral(resourceName: "IconaPrenotazione"), titolo: "Prenotazione Sale")
        listaElementiMenu.append(prenotazione)
        
        let impostazione = ElementoMenu(icona: #imageLiteral(resourceName: "IconaImpostazione"), titolo: "Impostazioni")
        listaElementiMenu.append(impostazione)
        
    }
    
    
    // MARK: - TableView delegate
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaElementiMenu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Prendo l'oggetto che devo rappresentare
        let elementoMenu = listaElementiMenu[indexPath.row]
        
        // Istanzio la cella
        var cell: CellElementoMenu
        
        if elementoMenu.isPortafoglio {
            cell = tableView.dequeueReusableCell(withIdentifier: "CellElementoPortafoglio") as! CellElementoMenu
        }
        else {
            cell = tableView.dequeueReusableCell(withIdentifier: "CellElementoMenu") as! CellElementoMenu
        }
        
        cell.setupConElementoMenu(elementoMenu)
        
        // Restitusico la cella
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Prendo l'elemento del menu selezionato
        let elementoMenuSelezionato = listaElementiMenu[indexPath.row]
        
        switch elementoMenuSelezionato.titolo {
        case "Portafoglio":
            MainController.gotoWallet(animated: false)
        case "Mappa":
            MainController.gotoHome(animated: false)
        default:
            break
        }
        
        MainController.closeMenu()
    }
    
    
    // MARK: - Actions
    
    @IBAction func buttonLogout(_ sender: Any) {
        
        UIUtility.mostraAlertDiConferma(titolo: "Logout", messaggio: "Vuoi davvero uscire dall'app?", pulsanteSi: "Si", pulsanteNo: "No", viewController: self) {
            (sceltaUtente) in
            
            if sceltaUtente {
                // Disconnetto l'utente connesso
                LoginUtility.utenteConnesso = nil
                LoginUtility.salvaUtenteConnesso()
                
                // Torno alla pagina della Welcome
                MainController.gotoWelcome(animated: true)
            }
        }
    }
    
    @IBAction func buttonProfiloUtente(_ sender: Any) {
        
    }
    
}
