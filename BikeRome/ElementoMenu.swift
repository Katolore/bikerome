//
//  ElementoMenu.swift
//  BikeRome
//
//  Created by Lorenzo Antista on 14/06/2018.
//  Copyright © 2018 IED. All rights reserved.
//

import UIKit

class ElementoMenu {
    
    var icona: UIImage?
    
    var titolo: String?
    
    
    var valore: String?
    
    var isPortafoglio = false
    
    
    
    convenience init(icona: UIImage?, titolo: String?) {
        
        self.init()
        
        self.icona = icona
        
        self.titolo = titolo
        
    }
    
}
