//
//  CellArticolo.swift
//  Main Screen
//
//  Created by Lorenzo Antista on 19/02/2018.
//  Copyright © 2018 Lorenzo Antista. All rights reserved.
//

import UIKit

class CellArticolo: UITableViewCell {

    
   
    
    @IBOutlet weak var labelCell: UILabel!
    
    @IBOutlet weak var imageCell: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupConArticolo(operaDaRappresentare: Opera) {
    
        labelCell.text = operaDaRappresentare.nome
        NetworkUtility.scaricaImmagine(conIndirizzoWeb: operaDaRappresentare.cover_url, perImageView: imageCell)
    
    }

}
