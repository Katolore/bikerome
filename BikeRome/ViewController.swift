//
//  ViewController.swift
//  BikeRome
//
//  Created by Lorenzo Antista on 12/04/2018.
//  Copyright © 2018 IED. All rights reserved.
//

import UIKit

class ViewController : UIViewController {
    
    // MARK: - Setup
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setupGrafica()
        setupContenuti()
    }
    
    /// Questa funzione imposta la grafica della schermata
    func setupGrafica() {
        // Da implementare nelle sottoclassi
    }
    
    /// Questa funzione imposta i contenuti da rappresentare sulla schermata
    func setupContenuti() {
        // Da implementare nelle sottoclassi
    }
    
}
