//
//  PinMappa.swift
//  20171130_App
//
//  Created by IED Student on 14/12/2017.
//  Copyright © 2017 IED Student. All rights reserved.
//

import UIKit
import MapKit

class PinMappa: NSObject, MKAnnotation {
    
    var title: String?
    var subtitle: String?
    var coordinate: CLLocationCoordinate2D

    
    var biciclettaAssociata: Bicicletta?
    
    
    override init() {
        // Inizializziamo il PinMappa con valori vuoti
        title = "Bici"
        subtitle = "Trova"
        coordinate = kCLLocationCoordinate2DInvalid
    }
    
    /// Costruttore semplificato per creare un PinMappa da un oggetto Evento
    init(bici: Bicicletta) {
        title = bici.nome
        subtitle = bici.indirizzo
        coordinate = bici.coordinate
        biciclettaAssociata = bici
    }
    
}

