//
//  CellElementoMenu.swift
//  BikeRome
//
//  Created by Lorenzo Antista on 14/06/2018.
//  Copyright © 2018 IED. All rights reserved.
//

import UIKit

class CellElementoMenu : UITableViewCell {

    // MARK: - Outlets
    
    @IBOutlet weak var imageIcona: UIImageView!
    
    @IBOutlet weak var labelTitolo: UILabel!
    
    @IBOutlet weak var labelValore: UILabel!
    
    
    // MARK: - Funzioni

    func setupConElementoMenu(_ elementoMenu: ElementoMenu) {
        // In questa funzione prendiamo i dati dell'oggetto e li mettiamo sulla cella
        
        labelTitolo.text = elementoMenu.titolo
        
        imageIcona.image = elementoMenu.icona
        
        
        if elementoMenu.isPortafoglio {
            labelValore.text = elementoMenu.valore
        }
    }
    
    
    //func setupConElementoMenu(elementoMenuDaRappresentare)
    
}
