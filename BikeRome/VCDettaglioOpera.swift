//
//  ViewController.swift
//  Main Screen
//
//  Created by Lorenzo Antista on 16/11/2017.
//  Copyright © 2017 Lorenzo Antista. All rights reserved.
//

import UIKit

class VCDettaglioOpera: UIViewController {
    
    // MARK: - Variabili UI
    
    
    @IBOutlet weak var ImageCopertina: UIImageView!
    
    @IBOutlet weak var LabelNome: UILabel!
    
    @IBOutlet weak var LabelIndirizzo: UILabel!
    
    @IBOutlet weak var LabelStile: UILabel!
    
    @IBOutlet weak var LabelDescrizione: UILabel!
    
    @IBOutlet weak var LabelDescrizioneBreve: UITextView!
    
    var operaDaRappresentare: Opera!
    
    let ilMioStile = Stile()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        if let opera = operaDaRappresentare {
            navigationItem.title = operaDaRappresentare.nome
        }
        
        setupGrafica()
        setupContenuti()
        
    }
    
    // MARK: - Setup
    
    ///Questa funzione imposta la grafica del ViewController
    
    func setupGrafica() {
        
        ImageCopertina.layer.masksToBounds = true
        ImageCopertina.layer.cornerRadius = 20.0
        // ImageCopertina.backgroundColor = UIColor.blue
        
    }
    
    func setupContenuti() {
        
        if let opera = operaDaRappresentare {
            LabelDescrizioneBreve.text = opera.descrizioneBreve
            
            LabelNome.text = opera.nome
            
            LabelIndirizzo.text = opera.indirizzo
            
            LabelStile.text = ilMioStile.nome
        }
        
    }
    
    // MARK: - Azione dei pulsanti
    
    func aggiornaContenuti() {
        
    
    }
    
    @IBAction func buttonInizia(_ sender: Any) {
        
        if operaDaRappresentare.aggiunto {
            print("\(operaDaRappresentare.nome) è gia stato aggiunto!")
        
        }else{
            
            operaDaRappresentare.aggiunto = true
            print("\(operaDaRappresentare.nome) aggiunta!")
            
            operaDaRappresentare.stile.numeroOpera += 1
            
        }
        
        aggiornaContenuti()
        //print("Percorso\(laMiaOpera.titolo) iniziato!")
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "SegueVCConfermaOpera"{
            if let next = segue.destination as? VCConfermaOpera {
                
                next.operaConfermata = operaDaRappresentare
                
            }
        }
    }
    
}
