//
//  Bicicletta.swift
//  Main Screen
//
//  Created by Lorenzo Antista on 14/12/2017.
//  Copyright © 2017 Lorenzo Antista. All rights reserved.
//

import UIKit
import CoreLocation

class Bicicletta {

    var nome: String?
    
    var indirizzo: String?
    
    var descrizione: String?
    
    var iconaPin: UIImage?
    
    
    // Oggetto con latitudine e longitudine
    var coordinate: CLLocationCoordinate2D!
    
}
