//
//  Utility.swift
//  Main Screen
//
//  Created by Lorenzo Antista on 11/01/2018.
//  Copyright © 2018 Lorenzo Antista. All rights reserved.
//

import UIKit
import CoreLocation
class Utility: NSObject {

    //Funzioni Generiche che utilizzeremo nell'app
    
    static func esegui(dopo secondi: TimeInterval, codice: @escaping(() -> Void)) {
        //Cosi non aspetta all'infinito, finisce il codice ".now()" + secondi specificati come parametro,
                DispatchQueue.main.asyncAfter(deadline: .now() + secondi) { 
                    //Parametro della funzione, al posto di escaping void. Premere invio e spunterà "Code"
                    codice()
        }
        
    }
    
      //Funzione che apre un indirizzo web sul browser di sistema
    static func apriIndirizzoWebSulBrowser(_ indirizzo: String) {
    
        if let url = URL(string: indirizzo) {
            let application = UIApplication.shared
            
            if application.canOpenURL(url) {
                application.open(url,options: [:], completionHandler: nil)
            
            }
        
        }
    
    }
    
    typealias CompletionCoordinate = ((CLLocationCoordinate2D) -> Void)
    
    //Geocode da indirizzo a coordinate GPS
    static func coordinateConIndirizzo (_ indirizzo: String, completion: CompletionCoordinate?) {
    
        let geocoder = CLGeocoder()
        
        geocoder.geocodeAddressString(indirizzo) {
            (placemarks, error) in
            
            if let primoPlacemark = placemarks?.first {
            
                if let location = primoPlacemark.location{
                
                    let coordinate = location.coordinate
                    completion?(coordinate)
                }
                
            }
            
        
        }
    }
}
