//
//  VCSchedaPercorso.swift
//  BikeRome
//
//  Created by Riccardo Piccialuti on 15/05/18.
//  Copyright © 2018 IED. All rights reserved.
//

import UIKit

class VCSchedaPercorso: ViewController {

    // MARK - Outlets
    
    @IBOutlet weak var buttonPreferiti: UIButton!
    
    @IBOutlet weak var buttonCondividi: UIButton!
    
    @IBOutlet weak var buttonMappa: UIButton!
    
    @IBOutlet weak var buttonMenuInfo: UIButton!
    
    @IBOutlet weak var buttonPuntiInteresse: UIButton!
    
    
    // MARK - Setup
    
    override func setupGrafica() {
        
    }
    
    override func setupContenuti() {
        
    }
    
    
    
    // MARK - Actions
    
    
    @IBAction func buttonPreferiti(_ sender: Any) {
    }
    
    @IBAction func buttonCondividi(_ sender: Any) {
    }
    
    @IBAction func buttonMappa(_ sender: Any) {
    }
    
    @IBAction func buttonMenuInfo(_ sender: Any) {
    }
    
    @IBAction func buttonPuntiInteresse(_ sender: Any) {
    }
}
