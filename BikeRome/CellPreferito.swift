//
//  CellPreferito.swift
//  Main Screen
//
//  Created by Lorenzo Antista on 07/12/2017.
//  Copyright © 2017 Lorenzo Antista. All rights reserved.
//

import UIKit

class CellPreferito: UICollectionViewCell {
 
    
    @IBOutlet weak var ImageCopertina: UIImageView!
    
    
    @IBOutlet weak var labelNome: UILabel!
    
    
    
    func setupConPreferito(preferito: Opera){
    
        labelNome.text = preferito.nome
        NetworkUtility.scaricaImmagine(conIndirizzoWeb: preferito.urlCopertina , perImageView: ImageCopertina)
    }
    
    
    
    
}
