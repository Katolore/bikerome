//
//  VCConfermaOpera.swift
//  Main Screen
//
//  Created by Lorenzo Antista on 30/11/2017.
//  Copyright © 2017 Lorenzo Antista. All rights reserved.
//

import UIKit

class VCConfermaOpera: UIViewController {

    var operaConfermata: Opera?
    
    var preferitoDaRappresentare: Opera!
    
    @IBOutlet weak var LabelConfermato: UILabel!
    
    @IBOutlet weak var ButtonConferma: UIButton!
    @IBOutlet weak var ImageViewConfermato: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupContenuti()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupContenuti() {
        LabelConfermato.text = operaConfermata?.nome
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
