//
//  MainController.swift
//  BikeRome
//
//  Created by Lorenzo Antista on 11/06/2018.
//  Copyright © 2018 IED. All rights reserved.
//

import UIKit

/**
 * Classe principale che dirige la navigazione dell'app.
 */
class MainController : DBNavigationStoryController {
    
    static var shared: MainController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // Salvo in memoria l'istanza principale dell'app
        MainController.shared = self
        
        // Mostro la splash
        let splash = StoryboardUtility.splashController()
        let splashStory = DBNavigationStory(viewController: splash, identifier: nil)
        pushNavigationStory(splashStory, with: .fade, reuseExistingStory: false, completionBlock: nil)
    }
    
    // MARK: - Public methods
    
    @objc static func openMenu() {
        DBSlideMenu.openLeftMenu(true)
    }
    
    @objc static func closeMenu() {
        DBSlideMenu.close(true)
    }
    
    // MARK: - Private methods
    
    /// Pushes the specified NavigationStory, eventually reusing an existing one
    @discardableResult private static func pushNavigationStory(_ navigationStory: DBNavigationStory, animated: Bool, completion: DBNavigationStoryControllerCompletionBlock?) -> DBNavigationStory {
        let transition = animated ? DBAnimationTransition.zoom : DBAnimationTransition.fade
        return shared.pushNavigationStory(navigationStory, with: transition, reuseExistingStory: true, completionBlock: completion)
    }
    
    /// Pushes the specified NavigationStory, replacing an eventually existing one
    private static func resetNavigationStory(_ navigationStory: DBNavigationStory, animated: Bool, completion: DBNavigationStoryControllerCompletionBlock?) {
        let transition = animated ? DBAnimationTransition.zoom : DBAnimationTransition.fade
        shared.pushNavigationStory(navigationStory, with: transition, reuseExistingStory: false, completionBlock: completion)
    }
    
    /// Clears all NavigationStories and pushes the specified one
    private static func resetStackWithNavigationStory(_ navigationStory: DBNavigationStory, animated: Bool, completion: DBNavigationStoryControllerCompletionBlock?) {
        let transition = animated ? DBAnimationTransition.zoom : DBAnimationTransition.fade
        
        shared.pushNavigationStory(navigationStory, with: transition, reuseExistingStory: false) {
            // Rimuovo le altre NavigationStory
            shared.clearNavigationStories()
            
            completion?()
        }
    }
    
    private static func setMenuVisibility(_ visible: Bool) {
        
        let menuInstance = DBSlideMenu.sharedInstance()!
        
        if visible, menuInstance.leftMenuController == nil {
            // Enabling the menu
            menuInstance.mainController = MainController.shared
            menuInstance.leftMenuController = StoryboardUtility.menuController()
            menuInstance.leftMenuOpeningMode = .aboveMainController
            menuInstance.enabled = true
        }
        else if !visible, menuInstance.leftMenuController != nil {
            // Disabling the menu
            menuInstance.enabled = false
            menuInstance.mainController = nil
            menuInstance.leftMenuController = nil
        }
        
        DBSlideMenu.close(true)
    }
    
    
    // MARK: - Fast forwards
    
    static func gotoWelcome(animated: Bool) {
        let viewController = StoryboardUtility.welcomeController()
        let story = DBNavigationStory(viewController: viewController, identifier: nil)!
        resetStackWithNavigationStory(story, animated: animated, completion: nil)
        setMenuVisibility(false)
    }
    
    static func gotoHome(animated: Bool) {
        let viewController = StoryboardUtility.mappaBiciController()
        let story = DBNavigationStory(viewController: viewController, identifier: nil)!
        pushNavigationStory(story, animated: animated, completion: nil)
        setMenuVisibility(true)
    }
    
    static func gotoWallet(animated: Bool) {
        let viewController = StoryboardUtility.portafoglioController()
        let story = DBNavigationStory(viewController: viewController, identifier: nil)!
        pushNavigationStory(story, animated: animated, completion: nil)
        setMenuVisibility(true)
    }
    
}
