//
//  VCDestinazione.swift
//  BikeRome
//
//  Created by Peppe on 15/05/18.
//  Copyright © 2018 IED. All rights reserved.
//

import UIKit

class VCDestinazione: ViewController {

    //MARK: - OUTLETS
    
    
    @IBOutlet weak var buttonIndietro: UIButton!
    
    @IBOutlet weak var buttonFreccinaTendina: UIButton!
    
    
    
    //MARK: - SETUP
    
    
    
    override func setupGrafica() {
        
        
    }
    
    override func setupContenuti() {
        
        
    }
    //MARK: - ACTION

    @IBAction func buttonIndietro(_ sender: Any) {
    }
    @IBAction func buttonFreccinaTendina(_ sender: Any) {
    }
}
