//
//  Network.swift
//  Main Screen
//
//  Created by Lorenzo Antista on 11/12/2017.
//  Copyright © 2017 Lorenzo Antista. All rights reserved.
//

import UIKit

typealias IEDDictionary = [String: Any]
typealias IEDArray = [IEDDictionary]


class Network {
    
    /// Funzione che chiede al server il meteo attuale di Roma
    static func getMeteoRoma() {
        
        let url = URL.init(string: "http://ied.apptoyou.it/app/meteo_roma.php")!
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: url){
            //Questa parte di codice viene eseguita quando l'app riceve la risposta del server
            (data,response,error) in
            
            //controllo se si sono verificati errori
            if error != nil {
                //si è verificato un errore con il server
                // La richiesta web non è andata a buon fine
            }else{
                //La richiesta web è andata bene
                
                //Controllo se la risposta ricevuta è quella che mi aspettavo
                
                if let responseData = data{
                    
                    let stringaMeteo = String(data: responseData, encoding: .utf8)
                    print(stringaMeteo ?? "Meteo non disponibile")
                }
            }
        }
        // Avvio la richiesta web
        task.resume()
    }
    
    static func getListaOpereAggiornata(completionBlock: @escaping ([Opera])-> Void) {
        
        let url = URL.init(string: "http://ied.apptoyou.it/app/articoli.php")!
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: url){
            //Questa parte di codice viene eseguita quando l'app riceve la risposta del server
            (data,response,error) in
            
            //controllo se si sono verificati errori
            if error != nil {
                //si è verificato un errore con il server
                // La richiesta web non è andata a buon fine
            }else{
                //La richiesta web è andata bene
                
                //Controllo se la risposta ricevuta è quella che mi aspettavo
                
                if let responseData = data{
                    let listaOpereAggiornata = NetworkParser.parseListaOpere(data: responseData)
                    
                    //Le operazioni di aggiornamento dell'interfaccia vanno eseguite sul Thread principale (main)
                    DispatchQueue.main.async {
                        completionBlock(listaOpereAggiornata)
                    }
                }
            }
        }
        // Avvio la richiesta web
        task.resume()
    }
    
    
    typealias NetworkCompletionBlock = ((_ data: Any?, _ error: Bool) -> Void)
    
    static func login(conEmail email: String, password: String, completion: NetworkCompletionBlock?) {
        
        // Indirizzo web del servizio di Login dell'app
        let url = "https://svoltabike.herokuapp.com/api/v1/auth"
        
        // Parametri
        var parametri = IEDDictionary()
        parametri["email"] = email
        parametri["password"] = password
        
    // registrazione(conNomeCognome nomecognome: String, conEmail email: String, password: String, completion: NetworkCompletionBlock?) {
        
        
        
        // Creo una richiesta di tipo POST
        IEDNetworking.jsonPost(url: url, authToken: nil, parameters: parametri) {
            (response) in
            
            if response.success {
                // La chiamata è andata a buon fine e sono stati ricevuti i dati dell'utente
                if let datiRicevuti = response.data as? IEDDictionary {
                    let utente = NetworkParser.parseUtente(data: datiRicevuti)
                    completion?(utente, false)
                }
            } else {
                if let datiRicevuti = response.data as? IEDDictionary {
                    let messaggioErrore = datiRicevuti["message"]
                    completion?(messaggioErrore, true)
                } else {
                    completion?(nil, true)
                }
            }
            
        }
    }
    
    
    
    static func modificaAvatar(nuovoAvatar: UIImage, completionBlock: NetworkCompletionBlock?) {
       
        //indirizzo web del servizio da richiamare
        let indirizzoWeb = "http://ied.apptoyou.it/app/modifica_avatar.php"
        
        //Utente connesso per prendere il suo auth token
        let utenteConnesso = LoginUtility.utenteConnesso
        
        let dataAvatar = UIImageJPEGRepresentation(nuovoAvatar, 1.0)
        let fileAvatar = IEDNetworkingMultipartFile(parameter: "avatar", name: "avatar.jpg", mimeType: "image/jpg", data: dataAvatar!)
        
        IEDNetworking.jsonMultipartPost(url: indirizzoWeb, authToken: utenteConnesso?.token, parameters: nil, multipartFiles: [fileAvatar]) { (response) in
            if response.success {
                if let jsonObject = response.data as? NSDictionary {
                    if let dataDictionary = jsonObject["data"] as? NSDictionary {
                        
                        //La chiamata è andata a buon fine e sono stati ricevuti i dati dell'utente
                        let utente = NetworkParser.parseUtente(data: dataDictionary as! IEDDictionary)
                        
                        DispatchQueue.main.async {
                            completionBlock?(utente, false)
                        }
                        
                    }
                }
            }
        }
    }
    
    
    static func registrazione(connomeCognome nomecognome: String, conEmail email: String, password: String, completion: NetworkCompletionBlock?) {
        
        // Indirizzo web del servizio di registrazione dell'app
        let url = "https://svoltabike.herokuapp.com/api/v1/auth"
        
        // Parametri
        var parametri = IEDDictionary()
        parametri["nomecognome"] = nomecognome
        parametri["email"] = email
        parametri["password"] = password
        
        
        // Creo una richiesta di tipo POST
        IEDNetworking.jsonPost(url: url, authToken: nil, parameters: parametri) {
            (response) in
            
            if response.success {
                // La chiamata è andata a buon fine e sono stati ricevuti i dati dell'utente
                if let datiRicevuti = response.data as? IEDDictionary {
                    let utente = NetworkParser.parseUtente(data: datiRicevuti)
                    completion?(utente, false)
                }
            } else {
                if let datiRicevuti = response.data as? IEDDictionary {
                    let messaggioErrore = datiRicevuti["message"]
                    completion?(messaggioErrore, true)
                }
            }
            
        }
    }
    
    
    
    /*static func modificaAvatar(nuovoAvatar: UIImage, completionBlock: NetworkCompletionBlock?) {
        
        //indirizzo web del servizio da richiamare
        let indirizzoWeb = "http://ied.apptoyou.it/app/modifica_avatar.php"
        
        //Utente connesso per prendere il suo auth token
        let utenteConnesso = LogInUtility.utenteConnesso()
        
        let dataAvatar = UIImageJPEGRepresentation(nuovoAvatar, 1.0)
        let fileAvatar = IEDNetworkingMultipartFile(parameter: "avatar", name: "avatar.jpg", mimeType: "image/jpg", data: dataAvatar!)
        
        IEDNetworking.jsonMultipartPost(url: indirizzoWeb, authToken: utenteConnesso?.token, parameters: nil, multipartFiles: [fileAvatar]) { (response) in
            if response.success {
                if let jsonObject = response.data as? NSDictionary {
                    if let dataDictionary = jsonObject["data"] as? NSDictionary {
                        
                        //La chiamata è andata a buon fine e sono stati ricevuti i dati dell'utente
                        let utente = NetworkParser.parseUtente(data: dataDictionary as! IEDDictionary)
                        
                        DispatchQueue.main.async {
                            completionBlock?(utente, false)
                        }
                        
                    }
                }
            }
        }
    }*/
    
    
    
    static func getListaNewsAggiornata(_ completion: NetworkCompletionBlock?){
        //Indirizzo Web del servizio da richiamare
        let indirizzoWeb = "http://ied.apptoyou.it/app/api/opere.php"
        
        IEDNetworking.jsonGet(url: indirizzoWeb, authToken: nil, parameters: nil) {
            (response) in
            
            if response.success {
            
                if let jsonObject = response.data as? NSDictionary {
                
                    if let dataDictionary = jsonObject ["data"] as? [NSDictionary] {
                        
                        let listaOpere = NetworkParser.parseListaOpere(data: dataDictionary)
                        
                        DispatchQueue.main.async {
                            completion?(listaOpere, false)
                        }
                    
                    }
                }
            }
        }
    }
    
    static func creaOpera(nome: String?, descrizione: String?, cover_url: UIImage?, indirizzo: String?,
                          latitudine: String?, longitudine: String?, web_url: String?, completion: NetworkCompletionBlock?) {
        
        // Indirizzo web del servizio da richiamare
        let indirizzoWeb = "http://ied.apptoyou.it/app/api/opere.php"
        
        // Utente connesso per prendere il suo auth token
        let utenteConnesso = LoginUtility.utenteConnesso
        let tokenUtente = utenteConnesso?.token
        
        // Il file dell'immagine cover da inviare al server
        var files: [IEDNetworkingMultipartFile]? = nil
        
        if let cover = cover_url {
            let dataCover = UIImageJPEGRepresentation(cover, 1.0)
            let fileCover = IEDNetworkingMultipartFile(parameter: "cover", name: "cover.jpg", mimeType: "image/jpg", data: dataCover!)
            files = [fileCover]
        }
        
        // Creo il dictionary dei parametri da inviare al server
        var parametri = [String: Any]()
        
        parametri["nome"] = nome ?? ""
        parametri["descrizione"] = descrizione ?? ""
       
        parametri["indirizzo"] = indirizzo ?? ""
        parametri["latitudine"] = latitudine ?? ""
        parametri["longitudine"] = longitudine ?? ""
        parametri["web_url"] = web_url ?? ""
        
        // Invio la richiesta web
        IEDNetworking.jsonMultipartPost(url: indirizzoWeb, authToken: tokenUtente, parameters: parametri, multipartFiles: files) {
            (response) in
            
            // Le operazioni di aggiornamento dell'interfaccia vanno eseguite sul Thread principale (main)
            DispatchQueue.main.async {
                // Controllo se la chiamata web è andata a buon fine
                if response.success {
                    completion?(false, false)
                } else {
                    completion?(true, true)
                }
            }
        }
        
      }
    
}
