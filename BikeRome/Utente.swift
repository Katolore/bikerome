//
//  Utente.swift
//  Main Screen
//
//  Created by Lorenzo Antista on 11/01/2018.
//  Copyright © 2018 Lorenzo Antista. All rights reserved.
//

import UIKit

class Utente: NSObject, NSCoding  {

    var email: String?
    
    var nome: String?
    
    var cognome: String?
    
    var descrizione: String?
    
    var urlAvatar: String?
    
    var citta: String?
    
    var data_nascita: String?

    var token: String?
    
    var timestamp: String?
    
    //Init per permettere il salvataggio nelle Userdefaults
    override init() {
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        token = aDecoder.decodeObject(forKey: "token") as? String
        email = aDecoder.decodeObject(forKey: "email") as? String
        nome = aDecoder.decodeObject(forKey: "nome") as? String
        cognome = aDecoder.decodeObject(forKey: "cognome") as? String
        descrizione = aDecoder.decodeObject(forKey: "descrizione") as? String
        urlAvatar = aDecoder.decodeObject(forKey: "urlAvatar") as? String
        citta = aDecoder.decodeObject(forKey: "citta") as? String
        data_nascita = aDecoder.decodeObject(forKey: "data_nascita") as? String
        timestamp = aDecoder.decodeObject(forKey: "timestamp") as? String

    }
 
    func encode(with aCoder: NSCoder) {
        aCoder.encode(token, forKey: "token")
        aCoder.encode(email, forKey: "email")
        aCoder.encode(nome, forKey: "nome")
        aCoder.encode(cognome, forKey: "cognome")
        aCoder.encode(descrizione, forKey: "descrizione")
        aCoder.encode(urlAvatar, forKey: "urlAvatar")
        aCoder.encode(citta, forKey: "citta")
        aCoder.encode(data_nascita, forKey: "data_nascita")
        aCoder.encode(timestamp, forKey: "timestamp")
        
        
    }
    
}

extension Utente {

    var nomeCompleto: String {
        get {
            if nome != nil, cognome != nil {
            return nome! + " " + cognome!
            }
            else if nome != nil{
                return nome!
            }
            else if cognome != nil{
                return cognome!
            }
            else {
                return ""
            }
        }
    }
    
    
    
}
