//
//  StoryboardUtility.swift
//  Main Screen
//
//  Created by Lorenzo Antista on 11/01/2018.
//  Copyright © 2018 Lorenzo Antista. All rights reserved.
//

import UIKit

// Questa classe conosce la posizione del view controller e semplifica la procedura per instanziarli

class StoryboardUtility: NSObject {
    
    // MARK: - Main
    
    static func splashController() -> UIViewController {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        
        //Istanzio il view controller
        let viewController = storyboard.instantiateViewController(withIdentifier: "VCSplash")
        
        // Lo restituisco
        return viewController
    }
    
    //Restituisce il view controller della schermata Welcome
    static func welcomeController() -> UIViewController {
        let storyboard = UIStoryboard.init(name: "LoginRegistrazione", bundle: nil)
        
        //Istanzio il view controller
        
        let viewController = storyboard.instantiateViewController(withIdentifier: "VCWelcome")
        
        //Lo restituisco
        return viewController
        
    }
    
    
    
    static func loginController() -> UIViewController {
        let storyboard = UIStoryboard.init(name: "LoginRegistrazione", bundle: nil)
        
        //Istanzio il view controller
        
        let viewController = storyboard.instantiateViewController(withIdentifier: "VCLogin")
        
        //Lo restituisco
        return viewController
        
    }
    
    static func registrazioneController() -> UIViewController {
        let storyboard = UIStoryboard.init(name: "LoginRegistrazione", bundle: nil)
        
        let viewController = storyboard.instantiateViewController(withIdentifier: "VCRegistrazione")
        
        return viewController
    }
    
    
    // MARK: - Menu
    
    static func menuController() -> UIViewController {
        let storyboard = UIStoryboard.init(name: "Menu", bundle: nil)
        
        //Istanzio il view controller
        let viewController = storyboard.instantiateViewController(withIdentifier: "VCMenu")
        
        // Lo restituisco
        return viewController
    }
    
    // MARK: - SbloccaBici
    
    static func cameraController() -> UIViewController {
        let storyboard = UIStoryboard.init(name: "Camera", bundle: nil)
        
        //Istanzio il view controller
        let viewController = storyboard.instantiateViewController(withIdentifier: "VCSbloccaBici")
        
        // Lo restituisco
        return viewController
    }
    
    
    
    
    static func homeController() -> UIViewController {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        
        let viewController = storyboard.instantiateViewController(withIdentifier: "MainTabBarController")
        
        return viewController
    }
    
    static func aggiungiOpera() -> UIViewController {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        
        let add = storyboard.instantiateViewController(withIdentifier: "VCCreaOpera")
        
        return add
        
    }
    
    static func schedaPercorsiController() -> UIViewController {
        let storyboard = UIStoryboard.init(name: "SchedaPercorsi", bundle: nil)
        
        //Istanzio il view controller
        
        let viewController = storyboard.instantiateViewController(withIdentifier: "VCSchedaPercorso")
        
        //Lo restituisco
        return viewController
        
    }
    
    //Restituisce il view controller della schermata MappaBici
    static func mappaBiciController() -> UIViewController {
        let storyboard = UIStoryboard.init(name: "Percorsi", bundle: nil)
        
        //Istanzio il view controller
        
        let viewController = storyboard.instantiateViewController(withIdentifier: "VCMappaBici")
        
        //Lo restituisco
        return viewController
        
    }
    
    //Restituisce il view controller della schermata MappaBici
    static func destinazioneController() -> UIViewController {
        let storyboard = UIStoryboard.init(name: "Percorsi", bundle: nil)
        
        //Istanzio il view controller
        
        let viewController = storyboard.instantiateViewController(withIdentifier: "VCDestinazione")
        
        //Lo restituisco
        return viewController
        
    }
    
    
    // MARK: - Condivisione
    
    static func ResetPasswordController() -> UIViewController {
        let storyboard = UIStoryboard.init(name: "Condivisione", bundle: nil)
        
        // Istanzio il view controller
        let viewController = storyboard.instantiateViewController(withIdentifier: "VCResetPassword")
        
        // Lo restituisco
        return viewController
    }
    
    // MARK: Mail inviata
    
    static func MailIInviataController() -> UIViewController {
        let storyboard = UIStoryboard.init(name: "Condivisione", bundle: nil)
        
        // Istanzio il view controller
        let viewController = storyboard.instantiateViewController(withIdentifier: "VCMailInviata")
        
        // Lo restituisco
        return viewController
        
    }
    
    // MARK: Mail inviata
    
    static func ConfermaPasswordRempostataController() -> UIViewController {
        let storyboard = UIStoryboard.init(name: "Condivisione", bundle: nil)
        
        // Istanzio il view controller
        let viewController = storyboard.instantiateViewController(withIdentifier: "VCConfermaPasswordRempostata")
        
        // Lo restituisco
        return viewController
    }
    
    // MARK: Mail inviata
    
    static func ConfermaPasswordReimpostataController() -> UIViewController {
        let storyboard = UIStoryboard.init(name: "Condivisione", bundle: nil)
        
        // Istanzio il view controller
        let viewController = storyboard.instantiateViewController(withIdentifier: "VCConfermaPasswordReimpostata")
        
        // Lo restituisco
        return viewController
    }
    
    // MARK: Mail inviata
    
    static func InserisciNuovaPasswordController() -> UIViewController {
        let storyboard = UIStoryboard.init(name: "Condivisione", bundle: nil)
        
        // Istanzio il view controller
        let viewController = storyboard.instantiateViewController(withIdentifier: "VCReimpostaNuovaPassword")
        
        // Lo restituisco
        return viewController
    }
    
    // MARK: Mail inviata
    
    static func CreaPercorsoController() -> UIViewController {
        let storyboard = UIStoryboard.init(name: "Condivisione", bundle: nil)
        
        // Istanzio il view controller
        let viewController = storyboard.instantiateViewController(withIdentifier: "VCCreaPercorso")
        
        // Lo restituisco
        return viewController
    }
    
    // TESI
    
    static func portafoglioController() -> UIViewController {
        let storyboard = UIStoryboard.init(name: "Menu", bundle: nil)
        
        // Istanzio il view controller
        let viewController = storyboard.instantiateViewController(withIdentifier: "VCPortafoglio")
        
        // Lo restituisco
        return viewController
    }
    
}


