//
//  GpsUtility.swift
//  BikeRome
//
//  Created by Peppe on 17/05/18.
//  Copyright © 2018 IED. All rights reserved.
//

import UIKit
import DBToolkit
import CoreLocation

class GpsUtility: NSObject {
    
    typealias GpsLocationBlock = ((CLLocationCoordinate2D) -> Void)
    
    static var gestore: GpsLocationBlock?
    
    
    static func richiediAutorizzazione() {
         DBGeolocation.requestWhenInUseAuthorization()
    }
    
    static func avviaTracciamento() {
        DBGeolocation.avviaGeolocalizzazioneConLocationBlock {
            (location) in
            
            self.gestore?(location)
        }
    }
    
    static func interrompiTracciamento() {
        DBGeolocation.interrompiGeolocalizzazione()
    }

}
